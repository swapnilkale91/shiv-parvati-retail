function oPasswordChange(r) {
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode); 
		this.vhrPassword = ko.observable(r.vhrPassword); 
		this.vhrNameOne = ko.observable(r.vhrNameOne); 
		this.vhrNameTwo = ko.observable(r.vhrNameTwo); 
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.observable(FormFuncs.loggedInRecCode()); 
		this.vhrPassword = ko.observable(""); 
		this.vhrNameOne = ko.observable(""); 
		this.vhrNameTwo = ko.observable(""); 
		this.IUD = ko.observable(1);
	}
}

$(function() {
//	ko.applyBindings(viewModel1);
});

var viewModel1 = {
	changepasswordrecord : ko.observableArray([]),
	
	// Selected
	selectedChangePassword: ko.observable(),
	
	postchangepassword: function() {
		if(viewModel.selectedChangePassword().vhrNameOne() != viewModel.selectedChangePassword().vhrNameTwo())
		{
			NotifitMessage("Password and Confirm Password doesn't match.", "error");
		}
		if(viewModel.selectedChangePassword().vhrNameOne() == viewModel.selectedChangePassword().vhrPassword())
		{
			NotifitMessage("Old Password and New Password can't be same.", "error");
		}
		else
		{
			var record = ko.toJSON(viewModel.selectedChangePassword());
			FormFuncs.postAjax(
				'../tblusers_Servlet',
				{Type: 'changePassword',
				 chrRecCode: viewModel.selectedChangePassword().chrRecCode(),
				 vhrPassword: viewModel.selectedChangePassword().vhrPassword(),
				 vhrNameOne: viewModel.selectedChangePassword().vhrNameOne()},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("Password changed successfully.", "success");
						window.location.replace("../");
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},

	//Add and Edit
	addEditChangePassword: function() {
		this.selectedChangePassword(new oPasswordChange());
	},
	
	closeAddEditChangePassword: function(revert) {
		$('#modal-changepassword').modal('hide');
	},
};
