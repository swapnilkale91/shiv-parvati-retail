<%
	Object attr = session.getAttribute("chrRecCode");
	if (attr == null) {
		response.sendRedirect("../");
		return;
	}
	response.setHeader("Cache-Control", "no-store, no-cache"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader ("Expires", -1); 
%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!-- -----------Hidden RecCode--------- -->

<input id="loggedinRecCode" type="hidden" value="<%=session.getAttribute("RecCode")%>" />
<input id="loggedinCompanyCode" type="hidden" value="<%=session.getAttribute("chrCompanyCode")%>" />
<input id="loggedinStoreCode" type="hidden" value="<%=session.getAttribute("chrStoreCode")%>" />

<header class="navbar" id="header-navbar">
	<div class="container">
		<a href="${pageContext.request.contextPath}/DashBoard" id="logo" class="navbar-brand"> <img
			src="${pageContext.request.contextPath}/img/logo.jpg" alt=""
			class="normal-logo logo-white hidden-xs hidden-sm" />
		</a>
		<div class="clearfix">
			<button class="navbar-toggle" data-target=".navbar-ex1-collapse"
				data-toggle="collapse" type="button">
				<span class="sr-only">Toggle navigation</span> <span
					class="fa fa-bars"></span>
			</button>
			<div
				class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
				<ul class="nav navbar-nav pull-left">
					<li>
						<a class="btn" id="make-small-nav"> 
							<i class="fa fa-bars"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="nav-no-collapse pull-right" id="header-nav">
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown profile-dropdown">
						<a href="#"	class="dropdown-toggle" data-toggle="dropdown"> 
							<span class="hidden-xs"><%=session.getAttribute("vhrName")%></span> 
							<b class="caret" style="top: 50%; position: absolute;"></b>
						</a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#modal-changepassword" data-toggle="modal" data-bind="click: function() { viewModel.addEditChangePassword(null); }"><i class="fa fa-key"></i>Change Password</a></li>
							<li><a href="../Logout"><i class="fa fa-power-off"></i>Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>

<!-- <!-- Add Edit Modal --> -->
<tag:addeditmodal postfn="postchangepassword" closefn="closeAddEditChangePassword" modal="modal-changepassword" title="Change Password" withbind="selectedChangePassword">
	<tag:input type="password" id="vhrPassword" label="Old Password" databind="vhrPassword" required="true" placeholder="Enter Old Password" maxlength="200"></tag:input>
	<tag:input type="password" id="vhrNameOne" label="New Password" databind="vhrNameOne" required="true" placeholder="Enter New Password" maxlength="200"></tag:input>
	<tag:input type="password" id="vhrNameTwo" label="Confirm Password" databind="vhrNameTwo" required="true" placeholder="Confirm Password" maxlength="200"></tag:input>
</tag:addeditmodal>

<script src="${pageContext.request.contextPath}/Common/Header.js" type="text/javascript"></script>
<div class="loaderoverlay"></div>