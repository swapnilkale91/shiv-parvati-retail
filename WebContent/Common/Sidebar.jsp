<div id="nav-col">
	<section id="col-left" class="col-left-nano">
		<div id="col-left-inner" class="col-left-nano-content">
			<div class="collapse navbar-collapse navbar-ex1-collapse"
				id="sidebar-nav">
				<ul class="nav nav-pills nav-stacked">
					<li class="main-menu" id="Dashboard"><a
						href="${pageContext.request.contextPath}/DashBoard"> <i
							class="fa fa-dashboard"></i> <span>Dashboard</span>
					</a></li>

					<li class="nav-header nav-header-first hidden-sm hidden-xs">
						Purchase & Store</li>
					<li class="main-menu" id="Master"><a href="#"
						class="dropdown-toggle"> <i class="fa fa-maxcdn"></i> <span>Masters</span><i
							class="fa fa-angle-right drop-icon"></i>
					</a>
						<ul class="submenu">
							<li class="sub-menu" id="Companies"><a
								href="${pageContext.request.contextPath}/companies">
									Companies </a></li>
							<li class="sub-menu" id="Stores"><a
								href="${pageContext.request.contextPath}/stores"> Stores </a></li>
							<li class="sub-menu" id="Racks"><a
								href="${pageContext.request.contextPath}/racks"> Racks </a></li>
							<li class="sub-menu" id="EmployeeCategories"><a
								href="${pageContext.request.contextPath}/employeecategories">
									Employee Categories </a></li>
							<li class="sub-menu" id="Employees"><a
								href="${pageContext.request.contextPath}/employees">
									Employees </a></li>
							<li class="sub-menu" id="Vendors"><a
								href="${pageContext.request.contextPath}/vendors"> Vendors </a></li>
							<li class="sub-menu" id="Customers"><a
								href="${pageContext.request.contextPath}/customers">
									Customers </a></li>
							
							<li class="sub-menu" id="UnitofMeasures"><a
								href="${pageContext.request.contextPath}/unitofmeasures">
									Unit of Measures</a></li>
							<li class="sub-menu" id="Items"><a
								href="${pageContext.request.contextPath}/items"> Items </a></li>
							<li class="sub-menu" id="ItemRackLocations"><a
								href="${pageContext.request.contextPath}/itemracklocations">
									Item Rack Locations </a></li>
							<li class="sub-menu" id="Terminals"><a
								href="${pageContext.request.contextPath}/terminals">
									Terminals</a></li>
							<li class="sub-menu" id="ItemCategories"><a
								href="${pageContext.request.contextPath}/itemcategories">
									Item Categories</a></li>

						</ul></li>

					<li class="main-menu" id="Transaction"><a href="#"
						class="dropdown-toggle"> <i class="fa fa-area-chart"></i> <span>Transactions</span><i
							class="fa fa-angle-right drop-icon"></i>
					</a>
						<ul class="submenu">
							
							<li class="sub-menu" id="Tax"><a
								href="${pageContext.request.contextPath}/tax"> Tax </a></li>
							<li class="sub-menu" id="Inwards"><a
								href="${pageContext.request.contextPath}/inwards"> Inwards </a></li>
							<li class="sub-menu" id="Sales"><a
								href="${pageContext.request.contextPath}/sales"> Sales </a></li>
							<li class="sub-menu" id="Sales"><a
								href="${pageContext.request.contextPath}/stockadjustments"> Stock adjustments</a></li>
							<li class="sub-menu" id="PurchaseOrders"><a
								href="${pageContext.request.contextPath}/purchaseorders"> Purchase Orders</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
	</section>
	<div id="nav-col-submenu"></div>
</div>