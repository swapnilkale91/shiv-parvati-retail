<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>Shiv Parvati Retail</title>
<%@include file="../ScriptCss.jsp"%>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">

		<%@include file="../Common/Header.jsp"%>
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>


				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-12">
									<div id="content-header" class="clearfix">
										<div class="pull-left">
											<ol class="breadcrumb">
												<li><a href="#">Home</a></li>
												<li class="active"><span>Dashboard</span></li>
											</ol>
											<h1>Dashboard</h1>
											
										</div>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 col-sm-6 col-xs-12">
									<div class="main-box infographic-box colored emerald-bg">
										<i class="fa fa-group"></i> <span class="headline">Users</span>
										<span class="value">92</span>
									</div>
								</div>
								<div class="col-lg-3 col-sm-6 col-xs-12">
									<div class="main-box infographic-box colored green-bg">
										<i class="fa fa-thumbs-o-up"></i> <span class="headline">Deals
											Completed</span> <span class="value">111</span>
									</div>
								</div>
								<div class="col-lg-3 col-sm-6 col-xs-12">
									<div class="main-box infographic-box colored red-bg">
										<i class="fa fa-thumbs-o-down"></i> <span class="headline">Pending
											Deals</span> <span class="value">22</span>
									</div>
								</div>

								<div class="col-lg-3 col-sm-6 col-xs-12">
									<div class="main-box infographic-box colored purple-bg">
										<i class="fa fa-tty"></i> <span class="headline">Enquiries</span>
										<span class="value">58</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="main-box">
										<header class="main-box-header clearfix">
										<h2 class="pull-left">Deals Completed</h2>
										</header>
										<div class="main-box-body clearfix">
											<div class="row">
												<div class="col-md-9">
													<div id="graph-bar"
														style="height: 240px; padding: 0px; position: relative;"></div>
												</div>
												<div class="col-md-3">
													<ul class="graph-stats">
														<li>
															<div class="clearfix">
																<div class="title pull-left">Earnings</div>
																<div class="value pull-right" title="10% down"
																	data-toggle="tooltip">
																	&#x20B9;94382 <i class="fa fa-level-down red"></i>
																</div>
															</div>
															<div class="progress">
																<div style="width: 69%;" aria-valuemax="100"
																	aria-valuemin="0" aria-valuenow="69" role="progressbar"
																	class="progress-bar">
																	<span class="sr-only">69% Complete</span>
																</div>
															</div>
														</li>
														<li>
															<div class="clearfix">
																<div class="title pull-left">Deals</div>
																<div class="value pull-right" title="24% up"
																	data-toggle="tooltip">
																	930 <i class="fa fa-level-up green"></i>
																</div>
															</div>
															<div class="progress">
																<div style="width: 42%;" aria-valuemax="100"
																	aria-valuemin="0" aria-valuenow="42" role="progressbar"
																	class="progress-bar progress-bar-danger">
																	<span class="sr-only">42% Complete</span>
																</div>
															</div>
														</li>
														<li>
															<div class="clearfix">
																<div class="title pull-left">New Clients</div>
																<div class="value pull-right" title="8% up"
																	data-toggle="tooltip">
																	894 <i class="fa fa-level-up green"></i>
																</div>
															</div>
															<div class="progress">
																<div style="width: 78%;" aria-valuemax="100"
																	aria-valuemin="0" aria-valuenow="78" role="progressbar"
																	class="progress-bar progress-bar-success">
																	<span class="sr-only">78% Complete</span>
																</div>
															</div>
														</li>
														<li>
															<div class="clearfix">
																<div class="title pull-left">Visitors</div>
																<div class="value pull-right" title="17% down"
																	data-toggle="tooltip">
																	824 <i class="fa fa-level-down red"></i>
																</div>
															</div>
															<div class="progress">
																<div style="width: 94%;" aria-valuemax="100"
																	aria-valuemin="0" aria-valuenow="94" role="progressbar"
																	class="progress-bar progress-bar-warning">
																	<span class="sr-only">94% Complete</span>
																</div>
															</div>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="main-box clearfix">
										<header class="main-box-header clearfix">
										<h2 class="pull-left">Last orders</h2>
										<div class="filter-block pull-right">
											<div class="form-group pull-left">
												<input type="text" class="form-control"
													placeholder="Search..."> <i
													class="fa fa-search search-icon"></i>
											</div>
											<a href="#" class="btn btn-primary pull-right"> <i
												class="fa fa-eye fa-lg"></i> View all
											</a>
										</div>
										</header>
										<div class="main-box-body clearfix">
											<div class="table-responsive clearfix">
												<table class="table table-hover">
													<thead>
														<tr>
															<th><span>Enquiry ID</span></th>
															<th><span>Date</span></th>
															<th><span>Customer</span></th>
															<th class="text-center"><span>Status</span></th>
															<th class="text-right"><span>Price</span></th>
															<th>&nbsp;</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><a href="#">#8002</a></td>
															<td>2014/08/08</td>
															<td><a href="#">Robert De Niro</a></td>
															<td class="text-center"><span
																class="label label-success">Completed</span></td>
															<td class="text-right">&#x20B9; 825.50</td>
															<td class="text-center" style="width: 15%;"><a
																href="#" class="table-link"> <span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																</span>
															</a></td>
														</tr>
														<tr>
															<td><a href="#">#5832</a></td>
															<td>2013/08/08</td>
															<td><a href="#">John Wayne</a></td>
															<td class="text-center"><span
																class="label label-warning">On hold</span></td>
															<td class="text-right">&#x20B9; 923.93</td>
															<td class="text-center" style="width: 15%;"><a
																href="#" class="table-link"> <span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																</span>
															</a></td>
														</tr>
														<tr>
															<td><a href="#">#2547</a></td>
															<td>2014/08/08</td>
															<td><a href="#">Anthony Hopkins</a></td>
															<td class="text-center"><span
																class="label label-info">Pending</span></td>
															<td class="text-right">&#x20B9; 1.625.50</td>
															<td class="text-center" style="width: 15%;"><a
																href="#" class="table-link"> <span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																</span>
															</a></td>
														</tr>
														<tr>
															<td><a href="#">#9274</a></td>
															<td>2014/08/08</td>
															<td><a href="#">Charles Chaplin</a></td>
															<td class="text-center"><span
																class="label label-danger">Cancelled</span></td>
															<td class="text-right">&#x20B9; 35.34</td>
															<td class="text-center" style="width: 15%;"><a
																href="#" class="table-link"> <span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																</span>
															</a></td>
														</tr>
														<tr>
															<td><a href="#">#8463</a></td>
															<td>2014/08/08</td>
															<td><a href="#">Gary Cooper</a></td>
															<td class="text-center"><span
																class="label label-success">Completed</span></td>
															<td class="text-right">&#x20B9; 34.199.99</td>
															<td class="text-center" style="width: 15%;"><a
																href="#" class="table-link"> <span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i> <i
																		class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																</span>
															</a></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-lg-12">
									<div class="row">
										<div class="col-lg-12">
											<ol class="breadcrumb">
												<li><a href="#">Home</a></li>
												<li class="active"><span>Form Elements</span></li>
											</ol>
											<h1>Form Elements</h1>
										</div>
									</div>



									<div class="row">
										<div class="col-lg-6">
											<div class="main-box">
												<header class="main-box-header clearfix">
												<h2>Horizontal form</h2>
												</header>
												<div class="main-box-body clearfix">
													<form class="form-horizontal" role="form">
														<div class="form-group">
															<label for="inputEmail1" class="col-lg-2 control-label">Email</label>
															<div class="col-lg-10">
																<input type="email" class="form-control"
																	id="inputEmail1" placeholder="Email">
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword1"
																class="col-lg-2 control-label">Password</label>
															<div class="col-lg-10">
																<input type="password" class="form-control"
																	id="inputPassword1" placeholder="Password">
															</div>
														</div>
														<div class="form-group">
															<div class="col-lg-offset-2 col-lg-10">
																<div class="checkbox-nice">
																	<input type="checkbox" id="remember-me2"
																		checked="checked" /> <label for="remember-me2">
																		Remember me </label>
																</div>
																<div class="radio">
																	<input type="radio" name="optionsRadios"
																		id="optionsRadios1" value="option1"> <label
																		for="optionsRadios1"> Radio </label>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="col-lg-offset-2 col-lg-10">
																<button type="submit" class="btn btn-success">Sign
																	in</button>
															</div>
														</div>
														<div class="col-lg-12">
															<div class="main-box">
																<header class="main-box-header clearfix">
																<h2>Switches</h2>
																</header>
																<div class="main-box-body clearfix">
																	<div class="row">
																		<div class="col-lg-12">
																			<div class="pull-left">
																				<div class="onoffswitch">
																					<input type="checkbox" name="onoffswitch"
																						class="onoffswitch-checkbox" id="myonoffswitch"
																						checked> <label class="onoffswitch-label"
																						for="myonoffswitch">
																						<div class="onoffswitch-inner"></div>
																						<div class="onoffswitch-switch"></div>
																					</label>
																				</div>
																			</div>
																			<div class="pull-left">
																				<div class="onoffswitch onoffswitch-danger">
																					<input type="checkbox" name="onoffswitch2"
																						class="onoffswitch-checkbox" id="myonoffswitch2"
																						checked> <label class="onoffswitch-label"
																						for="myonoffswitch2">
																						<div class="onoffswitch-inner"></div>
																						<div class="onoffswitch-switch"></div>
																					</label>
																				</div>
																			</div>
																			<div class="pull-left">
																				<div class="onoffswitch onoffswitch-warning">
																					<input type="checkbox" name="onoffswitch3"
																						class="onoffswitch-checkbox" id="myonoffswitch3"
																						checked> <label class="onoffswitch-label"
																						for="myonoffswitch3">
																						<div class="onoffswitch-inner"></div>
																						<div class="onoffswitch-switch"></div>
																					</label>
																				</div>
																			</div>
																			<div class="pull-left">
																				<div class="onoffswitch onoffswitch-success">
																					<input type="checkbox" name="onoffswitch4"
																						class="onoffswitch-checkbox" id="myonoffswitch4"
																						checked> <label class="onoffswitch-label"
																						for="myonoffswitch4">
																						<div class="onoffswitch-inner"></div>
																						<div class="onoffswitch-switch"></div>
																					</label>
																				</div>
																			</div>
																			<div class="clearfix"></div>

																		</div>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="main-box">
												<header class="main-box-header clearfix">
												<h2>Input groups</h2>
												</header>
												<div class="main-box-body clearfix">
													<form role="form">
														<div class="form-group">
															<label for="examplePrepend">Prepend</label>
															<div class="input-group">
																<span class="input-group-addon">@</span> <input
																	type="text" class="form-control" id="examplePrepend"
																	placeholder="prepend text here">
															</div>
														</div>
														<div class="form-group">
															<label for="exampleAppend">Append</label>
															<div class="input-group">
																<input type="text" class="form-control"
																	id="exampleAppend"> <span
																	class="input-group-addon">.00</span>
															</div>
														</div>
														<div class="form-group">
															<label for="exampleAppPre">Append and Prepend</label>
															<div class="input-group">
																<span class="input-group-addon">$</span> <input
																	type="text" class="form-control" id="exampleAppPre">
																<span class="input-group-addon">.00</span>
															</div>
														</div>
														<div class="form-group">
															<label for="exmaplePrependCheck">Prepend checkbox</label>
															<div class="input-group">
																<span class="input-group-addon"> <input
																	type="checkbox">
																</span> <input type="text" class="form-control"
																	id="exmaplePrependCheck">
															</div>
														</div>
														<div class="form-group">
															<label for="exampleRadio">Prepend radio</label>
															<div class="input-group">
																<span class="input-group-addon"> <input
																	type="radio">
																</span> <input type="text" class="form-control"
																	id="exampleRadio">
															</div>
														</div>
														<div class="form-group">
															<label for="exmaplePrependButton">Prepend button</label>
															<div class="input-group">
																<span class="input-group-btn">
																	<button class="btn btn-primary" type="button">Go!</button>
																</span> <input type="text" class="form-control"
																	id="exmaplePrependButton">
															</div>
														</div>
														<div class="form-group">
															<label for="examplePrependDropdown">Prepend
																button dropdown</label>
															<div class="input-group">
																<div class="input-group-btn">
																	<button type="button"
																		class="btn btn-primary dropdown-toggle"
																		data-toggle="dropdown">
																		Action <span class="caret"></span>
																	</button>
																	<ul class="dropdown-menu">
																		<li><a href="#">Action</a></li>
																		<li><a href="#">Another action</a></li>
																		<li><a href="#">Something else here</a></li>
																		<li class="divider"></li>
																		<li><a href="#">Separated link</a></li>
																	</ul>
																</div>
																<input type="text" class="form-control"
																	id="examplePrependDropdown">
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>




						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>




	<script>
		$(document)
				.ready(
						function() {

							//CHARTS
							function gd(year, day, month) {
								return new Date(year, month - 1, day).getTime();
							}

							if ($('#graph-bar').length) {
								var data1 = [ [ gd(2015, 1, 1), 838 ],
										[ gd(2015, 1, 2), 749 ],
										[ gd(2015, 1, 3), 634 ],
										[ gd(2015, 1, 4), 1080 ],
										[ gd(2015, 1, 5), 850 ],
										[ gd(2015, 1, 6), 465 ],
										[ gd(2015, 1, 7), 453 ],
										[ gd(2015, 1, 8), 646 ],
										[ gd(2015, 1, 9), 738 ],
										[ gd(2015, 1, 10), 899 ],
										[ gd(2015, 1, 11), 830 ],
										[ gd(2015, 1, 12), 789 ] ];

								var data2 = [ [ gd(2015, 1, 1), 342 ],
										[ gd(2015, 1, 2), 721 ],
										[ gd(2015, 1, 3), 493 ],
										[ gd(2015, 1, 4), 403 ],
										[ gd(2015, 1, 5), 657 ],
										[ gd(2015, 1, 6), 782 ],
										[ gd(2015, 1, 7), 609 ],
										[ gd(2015, 1, 8), 543 ],
										[ gd(2015, 1, 9), 599 ],
										[ gd(2015, 1, 10), 359 ],
										[ gd(2015, 1, 11), 783 ],
										[ gd(2015, 1, 12), 680 ] ];

								var series = new Array();

								series.push({
									data : data1,
									bars : {
										show : true,
										barWidth : 24 * 60 * 60 * 12000,
										lineWidth : 1,
										fill : 1,
										align : 'center'
									},
									label : 'Revenues'
								});
								series.push({
									data : data2,
									color : '#e84e40',
									lines : {
										show : true,
										lineWidth : 3,
									},
									points : {
										fillColor : "#e84e40",
										fillColor : '#ffffff',
										pointWidth : 1,
										show : true
									},
									label : 'Deals'
								});

								$
										.plot(
												"#graph-bar",
												series,
												{
													colors : [ '#03a9f4',
															'#f1c40f',
															'#2ecc71',
															'#3498db',
															'#9b59b6',
															'#95a5a6' ],
													grid : {
														tickColor : "#f2f2f2",
														borderWidth : 0,
														hoverable : true,
														clickable : true
													},
													legend : {
														noColumns : 1,
														labelBoxBorderColor : "#000000",
														position : "ne"
													},
													shadowSize : 0,
													xaxis : {
														mode : "time",
														tickSize : [ 1, "month" ],
														tickLength : 0,
														// axisLabel: "Date",
														axisLabelUseCanvas : true,
														axisLabelFontSizePixels : 12,
														axisLabelFontFamily : 'Open Sans, sans-serif',
														axisLabelPadding : 10
													}
												});

								var previousPoint = null;
								$("#graph-bar")
										.bind(
												"plothover",
												function(event, pos, item) {
													if (item) {
														if (previousPoint != item.dataIndex) {

															previousPoint = item.dataIndex;

															$("#flot-tooltip")
																	.remove();
															var x = item.datapoint[0], y = item.datapoint[1];

															showTooltip(
																	item.pageX,
																	item.pageY,
																	item.series.label,
																	y);
														}
													} else {
														$("#flot-tooltip")
																.remove();
														previousPoint = [ 0, 0,
																0 ];
													}
												});

								function showTooltip(x, y, label, data) {
									$(
											'<div id="flot-tooltip">' + '<b>'
													+ label + ': </b><i>'
													+ data + '</i>' + '</div>')
											.css({
												top : y + 5,
												left : x + 20
											}).appendTo("body").fadeIn(200);
								}
							}

						});
	</script>
</body>
</html>