<%
final String COMMON_SCRIPT_VERSION=new String("?v=1.1");
final String PROJECT_NAME=new String("Shiv Parvati Retail");
%>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/date.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/knockout.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.nanoscroller.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.slimscroll.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.flot.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.flot.threshold.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.flot.axislabels.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/scripts.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pace.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jstree.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/notifIt.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/site.js<%=COMMON_SCRIPT_VERSION %>"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jstree.min.css"  />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/nanoscroller.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/datepicker.css"  />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap-datetimepicker.min.css"  />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/notifIt.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/theme_styles.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap-multiselect.css"  />

<link rel="shortcut icon" href="${pageContext.request.contextPath}/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="${pageContext.request.contextPath}/img/favicon.ico" type="image/x-icon">