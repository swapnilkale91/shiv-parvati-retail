$(".main-menu").each(function() {
	if (this.id == "Master") {
		$(this).addClass("active");
	}
});
$(".sub-menu").each(function() {
	if (this.id == "Companies") {
		$(this).addClass("active");
	}
});

function  tblcompany(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.intPinCode = ko.observable(r.intPinCode); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrAddress1 = ko.observable(r.vhrAddress1); 
		this.vhrAddress2 = ko.observable(r.vhrAddress2); 
		this.vhrAddress3 = ko.observable(r.vhrAddress3); 
		this.vhrAddress4 = ko.observable(r.vhrAddress4); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrCSTNo = ko.observable(r.vhrCSTNo); 
		this.vhrName = ko.observable(r.vhrName); 
		this.vhrPANNo = ko.observable(r.vhrPANNo); 
		this.vhrServiceTaxNo = ko.observable(r.vhrServiceTaxNo); 
		this.vhrTel1 = ko.observable(r.vhrTel1); 
		this.vhrTel2 = ko.observable(r.vhrTel2); 
		this.vhrVATNo = ko.observable(r.vhrVATNo);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.intPinCode = ko.observable(""); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.vhrAddress1 = ko.observable(""); 
		this.vhrAddress2 = ko.observable(""); 
		this.vhrAddress3 = ko.observable(""); 
		this.vhrAddress4 = ko.observable(""); 
		this.vhrCode = ko.observable(""); 
		this.vhrCSTNo = ko.observable(""); 
		this.vhrName = ko.observable(""); 
		this.vhrPANNo = ko.observable(""); 
		this.vhrServiceTaxNo = ko.observable(""); 
		this.vhrTel1 = ko.observable(""); 
		this.vhrTel2 = ko.observable(""); 
		this.vhrVATNo = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
});

var viewModel = {
	// Array
	oatblcompanies: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtblcompany: ko.observable(),
	revertedtblcompany: ko.observable(),
	
	// Search
	searchTexttblcompany: ko.observable(''),
	searchtblcompanies: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	// Refresh
	refreshtblcompanies: function() {
		viewModel.searchTexttblcompany(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	// Add and Edit
	posttblcompany: function() {
		var record = ko.toJSON(this.selectedtblcompany());
		FormFuncs.postAjax(
			'../tblcompanies_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblcompany().IUD() == 1) {
						NotifitMessage("Company saved successfully.", "success");
					}
					else {
						NotifitMessage("Company updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblcompany: function(data) {
		if (data == null)
			this.selectedtblcompany(new  tblcompany());
		else
			this.selectedtblcompany(data);
		
		FormFuncs.makeCopy(this.selectedtblcompany, this.revertedtblcompany,  tblcompany);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblcompany').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblcompany, this.selectedtblcompany);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblcompanyPaging(null);
		}
	},
	
	// Delete
	deletetblcompany: function() {
		var record = ko.toJSON(this.selectedtblcompany());
		console.log("record " + record);
		FormFuncs.postAjax(
			'../tblcompanies_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Company deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	// Paging 
	counttblcompany: ko.observable(0),
	currentPagetblcompanies: ko.observable(0),
	noOfPagestblcompanies: ko.observable(0),
	pageSize: 12,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblcompanies_Servlet',
			viewModel.counttblcompany,
			viewModel.noOfPagestblcompanies,
			viewModel.pageSize,
			viewModel.searchTexttblcompany());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblcompanies_Servlet',
			viewModel.counttblcompany,
			viewModel.noOfPagestblcompanies,
			viewModel.pageSize);
	},
	pagetblcompanyPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblcompanies(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblcompanies_Servlet',
			type,
			viewModel.oatblcompanies,
			 tblcompany,
			viewModel.currentPagetblcompanies,
			viewModel.noOfPagestblcompanies,
			viewModel.pageSize,
			viewModel.searchTexttblcompany());
	},
	pagetblcompanyPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblcompanies(-1); // force refresh
		
		FormFuncs.pagingPages('../tblcompanies_Servlet',
			type, 
			viewModel.oatblcompanies,
			 tblcompany,
			viewModel.currentPagetblcompanies,
			viewModel.noOfPagestblcompanies,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblcompany() == '')
			viewModel.pagetblcompanyPaging('f');
		else
			viewModel.pagetblcompanyPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblcompany() == '')
			viewModel.pagetblcompanyPaging('p');
		else
			viewModel.pagetblcompanyPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblcompany() == '')
			viewModel.pagetblcompanyPaging('n');
		else
			viewModel.pagetblcompanyPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblcompany() == '')
			viewModel.pagetblcompanyPaging('l');
		else
			viewModel.pagetblcompanyPagingSearch('l');
	}
};