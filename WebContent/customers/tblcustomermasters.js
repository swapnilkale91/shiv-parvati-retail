$(".main-menu").each(function() {
	if (this.id == "Customers") {
		$(this).addClass("active");
	}
});

function otblcustomermaster(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intPinCode = ko.observable(r.intPinCode); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrAddress1 = ko.observable(r.vhrAddress1); 
		this.vhrAddress2 = ko.observable(r.vhrAddress2); 
		this.vhrAddress3 = ko.observable(r.vhrAddress3); 
		this.vhrAddress4 = ko.observable(r.vhrAddress4); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrName = ko.observable(r.vhrName); 
		this.vhrPANNo = ko.observable(r.vhrPANNo); 
		this.vhrTel1 = ko.observable(r.vhrTel1); 
		this.vhrTel2 = ko.observable(r.vhrTel2);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intPinCode = ko.observable(0); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrAddress1 = ko.observable(""); 
		this.vhrAddress2 = ko.observable(""); 
		this.vhrAddress3 = ko.observable(""); 
		this.vhrAddress4 = ko.observable(""); 
		this.vhrCode = ko.observable(""); 
		this.vhrName = ko.observable(""); 
		this.vhrPANNo = ko.observable(""); 
		this.vhrTel1 = ko.observable(""); 
		this.vhrTel2 = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function oStatus(r){
	this.intRecCode = r.intRecCode;
	this.vhrName = r.vhrName;
}


$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
//	viewModel.getDefaultStatuses();
});

var viewModel = {
	// Array
	oatblcustomermasters: ko.observableArray([]),
	oadefaultstatuses : ko.observableArray([]),
	// Selected and Revert 
	selectedtblcustomermaster: ko.observable(),
	revertedtblcustomermaster: ko.observable(),
	// Search
	searchTexttblcustomermaster: ko.observable(''),
	searchtblcustomermasters: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtblcustomermasters: function() {
		viewModel.searchTexttblcustomermaster(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	/*getDefaultStatuses: function() {
		FormFuncs.getAjax('../defaultlookups_Servlet', {Type : 'LookUpTypeCode',LookUpTypeCode : 1}, true, function(output) {FormFuncs.pushRs(viewModel.oadefaultstatuses, output, oStatus);});
	},*/
	// Add and Edit
	posttblcustomermaster: function() {
		var record = ko.toJSON(this.selectedtblcustomermaster());
		FormFuncs.postAjax(
			'../tblcustomermasters_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblcustomermaster().IUD() == 1) {
						NotifitMessage("Customer saved successfully.", "success");
					}
					else {
						NotifitMessage("Customer updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblcustomermaster: function(data) {
		if (data == null)
			this.selectedtblcustomermaster(new otblcustomermaster());
		else
			this.selectedtblcustomermaster(data);
		
		FormFuncs.makeCopy(this.selectedtblcustomermaster, this.revertedtblcustomermaster, otblcustomermaster);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblcustomermaster').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblcustomermaster, this.selectedtblcustomermaster);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblcustomermasterPaging(null);
		}
	},
	// Delete
	deletetblcustomermaster: function() {
		var record = ko.toJSON(this.selectedtblcustomermaster());
		FormFuncs.postAjax(
			'../tblcustomermasters_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Customer deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttblcustomermasters: ko.observable(0),
	currentPagetblcustomermasters: ko.observable(0),
	noOfPagestblcustomermasters: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblcustomermasters_Servlet',
			viewModel.counttblcustomermasters,
			viewModel.noOfPagestblcustomermasters,
			viewModel.pageSize,
			viewModel.searchTexttblcustomermaster());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblcustomermasters_Servlet',
			viewModel.counttblcustomermasters,
			viewModel.noOfPagestblcustomermasters,
			viewModel.pageSize);
	},
	pagetblcustomermasterPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblcustomermasters(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblcustomermasters_Servlet',
			type,
			viewModel.oatblcustomermasters,
			otblcustomermaster,
			viewModel.currentPagetblcustomermasters,
			viewModel.noOfPagestblcustomermasters,
			viewModel.pageSize,
			viewModel.searchTexttblcustomermaster());
	},
	pagetblcustomermasterPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblcustomermasters(-1); // force refresh
		
		FormFuncs.pagingPages('../tblcustomermasters_Servlet',
			type, 
			viewModel.oatblcustomermasters,
			otblcustomermaster,
			viewModel.currentPagetblcustomermasters,
			viewModel.noOfPagestblcustomermasters,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblcustomermaster() == '')
			viewModel.pagetblcustomermasterPaging('f');
		else
			viewModel.pagetblcustomermasterPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblcustomermaster() == '')
			viewModel.pagetblcustomermasterPaging('p');
		else
			viewModel.pagetblcustomermasterPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblcustomermaster() == '')
			viewModel.pagetblcustomermasterPaging('n');
		else
			viewModel.pagetblcustomermasterPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblcustomermaster() == '')
			viewModel.pagetblcustomermasterPaging('l');
		else
			viewModel.pagetblcustomermasterPagingSearch('l');
	}
};
