$(".main-menu").each(function() {
	if (this.id == "Master") {
		$(this).addClass("active");
	}
});
$(".sub-menu").each(function() {
	if (this.id == "EmployeeCategories") {
		$(this).addClass("active");
	}
});

function  tblemployeecategory(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {

		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.vhrCode = ko.observable(""); 
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
});

var viewModel = {
	// Array
	oatblemployeecategories: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtblemployeecategory: ko.observable(),
	revertedtblemployeecategory: ko.observable(),
	
	// Search
	searchTexttblemployeecategory: ko.observable(''),
	searchtblemployeecategories: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	// Refresh
	refreshtblemployeecategories: function() {
		viewModel.searchTexttblemployeecategory(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	// Add and Edit
	posttblemployeecategory: function() {
		var record = ko.toJSON(this.selectedtblemployeecategory());
		FormFuncs.postAjax(
			'../tblemployeecategories_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblemployeecategory().IUD() == 1) {
						NotifitMessage("Employee Category saved successfully.", "success");
					}
					else {
						NotifitMessage("Employee Category updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblemployeecategory: function(data) {
		if (data == null)
			this.selectedtblemployeecategory(new  tblemployeecategory());
		else
			this.selectedtblemployeecategory(data);
		
		FormFuncs.makeCopy(this.selectedtblemployeecategory, this.revertedtblemployeecategory,  tblemployeecategory);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblemployeecategory').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblemployeecategory, this.selectedtblemployeecategory);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblemployeecategoryPaging(null);
		}
	},
	
	// Delete
	deletetblemployeecategory: function() {
		var record = ko.toJSON(this.selectedtblemployeecategory());
		FormFuncs.postAjax(
			'../tblemployeecategories_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Employee Category deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	// Paging 
	currentPagetblemployeecategories: ko.observable(0),
	currentPagetblemployeecategory: ko.observable(0),
	noOfPagestblemployeecategories: ko.observable(0),
	pageSize: 12,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblemployeecategories_Servlet',
			viewModel.currentPagetblemployeecategories,
			viewModel.noOfPagestblemployeecategories,
			viewModel.pageSize,
			viewModel.searchTexttblemployeecategory());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblemployeecategories_Servlet',
			viewModel.currentPagetblemployeecategories,
			viewModel.noOfPagestblemployeecategories,
			viewModel.pageSize);
	},
	pagetblemployeecategoryPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblemployeecategory(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblemployeecategories_Servlet',
			type,
			viewModel.oatblemployeecategories,
			 tblemployeecategory,
			viewModel.currentPagetblemployeecategory,
			viewModel.noOfPagestblemployeecategories,
			viewModel.pageSize,
			viewModel.searchTexttblemployeecategory());
	},
	pagetblemployeecategoryPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblemployeecategory(-1); // force refresh
		
		FormFuncs.pagingPages('../tblemployeecategories_Servlet',
			type, 
			viewModel.oatblemployeecategories,
			 tblemployeecategory,
			viewModel.currentPagetblemployeecategory,
			viewModel.noOfPagestblemployeecategories,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblemployeecategory() == '')
			viewModel.pagetblemployeecategoryPaging('f');
		else
			viewModel.pagetblemployeecategoryPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblemployeecategory() == '')
			viewModel.pagetblemployeecategoryPaging('p');
		else
			viewModel.pagetblemployeecategoryPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblemployeecategory() == '')
			viewModel.pagetblemployeecategoryPaging('n');
		else
			viewModel.pagetblemployeecategoryPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblemployeecategory() == '')
			viewModel.pagetblemployeecategoryPaging('l');
		else
			viewModel.pagetblemployeecategoryPagingSearch('l');
	}
};