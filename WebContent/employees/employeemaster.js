$(".main-menu").each(function() {
	if (this.id == "Master") {
		$(this).addClass("active");
	}
});
$(".sub-menu").each(function() {
	if (this.id == "Items") {
		$(this).addClass("active");
	}
});

function  tblemployeemaster(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrCompanyName = ko.observable(r.chrCompanyName); 
		this.chrEmployeeCategoryCode = ko.observable(r.chrEmployeeCategoryCode); 
		this.chrEmployeeCategoryName = ko.observable(r.chrEmployeeCategoryName); 
		this.chrStoreCode = ko.observable(r.chrStoreCode); 
		this.chrStoreName = ko.observable(r.chrStoreName); 
		this.intPinCode = ko.observable(r.intPinCode); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.tinLockUser = ko.observable(r.tinLockUser); 
		this.vhrAddress1 = ko.observable(r.vhrAddress1); 
		this.vhrAddress2 = ko.observable(r.vhrAddress2); 
		this.vhrAddress3 = ko.observable(r.vhrAddress3); 
		this.vhrAddress4 = ko.observable(r.vhrAddress4); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrName = ko.observable(r.vhrName); 
		this.vhrPANNo = ko.observable(r.vhrPANNo); 
		this.vhrPassword = ko.observable(r.vhrPassword); 
		this.vhrTel1 = ko.observable(r.vhrTel1); 
		this.vhrTel2 = ko.observable(r.vhrTel2); 
		this.vhrUserName = ko.observable(r.vhrUserName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrCompanyCode = ko.observable(""); 
		this.chrCompanyName = ko.observable(""); 
		this.chrEmployeeCategoryCode = ko.observable(""); 
		this.chrEmployeeCategoryName = ko.observable(""); 
		this.chrStoreCode = ko.observable(""); 
		this.chrStoreName = ko.observable(""); 
		this.intPinCode = ko.observable(""); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.tinLockUser = ko.observable(1); 
		this.vhrAddress1 = ko.observable(""); 
		this.vhrAddress2 = ko.observable(""); 
		this.vhrAddress3 = ko.observable(""); 
		this.vhrAddress4 = ko.observable(""); 
		this.vhrCode = ko.observable(""); 
		this.vhrName = ko.observable(""); 
		this.vhrPANNo = ko.observable(""); 
		this.vhrPassword = ko.observable(""); 
		this.vhrTel1 = ko.observable(""); 
		this.vhrTel2 = ko.observable(""); 
		this.vhrUserName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblcompany(r){
	this.chrCompanyCode = r.chrRecCode;
	this.chrCompanyName = r.vhrName;
}

function tblemployeecategory(r){
	this.chrEmployeeCategoryCode = r.chrRecCode;
	this.chrEmployeeCategoryName = r.vhrName;
}

function tblstore(r){
	this.chrStoreCode = r.chrRecCode;
	this.chrStoreName = r.vhrName;
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.gettblemployeecategory();
	viewModel.gettblcompany();
	viewModel.gettblstore();
});

var viewModel = {
	// Array
	oatblemployeemasters: ko.observableArray([]),
	oatblcompanies: ko.observableArray([]),
	oatblstores: ko.observableArray([]),
	oatblemployeecategories: ko.observableArray([]),
	
	
	// Selected and Revert 
	selectedtblemployeemaster: ko.observable(),
	revertedtblemployeemaster: ko.observable(),
	
	// Search
	searchTexttblemployeemaster: ko.observable(''),
	searchtblemployeemasters: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	// Refresh
	refreshtblemployeemasters: function() {
		viewModel.searchTexttblemployeemaster(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	gettblemployeecategory: function() {
		FormFuncs.getAjax('../tblemployeecategories_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblemployeecategories, output, tblemployeecategory);});
	},
	
	gettblcompany: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblcompanies, output, tblcompany);});
	},
	
	gettblstore: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblstores, output, tblstore);});
	},
	
	// Add and Edit
	posttblemployeemaster: function() {
		var record = ko.toJSON(this.selectedtblemployeemaster());
		FormFuncs.postAjax(
			'../tblemployeemasters_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblemployeemaster().IUD() == 1) {
						NotifitMessage("Employee Details saved successfully.", "success");
					}
					else {
						NotifitMessage("Employee Details updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblemployeemaster: function(data) {
		if (data == null)
			this.selectedtblemployeemaster(new tblemployeemaster());
		else
			this.selectedtblemployeemaster(data);
		
		FormFuncs.makeCopy(this.selectedtblemployeemaster, this.revertedtblemployeemaster,  tblemployeemaster);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblemployeemaster').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblemployeemaster, this.selectedtblemployeemaster);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblemployeemasterPaging(null);
		}
	},
	
	// Delete
	deletetblemployeemaster: function() {
		var record = ko.toJSON(this.selectedtblemployeemaster());
		FormFuncs.postAjax(
			'../tblemployeemasters_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Employee Details deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	// Paging 
	counttblemployeemaster: ko.observable(0),
	currentPagetblemployeemasters: ko.observable(0),
	noOfPagestblemployeemasters: ko.observable(0),
	pageSize: 12,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblemployeemasters_Servlet',
			viewModel.counttblemployeemaster,
			viewModel.noOfPagestblemployeemasters,
			viewModel.pageSize,
			viewModel.searchTexttblemployeemaster());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblemployeemasters_Servlet',
			viewModel.counttblemployeemaster,
			viewModel.noOfPagestblemployeemasters,
			viewModel.pageSize);
	},
	pagetblemployeemasterPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblemployeemasters(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblemployeemasters_Servlet',
			type,
			viewModel.oatblemployeemasters,
			 tblemployeemaster,
			viewModel.currentPagetblemployeemasters,
			viewModel.noOfPagestblemployeemasters,
			viewModel.pageSize,
			viewModel.searchTexttblemployeemaster());
	},
	pagetblemployeemasterPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblemployeemasters(-1); // force refresh
		
		FormFuncs.pagingPages('../tblemployeemasters_Servlet',
			type, 
			viewModel.oatblemployeemasters,
			 tblemployeemaster,
			viewModel.currentPagetblemployeemasters,
			viewModel.noOfPagestblemployeemasters,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblemployeemaster() == '')
			viewModel.pagetblemployeemasterPaging('f');
		else
			viewModel.pagetblemployeemasterPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblemployeemaster() == '')
			viewModel.pagetblemployeemasterPaging('p');
		else
			viewModel.pagetblemployeemasterPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblemployeemaster() == '')
			viewModel.pagetblemployeemasterPaging('n');
		else
			viewModel.pagetblemployeemasterPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblemployeemaster() == '')
			viewModel.pagetblemployeemasterPaging('l');
		else
			viewModel.pagetblemployeemasterPagingSearch('l');
	}
};