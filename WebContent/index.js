 function oValidateEP(r) {
	if (r) {
		this.vhrUserName = ko.observable(r.vhrUserName);
		this.vhrPassword = ko.observable(r.vhrPassword);
	}
	else {
		this.vhrUserName = ko.observable("");
		this.vhrPassword = ko.observable("");
	}
}

$(function () {
    ko.applyBindings (viewModel);
    viewModel.start();
});

var viewModel = {
	selectedValidateEP: ko.observable(),

	start: function() {
		this.selectedValidateEP(new oValidateEP());
	},
	doLogin: function (form) {
		form = $(form);
		var json = JSON.stringify (FormFuncs.getValuesFromForm(form));
		json = JSON.parse (json);
		FormFuncs.getAjax (
			'tblemployeemasters_Servlet',
			{Type: 'login', 
			vhrUserName: viewModel.selectedValidateEP().vhrUserName(),
			vhrPassword: viewModel.selectedValidateEP().vhrPassword()},
			true,
			function(output) {
				if (output.length == 1) {
					window.location.replace("DashBoard");
				}
				else {
					NotifitMessage("Invalid Email Id or Password.", "error");
					
				}
			}
		);
	}
};