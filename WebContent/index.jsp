<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="ScriptCss.jsp"%>
<title><%=PROJECT_NAME %>: Login</title>
</head>
<body id="login-page-full">
	<div id="login-full-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="login-box">
						<div id="login-box-holder">
							<div class="row">
								<div class="col-xs-12">
									<header id="login-header">
									<div id="login-logo">
										<img src="img/logo.jpg" alt="" />
									</div>
									</header>
									<div id="login-box-inner">
										<div data-bind="with: selectedValidateEP,enterkey: function(){doLogin()}">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input class="form-control" id="vhrUserName" data-bind="value: vhrUserName"  placeholder="Enter User ID" maxlength="200">
											</div>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-key"></i></span>
											  	<input type="password" class="form-control" id="vhrPassword" data-bind="value: vhrPassword"  placeholder="Enter Password" maxlength="200">
											</div>
											<div class="row">
												<div class="col-xs-12">
													<button class="btn btn-success col-xs-12" data-bind="click: viewModel.doLogin">Sign In</button>
												</div>
											</div>
											<div id="remember-me-wrapper">
												<div class="row">
													<a href="ForgotPasswords/index.jsp" id="login-forget-link"
														class="col-xs-6"> Forgot password? </a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="loaderoverlay"></div>
	<script src="index.js<%=COMMON_SCRIPT_VERSION %>" type="text/javascript"></script>
</body>
</html>









<%-- 
<body>
	<div id="div-background">
		<div class="container">
			<div class="form-signin" data-bind="with: selectedValidateEP">
				<h3 class="form-signin-heading">Login</h3>
				<tag:input id="vhrEmail" databind="EmailId" required="true"
					placeholder="Enter Email ID" maxlength="200"></tag:input>
				<tag:input type="password" id="vhrPassword" databind="Password"
					required="true" placeholder="Enter Password" maxlength="200"></tag:input>
				<button class="btn btn-lg btn-primary btn-block"
					data-bind="click: viewModel.doLogin">Sign In</button>
				<div class="form-signin-footer">
					<a href="crmForgotPasswords" class="pull-right">Forgot
						Password?</a>
				</div>
			</div>
		</div>

	</div>
	<script src="index.js" type="text/javascript"></script>
</body>
</html> --%>