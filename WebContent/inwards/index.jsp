<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="../ScriptCss.jsp"%>
<title><%=PROJECT_NAME%>: Inwards</title>
</head>
<body class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		 <%@include file="../Common/Header.jsp"%>
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div id="main-container">
							
								<div class="main-box clearfix">
									<header class="main-box-header clearfix">
										<h2 class="pull-left"><b>Inward</b></h2>
										<div class="filter-block pull-right">
											<div class="col-md-10 pull-left">
												<tag:search databindresetfn="resettblinward" databindfn="searchtblinwards" databindvalue="searchTexttblinward"></tag:search>
											</div>
											<div class="col-md-2 pull-left">
												<tag:addbutton databind="addEdittblinward" title="Add Inward"></tag:addbutton>
											</div>
										</div>
									</header>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<table class="table table-hover table-bordered">
												<thead>
													<tr>
													
														<th><b>Inward No</b></th>
														<th><b>Credit Period</b></th>
														<th><b>Inward Date</b></th>
														<th><b>Lorry No</b></th>
														<th><b>Challan No</b></th>
														<th class="actioncolumn">Action</th>
													</tr>
												</thead>
												<tbody data-bind="foreach: viewModel.oatblinwards">
													<tr>
														<td><span data-bind="text: vhrInwardNo"></span></td>
														<td><span data-bind="text: intCreditPeriod"></span></td>
														<td><span data-bind="text: dtmInwarddate"></span></td>
														<td><span data-bind="text: vhrLorryNo"></span></td>
														<td><span data-bind="text: vhrChallanNo"></span></td>
														<td>
															<tag:editbutton modalid="modal-tblinward" databind="addEdittblinward" title="Edit Inward"></tag:editbutton>
															<tag:deletebutton modalid="modal-delete" databind="addEdittblinward" title="Delete Inward"></tag:deletebutton>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<tag:pagination currentpage="currentPagetblinwards" noofpages="noOfPagestblinwards"></tag:pagination>
									</div>
								</div>
							</div>
							<div id="detail-container" style="display: none">
							
								<form role="form" data-bind="submit: function() { viewModel.posttblinward() }" >
									<div class="main-box clearfix">
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Inward</b></h2>
										</header>
										<div class="main-box-body clearfix" data-bind="with: selectedtblinward">
											<div class="row">
											
												<div class="col-md-6">
													<tag:select label="Company" observable="oatblcompanies"
														bindvalue="chrCompanyCode" value="chrCompanyCode" text="chrCompanyName"
														caption="Select Company" required="true">
													</tag:select>
												</div>
												
												<div class="col-md-6">
													<tag:select label="Store" observable="oatblstores"
														bindvalue="chrStoreCode" value="chrStoreCode" text="chrStoreName"
														caption="Select Store" required="true">
													</tag:select>
												</div>
												
												<div class="col-md-6">
													<tag:datepicker label="Inward Date" id="dtmInwarddate"
														databind="dtmInwarddate" placeholder="Select Inward Date" 
														required="true">
													</tag:datepicker>
												</div>
												
												<div class="col-md-6">
													<tag:input label="Pan No" id="vhrInwardNo" databind="vhrInwardNo"
														required="true" placeholder="Enter Inward No" maxlength="20"></tag:input>
												</div>
												
												<div class="col-md-6">
													<tag:select label="PO" observable="oatblpos"
														bindvalue="chrPOCode" value="chrRecCode" text="chrPONo"
														caption="Select PO" required="true">
													</tag:select>
												</div>
												
												<div class="col-md-6">
													<tag:select label="Vendor" observable="oavendors"
														bindvalue="chrVendorCode" value="chrRecCode" text="vhrName"
														caption="Select Vendor" required="true">
													</tag:select>
												</div>
												
												<div class="col-md-6">
													<tag:input label="Enter Credit Period" id="intCreditPeriod" databind="intCreditPeriod" onkeypress="return isNumber(event)"
														required="true" placeholder="Enter ReOrder Quantity" maxlength="20"></tag:input>	
												</div>
												
												
												
												<div class="col-md-6">
													<tag:input label="Lorry No" id="chrlorryno" databind="vhrLorryNo"
														 placeholder="Enter Lorry No" maxlength="20"></tag:input>
												</div>
												
												<div class="col-md-6">
													<tag:input label="Challan No" id="chrchallanno" databind="vhrChallanNo"
														 placeholder="Enter Challan No" maxlength="20"></tag:input>
												</div>	
												
												<div class="col-md-6">
													<tag:input label="item total" id="intitemtotal" databind="decItemTotal" onkeypress="return isNumber(event)"
														required="true" placeholder="Enter Item Total" maxlength="20"></tag:input>	
													
												</div>
												
												<div class="col-md-6">
													<tag:input label="items scheme total" id="decItemSchemeTotal" databind="decItemSchemeTotal" onkeypress="return isNumber(event)"
														required="true" placeholder="Enter Items Scheme Total" maxlength="20"></tag:input>	
													
												</div>
												
												<div class="col-md-6">
													<tag:input label="Vat Tax Total" id="decvattaxtotal" databind="decVATTaxTotal" onkeypress="return isNumber(event)"
														required="true" placeholder="Enter Vat Tax Total" maxlength="20"></tag:input>	
													
												</div>
												
												<div class="col-md-6">
													<tag:input label="Vendor Payable Amount" id="decvendorpayableamount" databind="decVendorPayableAmount" onkeypress="return isNumber(event)"
														required="true" placeholder="Enter Vendor Payable Amount" maxlength="20"></tag:input>	
													
												</div>
												
												
												
												
										</div>
											
										</div>
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Inward Details</b></h2>
											<div class="filter-block pull-right">
												<div class="col-md-2 pull-left">
													<tag:addbutton modalid="modal-tblinwarddetails" databind="addEdittblinwarddetail" title="Add Inward Details"></tag:addbutton>
												</div>
											</div>
										</header>
										<div class="main-box-body clearfix">
											<div class="table-responsive clearfix">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th><b>Batch No</b></th>
															<th><b>MRP</b></th>
															<th><b>Item Amount</b></th>
															<th><b>Vendor Discount Amount</b></th>
															<th class="actioncolumn">Action</th>
														</tr>
													</thead>
													<tbody data-bind="foreach: tblinwarddetails">
														<tr>
															<td><span data-bind="text: vhrBatchNo"></span></td>
															<td><span data-bind="text: decMRP"></span></td>
															<td><span data-bind="text: decItemAmount"></span></td>
															<td><span data-bind="text: decVendorDiscountAmount1"></span></td>
															<td>
																<tag:editbutton modalid="modal-tblinwarddetails" databind="addEdittblinwarddetail" title="Edit Inward Details"></tag:editbutton>
																<tag:deletebutton id="Inwarddetails-a" modalid="modal-delete" databind="addEdittblinwarddetail" title="Delete Inward Details"></tag:deletebutton>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<div class="pull-right">
												<button type="cancel" class="btn btn-default" data-bind="click: function() { viewModel.closeAddEdittblinward(true); }">Cancel</button>
												<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="submittblinwarddetail" closefn="closeAddEdittblinwarddetail" modal="modal-tblinwarddetails" title="Inward Details" withbind="selectedtblinwarddetail">
		<div class="row">
		
			<div id="col1" class="col-md-6">
			
				<tag:select label="Item" id="selected_item" observable="oaitemmasters" bindvalue="chrItemCode" value="chrItemCode" text="vhrName" caption="Choose Item"  required="true" ></tag:select>
				<tag:select id="StoreRacks" label="Store Racks"  observable="oastoreracks" bindvalue="chrStoreRackCode" value="chrRecCode" text="vhrName" caption="Choose Stock Rack"  required="true" ></tag:select>
				<tag:input label="Packing" id="chrpacking" databind="vhrPacking" required="true" placeholder="Enter Packing" maxlength="20"></tag:input>
				<%-- <tag:input label="Quantity" id="decQuantity" databind="decQuantity" onkeypress="return isNumber(event)"
				required="true" placeholder="Enter Quantity" maxlength="20"></tag:input>	 --%>
				
				<tag:datepicker label="Manufacture Date" id="dtmManufactureDate"
					databind="dtmManufactureDate" placeholder="Select Manufacture Date">
				</tag:datepicker>
				
				<tag:input label="Batch No" id="vhrBatchNo" databind="vhrBatchNo"
					required="true" placeholder="Enter Batch No" maxlength="20"></tag:input>	
					
				<tag:input label="MRP" id="decMRP" databind="decMRP" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter MRP" maxlength="20"></tag:input>
					
				<tag:input label="Item Amount" id="decItemAmount" databind="decItemAmount" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Item Amount" maxlength="20"></tag:input>
					
				<tag:input label="Margin Percentage" id="decMarginPercentage" databind="decMarginPercentage" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Margin Percentage" maxlength="20"></tag:input>
					
				<tag:input label="Scheme Discount" id="decSchemeDiscount" databind="decSchemeDiscount" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Scheme Discount" maxlength="20"></tag:input>
					
				<tag:input label="Vendor Discount 1" id="decVendorDiscount1" databind="decVendorDiscount1" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Margin Percentage" maxlength="20"></tag:input>
					
				<tag:input label="Vendor Discount Amount1" id="decVendorDiscountAmount1" databind="decVendorDiscountAmount1" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Scheme Discount" maxlength="20"></tag:input>
					
				<tag:input label="Item Amount after tax" id="decItemAmountAfterTax" databind="decItemAmountAfterTax" onkeypress="return isNumber(event)"
					required="true" placeholder="Item Amount after tax" maxlength="20"></tag:input>
					
				<tag:input label="VAT Tax Amount" id="decVATTaxAmount" databind="decVATTaxAmount" onkeypress="return isNumber(event)"
					required="true" placeholder="Item Amount after tax" maxlength="20"></tag:input>
					
				<tag:input label="Customer Discount Percentage" id="decCustomerDiscountPercentage" databind="decCustomerDiscountPercentage" onkeypress="return isNumber(event)"
					required="true" placeholder="Customer Discount Percentage" maxlength="20"></tag:input>	
												
			</div>
			
			<div id="col1" class="col-md-6">
				<tag:input label="Item Quantity" id="decQuantity" databind="decQuantity" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Number of items" maxlength="20"></tag:input>	
					
				<tag:input label="Barcode" id="vhrBarCode" databind="vhrBarCode"
					required="true" placeholder="Enter Barcode" maxlength="20"></tag:input>
					
				<tag:input label="Free Quantity" id="decfreequantity" databind="decFreeQuantity" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Free Quantity" maxlength="20"></tag:input>	
					
				<tag:datepicker label="Expiry Date" id="dtmExpiryDate"
					databind="dtmExpiryDate" placeholder="Select Expiry Date">
				</tag:datepicker>
				
				<tag:input label="Generated Barcode" id="chrbarcode" databind="vhrGeneratedBarcode"
					required="true" placeholder="Enter Generated Barcode" maxlength="20"></tag:input>
					
				<tag:select label="Print Label" id="selected_printlabel" observable="oatbldefaultlookups" bindvalue="tinPrintLabel" value="intRecCode" text="vhrName" caption="Choose Print Label"  required="true" ></tag:select>
				
				<tag:input label="Margin Amount" id="decMarginAmount" databind="decMarginAmount" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Margin Amount" maxlength="20"></tag:input>
				
				<tag:input label="Scheme Discount Amount" id="decSchemeDiscountAmount" databind="decSchemeDiscountAmount" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Scheme Discount amount" maxlength="20"></tag:input>
					
				<tag:input label="Vendor Discount 2"  id="decVendorDiscount2" databind="decVendorDiscount2" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Margin Percentage" maxlength="20"></tag:input>
					
				<tag:input label="Vendor Discount Amount 2" id="decVendorDiscountAmount2" databind="decVendorDiscountAmount2" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Scheme Discount" maxlength="20"></tag:input>
					
				<tag:input label="Item Discount Amount" id="decItemDiscountAmount" databind="decItemDiscountAmount" onkeypress="return isNumber(event)"
					required="true" placeholder="Enter Item Discount Amount" maxlength="20"></tag:input>
				
				<tag:select id="VatTax" label="Vat Tax"  observable="oataxdetails" bindvalue="chrVATTaxRecCode" value="chrRecCode" text="vhrName" caption="Choose Vat Tax"  required="true" ></tag:select>
					
				<tag:input label="VAT Tax Percentage" id="decVATTaxPercentage" databind="decVATTaxPercentage" onkeypress="return isNumber(event)"
					required="true" placeholder="Vat Tax Percentage" maxlength="20"></tag:input>
					
				<tag:input label="Customer Discount Amount" id="decCustomerDiscountAmount" databind="decCustomerDiscountAmount" onkeypress="return isNumber(event)"
					required="true" placeholder="Vat Tax Percentage" maxlength="20"></tag:input>
					
				<tag:input label="Customer Item Rate" id="decCustomerItemRate" databind="decCustomerItemRate" onkeypress="return isNumber(event)"
					required="true" placeholder="Customer Item Rate" maxlength="20"></tag:input>
			
					
			</div>
		</div>
	</tag:addeditmodal>	
	<tag:deletemodal databindfn="deletetblinward"></tag:deletemodal>
	
	<script src="tblinwards.js" type="text/javascript"></script>
</body>
</html>