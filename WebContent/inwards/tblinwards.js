function tblinward(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrAuthorizationUserOne = ko.observable(r.chrAuthorizationUserOne); 
		this.chrAuthorizationUserThree = ko.observable(r.chrAuthorizationUserThree); 
		this.chrAuthorizationUserTwo = ko.observable(r.chrAuthorizationUserTwo); 
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIPAdd = ko.observable(r.chrIPAdd); 
		this.chrPOCode = ko.observable(r.chrPOCode); 
		this.chrStoreCode = ko.observable(r.chrStoreCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.chrVendorCode = ko.observable(r.chrVendorCode); 
		this.decItemDiscountTotal = ko.observable(r.decItemDiscountTotal); 
		this.decItemSchemeTotal = ko.observable(r.decItemSchemeTotal); 
		this.decItemTotal = ko.observable(r.decItemTotal); 
		this.decVATTaxTotal = ko.observable(r.decVATTaxTotal); 
		this.decVendorPayableAmount = ko.observable(r.decVendorPayableAmount); 
		this.dtmAuthorizationDateOne = ko.observable(r.dtmAuthorizationDateOne); 
		this.dtmAuthorizationDateThree = ko.observable(r.dtmAuthorizationDateThree); 
		this.dtmAuthorizationDateTwo = ko.observable(r.dtmAuthorizationDateTwo); 
		this.dtmDueDate = ko.observable(r.dtmDueDate); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmInwarddate = ko.observable(r.dtmInwarddate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intCreditPeriod = ko.observable(r.intCreditPeriod); 
		this.tinAuthorizationLevel = ko.observable(r.tinAuthorizationLevel); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.tinStatus = ko.observable(r.tinStatus); 
		this.vhrChallanNo = ko.observable(r.vhrChallanNo); 
		this.vhrInwardNo = ko.observable(r.vhrInwardNo); 
		this.vhrLorryNo = ko.observable(r.vhrLorryNo);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrAuthorizationUserOne = ko.observable(""); 
		this.chrAuthorizationUserThree = ko.observable(""); 
		this.chrAuthorizationUserTwo = ko.observable(""); 
		this.chrCompanyCode = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrIPAdd = ko.observable(""); 
		this.chrPOCode = ko.observable(""); 
		this.chrStoreCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.chrVendorCode = ko.observable(""); 
		this.decItemDiscountTotal = ko.observable(0); 
		this.decItemSchemeTotal = ko.observable(0); 
		this.decItemTotal = ko.observable(0); 
		this.decVATTaxTotal = ko.observable(0); 
		this.decVendorPayableAmount = ko.observable(0); 
		this.dtmAuthorizationDateOne = ko.observable(""); 
		this.dtmAuthorizationDateThree = ko.observable(""); 
		this.dtmAuthorizationDateTwo = ko.observable(""); 
		this.dtmDueDate = ko.observable(new Date()); 
		this.dtmInsertDate = ko.observable(new Date()); 
		this.dtmInwarddate = ko.observable(new Date()); 
		this.dtmUpdateDate = ko.observable(new Date()); 
		this.intCreditPeriod = ko.observable(0); 
		this.tinAuthorizationLevel = ko.observable(0); 
		this.tinIsDirty = ko.observable(0); 
		this.tinStatus = ko.observable(0); 
		this.vhrChallanNo = ko.observable(""); 
		this.vhrInwardNo = ko.observable(""); 
		this.vhrLorryNo = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblinwarditemdetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrItemCode = ko.observable(r.chrItemCode); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrStoreRackCode = ko.observable(r.chrStoreRackCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.chrVATTaxRecCode = ko.observable(r.chrVATTaxRecCode); 
		this.decCustomerDiscountAmount = ko.observable(r.decCustomerDiscountAmount); 
		this.decCustomerDiscountPercentage = ko.observable(r.decCustomerDiscountPercentage); 
		this.decCustomerItemRate = ko.observable(r.decCustomerItemRate); 
		this.decFreeQuantity = ko.observable(r.decFreeQuantity); 
		this.decItemAmount = ko.observable(r.decItemAmount); 
		this.decItemAmountAfterTax = ko.observable(r.decItemAmountAfterTax); 
		this.decItemDiscountAmount = ko.observable(r.decItemDiscountAmount); 
		this.decMarginAmount = ko.observable(r.decMarginAmount); 
		this.decMarginPercentage = ko.observable(r.decMarginPercentage); 
		this.decMRP = ko.observable(r.decMRP); 
		this.decQuantity = ko.observable(r.decQuantity); 
		this.decSchemeDiscount = ko.observable(r.decSchemeDiscount); 
		this.decSchemeDiscountAmount = ko.observable(r.decSchemeDiscountAmount); 
		this.decVATTaxAmount = ko.observable(r.decVATTaxAmount); 
		this.decVATTaxPercentage = ko.observable(r.decVATTaxPercentage); 
		this.decVendorDiscount1 = ko.observable(r.decVendorDiscount1); 
		this.decVendorDiscount2 = ko.observable(r.decVendorDiscount2); 
		this.decVendorDiscountAmount1 = ko.observable(r.decVendorDiscountAmount1); 
		this.decVendorDiscountAmount2 = ko.observable(r.decVendorDiscountAmount2); 
		this.dtmExpiryDate = ko.observable(r.dtmExpiryDate); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmManufactureDate = ko.observable(r.dtmManufactureDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.tinPrintLabel = ko.observable(r.tinPrintLabel); 
		this.vhrBarCode = ko.observable(r.vhrBarCode); 
		this.vhrBatchNo = ko.observable(r.vhrBatchNo); 
		this.vhrDescription = ko.observable(r.vhrDescription); 
		this.vhrGeneratedBarcode = ko.observable(r.vhrGeneratedBarcode); 
		this.vhrIpAddress = ko.observable(r.vhrIpAddress); 
		this.vhrPacking = ko.observable(r.vhrPacking);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrItemCode = ko.observable(""); 
		this.chrParentCode = ko.observable(viewModel.selectedtblinward().chrRecCode()); 
		this.chrStoreRackCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.chrVATTaxRecCode = ko.observable(""); 
		this.decCustomerDiscountAmount = ko.observable(0); 
		this.decCustomerDiscountPercentage = ko.observable(0); 
		this.decCustomerItemRate = ko.observable(0); 
		this.decFreeQuantity = ko.observable(0); 
		this.decItemAmount = ko.observable(0); 
		this.decItemAmountAfterTax = ko.observable(0); 
		this.decItemDiscountAmount = ko.observable(0); 
		this.decMarginAmount = ko.observable(0); 
		this.decMarginPercentage = ko.observable(0); 
		this.decMRP = ko.observable(0); 
		this.decQuantity = ko.observable(0); 
		this.decSchemeDiscount = ko.observable(0); 
		this.decSchemeDiscountAmount = ko.observable(0); 
		this.decVATTaxAmount = ko.observable(0); 
		this.decVATTaxPercentage = ko.observable(0); 
		this.decVendorDiscount1 = ko.observable(0); 
		this.decVendorDiscount2 = ko.observable(0); 
		this.decVendorDiscountAmount1 = ko.observable(0); 
		this.decVendorDiscountAmount2 = ko.observable(0); 
		this.dtmExpiryDate = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmManufactureDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinIsDirty = ko.observable(0); 
		this.tinPrintLabel = ko.observable(0); 
		this.vhrBarCode = ko.observable(""); 
		this.vhrBatchNo = ko.observable(""); 
		this.vhrDescription = ko.observable(""); 
		this.vhrGeneratedBarcode = ko.observable(""); 
		this.vhrIpAddress = ko.observable(""); 
		this.vhrPacking = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblstock(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrItemCode = ko.observable(r.chrItemCode); 
		this.chrStoreCode = ko.observable(r.chrStoreCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.decQuantity = ko.observable(r.decQuantity); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intStockType = ko.observable(r.intStockType); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrIPAdd = ko.observable(r.vhrIPAdd);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrCompanyCode = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrItemCode = ko.observable(""); 
		this.chrStoreCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.decQuantity = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intStockType = ko.observable(""); 
		this.tinIsDirty = ko.observable(""); 
		this.vhrIPAdd = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblcompany(r) {
	this.chrCompanyCode = r.chrRecCode;
	this.chrCompanyName = r.vhrName;
}

function tblstore(r){
	this.chrStoreCode = r.chrRecCode;
	this.chrStoreName = r.vhrName;
}

function tblpos(r){
	this.chrRecCode = r.chrRecCode;
	this.chrPONo = r.chrPONo;
}

function oItemMaster(r){
	this.chrItemCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStoreRacks(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStatus(r){
	this.intRecCode = r.intRecCode;
	this.vhrName = r.vhrName;
}

function oTaxDetail(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oVendor(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pagecounttblinwards();
	viewModel.pagingPages('f');
	viewModel.gettblcompany();
	viewModel.gettblstore();
	viewModel.gettblpos();
	viewModel.getItems();
	viewModel.getStoreRacks();
	viewModel.getDefaultStatuses();
	viewModel.getVATTax();
	viewModel.getVendors();
//	viewModel.getUOMs();
	
});

var viewModel = {
	pagingCurrentPage: -1,
	pagingNoOfPages: 0,
	pagingPageSize: 20,
	pagingCount: 0,
	
	oatblinwards: ko.observableArray([]),
	tblinwarddetails: ko.observableArray([]),
	oatblcompanies: ko.observableArray([]),
	oatblstores: ko.observableArray([]),
	oatblpos: ko.observableArray([]),
	oatbldefaultlookups: ko.observableArray([]),
	oaitemmasters: ko.observableArray([]),
	oastoreracks: ko.observableArray([]),
	oataxdetails: ko.observableArray([]),
	oavendors: ko.observableArray([]),
	
	
	
	selectedtblinward: ko.observable(null),
	revertabletblinward: ko.observable(null),
	selectedtblinwarddetail: ko.observable(null),
	revertabletblinwarddetail: ko.observable(null),
	
	gettblstore: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblstores, output, tblstore);});
	},
	
	gettblcompany: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblcompanies, output, tblcompany);});
	},
	
	gettblpos: function() {
		var self = this;
		FormFuncs.getAjax('../tblpurchaseorders_Servlet', {Type: 'combobox'}, true, function(output) { FormFuncs.pushRs(self.oatblpos, output, tblpos); });
	},
	
	getItems: function() {
		FormFuncs.getAjax('../tblitemmasters_Servlet', {Type : 'all',}, true, function(output) {FormFuncs.pushRs(viewModel.oaitemmasters, output, oItemMaster);});
	},
	
	
	gettblinwards: function(o, p) {
		var self = this;
		FormFuncs.getAjax('../tblinwards_Servlet', {Type: 'page', Offset: o, PageSize: p}, true, function(output) { FormFuncs.pushRs(self.oatblinwards, output, tblinward); });
	},
	
/*	getUOMs: function() {
		FormFuncs.getAjax('../tbluommasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oauoms, output, oUnitOfMeasure);});
	},*/
	
	getStoreRacks: function() {
		FormFuncs.getAjax('../tblstoreitemracks_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oastoreracks, output, oStoreRacks);});
	},

	getDefaultStatuses: function() {
		FormFuncs.getAjax('../tbldefaultlookups_Servlet', {Type : 'LookupTypeCode',LookupTypeCode : 1}, true, function(output) {FormFuncs.pushRs(viewModel.oatbldefaultlookups, output, oStatus);});
	},
	
	getVATTax: function() {
		FormFuncs.getAjax('../tbltaxdetails_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oataxdetails, output, oTaxDetail);});
	},
	
	getVendors: function() {
		FormFuncs.getAjax('../tblvendormasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oavendors, output, oVendor);});
	},
	

	searchtblinwards: function(form) {
		var self = this;
		var name = $(form).find('#textSearch').val();
		FormFuncs.getAjax('../tblinwards_Servlet', {type: 'search', value: name}, true, function(output) { FormFuncs.pushRs(self.oatblinwards, output, tblinward); });
	},
	/*posttblinward: function(form) {
		var self = this;
		FormFuncs.postAjax('../tblinwards_Servlet', {type: 'post', record: ko.toJSON(this.selectedtblinward())}, true, function(output){
			if (output.success == true) {
				FormFuncs.sdAlert('SuccessAlert');
				self.canceltblinward();
			}
			else if (output.success == false) {
				FormFuncs.sdAlert('ErrorAlert', output.message);
			}
		});
	},*/
	deletetblinwards: function() {
		FormFuncs.deleteRs(this.tblinwards, '../tblinwards_servlet');
	},
	canceltblinward: function() {
		$('#tblinward-modal').modal('hide');
		this.pagecounttblinwards();
		this.pagingRefresh();
	},
	pagecounttblinwards: function() { 
		var self = this;
		FormFuncs.getAjax('../tblinwards_Servlet', {Type: 'count'}, true, function(output) {
			self.pagingCount = output[0].RecordCount;
			self.pagingCalculate();
		});
	},
	pagingCalculate: function() { this.pagingNoOfPages = Math.ceil(this.pagingCount/this.pagingPageSize); $('#lblNoOfRecords').text(this.pagingCount);},
	pagingPages: function (type){
    	if (type == 'f') { if (this.pagingCurrentPage == 1) return; this.pagingCurrentPage = 1; }
    	else if (type == 'p') { if (this.pagingCurrentPage == 1) return; this.pagingCurrentPage--; }
    	else if (type == 'n') { if (this.pagingCurrentPage == this.pagingNoOfPages) return; this.pagingCurrentPage++; }
    	else if (type == 'l') { if (this.pagingCurrentPage == this.pagingNoOfPages) return; this.pagingCurrentPage = this.pagingNoOfPages; }
    	this.pagingRefresh();
    },
    pagingRefresh: function() {
    	this.gettblinwards((this.pagingCurrentPage - 1) * this.pagingPageSize, this.pagingPageSize);
    	$('#lblCurrentRecord').text(((this.pagingCurrentPage - 1) * this.pagingPageSize) + 1);
    },
	
	
	
	addEdittblinward: function(data) {
		  
		
		if (data == null){
			this.selectedtblinward(new tblinward());
			viewModel.tblinwarddetails.removeAll();
		}
		else {
			this.selectedtblinward(data);
			viewModel.getInwardDetail(data.chrRecCode());
		}
			
		$('#detail-container').show();
		$('#main-container').hide();
		
		FormFuncs.makeCopy(this.selectedtblinward, this.revertabletblinward, tblinward);
	},
	
	getInwardDetail: function(chrInwardCode) {
		FormFuncs.getAjax('../tblinwarditemdetails_Servlet', {Type: 'inwarddetails', InwardCode: chrInwardCode}, true, function(output) { FormFuncs.pushRs(viewModel.tblinwarddetails, output, tblinwarditemdetail); });
	},
	
	reverttblinward: function() {
		FormFuncs.revertFromCopy(this.revertabletblinward, this.selectedtblinward);
		this.canceltblinward();
	},
	
	// Add and Edit
	posttblinward: function() {
		if(viewModel.tblinwarddetails().length < 1) {
			NotifitMessage("Enter atleast one Product Brand Details", "success");
		} else {
		//	viewModel.selectedtblinward().dtmArrivalTime($("#dtmArrivalTime").val());
			var inward = ko.toJSON(this.selectedtblinward());
			var inwarddetails = ko.toJSON(this.tblinwarddetails());
			FormFuncs.postAjax(
				'../tblinwards_Servlet',
				{Type: 'postDetails', 
					inward: inward,
					inwarddetails: inwarddetails},
				true,
				function(output) {
					if (output.Success == true) {
						if(viewModel.selectedtblinward().IUD() == 1) {
							NotifitMessage("Inward saved successfully.", "success");
						}
						else {
							NotifitMessage("Inward updated successfully.", "success");
						}
						viewModel.closeAddEdittblinward(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	

	submittblinwarddetail: function() {
	/*	viewModel.selectedtblinwarddetail().vhrProduct($('#selected_product option:selected').text());
		viewModel.selectedtblinwarddetail().vhrBrand($('#selected_brand option:selected').text());
		viewModel.selectedtblinwarddetail().vhrModel($('#selected_model option:selected').text());*/
		viewModel.closeAddEdittblinwarddetail(false); 
	},
	addEdittblinward: function(data) {
		if (data == null){
			this.selectedtblinward(new tblinward());
			viewModel.tblinwarddetails.removeAll();
		}
		else{
			this.selectedtblinward(data);
			viewModel.getInwardDetail(data.chrRecCode());
		}
		
		$('#detail-container').show();
		$('#main-container').hide();
		FormFuncs.makeCopy(this.selectedtblinward, this.revertedtblinward, tblinward);
	},
	addEdittblinwarddetail: function(data) {
		if (data == null)
			this.selectedtblinwarddetail(new tblinwarditemdetail());
		else
		{
			var productcode = data.vhrNameOne();
			var brandcode = data.vhrNameTwo();
			var modelcode = data.chrModelCode();
			
			viewModel.selectedtblinwarddetail(data);
			
			viewModel.getBrands(productcode);
			viewModel.getModels(brandcode);
			
			
			viewModel.selectedtblinwarddetail().vhrNameOne(productcode);
			viewModel.selectedtblinwarddetail().vhrNameTwo(brandcode);
			viewModel.selectedtblinwarddetail().chrModelCode(modelcode);
			
			$('#selected_product').prop('disabled', true);
			$('#selected_brand').prop('disabled', true);
			$('#selected_model').prop('disabled', true);
		}
		
		FormFuncs.makeCopy(this.selectedtblinwarddetail, this.revertabletblinwarddetail, tblinwarditemdetail);
	},
	closeAddEdittblinward: function(revert) {
		$('#detail-container').hide();
		$('#main-container').show();
/*		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblinward, this.selectedtblinward);
		}
		else {*/
			viewModel.pageCount();
			viewModel.firstPage();
	//	}
	},
	closeAddEdittblinwarddetail: function(revert) {
		$('#modal-tblinwarddetails').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblinwarddetail, this.selectedtblinwarddetail);
		}
		else {
			FormFuncs.submitRs(viewModel.tblinwarddetails, viewModel.selectedtblinwarddetail, tblinwarditemdetail);
		}
	},
	// Delete
	deletetblinward: function() {
		var deleteref = this.deleteRefId;
		if (deleteref == 'asndetails-a') {
			FormFuncs.postAjax('../tblinwarddetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedtblinwarddetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("ASN Product Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedtblinwarddetail().chrRecCode());
					FormFuncs.removeRs(viewModel.tblinwarddetails, reccodes);
				}
			});
		} else {
			var record = ko.toJSON(this.selectedtblinward());
			FormFuncs.postAjax(
				'../tblinwards_Servlet',
				{Type: 'deletedetails', 
				 Record: record},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("ASN deleted successfully.", "success");
						viewModel.closeAddEdittblinward(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	
	searchTexttblinward : ko.observable(''),
	// Paging 
	counttblinwards: ko.observable(0),
	currentPagetblinwards: ko.observable(0),
	noOfPagestblinwards: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblinwards_Servlet',
			viewModel.counttblinwards,
			viewModel.noOfPagestblinwards,
			viewModel.pageSize,
			viewModel.searchTexttblinward());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblinwards_Servlet',
			viewModel.counttblinwards,
			viewModel.noOfPagestblinwards,
			viewModel.pageSize);
	},
	pagetblinwardPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblinwards(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblinwards_Servlet',
			type,
			viewModel.oatblinwards,
			tblinward,
			viewModel.currentPagetblinwards,
			viewModel.noOfPagestblinwards,
			viewModel.pageSize,
			viewModel.searchTexttblinward());
	},
	pagetblinwardPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblinwards(-1); // force refresh
		
		FormFuncs.pagingPages('../tblinwards_Servlet',
			type, 
			viewModel.oatblinwards,
			tblinward,
			viewModel.currentPagetblinwards,
			viewModel.noOfPagestblinwards,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblinward() == '')
			viewModel.pagetblinwardPaging('f');
		else
			viewModel.pagetblinwardPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblinward() == '')
			viewModel.pagetblinwardPaging('p');
		else
			viewModel.pagetblinwardPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblinward() == '')
			viewModel.pagetblinwardPaging('n');
		else
			viewModel.pagetblinwardPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblinward() == '')
			viewModel.pagetblinwardPaging('l');
		else
			viewModel.pagetblinwardPagingSearch('l');
	}
};
