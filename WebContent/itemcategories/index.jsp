<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Item Category</title>
<%@include file="../../ScriptCss.jsp"%>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../../Common/Header.jsp"%>
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="main-box clearfix">
								<header class="main-box-header clearfix">
									<h2 class="pull-left">
										<b>Item Category</b>
									</h2>
									<div class="filter-block pull-right">
										<div class="col-md-8 pull-left">
											<tag:search databindfn="searchtblitemcategory" databindresetfn="refreshtblitemcategory"
												databindvalue="searchTexttblitemcategory"></tag:search>
										</div>
										<div class="col-md-4 pull-left">
											<tag:addbutton modalid="modal-tblPurchaseUom"
												databind="addEdittblitemcategory" title="Add Item Category"></tag:addbutton>
										</div>
									</div>
								</header>
								<div class="main-box-body clearfix">
									<div class="table-responsive clearfix">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th><b>Name</b></th>
													<th><b>Code</b></th>
													<th class="actioncolumn">Action</th>
												</tr>
											</thead>
											<tbody data-bind="foreach: viewModel.oatblitemcategories">
												<tr>
													<td><span data-bind="text: vhrName"></span></td>
													<td><span data-bind="text: vhrCode"></span></td>
													<td><tag:editbutton modalid="modal-tblPurchaseUom"
															databind="addEdittblitemcategory"
															title="Edit Item Category"></tag:editbutton>
														<tag:deletebutton modalid="modal-delete" 
														databind="deletetblitemcategory" 
														title="Delete Item Category"></tag:deletebutton>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3"><tag:pagination
															currentpage="currentPagetblitemcategory"
															noofpages="noOfPagestblitemcategory"></tag:pagination></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<%@include file="../../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="posttblitemcategory" closefn="closeAddEdit"
		modal="modal-tblPurchaseUom" title="Item Category"	
		withbind="selectedtblitemcategory">
		<tag:input label="Item Category Name" id="vhrName" databind="vhrName" required="true"
			placeholder="Enter Item Category Name" maxlength="100"></tag:input>
		<tag:input label="Item Category Code" id="vhrCode" databind="vhrCode"
			required="true" placeholder="Enter Item Category Code" maxlength="20"></tag:input>
	</tag:addeditmodal>

	<!-- Delete Modal -->
	<tag:deletemodal databindfn="deletetblitemcategory"></tag:deletemodal>
	
	<script src="tblitemcategories.js" type="text/javascript"></script>
</body>
</html>