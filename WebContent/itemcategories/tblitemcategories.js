$(".main-menu").each(function() {
	if (this.id == "Master") {
		$(this).addClass("active");
	}
});
$(".sub-menu").each(function() {
	if (this.id == "ItemCategories") {
		$(this).addClass("active");
	}
});

function  tblitemcategory(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.vhrCode = ko.observable(""); 
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
});

var viewModel = {
	// Array
	oatblitemcategories: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtblitemcategory: ko.observable(),
	revertedtblitemcategory: ko.observable(),
	
	// Search
	searchTexttblitemcategory: ko.observable(''),
	searchtblitemcategory: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	// Refresh
	refreshtblitemcategory: function() {
		viewModel.searchTexttblitemcategory(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	// Add and Edit
	posttblitemcategory: function() {
		var record = ko.toJSON(this.selectedtblitemcategory());
		FormFuncs.postAjax(
			'../tblitemcategories_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblitemcategory().IUD() == 1) {
						NotifitMessage("Item Category saved successfully.", "success");
					}
					else {
						NotifitMessage("Item Category updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblitemcategory: function(data) {
		if (data == null)
			this.selectedtblitemcategory(new  tblitemcategory());
		else
			this.selectedtblitemcategory(data);
		
		FormFuncs.makeCopy(this.selectedtblitemcategory, this.revertedtblitemcategory,  tblitemcategory);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblPurchaseUom').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblitemcategory, this.selectedtblitemcategory);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblitemcategoryPaging(null);
		}
	},
	
	// Delete
	deletetblitemcategory: function() {
		var record = ko.toJSON(this.selectedtblitemcategory());
		console.log("record " + record);
		FormFuncs.postAjax(
			'../tblitemcategories_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Item Category deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	// Paging 
	counttblitemcategory: ko.observable(0),
	currentPagetblitemcategory: ko.observable(0),
	noOfPagestblitemcategory: ko.observable(0),
	pageSize: 12,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblitemcategories_Servlet',
			viewModel.counttblitemcategory,
			viewModel.noOfPagestblitemcategory,
			viewModel.pageSize,
			viewModel.searchTexttblitemcategory());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblitemcategories_Servlet',
			viewModel.counttblitemcategory,
			viewModel.noOfPagestblitemcategory,
			viewModel.pageSize);
	},
	pagetblitemcategoryPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblitemcategory(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblitemcategories_Servlet',
			type,
			viewModel.oatblitemcategories,
			 tblitemcategory,
			viewModel.currentPagetblitemcategory,
			viewModel.noOfPagestblitemcategory,
			viewModel.pageSize,
			viewModel.searchTexttblitemcategory());
	},
	pagetblitemcategoryPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblitemcategory(-1); // force refresh
		
		FormFuncs.pagingPages('../tblitemcategories_Servlet',
			type, 
			viewModel.oatblitemcategories,
			 tblitemcategory,
			viewModel.currentPagetblitemcategory,
			viewModel.noOfPagestblitemcategory,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblitemcategory() == '')
			viewModel.pagetblitemcategoryPaging('f');
		else
			viewModel.pagetblitemcategoryPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblitemcategory() == '')
			viewModel.pagetblitemcategoryPaging('p');
		else
			viewModel.pagetblitemcategoryPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblitemcategory() == '')
			viewModel.pagetblitemcategoryPaging('n');
		else
			viewModel.pagetblitemcategoryPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblitemcategory() == '')
			viewModel.pagetblitemcategoryPaging('l');
		else
			viewModel.pagetblitemcategoryPagingSearch('l');
	}
};