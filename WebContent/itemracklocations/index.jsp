<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Item Rack Location</title>
<%@include file="../ScriptCss.jsp"%>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../Common/Header.jsp"%>
		<div id="page-wrapper" class="container-fluid">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="main-box clearfix">
								<header class="main-box-header clearfix">
									<h2 class="pull-left">
										<b>Item Rack Location</b>
									</h2>
									<div class="filter-block pull-right">
										<div class="col-md-10 pull-left">
											<tag:search databindfn="searchtblitemracklocations"
												databindvalue="searchTexttblitemracklocation"
												databindresetfn="refreshtblitemracklocations"></tag:search>
										</div>
										<div class="col-md-2 pull-left">
											<tag:addbutton modalid="modal-tblitemracklocation"
												databind="addEdittblitemracklocation" title="Add Item Rack Location"></tag:addbutton>
										</div>
									</div>
								</header>
								<div class="main-box-body clearfix">
									<div class="table-responsive clearfix">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th><b>Item Name</b></th>
													<th><b>Item Rack Location</b></th>
													<th class="actioncolumn">Action</th>
												</tr>
											</thead>
											<tbody data-bind="foreach: viewModel.oatblitemracklocations">
												<tr>
													<td><span data-bind="text: vhrItem"></span></td>
													<td><span data-bind="text: vhrRackLocation"></span></td>
													<td><tag:editbutton modalid="modal-tblitemracklocation"
															databind="addEdittblitemracklocation"
															title="Edit Item Rack Location"></tag:editbutton> <tag:deletebutton
															modalid="modal-delete" databind="addEdittblitemracklocation"
															title="Delete Item Rack Location"></tag:deletebutton>
												   </td>
												</tr>
											</tbody>
										</table>
									</div>
									<tag:pagination currentpage="currentPagetblitemracklocations"
										noofpages="noOfPagestblitemracklocations"></tag:pagination>
								</div>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="posttblitemracklocation" closefn="closeAddEdit"
		modal="modal-tblitemracklocation" title="Item Rack Location"
		withbind="selectedtblitemracklocation">

		<tag:select label="Item"
			observable="oaitemmasters" bindvalue="chrItemCode"
			value="chrItemCode" text="vhrName" required="true" caption="Select Item"></tag:select>
		
		<tag:select label="Item Rack"
			observable="oastoreitemracklocations" bindvalue="chrRackLocationCode"
			value="chrRackLocationCode" text="vhrName" required="true" caption="Select Item Rack"></tag:select>
			
			
	</tag:addeditmodal>


	<tag:deletemodal databindfn="deletetblitemracklocation"></tag:deletemodal>

	<script src="tblitemracklocations.js?<%=COMMON_SCRIPT_VERSION%>"
		type="text/javascript"></script>

</body>
</html>