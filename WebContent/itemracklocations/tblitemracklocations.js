$(".main-menu").each(function() {
	if (this.id == "ItemCategories") {
		$(this).addClass("active");
	}
});
function otblitemracklocation(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrItemCode = ko.observable(r.chrItemCode); 
		this.chrRackLocationCode = ko.observable(r.chrRackLocationCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.tinStatus = ko.observable(r.tinStatus);
		this.vhrItem = ko.observable(r.vhrItem);
		this.vhrRackLocation = ko.observable(r.vhrRackLocation);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrItemCode = ko.observable(""); 
		this.chrRackLocationCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinIsDirty = ko.observable(0); 
		this.tinStatus = ko.observable(0);
		this.vhrItem = ko.observable("");
		this.vhrRackLocation = ko.observable("");
		this.IUD = ko.observable(1);
	}
}


function oItemMaster(r){
	this.chrItemCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStoreItemRackLocation(r){
	this.chrRackLocationCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}



$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.getItems();
	viewModel.getItemRackLocations();
});

var viewModel = {
	// Array
	oatblitemracklocations: ko.observableArray([]),
	oaitemmasters : ko.observableArray([]),
	oastoreitemracklocations : ko.observableArray([]),
	// Selected and Revert 
	selectedtblitemracklocation: ko.observable(),
	revertedtblitemracklocation: ko.observable(),
	// Search
	searchTexttblitemracklocation: ko.observable(''),
	searchtblitemracklocations: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtblitemracklocations: function() {
		viewModel.searchTexttblitemracklocation(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	getItems: function() {
		FormFuncs.getAjax('../tblitemmasters_Servlet', {Type : 'all',}, true, function(output) {FormFuncs.pushRs(viewModel.oaitemmasters, output, oItemMaster);});
	},
	
	getItemRackLocations: function() {
		FormFuncs.getAjax('../tblstoreitemracks_Servlet', {Type : 'all',}, true, function(output) {FormFuncs.pushRs(viewModel.oastoreitemracklocations, output, oStoreItemRackLocation);});
	},
	// Add and Edit
	posttblitemracklocation: function() {
		var record = ko.toJSON(this.selectedtblitemracklocation());
		FormFuncs.postAjax(
			'../tblitemracklocations_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblitemracklocation().IUD() == 1) {
						NotifitMessage("Item Rack Location saved successfully.", "success");
					}
					else {
						NotifitMessage("Item Rack Location updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblitemracklocation: function(data) {
		if (data == null)
			this.selectedtblitemracklocation(new otblitemracklocation());
		else
			this.selectedtblitemracklocation(data);
		
		FormFuncs.makeCopy(this.selectedtblitemracklocation, this.revertedtblitemracklocation, otblitemracklocation);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblitemracklocation').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblitemracklocation, this.selectedtblitemracklocation);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblitemracklocationPaging(null);
		}
	},
	// Delete
	deletetblitemracklocation: function() {
		var record = ko.toJSON(this.selectedtblitemracklocation());
		FormFuncs.postAjax(
			'../tblitemracklocations_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Item Rack Location deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttblitemracklocations: ko.observable(0),
	currentPagetblitemracklocations: ko.observable(0),
	noOfPagestblitemracklocations: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblitemracklocations_Servlet',
			viewModel.counttblitemracklocations,
			viewModel.noOfPagestblitemracklocations,
			viewModel.pageSize,
			viewModel.searchTexttblitemracklocation());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblitemracklocations_Servlet',
			viewModel.counttblitemracklocations,
			viewModel.noOfPagestblitemracklocations,
			viewModel.pageSize);
	},
	pagetblitemracklocationPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblitemracklocations(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblitemracklocations_Servlet',
			type,
			viewModel.oatblitemracklocations,
			otblitemracklocation,
			viewModel.currentPagetblitemracklocations,
			viewModel.noOfPagestblitemracklocations,
			viewModel.pageSize,
			viewModel.searchTexttblitemracklocation());
	},
	pagetblitemracklocationPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblitemracklocations(-1); // force refresh
		
		FormFuncs.pagingPages('../tblitemracklocations_Servlet',
			type, 
			viewModel.oatblitemracklocations,
			otblitemracklocation,
			viewModel.currentPagetblitemracklocations,
			viewModel.noOfPagestblitemracklocations,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblitemracklocation() == '')
			viewModel.pagetblitemracklocationPaging('f');
		else
			viewModel.pagetblitemracklocationPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblitemracklocation() == '')
			viewModel.pagetblitemracklocationPaging('p');
		else
			viewModel.pagetblitemracklocationPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblitemracklocation() == '')
			viewModel.pagetblitemracklocationPaging('n');
		else
			viewModel.pagetblitemracklocationPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblitemracklocation() == '')
			viewModel.pagetblitemracklocationPaging('l');
		else
			viewModel.pagetblitemracklocationPagingSearch('l');
	}
};