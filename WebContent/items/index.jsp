<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Item</title>
<%@include file="../../ScriptCss.jsp"%>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../../Common/Header.jsp"%>
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="main-box clearfix">
								<header class="main-box-header clearfix">
									<h2 class="pull-left">
										<b>Item</b>
									</h2>
									<div class="filter-block pull-right">
										<div class="col-md-8 pull-left">
											<tag:search databindfn="searchtblitemmaster" databindresetfn="refreshtblitemmaster"
												databindvalue="searchTexttblitemmaster"></tag:search>
										</div>
										<div class="col-md-4 pull-left">
											<tag:addbutton modalid="modal-tblPurchaseUom"
												databind="addEdittblitemmaster" title="Add Item"></tag:addbutton>
										</div>
									</div>
								</header>
								<div class="main-box-body clearfix">
									<div class="table-responsive clearfix">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th><b>Name</b></th>
													<th><b>Code</b></th>
													<th class="actioncolumn">Action</th>
												</tr>
											</thead>
											<tbody data-bind="foreach: viewModel.oatblitemmasters">
												<tr>
													<td><span data-bind="text: vhrName"></span></td>
													<td><span data-bind="text: vhrCode"></span></td>
													<td><tag:editbutton modalid="modal-tblPurchaseUom"
															databind="addEdittblitemmaster"
															title="Edit Item"></tag:editbutton>
														<tag:deletebutton modalid="modal-delete" 
														databind="deletetblitemmaster" 
														title="Delete Item"></tag:deletebutton>
													</td>
												</tr>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="3"><tag:pagination
															currentpage="currentPagetblitemmaster"
															noofpages="noOfPagestblitemmaster"></tag:pagination></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<%@include file="../../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="posttblitemmaster" closefn="closeAddEdit"
		modal="modal-tblPurchaseUom" title="Item"	
		withbind="selectedtblitemmaster">
		<!-- Combo box -->	
		<tag:select label="Item Category" id="chrItemCategoryCode" observable="oatblitemcategories" required="true"
			bindvalue="chrItemCategoryCode" value="chrItemCategoryCode" text="chrItemCategoryName" caption="Choose Item Category"></tag:select>
			
		<tag:input label="Item Name" id="vhrName" databind="vhrName"
			required="true" placeholder="Enter Item Name" maxlength="100"></tag:input>
			
		<tag:input label="Item Short Name" id="vhrShortName" databind="vhrShortName"
			required="true" placeholder="Enter Item Short Name" maxlength="20"></tag:input>
			
		<tag:input label="Item Code" id="vhrCode" databind="vhrCode"
			required="true" placeholder="Enter Item Code" maxlength="20"></tag:input>
			
		<tag:select label="chrUnitOfMeasureCode" id="chrUnitOfMeasureCode" observable="oatbluommasters" required="true"
			bindvalue="chrUnitOfMeasureCode" value="chrUnitOfMeasureCode" text="chrUnitOfMeasureName" caption="Choose Unit Of Measure"></tag:select>
			
		<tag:textarea label="Item Description" id="vhrDescription" databind="vhrDescription"
			required="true" placeholder="Enter Description" maxlength="1000" rows="3"></tag:textarea>
			
		<!-- Combo box -->	
		<tag:select label="Lose Sold" id="tinLoseSold" observable="oatbldefaultlookups" required="true"
			bindvalue="tinLoseSold" value="tinLoseSold" text="vhrLoseSold" caption="Choose Loss Sold"></tag:select>
			
		<tag:input label="vhrBarcode" id="vhrBarcode" databind="vhrBarcode" 
			required="true" placeholder="Enter vhrBarcode" maxlength="100"></tag:input>	
			
		<tag:input label="intReOrderQuantity" id="intReOrderQuantity" databind="intReOrderQuantity" onkeypress="return isNumber(event)"
			required="true" placeholder="Enter ReOrder Quantity" maxlength="20"></tag:input>	
	</tag:addeditmodal>

	<!-- Delete Modal -->
	<tag:deletemodal databindfn="deletetblitemmaster"></tag:deletemodal>
	
	<script src="tblitemmasters.js" type="text/javascript"></script>
</body>
</html>