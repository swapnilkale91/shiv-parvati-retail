$(".main-menu").each(function() {
	if (this.id == "Master") {
		$(this).addClass("active");
	}
});
$(".sub-menu").each(function() {
	if (this.id == "Items") {
		$(this).addClass("active");
	}
});

function  tblitemmaster(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrItemCategoryCode = ko.observable(r.chrItemCategoryCode); 
		this.chrItemCategoryName = ko.observable(r.chrItemCategoryName); 
		this.chrUnitOfMeasureCode = ko.observable(r.chrUnitOfMeasureCode); 
		this.chrUnitOfMeasureName = ko.observable(r.chrUnitOfMeasureName); 
		this.intReOrderQuantity = ko.observable(r.intReOrderQuantity); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.tinLoseSold = ko.observable(r.tinLoseSold); 
		this.vhrLoseSold = ko.observable(r.vhrLoseSold); 
		this.vhrBarcode = ko.observable(r.vhrBarcode); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrDescription = ko.observable(r.vhrDescription); 
		this.vhrName = ko.observable(r.vhrName); 
		this.vhrShortName = ko.observable(r.vhrShortName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrItemCategoryCode = ko.observable(""); 
		this.chrItemCategoryName = ko.observable(""); 
		this.chrUnitOfMeasureCode = ko.observable(""); 
		this.chrUnitOfMeasureName = ko.observable(""); 
		this.intReOrderQuantity = ko.observable(""); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.tinLoseSold = ko.observable(""); 
		this.vhrLoseSold = ko.observable(""); 
		this.vhrBarcode = ko.observable(""); 
		this.vhrCode = ko.observable(""); 
		this.vhrDescription = ko.observable(""); 
		this.vhrName = ko.observable(""); 
		this.vhrShortName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblitemcategory(r){
	this.chrItemCategoryCode = r.chrRecCode;
	this.chrItemCategoryName = r.vhrName;
}

function tbluommaster(r){
	this.chrUnitOfMeasureCode = r.chrRecCode;
	this.chrUnitOfMeasureName = r.vhrName;
}

function tbldefaultlookup(r){
	this.tinLoseSold = r.intRecCode;
	this.vhrLoseSold = r.vhrName;
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.getDefaultStatuses();
	viewModel.gettblitemcategory();
	viewModel.gettbluommaster();
});

var viewModel = {
	// Array
	oatblitemmasters: ko.observableArray([]),
	oatblitemcategories: ko.observableArray([]),
	oatbluommasters: ko.observableArray([]),
	oatbldefaultlookups: ko.observableArray([]),
	
	
	// Selected and Revert 
	selectedtblitemmaster: ko.observable(),
	revertedtblitemmaster: ko.observable(),
	
	// Search
	searchTexttblitemmaster: ko.observable(''),
	searchtblitemmaster: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	// Refresh
	refreshtblitemmaster: function() {
		viewModel.searchTexttblitemmaster(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	getDefaultStatuses: function() {
		FormFuncs.getAjax('../tbldefaultlookups_Servlet', {Type : 'LookupTypeCode',LookupTypeCode : 1}, true, function(output) {FormFuncs.pushRs(viewModel.oatbldefaultlookups, output, tbldefaultlookup);});
	},
	
	gettblitemcategory: function() {
		FormFuncs.getAjax('../tblitemcategories_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblitemcategories, output, tblitemcategory);});
	},
	
	gettbluommaster: function() {
		FormFuncs.getAjax('../tbluommasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatbluommasters, output, tbluommaster);});
	},
	
	// Add and Edit
	posttblitemmaster: function() {
		var record = ko.toJSON(this.selectedtblitemmaster());
		FormFuncs.postAjax(
			'../tblitemmasters_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblitemmaster().IUD() == 1) {
						NotifitMessage("Item saved successfully.", "success");
					}
					else {
						NotifitMessage("Item updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblitemmaster: function(data) {
		if (data == null)
			this.selectedtblitemmaster(new tblitemmaster());
		else
			this.selectedtblitemmaster(data);
		
		FormFuncs.makeCopy(this.selectedtblitemmaster, this.revertedtblitemmaster,  tblitemmaster);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblPurchaseUom').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblitemmaster, this.selectedtblitemmaster);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblitemmasterPaging(null);
		}
	},
	
	// Delete
	deletetblitemmaster: function() {
		var record = ko.toJSON(this.selectedtblitemmaster());
		FormFuncs.postAjax(
			'../tblitemmasters_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Item deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	// Paging 
	counttblitemmaster: ko.observable(0),
	currentPagetblitemmaster: ko.observable(0),
	noOfPagestblitemmaster: ko.observable(0),
	pageSize: 12,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblitemmasters_Servlet',
			viewModel.counttblitemmaster,
			viewModel.noOfPagestblitemmaster,
			viewModel.pageSize,
			viewModel.searchTexttblitemmaster());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblitemmasters_Servlet',
			viewModel.counttblitemmaster,
			viewModel.noOfPagestblitemmaster,
			viewModel.pageSize);
	},
	pagetblitemmasterPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblitemmaster(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblitemmasters_Servlet',
			type,
			viewModel.oatblitemmasters,
			 tblitemmaster,
			viewModel.currentPagetblitemmaster,
			viewModel.noOfPagestblitemmaster,
			viewModel.pageSize,
			viewModel.searchTexttblitemmaster());
	},
	pagetblitemmasterPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblitemmaster(-1); // force refresh
		
		FormFuncs.pagingPages('../tblitemmasters_Servlet',
			type, 
			viewModel.oatblitemmasters,
			 tblitemmaster,
			viewModel.currentPagetblitemmaster,
			viewModel.noOfPagestblitemmaster,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblitemmaster() == '')
			viewModel.pagetblitemmasterPaging('f');
		else
			viewModel.pagetblitemmasterPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblitemmaster() == '')
			viewModel.pagetblitemmasterPaging('p');
		else
			viewModel.pagetblitemmasterPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblitemmaster() == '')
			viewModel.pagetblitemmasterPaging('n');
		else
			viewModel.pagetblitemmasterPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblitemmaster() == '')
			viewModel.pagetblitemmasterPaging('l');
		else
			viewModel.pagetblitemmasterPagingSearch('l');
	}
};