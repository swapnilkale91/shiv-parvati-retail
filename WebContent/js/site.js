var FormFuncs = {
	getValuesFromForm: function (form) {
	        form = $(form);
	        var r = {};
	        form.find('input[type!=submit],select,textarea').each(function () {
	            r[this.name] = $(this).val();
	        });
	        return r;
	},
	getUUID: function () {
		var uuid = "empty";
	    uuid = (function () {
	        var i,
	            c = "89ab",
	            u = [];
	        for (i = 0; i < 36; i += 1) {
	            u[i] = (Math.random() * 16 | 0).toString(16);
	        }
	        u[8] = u[13] = u[18] = u[23] = "-";
	        u[14] = "4";
	        u[19] = c.charAt(Math.random() * 4 | 0);
	        return String(u.join(""));
	    })();
	    return {
	        toString: function () {
	            return String(uuid);
	        },
	        valueOf: function () {
	            return String(uuid);
	        }
	    };
	},
	loggedInRecCode: function() {
		return $("#loggedinRecCode").val();
	},
	
	// changed at 07-07-2015 
	loggedInCompanyCode: function() {
		return $("#loggedinCompanyCode").val();
	},
	
	// changed at 12-10-2015
	loggedinStoreCode: function() {
		return $("#loggedinStoreCode").val();
	},

	
	getAjax: function(servlet, jsonData, asynchronous, getAjaxCB) {
		$.ajax({
			url : servlet,
			type : 'GET',
			dataType: 'json',
			async: asynchronous,
			data: jsonData,
			processData : true,
			cache: false,
			beforeSend : function() { $("#loading").show(); },
			complete : function() { $("#loading").hide(); },
			success: function(rs) { 
				getAjaxCB(rs); 
			},
			failure: function(rs) { 
				getAjaxCB(rs);
			}
		});
	},
	
	getAjaxNoLoadingGif: function(servlet, jsonData, asynchronous, getAjaxCB) {
		$.ajax({
			url : servlet,
			type : 'GET',
			dataType: 'json',
			async: asynchronous,
			data: jsonData,
			cache: false,
			processData : true,
			success: function(rs) { 
				getAjaxCB(rs); 
			},
			failure: function(rs) { 
				getAjaxCB(rs); 
			}
		});
	},
	postAjax: function(servlet, jsonData, asynchronous, postAjaxCB) {
		$.ajax({
			url : servlet,
			type : 'POST',
			dataType: 'json',
			async: asynchronous,
			data: jsonData,
			processData : true,
			cache: false,
			beforeSend : function() { $("#loading").show(); },
			complete : function() { $("#loading").hide(); },
			success: function(rs) { postAjaxCB(rs); },
			failure: function(rs) { postAjaxCB(rs); }
		});
	},
	makeCopy: function(source, destination, obj) {
		destination(new obj());
		for (var prop in destination()) {
			destination()[prop](source()[prop]());
		}
	},
	revertFromCopy: function(source, destination) {
		for (var prop in destination()) {
			destination()[prop](source()[prop]());
	    }
	},
	submitRs: function(array, selected, obj) {
		var match = ko.utils.arrayFirst(array(), function (item) { return selected().chrRecCode() === item.chrRecCode(); });
		
		if (match != null)
			obj(selected());
		else
			array.push(selected());
	},
	deleteRs: function(array, servlet, postback) {
		var reccodes = new Array();
		var self = this;
		$.each(array(), function (index, r) { if (r.selected()) reccodes.push(r.chrRecCode()); });
		self.postAjax(servlet, {type: 'delete', data: reccodes.toString()}, true, function(output) {
				if (output == false) { FormFuncs.sdAlert('ErrorAlert'); }
				else self.removeRs(array, reccodes);
				if (postback != undefined) {
					var success;
					if (output == false) success = false;
					else success = true;
					
					postback(success);
				}
		});
	},
	pushRs: function(array, rs, obj) {
		array.removeAll();
		$.each(rs, function(index, item){ 
			array.push(new obj(item));
		});
	},
	removeRs: function(array, reccodes) {
		$.each(reccodes, function (index, r) {
			var match = ko.utils.arrayFirst(array(), function (item) { return r === item.chrRecCode(); });
			array.remove(match);
		});
	},
	/*HITS 2015/07/04*/
	removeRsOrderCode: function(array, reccodes) {
		$.each(reccodes, function (index, r) {
			var match = ko.utils.arrayFirst(array(), function (item) { return r === item.chrTDCOrderingInformationCode(); });
			array.remove(match);
		});
	},
	/*HITS 2015/07/04*/
	/*HITS 2015/07/15*/
	removeRecCodeRs: function(array, reccodes) {
		$.each(reccodes, function (index, r) {
			var match = ko.utils.arrayFirst(array(), function (item) { return r 
	=== item.RecCode(); });
			array.remove(match);
		});
	}, 
	/*HITS 2015/07/15*/
	removeSelectedRs: function(array) {
		var reccodes = new Array();
		$.each(array(), function (index, r) { if (r.selected()) reccodes.push(r.chrRecCode()); });
		this.removeRs(array, reccodes);
	},
	pageCount: function (sName, cObject, npObject, pageSize) {
		FormFuncs.getAjax(
			sName,
			{Type: 'count'},
			true,
			function(output) {
				cObject(output[0].RecordCount);
				npObject(Math.ceil(cObject()/pageSize));
		});
	},
	pageCountSearch: function (sName, cObject, npObject, pageSize, searchText) {
		FormFuncs.getAjax(
			sName,
			{Type: 'countsearch', SearchText: searchText},
			true,
			function(output) {
				cObject(output[0].RecordCount);
				npObject(Math.ceil(cObject()/pageSize));
		});
	},
	/*HITS 2015/08/05*/
	pageCountSearchByJobNo: function (sName, cObject, npObject, pageSize, searchText) {
		FormFuncs.getAjax(
			sName,
			{Type: 'countsearchbyjobno', SearchText: searchText},
			true,
			function(output) {
				cObject(output[0].RecordCount);
				npObject(Math.ceil(cObject()/pageSize));
		});
	},
	pagingRefreshSearch: function(sName, oArray, oObject, cpObject, npObject, pageSize, searchText) {
    	FormFuncs.getAjax (
			sName,
			{ Type: 'pagesearch', Offset: (cpObject() - 1) * pageSize, PageSize: pageSize, SearchText: searchText },
			true,
			function (output) {
				FormFuncs.pushRs(oArray, output, oObject);
			}
		);
    },
    /*HITS 2015/08/05*/
    pagingRefreshSearchByJobNo: function(sName, oArray, oObject, cpObject, npObject, pageSize, searchText) {
    	FormFuncs.getAjax (
			sName,
			{ Type: 'pagesearchbyjobno', Offset: (cpObject() - 1) * pageSize, PageSize: pageSize, SearchText: searchText },
			true,
			function (output) {
				FormFuncs.pushRs(oArray, output, oObject);
			}
		);
    },
	pagingRefresh: function(sName, oArray, oObject, cpObject, npObject, pageSize) {
    	FormFuncs.getAjax (
			sName,
			{ Type: 'page', Offset: (cpObject() - 1) * pageSize, PageSize: pageSize },
			true,
			function (output) {
				FormFuncs.pushRs(oArray, output, oObject);
			}
		);
    },
    pagingPagesSearch: function (sName, type, oArray, oObject, cpObject, npObject, pageSize, searchText){
    	if (type == 'f') { if (cpObject() == 1) return; cpObject(1); }
    	else if (type == 'p') { if (cpObject() == 1) return; cpObject(cpObject() - 1); }
    	else if (type == 'n') { if (cpObject() == npObject()) return; cpObject(cpObject() + 1); }
    	else if (type == 'l') { if (cpObject() == npObject()) return; cpObject(npObject()); }
    	FormFuncs.pagingRefreshSearch(sName, oArray, oObject, cpObject, npObject, pageSize, searchText);
    },
    /*HITS 2015/08/05*/
    pagingPagesSearchByJobNo: function (sName, type, oArray, oObject, cpObject, npObject, pageSize, searchText){
    	if (type == 'f') { if (cpObject() == 1) return; cpObject(1); }
    	else if (type == 'p') { if (cpObject() == 1) return; cpObject(cpObject() - 1); }
    	else if (type == 'n') { if (cpObject() == npObject()) return; cpObject(cpObject() + 1); }
    	else if (type == 'l') { if (cpObject() == npObject()) return; cpObject(npObject()); }
    	FormFuncs.pagingRefreshSearchByJobNo(sName, oArray, oObject, cpObject, npObject, pageSize, searchText);
    },
    pagingPages: function (sName, type, oArray, oObject, cpObject, npObject, pageSize){
    	if (type == 'f') { if (cpObject() == 1) return; cpObject(1); }
    	else if (type == 'p') { if (cpObject() == 1) return; cpObject(cpObject() - 1); }
    	else if (type == 'n') { if (cpObject() == npObject()) return; cpObject(cpObject() + 1); }
    	else if (type == 'l') { if (cpObject() == npObject()) return; cpObject(npObject()); }
    	FormFuncs.pagingRefresh(sName, oArray, oObject, cpObject, npObject, pageSize);
    },
    /*Hits 2015-04-02*/
    pageCountReport: function (sName, cObject, npObject, pageSize, reportCondition) {
		FormFuncs.getAjax(
			sName,
			{Type: 'countreport', ReportCondition: reportCondition},
			true,
			function(output) {
				cObject(output[0].RecordCount);
				npObject(Math.ceil(cObject()/pageSize));
		});
	},
	pagingPagesReport: function (sName, type, oArray, oObject, cpObject, npObject, pageSize, reportCondition){
    	if (type == 'f') { if (cpObject() == 1) return; cpObject(1); }
    	else if (type == 'p') { if (cpObject() == 1) return; cpObject(cpObject() - 1); }
    	else if (type == 'n') { if (cpObject() == npObject()) return; cpObject(cpObject() + 1); }
    	else if (type == 'l') { if (cpObject() == npObject()) return; cpObject(npObject()); }
    	FormFuncs.pagingRefreshReport(sName, oArray, oObject, cpObject, npObject, pageSize, reportCondition);
    },
	pagingRefreshReport: function(sName, oArray, oObject, cpObject, npObject, pageSize, reportCondition) {
    	FormFuncs.getAjax (
			sName,
			{ Type: 'pagereport', Offset: (cpObject() - 1) * pageSize, PageSize: pageSize , ReportCondition: reportCondition},
			true,
			function (output) {
				FormFuncs.pushRs(oArray, output, oObject);
			}
		);
    },

    pagingPagesReportSearch: function (sName, type, oArray, oObject, cpObject, npObject, pageSize, searchText, reportCondition){
    	if (type == 'f') { if (cpObject() == 1) return; cpObject(1); }
    	else if (type == 'p') { if (cpObject() == 1) return; cpObject(cpObject() - 1); }
    	else if (type == 'n') { if (cpObject() == npObject()) return; cpObject(cpObject() + 1); }
    	else if (type == 'l') { if (cpObject() == npObject()) return; cpObject(npObject()); }
    	FormFuncs.pagingRefreshReportSearch(sName, oArray, oObject, cpObject, npObject, pageSize, searchText, reportCondition);
    },
    pagingRefreshReportSearch: function(sName, oArray, oObject, cpObject, npObject, pageSize, searchText, reportCondition) {
    	FormFuncs.getAjax (
			sName,
			{ Type: 'pagereportsearch', Offset: (cpObject() - 1) * pageSize, PageSize: pageSize, SearchText: searchText, ReportCondition : reportCondition},
			true,
			function (output) {
				FormFuncs.pushRs(oArray, output, oObject);
			}
		);
    },
    /*Hits 2015-04-02*/
	sdAlert: function(alertName, message) {
		if (typeof message == 'undefined') message = "An Error has Occured"; 
		$('#labelErrorText').text(message);
		$('#' + alertName).removeClass('alertify-log-hide');
		setTimeout(function() { $('#' + alertName).addClass('alertify-log-hide'); }, 1500);
	}
};
ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {};
        $(element).datepicker(options).on("changeDate", function (ev) {
            var observable = valueAccessor();
            observable(ev.date);
        });
        
        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            from = $(element).val().split("/");
            if(from != ""){
            	 f = new Date(from[2], from[1] - 1, from[0]);
            	 console.log(f);
            	 observable(f);
            } else {
            	observable(null);
            	$(element).datepicker("");
            }
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value != null)
        	if (typeof value === "object")
        		$(element).datepicker("setValue", value);
        	else{
        		console.log(value);
        		$(element).datepicker("setValue", value.substr(8,2) + '/' + value.substr(5,2) + '/' + value.substr(0,4));
        	}
        		
        		
        $(element).datepicker('hide');
    }
};

ko.bindingHandlers.dateWithoutTime = 
{ 
	update : function(element, valueAccessor, allBindingsAccessor, viewModel) 
	{
		var value = valueAccessor();
		var formatString = allBindingsAccessor().formatString;
		var date = moment(value());
		if (formatString == null) 
		{
			if(date != null)
			{
				if(date._f == undefined)
					$(element).text("");
				else
					$(element).text(formatAMPMWithoutTime(new Date(date))); 
			}	
		}
		else 
		{
			$(element).text(date.format(formatString));
		}
	}
};

ko.bindingHandlers.dpc = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {};
        $(element).datepicker(options);

      //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
           
            from = $(element).val().split("/");
           
            if(from != ""){
                 f = new Date(from[2], from[1] - 1, from[0]);
                 observable(f);
            } else {
                $(element).datepicker("");
            }
          
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(element).datepicker("destroy");
        });

    },
    //update the control when the view model changes
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).datepicker("setDate", value);
    }
};
ko.bindingHandlers.dpicker = {
    init: function(element, valueAccessor, allBindingsAccessor) {
      //initialize datepicker with some optional options
      var options = allBindingsAccessor().datepickerOptions || {};
      $(element).datepicker(options);
      
      //when a user changes the date, update the view model
      ko.utils.registerEventHandler(element, "changeDate", function(event) {
             var value = valueAccessor();
             if (ko.isObservable(value)) {
                 value(event.date);
             }                
      });
        
      ko.utils.registerEventHandler(element, "change", function() {
             var value = valueAccessor();
             if (ko.isObservable(value)) {
                 value(new Date(element.value));
             }                
      });
    },
    update: function(element, valueAccessor)   {
        var widget = $(element).data("datepicker");
         //when the view model is updated, update the widget
        if (widget) {
            widget.date = ko.utils.unwrapObservable(valueAccessor());
            
            if (!widget.date) {
                return;
            }                
            
            if ($.type(widget.date) === "string") {
                widget.date = new Date(widget.date);
            }
            
            widget.setValue();
        }
    }
};
//wrapper for an observable that protects value until committed
ko.revertableObservable = function(initialValue) {
    //private variables
    var cache = ko.observable(initialValue),
        actual = ko.observable(initialValue);
    
    //cache the current value, so we can potentially revert back to it
    actual.commit = function() {
        cache(actual());
    };

    //revert back to the cached value
    actual.revert = function() {
        actual(cache());
    };

    return actual;
};

ko.bindingHandlers.enterkey = {
	    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
	       var allBindings = allBindingsAccessor();

    $(element).on('keypress', 'input, textarea, select', function (e) {
            var keyCode = e.which || e.keyCode;
            if (keyCode !== 13) {
                return true;
            }

            var target = e.target;
            target.blur();    

            allBindings.enterkey.call(viewModel, viewModel, target, element);

            return false;
        });
    }
};

function isNumber(evt) {
	   evt = (evt) ? evt : window.event;
	   var charCode = (evt.which) ? evt.which : evt.keyCode;
	   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	       return false;
	   }
	   return true;
}
// add by Aniket
function isDecimal(evt) {
	   evt = (evt) ? evt : window.event;
	   var charCode = (evt.which) ? evt.which : evt.keyCode;
	   if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46)) {
		   return false;
	   }
	   return true;
}

function formatAMPMWithoutTime(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  strTime = formatOnlyDate(date);
	  return strTime;
}

function isFloat(evt) {
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	   
	   if(charCode == 46){
		   
		   return true;
	   }
	   	return false;
    }
   		return true;
}

function formatOnlyDate(value)
{
   return ('0' + value.getDate()).slice(-2) + "-" + (value.getMonthName()).substring(0,3) + "-" + value.getFullYear().toString();
}

function NotifitMessage(data,type) {
    notif({
        msg: data,
        type: type,
        position: "center",
        time: 1000,
        fade: true,
        multiline: true
    });
}


