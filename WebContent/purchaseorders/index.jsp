<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Purchase Order</title>
<%@include file="../ScriptCss.jsp"%>
</head>
<body class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
	    <%@include file="../Common/Header.jsp"%> 
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div id="main-container">
							
								<div class="main-box clearfix">
									<header class="main-box-header clearfix">
										<h2 class="pull-left"><b>Purchase Order</b></h2>
										<div class="filter-block pull-right">
											<div class="col-md-10 pull-left">
												<tag:search databindresetfn="resettblpurchaseorder" databindfn="searchtblpurchaseorders" databindvalue="searchTexttblpurchaseorder"></tag:search>
											</div>
											<div class="col-md-2 pull-left">
												<tag:addbutton databind="addEdittblpurchaseorder" title="Add Purchase Order"></tag:addbutton>
											</div>
										</div>
									</header>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<table class="table table-hover table-bordered">
												<thead>
													<tr>
														<th><b>Company Name</b></th>
														<th><b>PO No</b></th>
														<th><b>PO Date</b></th>
														<th><b>PO Amount</b></th>
														<th class="actioncolumn">Action</th>
													</tr>
												</thead>
												<tbody data-bind="foreach: viewModel.oatblpurchaseorders">
													<tr>
														<td><span data-bind="text: vhrCompanyName"></span></td>
														<td><span data-bind="text: chrPONo"></span></td>
														<td><span data-bind="text: dtmPODate"></span></td>
														<td><span data-bind="text: decPOAmt"></span></td>
														<td>
															<tag:editbutton modalid="modal-tblpurchaseorder" databind="addEdittblpurchaseorder" title="Edit stock adjustment"></tag:editbutton>
															<tag:deletebutton modalid="modal-delete" databind="addEdittblpurchaseorder" title="Delete stock adjustment"></tag:deletebutton>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<tag:pagination currentpage="currentPagetblpurchaseorders" noofpages="noOfPagestblpurchaseorders"></tag:pagination>
									</div>
								</div>
							</div>
							<div id="detail-container" style="display: none">
							
								<form role="form" data-bind="submit: function() { viewModel.posttblpurchaseorder() }" >
									<div class="main-box clearfix">
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Purchase Order</b></h2>
										</header>
										<div class="main-box-body clearfix" data-bind="with: selectedtblpurchaseorder">
											<div class="row">
												<div id="col1" class="col-md-3">
													<tag:select label="Company" observable="oacompanies" bindvalue="chrCompanyCode" value="chrCompanyCode" text="vhrName" caption="Choose" required="true"></tag:select>
													<tag:input label="Credit Period" id="intCreditPeriod" databind="intCreditPeriod" required="true" placeholder="Enter Credit Period"></tag:input>
													<tag:input label="Discount Total" id="decDiscountTotal" databind="decDiscountTotal" required="true" placeholder="Enter Discount Total"></tag:input>
												
												
 											   </div>
												<div id="col1" class="col-md-3">
													<tag:input label="PO No" id="chrPONo" databind="chrPONo" required="true" placeholder="Enter PO No"></tag:input>
													<tag:input label="Item Total" id="decItemTotal" databind="decItemTotal" required="true" placeholder="Enter Item Total"></tag:input>
													<tag:input label="VAT Tax Total" id="decVatTaxTotal" databind="decVatTaxTotal" required="true" placeholder="Enter Vat Tax Total"></tag:input>
													
												</div>
												<div id="col1" class="col-md-3">
													<tag:datepicker label="PO Date" id="dtmPODate" databind="dtmPODate" placeholder="Select PO Date"></tag:datepicker>		
													<tag:input label="Margin Total" id="decMarginTotal" databind="decMarginTotal" required="true" placeholder="Enter Margin Total"></tag:input>
													<tag:input label="PO Amount" id="decPOAmt" databind="decPOAmt" required="true" placeholder="Enter PO Amount"></tag:input>													
													
												</div>	
												<div id="col1" class="col-md-3">
													<tag:select label="Vendor" observable="oavendors" id="vendorname" bindvalue="chrVendorCode" value="chrRecCode" text="vhrName" caption="Choose Vendor" required="true"></tag:select>
													<tag:input label="Scheme Total" id="decSchemeTotal" databind="decSchemeTotal" required="true" placeholder="Enter Scheme Total"></tag:input>
													
												</div>
											</div>
										</div>
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Purchase Order Item Details</b></h2>
											<div class="filter-block pull-right">
												<div class="col-md-2 pull-left">
													<tag:addbutton modalid="modal-tblpurchaseorderdetails" databind="addEdittblpurchaseorderdetail" title="Add Purchase Order Details"></tag:addbutton>
												</div>
											</div>
										</header>
										<div class="main-box-body clearfix">
											<div class="table-responsive clearfix">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th><b>Item Name</b></th>
															<th><b>Item Description</b></th>
															<th><b>Quantity</b></th>
															<th><b>UOM</b></th>
															<th><b>Rate</b></th>
															<th><b>Item Amount</b></th>
															<th><b>Margin Percentage</b></th>
															<th><b>Margin Amount</b></th>
															<th><b>Scheme Discount Percentage</b></th>
															<th><b>Scheme Discount Amount</b></th>
															<th><b>Vendor Discount Percentage 1</b></th>
															<th><b>Vendor Discount Amount 1</b></th>
															<th><b>Vendor Discount Percentage 2</b></th>
															<th><b>Vendor Discount Amount 2</b></th>
															<th><b>Item Discount Amount</b></th>
															<th><b>VATTax</b></th>
															<th><b>VAT Tax Percentage</b></th>
															<th><b>VAT Tax Amount</b></th>
															<th><b>Item Amount After Tax</b></th>
															<th class="actioncolumn">Action</th>
														</tr>
													</thead>
													<tbody data-bind="foreach: viewModel.oatblpurchaseorderdetails">
														<tr data-bind="click:viewModel.fetchitemdeliverydetails($data), css:{'btn btn-success':$root.selectedtblpurchaseorderdetail() == $data">
															<td><span data-bind="text: vhrItemName"></span></td>
															<td><span data-bind="text: chrItemDescription"></span></td>
															<td><span data-bind="text: decQuantity"></span></td>
															<td><span data-bind="text: vhrUMOName"></span></td>
															<td><span data-bind="text: decRate"></span></td>
															<td><span data-bind="text: decItemAmount"></span></td>
															<td><span data-bind="text: decMarginPercent"></span></td>
															<td><span data-bind="text: decMarginAmount"></span></td>
															<td><span data-bind="text: decSchemeDiscountPercent"></span></td>
															<td><span data-bind="text: decSchemeDiscountAmount"></span></td>
															<td><span data-bind="text: decVendorDiscoutPercent1"></span></td>
															<td><span data-bind="text: decVendorAmount1"></span></td>
															<td><span data-bind="text: decVendorDiscountPercent2"></span></td>
															<td><span data-bind="text: decVendorDiscountAmount2"></span></td>
															<td><span data-bind="text: decItemDiscount"></span></td>
															<td><span data-bind="text: vhrVatTaxName"></span></td>
															<td><span data-bind="text: decVatTaxPercent"></span></td>
															<td><span data-bind="text: decVatTaxAmount"></span></td>
															<td><span data-bind="text: decItemAmountAfterTax"></span></td>
															
															<td>
																<tag:editbutton modalid="modal-tblpurchaseorderdetails" databind="addEdittblpurchaseorderdetail" title="Edit ASN Details"></tag:editbutton>
																<tag:deletebutton id="asndetails-a" modalid="modal-delete" databind="addEdittblpurchaseorderdetail" title="Delete ASN Details"></tag:deletebutton>
<%-- 															<tag:addbutton modalid="modal-addtblshowpurchaseorderdetails" databind="addEditshowtblpurchaseorderdetail" title="add ASN Details"></tag:addbutton>
 --%>															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										
										
										<header class="main-box-header clearfix">
											<h2 class="pull-left" id="page-tblpurchaseorderitemdeliverydetailsLabel"><b>Add Purchase Order Item Delivery Details</b></h2>
											<div class="filter-block pull-right">
												<div class="col-md-2 pull-left">
													<tag:addbutton id="itemdeliverydetail" modalid="modal-tblpurchaseorderitemdeliverydetails" databind="addEdittblpurchaseorderitemdeliverydetail" title="Add Purchase Order Details"></tag:addbutton>
												</div>
											</div>
										</header>
										<div class="main-box-body clearfix">
											<div class="table-responsive clearfix">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th><b>Store Location</b></th>
															<th><b>Quantity</b></th>
															<th class="actioncolumn">Action</th>
														</tr>
													</thead>
													<tbody data-bind="foreach: viewModel.filtereditemdeliverydetails">
														<tr>
															<!-- <td><span data-bind="text: chrStoreLocationCode"></span></td> -->
															<td><tag:select label="" id="selected_location" observable="oastores" bindvalue="chrStoreLocationCode" value="chrRecCode" text="vhrName" caption="Choose Store Location"  required="true" ></tag:select></td>
															<!-- <td><span data-bind="text: decQuantity"></span></td> -->
															<td><tag:input label="" id="decQuantity" databind="decQuantity" placeholder="Enter Quantity" maxlength="10" onkeypress="return isNumber(event)" required="true"></tag:input>
															</td>
															<td>
																<tag:editbutton modalid="modal-tblpurchaseorderitemdeliverydetails" databind="addEdittblpurchaseorderitemdeliverydetail" title="Edit Purchase Item Delivery Details"></tag:editbutton>
																<tag:deletebutton id="tblpurchaseorderitemdeliverydetails-a" modalid="modal-delete" databind="addEdittblpurchaseorderitemdeliverydetail" title="Delete Purchase Item Delivery Details"></tag:deletebutton>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										
										</div>
									
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<div class="pull-right">
												<button type="cancel" class="btn btn-default" data-bind="click: function() { viewModel.closeAddEdittblpurchaseorder(true); }">Cancel</button>
												<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Add Edit Modal for purchase item detail-->
	<tag:addeditmodal postfn="submittblpurchaseorderdetail" closefn="closeAddEdittblpurchaseorderdetail" modal="modal-tblpurchaseorderdetails" title="Purchace Order Item Details" withbind="selectedtblpurchaseorderdetail">
		<div class="row">
			<div id="col1" class="col-md-6">
				<tag:select label="Item" id="selected_item" observable="oaitemmasters" bindvalue="chrItemCode" value="chrItemCode" text="vhrName" caption="Choose Item"  required="true" ></tag:select>
				<tag:input label="Item Description" id="chrItemDescription" databind="chrItemDescription" placeholder="Enter Item Description" maxlength="10" required="true"></tag:input>
				<tag:input label="Rate" id="decRate" databind="decRate" placeholder="Enter decRate" maxlength="10" required="true"></tag:input>
				<tag:input label="Margin Percent" id="decMarginPercent" databind="decMarginPercent" placeholder="Enter Margin Percent" maxlength="10" required="true"></tag:input>
				<tag:input label="Scheme Discount Percent" id="decSchemeDiscountPercent" databind="decSchemeDiscountPercent" placeholder="Enter Scheme Discount Percent" maxlength="10" required="true"></tag:input>
				<tag:input label="Vendor Discount Percent1" id="decVendorDiscoutPercent1" databind="decVendorDiscoutPercent1" placeholder="Enter Vendor Vendor Discout Percent 1" maxlength="10" required="true"></tag:input>
				<tag:input label="Vendor Discount Percent2" id="decVendorDiscountPercent2" databind="decVendorDiscountPercent2" placeholder="Enter Vendor Discount Percent 2" maxlength="10" required="true"></tag:input>
				<tag:input label="ItemDiscount" id="decItemDiscount" databind="decItemDiscount" placeholder="Enter Item Discount" maxlength="10" required="true"></tag:input>
				<tag:input label="Vat Tax Percent" id="decVatTaxPercent" databind="decVatTaxPercent" placeholder="Enter Vat Tax Percent" maxlength="10" required="true"></tag:input>
				<tag:input label="Item Amount After Tax" id="decItemAmountAfterTax" databind="decItemAmountAfterTax" placeholder="Enter Item Amount After Tax" maxlength="10" required="true"></tag:input>
			
			</div>
			<div id="col1" class="col-md-6">
				<tag:input label="Quantity" id="decQuantity" databind="decQuantity" placeholder="Enter Quantity" maxlength="10" onkeypress="return isNumber(event)" required="true"></tag:input>
				<tag:select id="UOM" label="UOM"  observable="oauoms" bindvalue="chrUOMCode" value="chrRecCode" text="vhrName" caption="Choose UOM"  required="true" ></tag:select>
				<tag:input label="Item Amount" id="decItemAmount" databind="decItemAmount" placeholder="Enter Item Amount" maxlength="10" required="true" disabled="true"></tag:input>
				<tag:input label="Margin Amount" id="decMarginAmount" databind="decMarginAmount" placeholder="Enter Margin Amount" maxlength="10" required="true"></tag:input>
				<tag:input label="Scheme Discount Amount" id="decSchemeDiscountAmount" databind="decSchemeDiscountAmount" disabled="true" placeholder="Enter Scheme Discount Amount" maxlength="10" required="true"></tag:input>
				<tag:input label="Vendor Discount Amount 1" id="decVendorAmount1" databind="decVendorAmount1" placeholder="Enter Vendor Discount Amount 1" maxlength="10" required="true"></tag:input>
				<tag:input label="Vendor Discount Amount 2" id="decVendorDiscountAmount2" databind="decVendorDiscountAmount2" placeholder="Enter Vendor Discount Amount2" maxlength="10" required="true"></tag:input>
				<tag:select id="VatTax" label="Vat Tax"  observable="oataxdetails" bindvalue="chrVatTaxRecCode" value="chrRecCode" text="vhrName" caption="Choose Vat Tax" databindoption="event: {change : viewModel.getVatPercentage($element.value)}" required="true"></tag:select>
				<tag:input label="Vat Tax Amount" id="decVatTaxAmount" databind="decVatTaxAmount" placeholder="Enter Vat Tax Amount" maxlength="10" required="true"></tag:input>
			
			</div>
		</div>
	</tag:addeditmodal>	
	
	<!-- Add Edit Modal for purchase item delivery detail-->
	<%-- <tag:addeditmodal postfn="submittblpurchaseorderitemdeliverydetail" closefn="closeAddEdittblpurchaseorderitemdeliverydetail" modal="modal-tblpurchaseorderitemdeliverydetails" title="Purchase Order Item Delivery Details" withbind="selectedtblpurchaseorderitemdeliverydetail">
		<div class="row">
			<div id="col1" class="col-md-6">
 				<tag:select label="Store Location " id="selected_location" observable="oastores" bindvalue="chrStoreLocationCode" value="chrRecCode" text="vhrName" caption="Choose Store Location"  required="true" ></tag:select>
			</div>
			<div id="col1" class="col-md-6">
 				<tag:input label="Quantity" id="decQuantity" databind="decQuantity" placeholder="Enter Quantity" maxlength="10" onkeypress="return isNumber(event)" required="true"></tag:input>
 			</div>
		</div>
	</tag:addeditmodal>	 --%>
	<tag:deletemodal databindfn="deletetblpurchaseorder"></tag:deletemodal>
		
	<script src="tblpurchaseorders.js" type="text/javascript"></script>
<!-- 	<script> -->
// 	$(function () {
// 	    $('#dtp').datetimepicker();
// 	});
<!-- 	</script> -->
</body>
</html>