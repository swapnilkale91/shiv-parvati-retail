$(".main-menu").each(function() {
	if (this.id == "PurchaseOrders") {
		$(this).addClass("active");
	}
});
function otblpurchaseorder(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrAuthorizationUserOne = ko.observable(r.chrAuthorizationUserOne); 
		this.chrAuthorizationUserThree = ko.observable(r.chrAuthorizationUserThree); 
		this.chrAuthorizationUserTwo = ko.observable(r.chrAuthorizationUserTwo); 
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrPONo = ko.observable(r.chrPONo); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.chrVendorCode = ko.observable(r.chrVendorCode); 
		this.decDiscountTotal = ko.observable(r.decDiscountTotal); 
		this.decItemTotal = ko.observable(r.decItemTotal); 
		this.decMarginTotal = ko.observable(r.decMarginTotal); 
		this.decPOAmt = ko.observable(r.decPOAmt); 
		this.decSchemeTotal = ko.observable(r.decSchemeTotal); 
		this.decVatTaxTotal = ko.observable(r.decVatTaxTotal); 
		this.dtmAuthorizationDateOne = ko.observable(r.dtmAuthorizationDateOne); 
		this.dtmAuthorizationDateThree = ko.observable(r.dtmAuthorizationDateThree); 
		this.dtmAuthorizationDateTwo = ko.observable(r.dtmAuthorizationDateTwo); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmPODate = ko.observable(r.dtmPODate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intCreditPeriod = ko.observable(r.intCreditPeriod); 
		this.tinAuthorizationLevel = ko.observable(r.tinAuthorizationLevel); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCompanyName = ko.observable(r.vhrCompanyName); 
		this.tinStatus = ko.observable(r.tinStatus);
		
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrAuthorizationUserOne = ko.observable(null); 
		this.chrAuthorizationUserThree = ko.observable(null); 
		this.chrAuthorizationUserTwo = ko.observable(null); 
		this.chrCompanyCode = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrPONo = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.chrVendorCode = ko.observable(""); 
		this.decDiscountTotal = ko.observable(""); 
		this.decItemTotal = ko.observable(""); 
		this.decMarginTotal = ko.observable(""); 
		this.decPOAmt = ko.observable(""); 
		this.decSchemeTotal = ko.observable(""); 
		this.decVatTaxTotal = ko.observable(""); 
		this.dtmAuthorizationDateOne = ko.observable(new Date()); 
		this.dtmAuthorizationDateThree = ko.observable(new Date()); 
		this.dtmAuthorizationDateTwo = ko.observable(new Date()); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmPODate = ko.observable(new Date()); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intCreditPeriod = ko.observable(""); 
		this.tinAuthorizationLevel = ko.observable("0"); 
		this.tinIsDirty = ko.observable(0); 
		this.tinStatus = ko.observable(0);
		this.vhrCompanyName = ko.observable(""); 
		this.IUD = ko.observable(1);
	}
}

function otblpurchaseorderitemdetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrAmount = ko.observable(r.chrAmount); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrItemCode = ko.observable(r.chrItemCode); 
		this.chrItemDescription = ko.observable(r.chrItemDescription); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrUOMCode = ko.observable(r.chrUOMCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.chrVatTaxRecCode = ko.observable(r.chrVatTaxRecCode); 
		this.decItemAmount = ko.observable(r.decItemAmount); 
		this.decItemAmountAfterTax = ko.observable(r.decItemAmountAfterTax); 
		this.decItemDiscount = ko.observable(r.decItemDiscount); 
		this.decMarginAmount = ko.observable(r.decMarginAmount); 
		this.decMarginPercent = ko.observable(r.decMarginPercent); 
		this.decQuantity = ko.observable(r.decQuantity); 
		this.decRate = ko.observable(r.decRate); 
		this.decSchemeDiscountAmount = ko.observable(r.decSchemeDiscountAmount); 
		this.decSchemeDiscountPercent = ko.observable(r.decSchemeDiscountPercent); 
		this.decVatTaxAmount = ko.observable(r.decVatTaxAmount); 
		this.decVatTaxPercent = ko.observable(r.decVatTaxPercent); 
		this.decVendorAmount1 = ko.observable(r.decVendorAmount1); 
		this.decVendorDiscountAmount2 = ko.observable(r.decVendorDiscountAmount2); 
		this.decVendorDiscountPercent2 = ko.observable(r.decVendorDiscountPercent2); 
		this.decVendorDiscoutPercent1 = ko.observable(r.decVendorDiscoutPercent1); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinIsDirty = ko.observable(r.tinIsDirty);
		this.vhrItemName = ko.observable(r.vhrItemName);
		this.vhrUMOName = ko.observable(r.vhrUMOName);
		this.vhrVatTaxName = ko.observable(r.vhrVatTaxName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrAmount = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrItemCode = ko.observable(""); 
		this.chrItemDescription = ko.observable(""); 
		this.chrParentCode = ko.observable(viewModel.selectedtblpurchaseorder().chrRecCode()); 
		this.chrUOMCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.chrVatTaxRecCode = ko.observable(""); 
		this.decItemAmount = ko.observable(""); 
		this.decItemAmountAfterTax = ko.observable(""); 
		this.decItemDiscount = ko.observable(""); 
		this.decMarginAmount = ko.observable(""); 
		this.decMarginPercent = ko.observable(""); 
		this.decQuantity = ko.observable(""); 
		this.decRate = ko.observable(""); 
		this.decSchemeDiscountAmount = ko.observable(""); 
		this.decSchemeDiscountPercent = ko.observable(""); 
		this.decVatTaxAmount = ko.observable(""); 
		this.decVatTaxPercent = ko.observable(""); 
		this.decVendorAmount1 = ko.observable(""); 
		this.decVendorDiscountAmount2 = ko.observable(""); 
		this.decVendorDiscountPercent2 = ko.observable(""); 
		this.decVendorDiscoutPercent1 = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinIsDirty = ko.observable(0);
		this.vhrItemName = ko.observable("");
		this.vhrVatTaxName = ko.observable("");
		this.IUD = ko.observable(1);
	}
	
	this.decItemAmount = ko.computed(function() {
		return (this.decRate() * this.decQuantity());
	}, this);
	
	
	/* Discounts */
	
	this.decSchemeDiscountAmount = ko.computed(function() {
		return this.decItemAmount() * (this.decSchemeDiscountPercent()/100);
	 }, this);
	
	this.decMarginAmount = ko.computed(function() {
		return this.decItemAmount() * (this.decMarginPercent()/100);
	 }, this);
	
	this.decVendorDiscountAmount2 = ko.computed(function() {
		return this.decItemAmount() * (this.decVendorDiscountPercent2()/100);
	 }, this);
	
	// Vendor Discount Amount 1
	this.decVendorAmount1 = ko.computed(function() {
		return this.decItemAmount() * (this.decVendorDiscoutPercent1()/100);
	 }, this);
	
	/* item discount amount */
	this.decItemDiscount = ko.computed(function() {
		return this.decSchemeDiscountAmount() + this.decMarginAmount() + this.decVendorAmount1() + this.decVendorDiscountAmount2();
	 }, this);
	
	this.decVatTaxAmount = ko.computed(function() {
		return (this.decItemAmount() - this.decItemDiscount()) * (this.decVatTaxPercent()/100);
	 }, this);
	
	/* item amount after tax */	
	this.decItemAmountAfterTax = ko.computed(function() {
		return (this.decItemAmount() - this.decItemDiscount()) + this.decVatTaxAmount();
	 }, this);

}

function otblpurchaseorderitemdeliverydetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrStoreLocationCode = ko.observable(r.chrStoreLocationCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.decQuantity = ko.observable(r.decQuantity); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinIsDirty = ko.observable(r.tinIsDirty);
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrParentCode = ko.observable(viewModel.selectedtblpurchaseorderdetail().chrRecCode()); 
		this.chrStoreLocationCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.decQuantity = ko.observable(0); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinIsDirty = ko.observable(0);	
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}


function oCompany(r){
	this.chrCompanyCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStore(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStatus(r){
	this.intRecCode = r.intRecCode;
	this.vhrName = r.vhrName;
}

function oItemMaster(r){
	this.chrItemCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oUnitOfMeasure(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oTaxDetail(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}
function oVendor(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

$(document).on("click", ".fa-trash-o", function () {
    viewModel.deleteRefId = $(this).attr('id');
});

$(function() {
	ko.applyBindings(viewModel);
	//viewModel.getProducts();
	//viewModel.getBrands();
	viewModel.getStores();
	viewModel.getCompanies();
	viewModel.getItems();
	viewModel.getUOMs();
	viewModel.getVATTax();
	viewModel.getVendors();
	
	viewModel.getDefaultStatuses();
//	viewModel.getAsnStatuses();
	viewModel.pageCount();
	viewModel.firstPage();
	$('#modal-tblpurchaseorderitemdeliverydetails').hide();
});

var viewModel = {
	deleteRefId: '',
	oadefaultstatuses: ko.observableArray([]),
	oaasnstatuses: ko.observableArray([]),
	oacompanies: ko.observableArray([]),
	oastores: ko.observableArray([]),
	oatbldefaultlookups: ko.observableArray([]),
	oaitemmasters: ko.observableArray([]),
	oavendors: ko.observableArray([]),
	oauoms: ko.observableArray([]),
	oataxdetails: ko.observableArray([]),
	filtereditemdeliverydetails: ko.observableArray([]),
	
	// Array
	oatblpurchaseorders: ko.observableArray([]),
	oatblpurchaseorderdetails: ko.observableArray([]),
	oatblpurchaseorderitemdeliverydetails: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtblpurchaseorder: ko.observable(),
	revertedtblpurchaseorder: ko.observable(),
	selectedtblpurchaseorderdetail: ko.observable(),
	revertedtblpurchaseorderdetail: ko.observable(),
	selectedtblpurchaseorderitemdeliverydetail: ko.observable(),
	revertabletblpurchaseorderitemdeliverydetail: ko.observable(),
	// Search
	searchTexttblpurchaseorder: ko.observable(''),
	searchtblpurchaseorders: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	/*getDefaultStatuses: function() {
		FormFuncs.getAjax('../tblstatuses_Servlet', {Type : 'YesNo'}, true, function(output) {FormFuncs.pushRs(viewModel.oadefaultstatuses, output, oStatus);});
	},
	getAsnStatuses: function() {
		FormFuncs.getAjax('../tblstatuses_Servlet', {Type : 'ASN'}, true, function(output) {FormFuncs.pushRs(viewModel.oaasnstatuses, output, oStatus);});
	},*/
	getCompanies: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacompanies, output, oCompany);});
	},
	getStores: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oastores, output, oStore);});
	},
	getDefaultStatuses: function() {  //put the value in default lookup add/remove
		FormFuncs.getAjax('../tbldefaultlookups_Servlet', {Type : 'LookUpTypeCode',LookUpTypeCode : 2}, true, function(output) {FormFuncs.pushRs(viewModel.oatbldefaultlookups, output, oStatus);});
	},
	getItems: function() {
		FormFuncs.getAjax('../tblitemmasters_Servlet', {Type : 'all',}, true, function(output) {FormFuncs.pushRs(viewModel.oaitemmasters, output, oItemMaster);});
	},
	
	
	getVendors: function() {
		FormFuncs.getAjax('../tblvendormasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oavendors, output, oCompany);});
	},
	getUOMs: function() {
		FormFuncs.getAjax('../tbluommasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oauoms, output, oUnitOfMeasure);});
	},
	getVATTax: function() {
		FormFuncs.getAjax('../tbltaxdetails_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oataxdetails, output, oTaxDetail);});
	},
	getVendors: function() {
		FormFuncs.getAjax('../tblvendormasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oavendors, output, oVendor);});
	},
	
	getPurchaseOrderItemDetail: function(chrParentCode) {
		FormFuncs.getAjax('../tblpurchaseorderitemdetails_Servlet', {Type: 'purchaseorderitemdetail', chrParentCode: chrParentCode}, true, function(output) { FormFuncs.pushRs(viewModel.oatblpurchaseorderdetails, output, otblpurchaseorderitemdetail); });
	},
	
	getPurchaseOrderItemDeliveryDetail: function(chrParentCode) {
		FormFuncs.getAjax('../tblpurchaseorderitemdeliverydetails_Servlet', {Type: 'purchaseorderitemdeliverydetail', chrParentCode: chrParentCode}, true, function(output) { FormFuncs.pushRs(viewModel.oatblpurchaseorderitemdeliverydetails, output, otblpurchaseorderitemdeliverydetail); });
		
	//	viewModel.fetchitemdeliverydetails(viewModel.oatblpurchaseorderdetails()[0]);
	
	},
	
	getVatPercentage: function(chrVatTaxCode){
		
		var podate = this.selectedtblpurchaseorder().dtmPODate();
		
		if(chrVatTaxCode != ''){
			FormFuncs.getAjax('../tblinwarditemdetails_Servlet', {Type: 'gettaxpercentage', chrVatTaxCode: chrVatTaxCode, PODate: ko.toJSON(podate)}, true, function(output) {	
		});
			
	  }
	},
	
	
	
	
	// Add and Edit
	posttblpurchaseorder: function() {
		if(viewModel.oatblpurchaseorderdetails().length < 1) {
			NotifitMessage("Enter atleast one Item Details", "success");
		} else {
			/*viewModel.selectedtblpurchaseorder().dtmArrivalTime($("#dtmArrivalTime").val());*/
			var purchaseorder = ko.toJSON(this.selectedtblpurchaseorder());
			var purchaseoderitemdetails = ko.toJSON(this.oatblpurchaseorderdetails());
			var purchaseoderitemdeliverydetails = ko.toJSON(this.filtereditemdeliverydetails());
			/*var deletestockadjustmentdetails = ko.toJSON(this.oadeletelhstubedetails());*/
			FormFuncs.postAjax(
				'../tblpurchaseorders_Servlet',
				{Type: 'postDetails', 
				 PurchaseOrder: purchaseorder,
				 PurchaseOderItemDetails: purchaseoderitemdetails,
				 PurchaseOderItemDeliveryDetails: purchaseoderitemdeliverydetails},
				true,
				function(output) {
					if (output.Success == true) {
						if(viewModel.selectedtblpurchaseorder().IUD() == 1) {
							NotifitMessage("Purchase Order saved successfully.", "success");
							location.reload(); 
						}
						else {
							NotifitMessage("Purchase Order updated successfully.", "success");
							location.reload(); 
						}
						viewModel.closeAddEdittblpurchaseorder(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	submittblpurchaseorderdetail: function() {
		viewModel.selectedtblpurchaseorderdetail().vhrName($('#selected_item option:selected').text());
	//	viewModel.selectedtblpurchaseorderdetail().vhrAdjustmentType($('#selected_adjustmenttype option:selected').text());
		viewModel.closeAddEdittblpurchaseorderdetail(false); 
	},
	
	
	submittblpurchaseorderitemdeliverydetail: function() {
		viewModel.selectedtblpurchaseorderitemdeliverydetail().vhrName($('#selected_location option:selected').text());
//		viewModel.selectedtblpurchaseorderitemdeliverydetail().vhrQuantity($('#selected_adjustmenttype option:selected').text());
		viewModel.closeAddEdittblpurchaseorderitemdeliverydetail(false); 
	},
	
	addEdittblpurchaseorder: function(data) {
		if (data == null){
			this.selectedtblpurchaseorder(new otblpurchaseorder());
			viewModel.oatblpurchaseorderdetails.removeAll();
			viewModel.oatblpurchaseorderitemdeliverydetails.removeAll();
			viewModel.filtereditemdeliverydetails.removeAll();
			/*$('#itemdeliverydetail').hide();*/
		}
		else{
			this.selectedtblpurchaseorder(data);
			viewModel.getPurchaseOrderItemDetail(data.chrRecCode());
			viewModel.getPurchaseOrderItemDeliveryDetail(data.chrRecCode());
			
			
			/*$('#itemdeliverydetail').hide();*/
		}
		/*$(function () {
		    $('#dtmArrivalTime').datetimepicker({
		    	format: 'YYYY-MM-DD HH:mm:ss',
		    	minDate:new Date(),
		    });
		});*/
		$('#detail-container').show();
		$('#main-container').hide();
		FormFuncs.makeCopy(this.selectedtblpurchaseorder, this.revertedtblpurchaseorder, otblpurchaseorder);
	},
	addEdittblpurchaseorderdetail: function(data) {
		if (data == null){
			this.selectedtblpurchaseorderdetail(new otblpurchaseorderitemdetail());
			
		}
		else
		{
			/*var productcode = data.vhrNameOne();
			var brandcode = data.vhrNameTwo();
			var modelcode = data.chrModelCode();*/
			
			viewModel.selectedtblpurchaseorderdetail(data);
			viewModel.getPurchaseOrderItemDeliveryDetail(data.chrRecCode());
			
			/*viewModel.getBrands(productcode);
			viewModel.getModels(brandcode);
			
			
			viewModel.selectedtblpurchaseorderdetail().vhrNameOne(productcode);
			viewModel.selectedtblpurchaseorderdetail().vhrNameTwo(brandcode);
			viewModel.selectedtblpurchaseorderdetail().chrModelCode(modelcode);
			
			$('#selected_product').prop('disabled', true);
			$('#selected_brand').prop('disabled', true);
			$('#selected_model').prop('disabled', true);*/
		}
		
	//	FormFuncs.makeCopy(this.selectedtblpurchaseorderdetail, this.revertedtblpurchaseorderdetail, otblpurchaseorderitemdetail);
	},
	
	
	/*addEdittblpurchaseorderitemdeliverydetail: function(data) {
		if (data == null){
			this.selectedtblpurchaseorderitemdeliverydetail(new otblpurchaseorderitemdeliverydetail());
			
		}
		else
		{
			var productcode = data.vhrNameOne();
			var brandcode = data.vhrNameTwo();
			var modelcode = data.chrModelCode();
			
			viewModel.selectedtblpurchaseorderitemdeliverydetail(data);
			
			viewModel.getBrands(productcode);
			viewModel.getModels(brandcode);
			
			
			viewModel.selectedtblpurchaseorderdetail().vhrNameOne(productcode);
			viewModel.selectedtblpurchaseorderdetail().vhrNameTwo(brandcode);
			viewModel.selectedtblpurchaseorderdetail().chrModelCode(modelcode);
			
			$('#selected_product').prop('disabled', true);
			$('#selected_brand').prop('disabled', true);
			$('#selected_model').prop('disabled', true);
		}
		
		FormFuncs.makeCopy(this.selectedtblpurchaseorderdetail, this.revertedtblpurchaseorderdetail, otblpurchaseorderitemdetail);
	},*/
	
	addEdittblpurchaseorderitemdeliverydetail: function(data) {
		if(viewModel.oatblpurchaseorderdetails().length < 1)
		{
			NotifitMessage("Please enter atleast 1 Seam Identification Detail.", "success");
		}
		else{
		if (data == null){
			this.oatblpurchaseorderitemdeliverydetails.push(new otblpurchaseorderitemdeliverydetail());
			viewModel.filtereditemdeliverydetails.push(new otblpurchaseorderitemdeliverydetail());
		}
		else
		
			viewModel.selectedtblpurchaseorderitemdeliverydetail(data); 	
		
		
		FormFuncs.makeCopy(this.selectedtblpurchaseorderitemdeliverydetail, this.revertedtblpurchaseorderitemdeliverydetail, otblpurchaseorderitemdeliverydetail);
		}
	},
	
	
	closeAddEdittblpurchaseorder: function(revert) {
		$('#detail-container').hide();
		$('#main-container').show();
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblpurchaseorder, this.selectedtblpurchaseorder);
		}
		else {
			viewModel.pageCount();
			viewModel.firstPage();
		}
	},
	closeAddEdittblpurchaseorderdetail: function(revert) {
		$('#modal-tblpurchaseorderdetails').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblpurchaseorderdetail, this.selectedtblpurchaseorderdetail);
		}
		else {
			FormFuncs.submitRs(viewModel.oatblpurchaseorderdetails, viewModel.selectedtblpurchaseorderdetail, otblpurchaseorderitemdetail);
		}
	},
	closeAddEdittblpurchaseorderitemdeliverydetail: function(revert) {
		$('#modal-tblpurchaseorderitemdeliverydetails').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblpurchaseorderitemdeliverydetail, this.selectedtblpurchaseorderitemdeliverydetail);
		}
		else {
			FormFuncs.submitRs(viewModel.oatblpurchaseorderitemdeliverydetails, viewModel.selectedtblpurchaseorderitemdeliverydetail, otblpurchaseorderitemdeliverydetail);
		}
	},
	
	deletetblpurchaseorder: function() {
		var deleteref = this.deleteRefId;
		if (deleteref == 'tblpurchaseorderitemdetails-a') {
			/*FormFuncs.postAjax('../../SeamIdentificationDetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedseamidentificationdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Seam Identification Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedseamidentificationdetail().RecCode());
					FormFuncs.removeRecCodeRs(viewModel.oaseamidentificationdetails, reccodes);
				}
			});*/
			
			
			
			NotifitMessage("Purchase Order Detail deleted successfully.", "success"); 
			var reccodes = new Array (viewModel.selectedtblpurchaseorderdetail().chrRecCode());
			viewModel.oadeleteseamidentificationdetails.push(viewModel.selectedtblpurchaseorderdetail());
			
			FormFuncs.removeRecCodeRs(viewModel.oatblpurchaseorderdetails, reccodes);
			
			var reccodes1 = new Array (viewModel.selectedreportdetail().SeamIdentificationDetailsCode());
			viewModel.oadeletereportdetails.push(viewModel.selectedreportdetail());
			FormFuncs.removeRecCodeRs(viewModel.filteredreportdetails, reccodes1);
			
			
				
		} else if (deleteref == 'purchaseorderitemdeliverydetails-a') {
			/*FormFuncs.postAjax('../../RadiographyDetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedreportdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Seam Identification Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedreportdetail().RecCode());
					FormFuncs.removeRecCodeRs(viewModel.oareportdetails, reccodes);
				}
			});*/
			
			NotifitMessage("Purchase Order Item Delivery Detail deleted successfully.", "success"); 
			var reccodes = new Array (viewModel.selectedreportdetail().RecCode());
			viewModel.oatblpurchaseorderdetails.push(viewModel.selectedreportdetail());
			FormFuncs.removeRecCodeRs(viewModel.filteredreportdetails, reccodes);
			
		} else {
			var record = ko.toJSON(this.selectedradiography());
			FormFuncs.postAjax(
				'../../Radiographys_Servlet',
				{Type: 'deletedetails', 
				 Record: record},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("Record deleted successfully.", "success");
						viewModel.closeAddEditRadiography(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	
	/*// Delete
	deletetblpurchaseorder: function() {
		var deleteref = this.deleteRefId;
		if (deleteref == 'asndetails-a') {
			FormFuncs.postAjax('../tblpurchaseorderdetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedtblpurchaseorderdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Stock Adjustment Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedtblpurchaseorderdetail().chrRecCode());
					FormFuncs.removeRs(viewModel.oatblpurchaseorderdetails, reccodes);
				}
			});
		} else {
			var record = ko.toJSON(this.selectedtblpurchaseorder());
			FormFuncs.postAjax(
				'../tblpurchaseorders_Servlet',
				{Type: 'deletedetails', 
				 Record: record},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("ASN deleted successfully.", "success");
						viewModel.closeAddEdittblpurchaseorder(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},*/
	// Paging 
	counttblpurchaseorders: ko.observable(0),
	currentPagetblpurchaseorders: ko.observable(0),
	noOfPagestblpurchaseorders: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblpurchaseorders_Servlet',
			viewModel.counttblpurchaseorders,
			viewModel.noOfPagestblpurchaseorders,
			viewModel.pageSize,
			viewModel.searchTexttblpurchaseorder());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblpurchaseorders_Servlet',
			viewModel.counttblpurchaseorders,
			viewModel.noOfPagestblpurchaseorders,
			viewModel.pageSize);
	},
	pagetblpurchaseorderPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblpurchaseorders(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblpurchaseorders_Servlet',
			type,
			viewModel.oatblpurchaseorders,
			otblpurchaseorder,
			viewModel.currentPagetblpurchaseorders,
			viewModel.noOfPagestblpurchaseorders,
			viewModel.pageSize,
			viewModel.searchTexttblpurchaseorder());
	},
	pagetblpurchaseorderPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblpurchaseorders(-1); // force refresh
		
		FormFuncs.pagingPages('../tblpurchaseorders_Servlet',
			type, 
			viewModel.oatblpurchaseorders,
			otblpurchaseorder,
			viewModel.currentPagetblpurchaseorders,
			viewModel.noOfPagestblpurchaseorders,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblpurchaseorder() == '')
			viewModel.pagetblpurchaseorderPaging('f');
		else
			viewModel.pagetblpurchaseorderPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblpurchaseorder() == '')
			viewModel.pagetblpurchaseorderPaging('p');
		else
			viewModel.pagetblpurchaseorderPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblpurchaseorder() == '')
			viewModel.pagetblpurchaseorderPaging('n');
		else
			viewModel.pagetblpurchaseorderPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblpurchaseorder() == '')
			viewModel.pagetblpurchaseorderPaging('l');
		else
			viewModel.pagetblpurchaseorderPagingSearch('l');
	},
	fetchitemdeliverydetails: function(data) {
		viewModel.filtereditemdeliverydetails.removeAll();
		
		if (data != null) {
			viewModel.selectedtblpurchaseorderdetail(data);
			
			// filter the item delivery detail as per item details
			
			ko.utils.arrayForEach(viewModel.oatblpurchaseorderitemdeliverydetails(), function(item) {
				if (item.chrParentCode() == data.chrRecCode()) {
					viewModel.filtereditemdeliverydetails.push(item);
				}
			});
			
		}
		if(viewModel.oatblpurchaseorderdetails().length > 0){
		var title = 'Add Purchase Order Item Details For [' + viewModel.selectedtblpurchaseorderdetail().vhrItemName() + ']';
		$('#page-tblpurchaseorderitemdeliverydetailsLabel').text(title);
		}
	
	},
};
