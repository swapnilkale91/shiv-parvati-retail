<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="../ScriptCss.jsp"%>
<title><%=PROJECT_NAME%>: Racks</title>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../Common/Header.jsp"%>
		<div id="page-wrapper" class="container-fluid">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="main-box clearfix">
								<header class="main-box-header clearfix">
									<h2 class="pull-left">
										<b>Store Item Rack</b>
									</h2>
									<div class="filter-block pull-right">
										<div class="col-md-10 pull-left">
											<tag:search databindfn="searchtblstoreitemracks"
												databindvalue="searchTexttblstoreitemrack"
												databindresetfn="refreshtblstoreitemracks"></tag:search>
										</div>
										<div class="col-md-2 pull-left">
											<tag:addbutton modalid="modal-tblstoreitemrack"
												databind="addEdittblstoreitemrack"
												title="Add Store Item Rack"></tag:addbutton>
										</div>
									</div>
								</header>
								<div class="main-box-body clearfix">
									<div class="table-responsive clearfix">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th><b>Name</b></th>
													<th><b>Code</b></th>
													<th class="actioncolumn">Action</th>
												</tr>
											</thead>
											<tbody data-bind="foreach: viewModel.oatblstoreitemracks">
												<tr>
													<td><span data-bind="text: vhrName"></span></td>
													<td><span data-bind="text: vhrCode"></span></td>
													<td><tag:editbutton modalid="modal-tblstoreitemrack"
															databind="addEdittblstoreitemrack" title="Edit Store Rack Item"></tag:editbutton>
														<tag:deletebutton modalid="modal-delete"
															databind="addEdittblstoreitemrack" title="Delete Store Rack Item"></tag:deletebutton></td>
												</tr>
											</tbody>
										</table>
									</div>
									<tag:pagination currentpage="currentPagetblstoreitemracks"
										noofpages="noOfPagestblstoreitemracks"></tag:pagination>
								</div>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="posttblstoreitemrack" closefn="closeAddEdit"
		modal="modal-tblstoreitemrack" title="Store Item Rack"
		withbind="selectedtblstoreitemrack">
		<div class="row">
			<div class="col-md-12">
				<tag:select label="Store" observable="oatblstores"
					bindvalue="chrParentCode" value="chrParentCode" text="vhrName"
					caption="Select Store" required="true"></tag:select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<tag:select label="Store Location Category"
					observable="oatbldefaultlookups"
					bindvalue="intStoreLocationCategory" value="intRecCode"
					text="vhrName" caption="Select Store Location Category"
					required="true"></tag:select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<tag:input label="Name" id="vhrName" databind="vhrName"
					required="true" placeholder="Enter Name" maxlength="100"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<tag:input label="Code" id="vhrCode" databind="vhrCode"
					required="true" placeholder="Enter Code" maxlength="20"></tag:input>
			</div>
		</div>
	</tag:addeditmodal>
	<tag:deletemodal databindfn="deletetblstoreitemrack"></tag:deletemodal>

	<script src="tblstoreitemracks.js?<%=COMMON_SCRIPT_VERSION%>" type="text/javascript"></script>

</body>
</html>