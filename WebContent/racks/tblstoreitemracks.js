$(".main-menu").each(function() {
	if (this.id == "Racks") {
		$(this).addClass("active");
	}
});
function otblstoreitemrack(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intStoreLocationCategory = ko.observable(r.intStoreLocationCategory); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrParentCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intStoreLocationCategory = ko.observable(0); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrCode = ko.observable(""); 
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}


function oStatus(r){
	this.intRecCode = r.intRecCode;
	this.vhrName = r.vhrName;
}

function oStores(r){
	this.chrParentCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}


$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.getDefaultStatuses();
	viewModel.getStores();
});

var viewModel = {
	// Array
	oatblstoreitemracks: ko.observableArray([]),
	oatbldefaultlookups: ko.observableArray([]),
	oatblstores: ko.observableArray([]),
	// Selected and Revert 
	selectedtblstoreitemrack: ko.observable(),
	revertedtblstoreitemrack: ko.observable(),
	// Search
	searchTexttblstoreitemrack: ko.observable(''),
	searchtblstoreitemracks: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtblstoreitemracks: function() {
		viewModel.searchTexttblstoreitemrack(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	getDefaultStatuses: function() {
		FormFuncs.getAjax('../tbldefaultlookups_Servlet', {Type : 'LookupTypeCode',LookupTypeCode : 2}, true, function(output) {FormFuncs.pushRs(viewModel.oatbldefaultlookups, output, oStatus);});
	},
	
	getStores: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oatblstores, output, oStores);});
	},
	
	// Add and Edit
	posttblstoreitemrack: function() {
		var record = ko.toJSON(this.selectedtblstoreitemrack());
		FormFuncs.postAjax(
			'../tblstoreitemracks_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblstoreitemrack().IUD() == 1) {
						NotifitMessage("Store Item Rack saved successfully.", "success");
					}
					else {
						NotifitMessage("Store Item Rack updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblstoreitemrack: function(data) {
		if (data == null)
			this.selectedtblstoreitemrack(new otblstoreitemrack());
		else
			this.selectedtblstoreitemrack(data);
		
		FormFuncs.makeCopy(this.selectedtblstoreitemrack, this.revertedtblstoreitemrack, otblstoreitemrack);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblstoreitemrack').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblstoreitemrack, this.selectedtblstoreitemrack);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblstoreitemrackPaging(null);
		}
	},
	// Delete
	deletetblstoreitemrack: function() {
		var record = ko.toJSON(this.selectedtblstoreitemrack());
		FormFuncs.postAjax(
			'../tblstoreitemracks_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Store Item Rack deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttblstoreitemracks: ko.observable(0),
	currentPagetblstoreitemracks: ko.observable(0),
	noOfPagestblstoreitemracks: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblstoreitemracks_Servlet',
			viewModel.counttblstoreitemracks,
			viewModel.noOfPagestblstoreitemracks,
			viewModel.pageSize,
			viewModel.searchTexttblstoreitemrack());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblstoreitemracks_Servlet',
			viewModel.counttblstoreitemracks,
			viewModel.noOfPagestblstoreitemracks,
			viewModel.pageSize);
	},
	pagetblstoreitemrackPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblstoreitemracks(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblstoreitemracks_Servlet',
			type,
			viewModel.oatblstoreitemracks,
			otblstoreitemrack,
			viewModel.currentPagetblstoreitemracks,
			viewModel.noOfPagestblstoreitemracks,
			viewModel.pageSize,
			viewModel.searchTexttblstoreitemrack());
	},
	pagetblstoreitemrackPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblstoreitemracks(-1); // force refresh
		
		FormFuncs.pagingPages('../tblstoreitemracks_Servlet',
			type, 
			viewModel.oatblstoreitemracks,
			otblstoreitemrack,
			viewModel.currentPagetblstoreitemracks,
			viewModel.noOfPagestblstoreitemracks,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblstoreitemrack() == '')
			viewModel.pagetblstoreitemrackPaging('f');
		else
			viewModel.pagetblstoreitemrackPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblstoreitemrack() == '')
			viewModel.pagetblstoreitemrackPaging('p');
		else
			viewModel.pagetblstoreitemrackPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblstoreitemrack() == '')
			viewModel.pagetblstoreitemrackPaging('n');
		else
			viewModel.pagetblstoreitemrackPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblstoreitemrack() == '')
			viewModel.pagetblstoreitemrackPaging('l');
		else
			viewModel.pagetblstoreitemrackPagingSearch('l');
	}
};
