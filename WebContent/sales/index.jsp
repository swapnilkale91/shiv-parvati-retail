<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="../ScriptCss.jsp"%>
<title><%=PROJECT_NAME%>: Sales</title>
<style type="text/css">
html, body, #theme-wrapper, #page-wrapper {
	height: 100%;
}

body {
	position: fixed;
	width: 100%;
}

#content-wrapper {
	margin-left: 0px;
	padding: 0px;
}

.fixed-header #page-wrapper {
	padding: 0px;
}

.theme-whbl .pace .pace-progress {
	background-color: #15b1b1;
}

.pace .pace-progress:after {
	color: #fff;
}

#headercontainer {
	float: left;
	width: 100%;
	height: 13%;
}

#detailscontainer {
	float: left;
	height: 37%;
	width: 100%;
	overflow-y: scroll;
}

#amountcontainer {
	float: left;
	height: 15%;
	width: 100%;
}

#paymentcontainer {
	float: left;
	height: 30%;
	width: 100%;
	padding-top: 30px;
}

.main-box {
	margin-bottom: 0px;
	box-shadow: none;
	border-radius: 0px;
}

#footer-bar {
	border-top: 1px solid #15b1b1;
}
</style>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<input id="loggedinRecCode" type="hidden" value="<%=session.getAttribute("chrRecCode")%>" />
	<input id="loggedinCompanyCode" type="hidden" value="<%=session.getAttribute("chrCompanyCode")%>" />
	<input id="loggedinStoreCode" type="hidden" value="<%=session.getAttribute("chrStoreCode")%>" />
	<div id="theme-wrapper">

		<div id="page-wrapper" class="container-fluid">


			<div id="content-wrapper">
				<!-- -------Header Container Start------- -->
				<div class="main-box clearfix" id="headercontainer">
					<header class="main-box-header clearfix">
						<h2 class="pull-left">
							<b>Sales</b>

						</h2>


					</header>
					<div class="main-box-body clearfix"  data-bind="with: selectedtblsale">
						<div class="row">
							<div class="col-md-4 col-xs-4">
								<tag:input label="Bill No" id="vhrBillNo" databind="vhrBillNo"
									required="true" placeholder="Enter Bill No" maxlength="100"></tag:input>
							</div>
							 <div class="col-md-4 col-xs-4">
								<%-- <tag:input label="Bill Date" id="dtmBillDate"
									databind="dtmBillDate" required="true"
									placeholder="Enter Bill Date" maxlength="100"></tag:input> --%>
								<tag:datepicker label="Bill Date" id="dtmBillDate" 
									databind="dtmBillDate" placeholder="Select Bill Date" required="true"></tag:datepicker>
							</div>
							<div class="col-md-4 col-xs-4">
								<tag:select label="Customer" observable="oacustomers"
									bindvalue="chrCustomerCode" value="chrRecCode" text="vhrName"
									caption="Select Customer" required="true"></tag:select>
							</div> 
						</div>
					</div>
				</div>
				<!-- -------Header Container End------- -->
				<!-- -------Details Container Start------- -->
				<div class="main-box clearfix" id="detailscontainer">
					<header class="main-box-header clearfix">
						<h2 class="pull-left">
							<b>Item Details</b>
						</h2>
						<div class="filter-block pull-right">
							<div class="col-md-10 col-xs-10 pull-left">
								<tag:search databindfn="searchtblsalesitemdetails"
									databindvalue="searchTexttblsalesitemdetail"
									databindresetfn="refreshtblsalesitemdetails"></tag:search>
							</div>
							<div class="col-md-2 col-xs-2 pull-left">
								<tag:addbutton modalid="modal-tblsalesitemdetail"
									databind="addEdittblsalesitemdetail" title="Add Item Details"></tag:addbutton>
							</div>
						</div>
					</header>
					<div class="main-box-body clearfix">
						<div class="table-responsive clearfix">
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<th><b>Item Name</b></th>
										<th><b>Bar Code No</b></th>
										<th><b>Quantity</b></th>
										<th><b>MRP</b></th>
										<th><b>Discount</b></th>
										<th><b>Total Cost</b></th>
										<th class="actioncolumn">Action</th>
									</tr>
								</thead>
								<tbody data-bind="foreach: viewModel.oatblsalesitemdetails">
									<tr>
										<td><span data-bind="text: vhrName"></span></td>
										<td><span data-bind="text: vhrOurGeneratedBarCode"></span></td>
										<td><span data-bind="text: decQuantity"></span></td>
										<td><span data-bind="text: decMRP"></span></td>
										<td><span data-bind="text: decCustomerRate"></span></td>
										<td><span data-bind="text: de	cItemCost"></span></td>
										<td><tag:editbutton modalid="modal-tblsalesitemdetail"
												databind="addEdittblsalesitemdetail" title="Edit Employee"></tag:editbutton>
											<tag:deletebutton modalid="modal-delete"
												databind="addEdittblsalesitemdetail" title="Delete Employee"></tag:deletebutton></td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div>
				<!-- -------Details Container End------- -->
				<!-- -------Amount Container Start------- -->
				<div class="main-box clearfix" id="amountcontainer">
					<header class="main-box-header clearfix">
						<h2 class="pull-left">
							<b>Amount Details</b>
						</h2>

					</header>
					<div class="main-box-body clearfix" data-bind="with: selectedtblsale">
						<div class="row">
							<div class="col-md-3 col-xs-3">
								<div class="social-box-wrapper">
									<div class="social-box col-md-12 col-sm-12 bgsocialbox">
										<i class="fa fa-cart-plus hidden-xs"></i>
										<div class="clearfix">
											<span class="social-name">Item Total</span> <span
												class="social-count">&#x20B9;&nbsp;<span
												data-bind="text: decItemTotal">1000</span>&nbsp;/-
											</span>
										</div>

									</div>

								</div>
							</div>
							<div class="col-md-3 col-xs-3">
								<div class="social-box-wrapper">
									<div class="social-box col-md-12 col-sm-12 bgsocialbox">
										<i class="fa fa-cart-arrow-down hidden-xs"></i>
										<div class="clearfix">
											<span class="social-name">Discount Total</span> <span
												class="social-count">&#x20B9;&nbsp;<span
												data-bind="text: decDiscountTotal">100</span>&nbsp;/-
											</span>
										</div>

									</div>

								</div>
							</div>
							<div class="col-md-3 col-xs-3">
								<div class="social-box-wrapper">
									<div class="social-box col-md-12 col-sm-12 bgsocialbox">
										<i class="fa fa-file-text-o hidden-xs"></i>
										<div class="clearfix">
											<span class="social-name">VAT Tax Total</span> <span
												class="social-count">&#x20B9;&nbsp;<span
												data-bind="text: decVATTaxTotal">150</span>&nbsp;/-
											</span>
										</div>

									</div>

								</div>
							</div>
							<div class="col-md-3 col-xs-3">
								<div class="social-box-wrapper">
									<div class="social-box col-md-12 col-sm-12 bgsocialbox">
										<i class="fa fa-money hidden-xs"></i>
										<div class="clearfix">
											<span class="social-name">Total Amount</span> <span
												class="social-count">&#x20B9;&nbsp;<span
												data-bind="text: decTotalAmount">1250</span>&nbsp;/-
											</span>
										</div>

									</div>

								</div>
							</div>
						</div>

					</div>
				</div>
				<!-- -------Amount Container End------- -->
				<!-- -------Payment Container Start------- -->
				<div class="main-box clearfix" id="paymentcontainer">

					<div class="main-box-body clearfix" data-bind="with: selectedtblsale">
						<div class="row text-center">


							<div class="col-md-4 col-xs-4">
								<div class="main-box small-graph-box green-bg"
									data-bind="click: function() { viewModel.posttblsale() }">
									<span class="value">Save</span>

								</div>
							</div>
							<div class="col-md-4 col-xs-4">
								<div class="main-box small-graph-box yellow-bg"	>
									<span class="value">Print</span>

								</div>
							</div>
							<div class="col-md-4 col-xs-4">
								<div class="main-box small-graph-box emerald-bg" onclick=""
									data-bind="click: function() { viewModel.openmodalpayment() }">
									<!-- <span class="value">Pay</span> -->
									<span>Pay</span>

								</div>
							</div>



						</div>
					</div>
				</div>
				<!-- -------Payment Container End------- -->
				<%@include file="../Common/Footer.jsp"%>
			</div>
		</div>
	</div>

	<!-- Item Details Modal -->
	<tag:addeditmodal postfn="submittblsalesitemdetail"
		closefn="closeAddEdittblsalesitemdetail" modal="modal-tblsalesitemdetail"
		title="Item Details" withbind="selectedtblsalesitemdetail">
		<div class="row">
			<div class="col-md-12">
				<tag:select id="selected_item" label="Item" observable="oaitemmasters"
					bindvalue="chrItemCode" value="chrRecCode" text="vhrName"
					caption="Select Item" required="true"></tag:select>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<tag:input label="Short Name" id="vhrShortName"
					databind="vhrShortName" required="true"
					placeholder="Enter Short Name" maxlength="20"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Description" id="vhrDescription"
					databind="vhrDescription" required="true"
					placeholder="Enter Description" maxlength="100"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="Generated Bar Code" id="vhrOurGeneratedBarCode"
					databind="vhrOurGeneratedBarCode" required="true"
					placeholder="Enter Generated Bar Code" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Quantity" id="decQuantity" databind="decQuantity"
					required="true" placeholder="Enter Quantity" maxlength="20"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:datepicker label="Manufacture Date" id="dtmManufactureDate"
					databind="dtmManufactureDate" placeholder="Select Manufacture Date"></tag:datepicker>
			</div>
			<div class="col-md-6">
				<tag:datepicker label="Expiry Date" id="dtmExpiryDate"
					databind="dtmExpiryDate" placeholder="Select Expiry Date"></tag:datepicker>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="Batch No" id="vhrBatchNo" databind="vhrBatchNo"
					required="true" placeholder="Enter Batch No" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="MRP" id="decMRP" databind="decMRP" required="true"
					placeholder="Enter MRP" maxlength="20"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="Customer Discount" id="decCustomerDiscount"
					databind="decCustomerDiscount" required="true"
					placeholder="Enter Customer Discount" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Customer Rate" id="decCustomerRate"
					databind="decCustomerRate" required="true"
					placeholder="Enter Customer Rate" maxlength="20"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="VAT Tax Percentage" id="decVatTaxPercentage"
					databind="decVatTaxPercentage" required="true"
					placeholder="Enter VAT Tax Percentage" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:select label="Tax Details" observable="oataxdetails"
					bindvalue="chrVatTaxRecCode" value="chrRecCode" text="vhrName"
					caption="Select Tax Details" required="true"></tag:select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="VAT Tax Amount" id="decVatTaxAmount"
					databind="decVatTaxAmount" required="true"
					placeholder="Enter VAT Tax Amount" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Item Cost" id="decItemCost" databind="decItemCost"
					required="true" placeholder="Enter Item Cost" maxlength="20"></tag:input>
			</div>
		</div>
	</tag:addeditmodal>
	<!-- Item Details Modal -->
	<!-- Payment Modal -->
	<tag:addeditmodal postfn="submittblsalespaymentdetail"
		closefn="closeAddEdittblsalespaymentdetail" modal="modal-tblsalespaymentdetail" 
		title="Payment Details" withbind="selectedtblsalespaymentdetail">
		<div class="row">
			<div class="col-md-6">
				<tag:select label="Payment Mode" observable="oatbldefaultlookups"
					bindvalue="intPaymentModeCode" value="intRecCode" text="vhrName"
					caption="Select Payment Mode" required="true"></tag:select>

			</div>
			<div class="col-md-6">
				<tag:input label="Amount" id="decAmount" databind="decAmount"
					required="true" placeholder="Enter Amount" maxlength="100"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="Cash Received" id="decCashReceived"
					databind="decCashReceived" required="true"
					placeholder="Enter Cash Received" maxlength="20"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Balance" id="decBalance" databind="decBalance"
					required="true" placeholder="Enter Balance" maxlength="100"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<%-- <tag:input label="Coupon Denomination" id="dtmCouponDenomination"
					databind="dtmCouponDenomination"
					placeholder="Enter Coupon Denomination" maxlength="20"></tag:input> --%>
			
			<tag:datepicker label="Coupon Denomination" id="dtmCouponDenomination" 
				databind="dtmCouponDenomination" placeholder="SelectCoupon Denomination" required="true"></tag:datepicker>
			
			</div>
			<div class="col-md-6">
				<tag:input label="Card Details" id="vhrCardDetails"
					databind="vhrCardDetails" placeholder="Enter Card Details"
					maxlength="100"></tag:input>
			</div>
		</div>
	</tag:addeditmodal>
	<!-- Payment Modal -->
	<tag:deletemodal databindfn="deletetblcompany"></tag:deletemodal>

	<script src="tblsales.js?<%=COMMON_SCRIPT_VERSION%>"
		type="text/javascript"></script>
	<!-- <script type="text/javascript">
		function openmodalpayment() {
			$('#modal-tblsalespaymentdetail').modal('show');
		}
	</script> -->

</body>
</html>