$(".main-menu").each(function() {
	if (this.id == "Sales") {
		$(this).addClass("active");
	}
});

function tblsale(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrAuthorizationUserOne = ko.observable(r.chrAuthorizationUserOne); 
		this.chrAuthorizationUserThree = ko.observable(r.chrAuthorizationUserThree); 
		this.chrAuthorizationUserTwo = ko.observable(r.chrAuthorizationUserTwo); 
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrCustomerCode = ko.observable(r.chrCustomerCode); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrOperatorCode = ko.observable(r.chrOperatorCode); 
		this.chrStoreCode = ko.observable(r.chrStoreCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.decDiscountTotal = ko.observable(r.decDiscountTotal); 
		this.decItemTotal = ko.observable(r.decItemTotal); 
		this.decTotalAmount = ko.observable(r.decTotalAmount); 
		this.decVATTaxTotal = ko.observable(r.decVATTaxTotal); 
		this.dtmAuthorizationDateOne = ko.observable(r.dtmAuthorizationDateOne); 
		this.dtmAuthorizationDateThree = ko.observable(r.dtmAuthorizationDateThree); 
		this.dtmAuthorizationDateTwo = ko.observable(r.dtmAuthorizationDateTwo); 
		this.dtmBillDate = ko.observable(r.dtmBillDate); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intAuthorizationLevel = ko.observable(r.intAuthorizationLevel); 
		this.intStatus = ko.observable(r.intStatus); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrBillNo = ko.observable(r.vhrBillNo);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrAuthorizationUserOne = ko.observable(null); 
		this.chrAuthorizationUserThree = ko.observable(null); 
		this.chrAuthorizationUserTwo = ko.observable(null); 
		this.chrCompanyCode = ko.observable(FormFuncs.loggedInCompanyCode()); 
		this.chrCustomerCode = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrOperatorCode = ko.observable(FormFuncs.loggedInRecCode()); 
		this.chrStoreCode = ko.observable(FormFuncs.loggedinStoreCode()); 
		this.chrUpdateUser = ko.observable(""); 
		this.decDiscountTotal = ko.observable(0); 
		this.decItemTotal = ko.observable(0); 
		this.decTotalAmount = ko.observable(0); 
		this.decVATTaxTotal = ko.observable(0); 
		this.dtmAuthorizationDateOne = ko.observable(new Date()); 
		this.dtmAuthorizationDateThree = ko.observable(new Date()); 
		this.dtmAuthorizationDateTwo = ko.observable(new Date()); 
		this.dtmBillDate = ko.observable(new Date()); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intAuthorizationLevel = ko.observable("0"); 
		this.intStatus = ko.observable(0); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrBillNo = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblsalesitemdetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrItemCode = ko.observable(r.chrItemCode); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.chrVatTaxRecCode = ko.observable(r.chrVatTaxRecCode); 
		this.decCustomerDiscount = ko.observable(r.decCustomerDiscount); 
		this.decCustomerRate = ko.observable(r.decCustomerRate); 
		this.decItemCost = ko.observable(r.decItemCost); 
		this.decMRP = ko.observable(r.decMRP); 
		this.decQuantity = ko.observable(r.decQuantity); 
		this.decVatTaxAmount = ko.observable(r.decVatTaxAmount); 
		this.decVatTaxPercentage = ko.observable(r.decVatTaxPercentage); 
		this.dtmExpiryDate = ko.observable(r.dtmExpiryDate); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmManufactureDate = ko.observable(r.dtmManufactureDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intStatus = ko.observable(r.intStatus); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrBatchNo = ko.observable(r.vhrBatchNo); 
		this.vhrDescription = ko.observable(r.vhrDescription); 
		this.vhrOurGeneratedBarCode = ko.observable(r.vhrOurGeneratedBarCode); 
		this.vhrShortName = ko.observable(r.vhrShortName);
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrItemCode = ko.observable(""); 
		this.chrParentCode = ko.observable(viewModel.selectedtblsale().chrRecCode()); 
		this.chrUpdateUser = ko.observable(""); 
		this.chrVatTaxRecCode = ko.observable(""); 
		this.decCustomerDiscount = ko.observable(0); 
		this.decCustomerRate = ko.observable(0); 
		this.decItemCost = ko.observable(0); 
		this.decMRP = ko.observable(0); 
		this.decQuantity = ko.observable(0); 
		this.decVatTaxAmount = ko.observable(0); 
		this.decVatTaxPercentage = ko.observable(0); 
		this.dtmExpiryDate = ko.observable(new Date()); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmManufactureDate = ko.observable(new Date()); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intStatus = ko.observable(0); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrBatchNo = ko.observable(""); 
		this.vhrDescription = ko.observable(""); 
		this.vhrOurGeneratedBarCode = ko.observable(""); 
		this.vhrShortName = ko.observable("");
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tblsalespaymentdetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.intPaymentModeCode = ko.observable(r.intPaymentModeCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.decAmount = ko.observable(r.decAmount); 
		this.decBalance = ko.observable(r.decBalance); 
		this.decCashReceived = ko.observable(r.decCashReceived); 
		this.dtmCouponDenomination = ko.observable(r.dtmCouponDenomination); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCardDetails = ko.observable(r.vhrCardDetails);
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrParentCode = ko.observable(viewModel.selectedtblsale().chrRecCode()); 
		this.intPaymentModeCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.decAmount = ko.observable(0); 
		this.decBalance = ko.observable(0); 
		this.decCashReceived = ko.observable(""); 
		this.dtmCouponDenomination = ko.observable(new Date()); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrCardDetails = ko.observable("");
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);	
	}
}



function oCustomer(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStore(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStatus(r){
	this.intRecCode = r.intRecCode;
	this.vhrName = r.vhrName;
}

function oItemMaster(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oUnitOfMeasure(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oTaxDetail(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}
function oVendor(r){
	this.chrRecCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

$(document).on("click", ".fa-trash-o", function () {
    viewModel.deleteRefId = $(this).attr('id');
});

$(function() {
	ko.applyBindings(viewModel);
	//viewModel.getProducts();
	//viewModel.getBrands();
	viewModel.getStores();
	viewModel.getCustomers();
	viewModel.getItems();
	viewModel.getUOMs();
	viewModel.getVATTax();
	viewModel.getVendors();
	
	viewModel.getDefaultStatuses();
//	viewModel.getAsnStatuses();
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.addEdittblsale(null);
	
	
});

var viewModel = {
	deleteRefId: '',
	oadefaultstatuses: ko.observableArray([]),
	oaasnstatuses: ko.observableArray([]),
	oacustomers: ko.observableArray([]),
	oastores: ko.observableArray([]),
	oatbldefaultlookups: ko.observableArray([]),
	oaitemmasters: ko.observableArray([]),
	oavendors: ko.observableArray([]),
	oauoms: ko.observableArray([]),
	oataxdetails: ko.observableArray([]),
	oavendors: ko.observableArray([]),
	filtereditemdeliverydetails: ko.observableArray([]),
	
	// Array
	oatblsales: ko.observableArray([]),
	oatblsalesitemdetails: ko.observableArray([]),
	oatblsalespaymentdetails: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtblsale: ko.observable(),
	revertedtblsale: ko.observable(),
	selectedtblsalesitemdetail: ko.observable(),
	revertedtblsalesitemdetail: ko.observable(),
	selectedtblsalespaymentdetail: ko.observable(),
	revertabletblsalespaymentdetail: ko.observable(),
	// Search
	searchTexttblsale: ko.observable(''),
	searchtblsales: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	/*getDefaultStatuses: function() {
		FormFuncs.getAjax('../tblstatuses_Servlet', {Type : 'YesNo'}, true, function(output) {FormFuncs.pushRs(viewModel.oadefaultstatuses, output, oStatus);});
	},
	getAsnStatuses: function() {
		FormFuncs.getAjax('../tblstatuses_Servlet', {Type : 'ASN'}, true, function(output) {FormFuncs.pushRs(viewModel.oaasnstatuses, output, oStatus);});
	},*/
	getCustomers: function() {
		FormFuncs.getAjax('../tblcustomermasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacustomers, output, oCustomer);});
	},
	getStores: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oastores, output, oStore);});
	},
	getDefaultStatuses: function() {
		FormFuncs.getAjax('../tbldefaultlookups_Servlet', {Type : 'LookupTypeCode',LookupTypeCode : 4}, true, function(output) {FormFuncs.pushRs(viewModel.oatbldefaultlookups, output, oStatus);});
	},
	getItems: function() {
		FormFuncs.getAjax('../tblitemmasters_Servlet', {Type : 'all',}, true, function(output) {FormFuncs.pushRs(viewModel.oaitemmasters, output, oItemMaster);});
	},
	
	
	getVendors: function() {
		FormFuncs.getAjax('../tblvendormasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oavendors, output, oCompany);});
	},
	getUOMs: function() {
		FormFuncs.getAjax('../tbluommasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oauoms, output, oUnitOfMeasure);});
	},
	getVATTax: function() {
		FormFuncs.getAjax('../tbltaxdetails_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oataxdetails, output, oTaxDetail);});
	},
	getVendors: function() {
		FormFuncs.getAjax('../tblvendormasters_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oavendors, output, oVendor);});
	},
	
	getPurchaseOrderItemDetail: function(chrParentCode) {
		FormFuncs.getAjax('../tblsaleitemdetails_Servlet', {Type: 'purchaseorderitemdetail', chrParentCode: chrParentCode}, true, function(output) { FormFuncs.pushRs(viewModel.oatblsalesitemdetails, output, tblsaleitemdetail); });
	},
	
	getPurchaseOrderItemDeliveryDetail: function(chrParentCode) {
		FormFuncs.getAjax('../tblsalespaymentdetails_Servlet', {Type: 'purchaseorderitemdeliverydetail', chrParentCode: chrParentCode}, true, function(output) { FormFuncs.pushRs(viewModel.oatblsalespaymentdetails, output, tblsalespaymentdetail); });
		
	//	viewModel.fetchitemdeliverydetails(viewModel.oatblsalesitemdetails()[0]);
	
	},
	
	
	
	
	// Add and Edit
	posttblsale: function() {
		if(viewModel.oatblsalesitemdetails().length < 1) {
			NotifitMessage("Enter atleast one Item Details", "success");
		} else {
			/*viewModel.selectedtblsale().dtmArrivalTime($("#dtmArrivalTime").val());*/
			var sales = ko.toJSON(this.selectedtblsale());
			var saleitemdetails = ko.toJSON(this.oatblsalesitemdetails());
			var salepaymentdetails = ko.toJSON(this.selectedtblsalespaymentdetail());
			/*var deletestockadjustmentdetails = ko.toJSON(this.oadeletelhstubedetails());*/
			FormFuncs.postAjax(
				'../tblsales_Servlet',
				{Type: 'postDetails', 
					Sales: sales,
					SaleItemDetails: saleitemdetails,
					SalePaymentDetails: salepaymentdetails},
				true,
				function(output) {
					if (output.Success == true) {
						if(viewModel.selectedtblsale().IUD() == 1) {
							NotifitMessage("Sales saved successfully.", "success");
							
						}
						else {
							NotifitMessage("Sales updated successfully.", "success");
							 
						}
						viewModel.closeAddEdittblsale(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	submittblsalesitemdetail: function() {
		viewModel.selectedtblsalesitemdetail().vhrName($('#selected_item option:selected').text());
	//	viewModel.selectedtblsalesitemdetail().vhrAdjustmentType($('#selected_adjustmenttype option:selected').text());
		viewModel.closeAddEdittblsalesitemdetail(false); 
	},
	
	
	submittblsalespaymentdetail: function() {
		viewModel.selectedtblsalespaymentdetail().vhrName($('#selected_mode option:selected').text());
//		viewModel.selectedtblsalespaymentdetail().vhrQuantity($('#selected_adjustmenttype option:selected').text());
		viewModel.closeAddEdittblsalespaymentdetail(false); 
	},
	
	addEdittblsale: function(data) {
		if (data == null){
			this.selectedtblsale(new tblsale());
			viewModel.oatblsalesitemdetails.removeAll();
			viewModel.oatblsalespaymentdetails.removeAll();
			viewModel.filtereditemdeliverydetails.removeAll();
			/*$('#itemdeliverydetail').hide();*/
		}
		else{
			this.selectedtblsale(data);
			viewModel.getPurchaseOrderItemDetail(data.chrRecCode());
			viewModel.getPurchaseOrderItemDeliveryDetail(data.chrRecCode());
			
			
			/*$('#itemdeliverydetail').hide();*/
		}
		/*$(function () {
		    $('#dtmArrivalTime').datetimepicker({
		    	format: 'YYYY-MM-DD HH:mm:ss',
		    	minDate:new Date(),
		    });
		});*/
		$('#detail-container').show();
		$('#main-container').hide();
		FormFuncs.makeCopy(this.selectedtblsale, this.revertedtblsale, tblsale);
	},
	addEdittblsalesitemdetail: function(data) {
		if (data == null){
			this.selectedtblsalesitemdetail(new tblsalesitemdetail());
			
		}
		else
		{
			viewModel.selectedtblsalesitemdetail(data);
			viewModel.getItems(data.chrRecCode());
		}
		
	//	FormFuncs.makeCopy(this.selectedtblsalesitemdetail, this.revertedtblsalesitemdetail, tblsaleitemdetail);
	},
	
	openmodalpayment: function() {
		
			this.selectedtblsalespaymentdetail(new tblsalespaymentdetail());
			$('#modal-tblsalespaymentdetail').modal('show');
		
		
	//	FormFuncs.makeCopy(this.selectedtblsalesitemdetail, this.revertedtblsalesitemdetail, tblsaleitemdetail);
	},
	
	
	/*addEdittblsalespaymentdetail: function(data) {
		if (data == null){
			this.selectedtblsalespaymentdetail(new tblsalespaymentdetail());
			
		}
		else
		{
			var productcode = data.vhrNameOne();
			var brandcode = data.vhrNameTwo();
			var modelcode = data.chrModelCode();
			
			viewModel.selectedtblsalespaymentdetail(data);
			
			viewModel.getBrands(productcode);
			viewModel.getModels(brandcode);
			
			
			viewModel.selectedtblsalesitemdetail().vhrNameOne(productcode);
			viewModel.selectedtblsalesitemdetail().vhrNameTwo(brandcode);
			viewModel.selectedtblsalesitemdetail().chrModelCode(modelcode);
			
			$('#selected_product').prop('disabled', true);
			$('#selected_brand').prop('disabled', true);
			$('#selected_model').prop('disabled', true);
		}
		
		FormFuncs.makeCopy(this.selectedtblsalesitemdetail, this.revertedtblsalesitemdetail, tblsaleitemdetail);
	},*/
	
	addEdittblsalespaymentdetail: function(data) {
		if(viewModel.oatblsalesitemdetails().length < 1)
		{
			NotifitMessage("Please enter atleast 1 Seam Identification Detail.", "success");
		}
		else{
		if (data == null){
			this.oatblsalespaymentdetails.push(new tblsalespaymentdetail());
			viewModel.filtereditemdeliverydetails.push(new tblsalespaymentdetail());
		}
		else
		
			viewModel.selectedtblsalespaymentdetail(data); 	
		
		
		FormFuncs.makeCopy(this.selectedtblsalespaymentdetail, this.revertedtblsalespaymentdetail, tblsalespaymentdetail);
		}
	},
	
	
	closeAddEdittblsale: function(revert) {
		$('#detail-container').hide();
		$('#main-container').show();
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblsale, this.selectedtblsale);
		}
		else {
			viewModel.pageCount();
			viewModel.firstPage();
		}
	},
	closeAddEdittblsalesitemdetail: function(revert) {
		$('#modal-tblsalesitemdetail').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblsalesitemdetail, this.selectedtblsalesitemdetail);
		}
		else {
			FormFuncs.submitRs(viewModel.oatblsalesitemdetails, viewModel.selectedtblsalesitemdetail, tblsalesitemdetail);
		}
	},
	closeAddEdittblsalespaymentdetail: function(revert) {
		$('#modal-tblsalespaymentdetail').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblsalespaymentdetail, this.selectedtblsalespaymentdetail);
		}
		else {
			FormFuncs.submitRs(viewModel.oatblsalespaymentdetails, viewModel.selectedtblsalespaymentdetail, tblsalespaymentdetail);
		}
	},
	
	deletetblsale: function() {
		var deleteref = this.deleteRefId;
		if (deleteref == 'tblsaleitemdetails-a') {
			/*FormFuncs.postAjax('../../SeamIdentificationDetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedseamidentificationdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Seam Identification Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedseamidentificationdetail().RecCode());
					FormFuncs.removeRecCodeRs(viewModel.oaseamidentificationdetails, reccodes);
				}
			});*/
			
			
			
			NotifitMessage("Purchase Order Detail deleted successfully.", "success"); 
			var reccodes = new Array (viewModel.selectedtblsalesitemdetail().chrRecCode());
			viewModel.oadeleteseamidentificationdetails.push(viewModel.selectedtblsalesitemdetail());
			
			FormFuncs.removeRecCodeRs(viewModel.oatblsalesitemdetails, reccodes);
			
			var reccodes1 = new Array (viewModel.selectedreportdetail().SeamIdentificationDetailsCode());
			viewModel.oadeletereportdetails.push(viewModel.selectedreportdetail());
			FormFuncs.removeRecCodeRs(viewModel.filteredreportdetails, reccodes1);
			
			
				
		} else if (deleteref == 'purchaseorderitemdeliverydetails-a') {
			/*FormFuncs.postAjax('../../RadiographyDetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedreportdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Seam Identification Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedreportdetail().RecCode());
					FormFuncs.removeRecCodeRs(viewModel.oareportdetails, reccodes);
				}
			});*/
			
			NotifitMessage("Purchase Order Item Delivery Detail deleted successfully.", "success"); 
			var reccodes = new Array (viewModel.selectedreportdetail().RecCode());
			viewModel.oatblsalesitemdetails.push(viewModel.selectedreportdetail());
			FormFuncs.removeRecCodeRs(viewModel.filteredreportdetails, reccodes);
			
		} else {
			var record = ko.toJSON(this.selectedradiography());
			FormFuncs.postAjax(
				'../../Radiographys_Servlet',
				{Type: 'deletedetails', 
				 Record: record},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("Record deleted successfully.", "success");
						viewModel.closeAddEditRadiography(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	
	/*// Delete
	deletetblsale: function() {
		var deleteref = this.deleteRefId;
		if (deleteref == 'asndetails-a') {
			FormFuncs.postAjax('../tblsalesitemdetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedtblsalesitemdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Stock Adjustment Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedtblsalesitemdetail().chrRecCode());
					FormFuncs.removeRs(viewModel.oatblsalesitemdetails, reccodes);
				}
			});
		} else {
			var record = ko.toJSON(this.selectedtblsale());
			FormFuncs.postAjax(
				'../tblsales_Servlet',
				{Type: 'deletedetails', 
				 Record: record},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("ASN deleted successfully.", "success");
						viewModel.closeAddEdittblsale(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},*/
	// Paging 
	counttblsales: ko.observable(0),
	currentPagetblsales: ko.observable(0),
	noOfPagestblsales: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblsales_Servlet',
			viewModel.counttblsales,
			viewModel.noOfPagestblsales,
			viewModel.pageSize,
			viewModel.searchTexttblsale());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblsales_Servlet',
			viewModel.counttblsales,
			viewModel.noOfPagestblsales,
			viewModel.pageSize);
	},
	pagetblsalePagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblsales(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblsales_Servlet',
			type,
			viewModel.oatblsales,
			tblsale,
			viewModel.currentPagetblsales,
			viewModel.noOfPagestblsales,
			viewModel.pageSize,
			viewModel.searchTexttblsale());
	},
	pagetblsalePaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblsales(-1); // force refresh
		
		FormFuncs.pagingPages('../tblsales_Servlet',
			type, 
			viewModel.oatblsales,
			tblsale,
			viewModel.currentPagetblsales,
			viewModel.noOfPagestblsales,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblsale() == '')
			viewModel.pagetblsalePaging('f');
		else
			viewModel.pagetblsalePagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblsale() == '')
			viewModel.pagetblsalePaging('p');
		else
			viewModel.pagetblsalePagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblsale() == '')
			viewModel.pagetblsalePaging('n');
		else
			viewModel.pagetblsalePagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblsale() == '')
			viewModel.pagetblsalePaging('l');
		else
			viewModel.pagetblsalePagingSearch('l');
	},
	/*fetchitemdeliverydetails: function(data) {
		viewModel.filtereditemdeliverydetails.removeAll();
		
		if (data != null) {
			viewModel.selectedtblsalesitemdetail(data);
			
			// filter the item delivery detail as per item details
			
			ko.utils.arrayForEach(viewModel.oatblsalespaymentdetails(), function(item) {
				if (item.chrParentCode() == data.chrRecCode()) {
					viewModel.filtereditemdeliverydetails.push(item);
				}
			});
			
		}
		if(viewModel.oatblsalesitemdetails().length > 0){
		var title = 'Add Purchase Order Item Details For [' + viewModel.selectedtblsalesitemdetail().vhrItemName() + ']';
		$('#page-tblsalespaymentdetailsLabel').text(title);
		}
	
	},*/
};

