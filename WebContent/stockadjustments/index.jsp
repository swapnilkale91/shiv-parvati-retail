<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Stock Adjustment</title>
<%@include file="../ScriptCss.jsp"%>
</head>
<body class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
	    <%@include file="../Common/Header.jsp"%> 
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div id="main-container">
							
								<div class="main-box clearfix">
									<header class="main-box-header clearfix">
										<h2 class="pull-left"><b>Stock Adjustment Details</b></h2>
										<div class="filter-block pull-right">
											<div class="col-md-10 pull-left">
												<tag:search databindresetfn="resettblstockadjustment" databindfn="searchtblstockadjustments" databindvalue="searchTexttblstockadjustment"></tag:search>
											</div>
											<div class="col-md-2 pull-left">
												<tag:addbutton databind="addEdittblstockadjustment" title="Add Stock Adjustment"></tag:addbutton>
											</div>
										</div>
									</header>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<table class="table table-hover table-bordered">
												<thead>
													<tr>
														<th><b>Company Name</b></th>
														<th><b>Store Name</b></th>
														<th><b>Adjustment No</b></th>
														<th><b>Adjustment Date</b></th>
														<th class="actioncolumn">Action</th>
													</tr>
												</thead>
												<tbody data-bind="foreach: viewModel.oatblstockadjustments">
													<tr>
														<td><span data-bind="text: vhrCompanyName"></span></td>
														<td><span data-bind="text: vhrStoreName"></span></td>
														<td><span data-bind="text: vhrAdjustmentNo"></span></td>
														<td><span data-bind="text: dtmAdjustmentDate"></span></td>
														<td>
															<tag:editbutton modalid="modal-tblstockadjustment" databind="addEdittblstockadjustment" title="Edit Stock Adjustment"></tag:editbutton>
															<tag:deletebutton modalid="modal-delete" databind="addEdittblstockadjustment" title="Delete Stock Adjustment"></tag:deletebutton>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<tag:pagination currentpage="currentPagetblstockadjustments" noofpages="noOfPagestblstockadjustments"></tag:pagination>
									</div>
								</div>
							</div>
							<div id="detail-container" style="display: none">
							
								<form role="form" data-bind="submit: function() { viewModel.posttblstockadjustment() }" >
									<div class="main-box clearfix">
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Stock Adjustment</b></h2>
										</header>
										<div class="main-box-body clearfix" data-bind="with: selectedtblstockadjustment">
											<div class="row">
												<div id="col1" class="col-md-3">
													<tag:select label="Company" observable="oacompanies" bindvalue="chrCompanyCode" value="chrCompanyCode" text="vhrName" caption="Choose" required="true"></tag:select>
												
 											   </div>
												<div id="col1" class="col-md-3">
													<tag:select label="Store" observable="oastores" id="storename" bindvalue="chrStoreCode" value="chrStoreCode" text="vhrName" caption="Choose Store" required="true"></tag:select>
												</div>
												<div id="col1" class="col-md-3">
													<tag:input label="Adjustment No" id="vhrAdjustmentNo" databind="vhrAdjustmentNo" required="true" placeholder="Select Adjustment No"></tag:input>
												</div>
												<div id="col1" class="col-md-3">
													<tag:datepicker label="Adjustment Date" id="dtmAdjustmentDate"
													databind="dtmAdjustmentDate" placeholder="Select Adjustment Date"></tag:datepicker>		
												</div>
											</div>
										</div>
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Stock Adjustment Details</b></h2>
											<div class="filter-block pull-right">
												<div class="col-md-2 pull-left">
													<tag:addbutton modalid="modal-tblstockadjustmentdetails" databind="addEdittblstockadjustmentdetail" title="Add Stock Adjustment Details"></tag:addbutton>
												</div>
											</div>
										</header>
										<div class="main-box-body clearfix">
											<div class="table-responsive clearfix">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th><b>Item</b></th>
															<th><b>Item Description</b></th>
															<th><b>Adjustment Type</b></th>
															<th><b>Quantity</b></th>
															<th class="actioncolumn">Action</th>
														</tr>
													</thead>
													<tbody data-bind="foreach: viewModel.oatblstockadjustmentdetails">
														<tr>
															<td><span data-bind="text: vhrItemName"></span></td>
															<td><span data-bind="text: vhrDescription"></span></td>
															<td><span data-bind="text: intAdjustmentType"></span></td>
															<td><span data-bind="text: decQuantity"></span></td>
															<td>
																<tag:editbutton modalid="modal-tblstockadjustmentdetails" databind="addEdittblstockadjustmentdetail" title="Edit Stock Adjustment Details"></tag:editbutton>
																<tag:deletebutton id="asndetails-a" modalid="modal-delete" databind="addEdittblstockadjustmentdetail" title="Delete Stock Adjustment Details"></tag:deletebutton>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<div class="pull-right">
												<button type="cancel" class="btn btn-default" data-bind="click: function() { viewModel.closeAddEdittblstockadjustment(true); }">Cancel</button>
												<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="submittblstockadjustmentdetail" closefn="closeAddEdittblstockadjustmentdetail" modal="modal-tblstockadjustmentdetails" title="Stock Adjustment Details" withbind="selectedtblstockadjustmentdetail">
		<div class="row">
			<div id="col1" class="col-md-6">
				<tag:select label="Item" id="selected_item" observable="oaitemmasters" bindvalue="chrItemCode" value="chrItemCode" text="vhrName" caption="Choose Item"  required="true" ></tag:select>
				<tag:input label="Item Description" id="vhrDescription" databind="vhrDescription" placeholder="Enter Item Description" maxlength="100" required="true"></tag:input>
			</div>
			<div id="col1" class="col-md-6">
				<tag:select id="selected_adjustmenttype" label="Adjustment Type"  observable="oatbldefaultlookups" bindvalue="intAdjustmentType" value="intRecCode" text="vhrName" caption="Choose Adjustment Type"  required="true" ></tag:select>
				<tag:input label="Quantity" id="decQuantity" databind="decQuantity" placeholder="Enter Quantity" maxlength="10" onkeypress="return isNumber(event)" required="true"></tag:input>
			</div>
		</div>
	</tag:addeditmodal>	
	<tag:deletemodal databindfn="deletetblstockadjustment"></tag:deletemodal>
		
	<script src="tblstockadjustments.js" type="text/javascript"></script>
<!-- 	<script> -->
// 	$(function () {
// 	    $('#dtp').datetimepicker();
// 	});
<!-- 	</script> -->
</body>
</html>