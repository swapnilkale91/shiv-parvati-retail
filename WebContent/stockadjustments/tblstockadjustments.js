$(".main-menu").each(function() {
	if (this.id == "StockAdjustments") {
		$(this).addClass("active");
	}
});
function otblstockadjustment(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrAuthorizationUserOne = ko.observable(r.chrAuthorizationUserOne); 
		this.chrAuthorizationUserThree = ko.observable(r.chrAuthorizationUserThree); 
		this.chrAuthorizationUserTwo = ko.observable(r.chrAuthorizationUserTwo); 
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrStoreCode = ko.observable(r.chrStoreCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmAdjustmentDate = ko.observable(r.dtmAdjustmentDate); 
		this.dtmAuthorizationDateOne = ko.observable(r.dtmAuthorizationDateOne); 
		this.dtmAuthorizationDateThree = ko.observable(r.dtmAuthorizationDateThree); 
		this.dtmAuthorizationDateTwo = ko.observable(r.dtmAuthorizationDateTwo); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intAuthorizationLevel = ko.observable(r.intAuthorizationLevel); 
		this.intStatus = ko.observable(r.intStatus); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrAdjustmentNo = ko.observable(r.vhrAdjustmentNo);
		this.vhrCompanyName = ko.observable(r.vhrCompanyName);
		this.vhrStoreName = ko.observable(r.vhrStoreName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrAuthorizationUserOne = ko.observable(null); 
		this.chrAuthorizationUserThree = ko.observable(null); 
		this.chrAuthorizationUserTwo = ko.observable(null); 
		this.chrCompanyCode = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrStoreCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmAdjustmentDate = ko.observable(Date.today()); 
		this.dtmAuthorizationDateOne = ko.observable(null); 
		this.dtmAuthorizationDateThree = ko.observable(null); 
		this.dtmAuthorizationDateTwo = ko.observable(null); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intAuthorizationLevel = ko.observable("0"); 
		this.intStatus = ko.observable(0); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrAdjustmentNo = ko.observable("");
		this.vhrCompanyName = ko.observable("");
		this.vhrStoreName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function otblstockadjustmentitemdetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrItemCode = ko.observable(r.chrItemCode); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.decQuantity = ko.observable(r.decQuantity); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intAdjustmentType = ko.observable(r.intAdjustmentType); 
		this.intStatus = ko.observable(r.intStatus); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrDescription = ko.observable(r.vhrDescription);
		this.vhrItemName = ko.observable(r.vhrItemName);
		this.vhrAdjustmentType = ko.observable(r.vhrAdjustmentType);
		this.vhrItemName = ko.observable(r.vhrItemName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrItemCode = ko.observable(""); 
		this.chrParentCode = ko.observable(viewModel.selectedtblstockadjustment().chrRecCode()); 
		this.chrUpdateUser = ko.observable(""); 
		this.decQuantity = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intAdjustmentType = ko.observable(""); 
		this.intStatus = ko.observable(0); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrDescription = ko.observable("");
		this.vhrItemName = ko.observable("");
		this.vhrAdjustmentType = ko.observable("");
		this.vhrItemName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function oCompany(r){
	this.chrCompanyCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStore(r){
	this.chrStoreCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStatus(r){
	this.intRecCode = r.intRecCode;
	this.vhrName = r.vhrName;
}

function oItemMaster(r){
	this.chrItemCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

$(document).on("click", ".fa-trash-o", function () {
    viewModel.deleteRefId = $(this).attr('id');
});

$(function() {
	ko.applyBindings(viewModel);
	//viewModel.getProducts();
	//viewModel.getBrands();
	viewModel.getStores();
	viewModel.getCompanies();
	viewModel.getItems();
	
	viewModel.getDefaultStatuses();
//	viewModel.getAsnStatuses();
	viewModel.pageCount();
	viewModel.firstPage();
});

var viewModel = {
	deleteRefId: '',
	oadefaultstatuses: ko.observableArray([]),
	oaasnstatuses: ko.observableArray([]),
	oacompanies: ko.observableArray([]),
	oastores: ko.observableArray([]),
	oatbldefaultlookups: ko.observableArray([]),
	oaitemmasters: ko.observableArray([]),
	
	// Array
	oatblstockadjustments: ko.observableArray([]),
	oatblstockadjustmentdetails: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtblstockadjustment: ko.observable(),
	revertedtblstockadjustment: ko.observable(),
	selectedtblstockadjustmentdetail: ko.observable(),
	revertedtblstockadjustmentdetail: ko.observable(),
	// Search
	searchTexttblstockadjustment: ko.observable(''),
	searchtblstockadjustments: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	/*getDefaultStatuses: function() {
		FormFuncs.getAjax('../tblstatuses_Servlet', {Type : 'YesNo'}, true, function(output) {FormFuncs.pushRs(viewModel.oadefaultstatuses, output, oStatus);});
	},
	getAsnStatuses: function() {
		FormFuncs.getAjax('../tblstatuses_Servlet', {Type : 'ASN'}, true, function(output) {FormFuncs.pushRs(viewModel.oaasnstatuses, output, oStatus);});
	},*/
	getCompanies: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacompanies, output, oCompany);});
	},
	getStores: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oastores, output, oStore);});
	},
	getDefaultStatuses: function() {  //put the value in default lookup add/remove
		FormFuncs.getAjax('../tbldefaultlookups_Servlet', {Type : 'LookupTypeCode',LookupTypeCode : 3}, true, function(output) {FormFuncs.pushRs(viewModel.oatbldefaultlookups, output, oStatus);});
	},
	getItems: function() {
		FormFuncs.getAjax('../tblitemmasters_Servlet', {Type : 'all',}, true, function(output) {FormFuncs.pushRs(viewModel.oaitemmasters, output, oItemMaster);});
	},
	
	
	
	getStockAdjustmentDetail: function(chrParentCode) {
		FormFuncs.getAjax('../tblstockadjustments_Servlet', {Type: 'stockdetail', chrParentCode: chrParentCode}, true, function(output) { FormFuncs.pushRs(viewModel.oatblstockadjustmentdetails, output, otblstockadjustmentitemdetail); });
	},
	
	
	// Add and Edit
	posttblstockadjustment: function() {
		if(viewModel.oatblstockadjustmentdetails().length < 1) {
			NotifitMessage("Enter atleast one Product Brand Details", "success");
		} else {
			/*viewModel.selectedtblstockadjustment().dtmArrivalTime($("#dtmArrivalTime").val());*/
			var stockadjustment = ko.toJSON(this.selectedtblstockadjustment());
			var stockadjustmentdetails = ko.toJSON(this.oatblstockadjustmentdetails());
			/*var deletestockadjustmentdetails = ko.toJSON(this.oadeletelhstubedetails());*/
			FormFuncs.postAjax(
				'../tblstockadjustments_Servlet',
				{Type: 'postDetails', 
				 StockAdjustment: stockadjustment,
				 StockAdjustmentDetails: stockadjustmentdetails},
				true,
				function(output) {
					if (output.Success == true) {
						if(viewModel.selectedtblstockadjustment().IUD() == 1) {
							NotifitMessage("Stock Adjustment saved successfully.", "success");
						}
						else {
							NotifitMessage("Stock Adjustment updated successfully.", "success");
						}
						viewModel.closeAddEdittblstockadjustment(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	submittblstockadjustmentdetail: function() {
		viewModel.selectedtblstockadjustmentdetail().vhrItemName($('#selected_item option:selected').text());
		viewModel.selectedtblstockadjustmentdetail().vhrAdjustmentType($('#selected_adjustmenttype option:selected').text());
		viewModel.closeAddEdittblstockadjustmentdetail(false); 
	},
	addEdittblstockadjustment: function(data) {
		if (data == null){
			this.selectedtblstockadjustment(new otblstockadjustment());
			viewModel.oatblstockadjustmentdetails.removeAll();
		}
		else{
			this.selectedtblstockadjustment(data);
			viewModel.getStockAdjustmentDetail(data.chrRecCode());
		}
		/*$(function () {
		    $('#dtmArrivalTime').datetimepicker({
		    	format: 'YYYY-MM-DD HH:mm:ss',
		    	minDate:new Date(),
		    });
		});*/
		$('#detail-container').show();
		$('#main-container').hide();
		FormFuncs.makeCopy(this.selectedtblstockadjustment, this.revertedtblstockadjustment, otblstockadjustment);
	},
	addEdittblstockadjustmentdetail: function(data) {
		if (data == null)
			this.selectedtblstockadjustmentdetail(new otblstockadjustmentitemdetail());
		else
		{
			/*var productcode = data.vhrNameOne();
			var brandcode = data.vhrNameTwo();
			var modelcode = data.chrModelCode();*/
			
			viewModel.selectedtblstockadjustmentdetail(data);
			
			/*viewModel.getBrands(productcode);
			viewModel.getModels(brandcode);
			
			
			viewModel.selectedtblstockadjustmentdetail().vhrNameOne(productcode);
			viewModel.selectedtblstockadjustmentdetail().vhrNameTwo(brandcode);
			viewModel.selectedtblstockadjustmentdetail().chrModelCode(modelcode);
			
			$('#selected_product').prop('disabled', true);
			$('#selected_brand').prop('disabled', true);
			$('#selected_model').prop('disabled', true);*/
		}
		
	//	FormFuncs.makeCopy(this.selectedtblstockadjustmentdetail, this.revertedtblstockadjustmentdetail, otblstockadjustmentitemdetail);
	},
	closeAddEdittblstockadjustment: function(revert) {
		$('#detail-container').hide();
		$('#main-container').show();
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblstockadjustment, this.selectedtblstockadjustment);
		}
		else {
			viewModel.pageCount();
			viewModel.firstPage();
		}
	},
	closeAddEdittblstockadjustmentdetail: function(revert) {
		$('#modal-tblstockadjustmentdetails').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblstockadjustmentdetail, this.selectedtblstockadjustmentdetail);
		}
		else {
			FormFuncs.submitRs(viewModel.oatblstockadjustmentdetails, viewModel.selectedtblstockadjustmentdetail, otblstockadjustmentitemdetail);
		}
	},
	// Delete
	deletetblstockadjustment: function() {
		var deleteref = this.deleteRefId;
		if (deleteref == 'asndetails-a') {
			FormFuncs.postAjax('../tblstockadjustmentdetails_Servlet', {Type: 'delete', Record: ko.toJSON(viewModel.selectedtblstockadjustmentdetail())}, true, function(output) {
				if (output.Success == false) { 
					NotifitMessage("Error deleting record.", "error"); 
				}
				else {
					NotifitMessage("Stock Adjustment Detail deleted successfully.", "success"); 
					var reccodes = new Array (viewModel.selectedtblstockadjustmentdetail().chrRecCode());
					FormFuncs.removeRs(viewModel.oatblstockadjustmentdetails, reccodes);
				}
			});
		} else {
			var record = ko.toJSON(this.selectedtblstockadjustment());
			FormFuncs.postAjax(
				'../tblstockadjustments_Servlet',
				{Type: 'deletedetails', 
				 Record: record},
				true,
				function(output) {
					if (output.Success == true) {
						NotifitMessage("Stock Adjustment deleted successfully.", "success");
						viewModel.closeAddEdittblstockadjustment(false);
					}
					else if (output.Success == false) {
						NotifitMessage(output.Message, "error");
					}
				}
			);
		}
	},
	// Paging 
	counttblstockadjustments: ko.observable(0),
	currentPagetblstockadjustments: ko.observable(0),
	noOfPagestblstockadjustments: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblstockadjustments_Servlet',
			viewModel.counttblstockadjustments,
			viewModel.noOfPagestblstockadjustments,
			viewModel.pageSize,
			viewModel.searchTexttblstockadjustment());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblstockadjustments_Servlet',
			viewModel.counttblstockadjustments,
			viewModel.noOfPagestblstockadjustments,
			viewModel.pageSize);
	},
	pagetblstockadjustmentPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblstockadjustments(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblstockadjustments_Servlet',
			type,
			viewModel.oatblstockadjustments,
			otblstockadjustment,
			viewModel.currentPagetblstockadjustments,
			viewModel.noOfPagestblstockadjustments,
			viewModel.pageSize,
			viewModel.searchTexttblstockadjustment());
	},
	pagetblstockadjustmentPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblstockadjustments(-1); // force refresh
		
		FormFuncs.pagingPages('../tblstockadjustments_Servlet',
			type, 
			viewModel.oatblstockadjustments,
			otblstockadjustment,
			viewModel.currentPagetblstockadjustments,
			viewModel.noOfPagestblstockadjustments,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblstockadjustment() == '')
			viewModel.pagetblstockadjustmentPaging('f');
		else
			viewModel.pagetblstockadjustmentPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblstockadjustment() == '')
			viewModel.pagetblstockadjustmentPaging('p');
		else
			viewModel.pagetblstockadjustmentPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblstockadjustment() == '')
			viewModel.pagetblstockadjustmentPaging('n');
		else
			viewModel.pagetblstockadjustmentPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblstockadjustment() == '')
			viewModel.pagetblstockadjustmentPaging('l');
		else
			viewModel.pagetblstockadjustmentPagingSearch('l');
	}
};
