<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@include file="../ScriptCss.jsp"%>
<title><%=PROJECT_NAME%>: Stores</title>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../Common/Header.jsp"%>
		<div id="page-wrapper" class="container-fluid">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="main-box clearfix">
								<header class="main-box-header clearfix">
									<h2 class="pull-left">
										<b>Stores</b>
									</h2>
									<div class="filter-block pull-right">
										<div class="col-md-10 pull-left">
											<tag:search databindfn="searchtblstores"
												databindvalue="searchTexttblstore"
												databindresetfn="refreshtblstores"></tag:search>
										</div>
										<div class="col-md-2 pull-left">
											<tag:addbutton modalid="modal-tblstore"
												databind="addEdittblstore" title="Add Store"></tag:addbutton>
										</div>
									</div>
								</header>
								<div class="main-box-body clearfix">
									<div class="table-responsive clearfix">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th><b>Name</b></th>
													<th><b>Code</b></th>
													<th><b>Address</b></th>
													<th><b>Telephone</b></th>
													<th class="actioncolumn">Action</th>
												</tr>
											</thead>
											<tbody data-bind="foreach: viewModel.oatblstores">
												<tr>
													<td><span data-bind="text: vhrName"></span></td>
													<td><span data-bind="text: vhrCode"></span></td>
													<td><span data-bind="text: vhrAddress1"></span>&nbsp;<span
														data-bind="text: vhrAddress2"></span>&nbsp;<span
														data-bind="text: vhrAddress3"></span></td>
													<td><span data-bind="text: vhrTel1"></span>&nbsp;<span
														data-bind="text: vhrTel2"></span></td>
													<td><tag:editbutton modalid="modal-tblstore"
															databind="addEdittblstore" title="Edit Store"></tag:editbutton>
														<tag:deletebutton modalid="modal-delete"
															databind="addEdittblstore" title="Delete Store"></tag:deletebutton></td>
												</tr>
											</tbody>
										</table>
									</div>
									<tag:pagination currentpage="currentPagetblstores"
										noofpages="noOfPagestblstores"></tag:pagination>
								</div>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="posttblstore" closefn="closeAddEdit"
		modal="modal-tblstore" title="Store" withbind="selectedtblstore">
		<div class="row">
			<div class="col-md-6">
				<tag:select label="Company"
					observable="oacompanies" bindvalue="chrParentCode"
					value="chrParentCode" text="vhrName" caption="Select Company"
					required="true"></tag:select>
			</div>
			<div class="col-md-6">
				<tag:input label="Store" id="vhrName" databind="vhrName"
					required="true" placeholder="Enter Store Name" maxlength="100"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:textarea label="Address1" id="vhrAddress1" required="true"
					databind="vhrAddress1" placeholder="Enter Address" maxlength="100"
					rows="2"></tag:textarea>
			</div>
			<div class="col-md-6">
				<tag:textarea label="Address2" id="vhrAddress2"
					databind="vhrAddress2" placeholder="Enter Address" maxlength="100"
					rows="2"></tag:textarea>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:textarea label="Address3" id="vhrAddress3"
					databind="vhrAddress3" placeholder="Enter Address" maxlength="100"
					rows="2"></tag:textarea>
			</div>
			<div class="col-md-6">
				<tag:textarea label="Address4" id="vhrAddress4"
					databind="vhrAddress4" placeholder="Enter Address" maxlength="100"
					rows="2"></tag:textarea>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="PinCode" id="intPinCode" databind="intPinCode"
					required="true" placeholder="Enter PinCode" maxlength="10"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Code" id="vhrCode" databind="vhrCode"
					required="true" placeholder="Enter Code" maxlength="20"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="Tel1" id="vhrTel1" databind="vhrTel1"
					placeholder="Enter Tel1" maxlength="20"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Tel2" id="vhrTel2" databind="vhrTel2"
					placeholder="Enter Tel2" maxlength="20"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="PAN No" id="vhrPANNo" databind="vhrPANNo"
					required="true" placeholder="Enter PAN No" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="Service Tax No" id="vhrServiceTaxNo"
					databind="vhrServiceTaxNo" required="true"
					placeholder="Enter Service Tax No" maxlength="100"></tag:input>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<tag:input label="CST No" id="vhrCSTNo" databind="vhrCSTNo"
					required="true" placeholder="Enter CST No" maxlength="100"></tag:input>
			</div>
			<div class="col-md-6">
				<tag:input label="VAT No" id="vhrVATNo" databind="vhrVATNo"
					required="true" placeholder="Enter VAT No" maxlength="100"></tag:input>
			</div>
		</div>

	</tag:addeditmodal>


	<tag:deletemodal databindfn="deletetblstore"></tag:deletemodal>

	<script src="tblstores.js?<%=COMMON_SCRIPT_VERSION%>"
		type="text/javascript"></script>

</body>
</html>