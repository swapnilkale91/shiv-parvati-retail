$(".main-menu").each(function() {
	if (this.id == "Stores") {
		$(this).addClass("active");
	}
});
function otblstore(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intPinCode = ko.observable(r.intPinCode); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrAddress1 = ko.observable(r.vhrAddress1); 
		this.vhrAddress2 = ko.observable(r.vhrAddress2); 
		this.vhrAddress3 = ko.observable(r.vhrAddress3); 
		this.vhrAddress4 = ko.observable(r.vhrAddress4); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrCSTNo = ko.observable(r.vhrCSTNo); 
		this.vhrName = ko.observable(r.vhrName); 
		this.vhrPANNo = ko.observable(r.vhrPANNo); 
		this.vhrServiceTaxNo = ko.observable(r.vhrServiceTaxNo); 
		this.vhrTel1 = ko.observable(r.vhrTel1); 
		this.vhrTel2 = ko.observable(r.vhrTel2); 
		this.vhrVATNo = ko.observable(r.vhrVATNo);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrParentCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intPinCode = ko.observable(""); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrAddress1 = ko.observable(""); 
		this.vhrAddress2 = ko.observable(""); 
		this.vhrAddress3 = ko.observable(""); 
		this.vhrAddress4 = ko.observable(""); 
		this.vhrCode = ko.observable(""); 
		this.vhrCSTNo = ko.observable(""); 
		this.vhrName = ko.observable(""); 
		this.vhrPANNo = ko.observable(""); 
		this.vhrServiceTaxNo = ko.observable(""); 
		this.vhrTel1 = ko.observable(""); 
		this.vhrTel2 = ko.observable(""); 
		this.vhrVATNo = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function oCompany(r){
	this.chrParentCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}


$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.getCompanies();
	/*viewModel.getStores();*/
//	viewModel.getDefaultStatuses();
});

var viewModel = {
	// Array
	oatblstores: ko.observableArray([]),
	oacompanies : ko.observableArray([]),
	oastores : ko.observableArray([]),
	// Selected and Revert 
	selectedtblstore: ko.observable(),
	revertedtblstore: ko.observable(),
	// Search
	searchTexttblstore: ko.observable(''),
	searchtblstores: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtblstores: function() {
		viewModel.searchTexttblstore(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	getCompanies: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacompanies, output, oCompany);});
	},
	
	
	// Add and Edit
	posttblstore: function() {
		var record = ko.toJSON(this.selectedtblstore());
		FormFuncs.postAjax(
			'../tblstores_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblstore().IUD() == 1) {
						NotifitMessage("Store saved successfully.", "success");
					}
					else {
						NotifitMessage("Store updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblstore: function(data) {
		if (data == null)
			this.selectedtblstore(new otblstore());
		else
			this.selectedtblstore(data);
		
		FormFuncs.makeCopy(this.selectedtblstore, this.revertedtblstore, otblstore);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblstore').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblstore, this.selectedtblstore);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblstorePaging(null);
		}
	},
	// Delete
	deletetblstore: function() {
		var record = ko.toJSON(this.selectedtblstore());
		FormFuncs.postAjax(
			'../tblstores_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Store deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttblstores: ko.observable(0),
	currentPagetblstores: ko.observable(0),
	noOfPagestblstores: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblstores_Servlet',
			viewModel.counttblstores,
			viewModel.noOfPagestblstores,
			viewModel.pageSize,
			viewModel.searchTexttblstore());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblstores_Servlet',
			viewModel.counttblstores,
			viewModel.noOfPagestblstores,
			viewModel.pageSize);
	},
	pagetblstorePagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblstores(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblstores_Servlet',
			type,
			viewModel.oatblstores,
			otblstore,
			viewModel.currentPagetblstores,
			viewModel.noOfPagestblstores,
			viewModel.pageSize,
			viewModel.searchTexttblstore());
	},
	pagetblstorePaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblstores(-1); // force refresh
		
		FormFuncs.pagingPages('../tblstores_Servlet',
			type, 
			viewModel.oatblstores,
			otblstore,
			viewModel.currentPagetblstores,
			viewModel.noOfPagestblstores,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblstore() == '')
			viewModel.pagetblstorePaging('f');
		else
			viewModel.pagetblstorePagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblstore() == '')
			viewModel.pagetblstorePaging('p');
		else
			viewModel.pagetblstorePagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblstore() == '')
			viewModel.pagetblstorePaging('n');
		else
			viewModel.pagetblstorePagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblstore() == '')
			viewModel.pagetblstorePaging('l');
		else
			viewModel.pagetblstorePagingSearch('l');
	}
};
