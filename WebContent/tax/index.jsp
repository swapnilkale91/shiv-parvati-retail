<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tax</title>
<%@include file="../ScriptCss.jsp"%>
</head>
<body class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../Common/Header.jsp"%> 
		<div id="page-wrapper" class="container">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div id="main-container">
							
								<div class="main-box clearfix">
									<header class="main-box-header clearfix">
										<h2 class="pull-left"><b>Tax</b></h2>
										<div class="filter-block pull-right">
											<div class="col-md-10 pull-left">
												<tag:search databindresetfn="refreshtbltaxmaster" databindfn="searchtbltaxmaster" databindvalue="searchTexttbltaxmaster"></tag:search>
											</div>
											<div class="col-md-2 pull-left">
												<tag:addbutton databind="addEdittbltaxmaster" title="Add Tax"></tag:addbutton>
											</div>
										</div>
									</header>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<table class="table table-hover table-bordered">
												<thead>
													<tr>
														<th><b>Name</b></th>
														<th><b>Code</b></th>
														<th class="actioncolumn">Action</th>
													</tr>
												</thead>
												<tbody data-bind="foreach: viewModel.oatbltaxmasters">
													<tr>
														<td><span data-bind="text: vhrName"></span></td>
													<td><span data-bind="text: vhrCode"></span></td>
													<td><tag:editbutton modalid="modal-tblPurchaseUom"
															databind="addEdittbltaxmaster"
															title="Edit TAX"></tag:editbutton>
														<tag:deletebutton modalid="modal-delete" 
														databind="deletetbltaxmaster" 
														title="Delete TAX"></tag:deletebutton>
													</td>
													</tr>
												</tbody>
											</table>
										</div>
										<tag:pagination currentpage="currentPagetbltaxmaster" noofpages="noOfPagestbltaxmaster"></tag:pagination>
									</div>
								</div>
							</div>
							<div id="detail-container" style="display: none">
							
								<form role="form" data-bind="submit: function() { viewModel.posttbltaxmaster() }" >
									<div class="main-box clearfix">
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Tax</b></h2>
										</header>
										<div class="main-box-body clearfix" data-bind="with: selectedtbltaxmaster">
											<div class="row">
												<div id="col1" class="col-md-6">
													<tag:input label="Tax Name" id="vhrName" databind="vhrName" required="true"
															placeholder="Enter Tax Name" maxlength="100"></tag:input>
												</div>
												<div id="col1" class="col-md-6">
													<tag:input label="Tax Code" id="vhrCode" databind="vhrCode"
														required="true" placeholder="Enter Code" maxlength="20"></tag:input>
												</div>
											</div>
										</div>
										<header class="main-box-header clearfix">
											<h2 class="pull-left"><b>Add Tax Details</b></h2>
											<div class="filter-block pull-right">
												<div class="col-md-2 pull-left">
													<tag:addbutton modalid="modal-tbltaxdetails" databind="addEdittbltaxdetails" title="Add Tax Details"></tag:addbutton>
												</div>
											</div>
										</header>
										<div class="main-box-body clearfix">
											<div class="table-responsive clearfix">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th><b>Name</b></th>
															<th><b>Effective Date</b></th>
															<th><b>Percentage</b></th>
															<th class="actioncolumn">Action</th>
														</tr>
													</thead>
													<tbody data-bind="foreach: viewModel.oatbltaxdetails">
														<tr>
															<td><span data-bind="text: vhrName"></span></td>
															<td><span data-bind="text: dtmEffectiveDate"></span></td>
															<td><span data-bind="text: decPercentage"></span></td>
															<td>
																<tag:editbutton modalid="modal-tbltaxdetails" databind="addEdittbltaxdetails" title="Edit Tax Details"></tag:editbutton>
																<tag:deletebutton id="taxdetails-a" modalid="modal-delete" databind="addEdittbltaxdetail" title="Delete Tax Details"></tag:deletebutton>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="main-box-body clearfix">
										<div class="table-responsive clearfix">
											<div class="pull-right">
												<button type="cancel" class="btn btn-default" data-bind="click: function() { viewModel.closeAddEdittbltaxmaster(true); }">Cancel</button>
												<button type="submit" id="btn_save" class="btn btn-primary">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="submittbltaxdetail" closefn="closeAddEdittbltaxdetail" modal="modal-tbltaxdetails" title="TAX Details" withbind="selectedtbltaxdetail">
		<tag:input label="Name" id="vhrName" databind="vhrName" required="true"
			placeholder="Enter Name" maxlength="100"></tag:input>
		<tag:datepicker label="Effective Date" id="dtmEffectiveDate" 
			databind="dtmEffectiveDate" placeholder="Select Effective Date" required="true"></tag:datepicker>
		<tag:input label="Percentage" id="decPercentage" databind="decPercentage" onkeypress="return isDecimal(event)"
			required="true" placeholder="Enter Percentage" maxlength="10"></tag:input>
	</tag:addeditmodal>	
	
	<tag:deletemodal databindfn="deletetbltaxmaster"></tag:deletemodal>
	
	<script src="tbltaxmasters.js" type="text/javascript"></script>

</body>
</html>