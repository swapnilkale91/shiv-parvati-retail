$(".main-menu").each(function() {
	if (this.id == "Master") {
		$(this).addClass("active");
	}
});
$(".sub-menu").each(function() {
	if (this.id == "Tax") {
		$(this).addClass("active");
	}
});

function  tbltaxmaster(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.vhrCode = ko.observable(""); 
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function tbltaxdetail(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrParentCode = ko.observable(r.chrParentCode); 
		this.decPercentage = ko.observable(r.decPercentage); 
		this.dtmEffectiveDate = ko.observable(r.dtmEffectiveDate); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrParentCode = ko.observable(viewModel.selectedtbltaxmaster().chrRecCode()); 
		this.decPercentage = ko.observable(""); 
		this.dtmEffectiveDate = ko.observable(new Date()); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(1); 
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}


$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
});

var viewModel = {
	// Array
	oatbltaxmasters: ko.observableArray([]),
	oatbltaxdetails: ko.observableArray([]),
	oadeletetbltaxdetails: ko.observableArray([]),
	
	// Selected and Revert 
	selectedtbltaxmaster: ko.observable(),
	revertedtbltaxmaster: ko.observable(),
	selectedtbltaxdetail: ko.observable(),
	revertedtbltaxdetail: ko.observable(),
	
	// Search
	searchTexttbltaxmaster: ko.observable(''),
	searchtbltaxmaster: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	
	// Refresh
	refreshtbltaxmaster: function() {
		viewModel.searchTexttbltaxmaster(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	gettbltaxdetails: function(chrParentCode) {
		FormFuncs.getAjax('../tbltaxdetails_Servlet', {Type: 'taxdetails', chrParentCode: chrParentCode}, true, function(output) { FormFuncs.pushRs(viewModel.oatbltaxdetails, output, tbltaxdetail); });
	},
	
	// Add and Edit
	posttbltaxmaster: function() {
		var taxmaster = ko.toJSON(this.selectedtbltaxmaster());
		var taxdetail = ko.toJSON(this.oatbltaxdetails());
		var deletetaxdetail = ko.toJSON(this.oadeletetbltaxdetails());
		FormFuncs.postAjax(
			'../tbltaxmasters_Servlet',
			{Type: 'postDetails', 
				TaxMaster: taxmaster,
				TaxDetail: taxdetail,
				DeleteTaxDetail: deletetaxdetail},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtbltaxmaster().IUD() == 1) {
						NotifitMessage("Tax saved successfully.", "success");
					}
					else {
						NotifitMessage("Tax updated successfully.", "success");
					} 
					viewModel.closeAddEdittbltaxmaster(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	submittbltaxdetail: function() {
		viewModel.selectedtbltaxdetail().vhrName();
		viewModel.selectedtbltaxdetail().dtmEffectiveDate();
		viewModel.selectedtbltaxdetail().decPercentage();
		
		viewModel.closeAddEdittbltaxdetail(false); 
	},
	
	addEdittbltaxmaster: function(data) {
		if (data == null)
		{
			this.selectedtbltaxmaster(new  tbltaxmaster());
			viewModel.oatbltaxdetails.removeAll();
		}
		else
		{
			this.selectedtbltaxmaster(data);
			viewModel.gettbltaxdetails(data.chrRecCode());
		}
		$('#detail-container').show();
		$('#main-container').hide();
		FormFuncs.makeCopy(this.selectedtbltaxmaster, this.revertedtbltaxdetail,  tbltaxmaster);
	},
	addEdittbltaxdetails: function(data) {
		if (data == null)
			this.selectedtbltaxdetail(new tbltaxdetail());
		else
		{
			var productcode = data.vhrName();
			var brandcode = data.dtmEffectiveDate();
			var modelcode = data.decPercentage();
			
			viewModel.selectedtbltaxdetail(data);
			
			viewModel.selectedtbltaxdetail().vhrName(productcode);
			viewModel.selectedtbltaxdetail().dtmEffectiveDate(brandcode);
			viewModel.selectedtbltaxdetail().decPercentage(modelcode);
			
		}
		
		FormFuncs.makeCopy(this.selectedtbltaxdetail, this.revertedtblasndetail, tbltaxdetail);
	},
	
	closeAddEdittbltaxmaster: function(revert) {
		$('#detail-container').hide();
		$('#main-container').show();
		//$('#modal-tblPurchaseUom').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtbltaxmaster, this.selectedtbltaxmaster);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetbltaxmasterPaging(null);
		}
	},
	
	closeAddEdittbltaxdetail: function(revert) {
		$('#modal-tbltaxdetails').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtbltaxdetail, this.selectedtbltaxdetail);
		}
		else {
			FormFuncs.submitRs(viewModel.oatbltaxdetails, viewModel.selectedtbltaxdetail, tbltaxdetail);
		}
	},
	
	// Delete
	deletetbltaxmaster: function() {
		var record = ko.toJSON(this.selectedtbltaxmaster());
		FormFuncs.postAjax(
			'../tbltaxmasters_Servlet',
			{Type: 'deletedetails', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Tax deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	
	// Paging 
	counttbltaxmaster: ko.observable(0),
	currentPagetbltaxmaster: ko.observable(0),
	noOfPagestbltaxmaster: ko.observable(0),
	pageSize: 12,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tbltaxmasters_Servlet',
			viewModel.counttbltaxmaster,
			viewModel.noOfPagestbltaxmaster,
			viewModel.pageSize,
			viewModel.searchTexttbltaxmaster());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tbltaxmasters_Servlet',
			viewModel.counttbltaxmaster,
			viewModel.noOfPagestbltaxmaster,
			viewModel.pageSize);
	},
	pagetbltaxmasterPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetbltaxmaster(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tbltaxmasters_Servlet',
			type,
			viewModel.oatbltaxmasters,
			 tbltaxmaster,
			viewModel.currentPagetbltaxmaster,
			viewModel.noOfPagestbltaxmaster,
			viewModel.pageSize,
			viewModel.searchTexttbltaxmaster());
	},
	pagetbltaxmasterPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetbltaxmaster(-1); // force refresh
		
		FormFuncs.pagingPages('../tbltaxmasters_Servlet',
			type, 
			viewModel.oatbltaxmasters,
			 tbltaxmaster,
			viewModel.currentPagetbltaxmaster,
			viewModel.noOfPagestbltaxmaster,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttbltaxmaster() == '')
			viewModel.pagetbltaxmasterPaging('f');
		else
			viewModel.pagetbltaxmasterPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttbltaxmaster() == '')
			viewModel.pagetbltaxmasterPaging('p');
		else
			viewModel.pagetbltaxmasterPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttbltaxmaster() == '')
			viewModel.pagetbltaxmasterPaging('n');
		else
			viewModel.pagetbltaxmasterPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttbltaxmaster() == '')
			viewModel.pagetbltaxmasterPaging('l');
		else
			viewModel.pagetbltaxmasterPagingSearch('l');
	}
};