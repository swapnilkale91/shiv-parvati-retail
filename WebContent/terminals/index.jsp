<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="tag" uri="../../WEB-INF/tld/htmlcontrol.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> Course Category</title>
<%@include file="../ScriptCss.jsp"%>
</head>
<body
	class="pace-done theme-whbl fixed-header fixed-leftmenu fixed-footer">
	<div id="theme-wrapper">
		<%@include file="../Common/Header.jsp"%>
		<div id="page-wrapper" class="container-fluid">
			<div class="row">
				<%@include file="../Common/Sidebar.jsp"%>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<div class="main-box clearfix">
								<header class="main-box-header clearfix">
									<h2 class="pull-left">
										<b>Terminal</b>
									</h2>
									<div class="filter-block pull-right">
										<div class="col-md-10 pull-left">
											<tag:search databindfn="searchtblterminals"
												databindvalue="searchTexttblterminal"
												databindresetfn="refreshtblterminals"></tag:search>
										</div>
										<div class="col-md-2 pull-left">
											<tag:addbutton modalid="modal-tblterminal"
												databind="addEdittblterminal" title="Add Terminal"></tag:addbutton>
										</div>
									</div>
								</header>
								<div class="main-box-body clearfix">
									<div class="table-responsive clearfix">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<th><b>Company</b></th>
													<th><b>Store</b></th>
													<th><b>Terminal Code</b></th>
													<th class="actioncolumn">Action</th>
												</tr>
											</thead>
											<tbody data-bind="foreach: viewModel.oatblterminals">
												<tr>
													<td><span data-bind="text: vhrCompanyName"></span></td>
													<td><span data-bind="text: vhrStoreName"></span></td>
													<td><span data-bind="text: chrTerminalCode"></span></td>
													
													<td><tag:editbutton modalid="modal-tblterminal"
															databind="addEdittblterminal"
															title="Edit Terminal"></tag:editbutton> <tag:deletebutton
															modalid="modal-delete" databind="addEdittblterminal"
															title="Delete Terminal"></tag:deletebutton>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<tag:pagination currentpage="currentPagetblterminals"
										noofpages="noOfPagestblterminals"></tag:pagination>
								</div>
							</div>
						</div>
					</div>
					<%@include file="../Common/Footer.jsp"%>
				</div>
			</div>
		</div>
	</div>

	<!-- Add Edit Modal -->
	<tag:addeditmodal postfn="posttblterminal" closefn="closeAddEdit"
		modal="modal-tblterminal" title="Terminal"
		withbind="selectedtblterminal">

		<tag:select label="Company"
			observable="oacompanies" bindvalue="chrCompanyCode"
			value="chrCompanyCode" text="vhrName" caption="Select Company"
			required="true"></tag:select>
		
		<tag:select label="Store"
			observable="oastores" bindvalue="chrStoreCode"
			value="chrStoreCode" text="vhrName" caption="Select Store"
			required="true"></tag:select>


		<tag:input label="Terminal Code" id="chrTerminalCode" databind="chrTerminalCode"
			required="true" placeholder="Enter Terminal Code"
			maxlength="100"></tag:input>
		


	</tag:addeditmodal>


	<tag:deletemodal databindfn="deletetblterminal"></tag:deletemodal>

	<script src="tblterminals.js?<%=COMMON_SCRIPT_VERSION%>"
		type="text/javascript"></script>

</body>
</html>