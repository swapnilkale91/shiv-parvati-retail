$(".main-menu").each(function() {
	if (this.id == "Terminals") {
		$(this).addClass("active");
	}
});
function otblterminal(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrCompanyCode = ko.observable(r.chrCompanyCode); 
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrStoreCode = ko.observable(r.chrStoreCode); 
		this.chrTerminalCode = ko.observable(r.chrTerminalCode); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.tinStatus = ko.observable(r.tinStatus);
		this.vhrCompanyName = ko.observable(r.vhrCompanyName);
		this.vhrStoreName = ko.observable(r.vhrStoreName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrCompanyCode = ko.observable(""); 
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrStoreCode = ko.observable(""); 
		this.chrTerminalCode = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinIsDirty = ko.observable(0); 
		this.tinStatus = ko.observable(0);
		this.vhrCompanyName = ko.observable("");
		this.vhrStoreName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function oCompany(r){
	this.chrCompanyCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}

function oStore(r){
	this.chrStoreCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}


$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
	viewModel.getCompanies();
	viewModel.getStores();
//	viewModel.getDefaultStatuses();
});

var viewModel = {
	// Array
	oatblterminals: ko.observableArray([]),
	oacompanies : ko.observableArray([]),
	oastores : ko.observableArray([]),
	// Selected and Revert 
	selectedtblterminal: ko.observable(),
	revertedtblterminal: ko.observable(),
	// Search
	searchTexttblterminal: ko.observable(''),
	searchtblterminals: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtblterminals: function() {
		viewModel.searchTexttblterminal(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	getCompanies: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacompanies, output, oCompany);});
	},
	getStores: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oastores, output, oStore);});
	},
	
	
	// Add and Edit
	posttblterminal: function() {
		var record = ko.toJSON(this.selectedtblterminal());
		FormFuncs.postAjax(
			'../tblterminals_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblterminal().IUD() == 1) {
						NotifitMessage("Terminal saved successfully.", "success");
					}
					else {
						NotifitMessage("Terminal updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblterminal: function(data) {
		if (data == null)
			this.selectedtblterminal(new otblterminal());
		else
			this.selectedtblterminal(data);
		
		FormFuncs.makeCopy(this.selectedtblterminal, this.revertedtblterminal, otblterminal);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblterminal').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblterminal, this.selectedtblterminal);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblterminalPaging(null);
		}
	},
	// Delete
	deletetblterminal: function() {
		var record = ko.toJSON(this.selectedtblterminal());
		FormFuncs.postAjax(
			'../tblterminals_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Terminal deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttblterminals: ko.observable(0),
	currentPagetblterminals: ko.observable(0),
	noOfPagestblterminals: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblterminals_Servlet',
			viewModel.counttblterminals,
			viewModel.noOfPagestblterminals,
			viewModel.pageSize,
			viewModel.searchTexttblterminal());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblterminals_Servlet',
			viewModel.counttblterminals,
			viewModel.noOfPagestblterminals,
			viewModel.pageSize);
	},
	pagetblterminalPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblterminals(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblterminals_Servlet',
			type,
			viewModel.oatblterminals,
			otblterminal,
			viewModel.currentPagetblterminals,
			viewModel.noOfPagestblterminals,
			viewModel.pageSize,
			viewModel.searchTexttblterminal());
	},
	pagetblterminalPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblterminals(-1); // force refresh
		
		FormFuncs.pagingPages('../tblterminals_Servlet',
			type, 
			viewModel.oatblterminals,
			otblterminal,
			viewModel.currentPagetblterminals,
			viewModel.noOfPagestblterminals,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblterminal() == '')
			viewModel.pagetblterminalPaging('f');
		else
			viewModel.pagetblterminalPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblterminal() == '')
			viewModel.pagetblterminalPaging('p');
		else
			viewModel.pagetblterminalPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblterminal() == '')
			viewModel.pagetblterminalPaging('n');
		else
			viewModel.pagetblterminalPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblterminal() == '')
			viewModel.pagetblterminalPaging('l');
		else
			viewModel.pagetblterminalPagingSearch('l');
	}
};

