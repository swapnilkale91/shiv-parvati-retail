$(".main-menu").each(function() {
	if (this.id == "UnitofMeasures") {
		$(this).addClass("active");
	}
});
function otbluommaster(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrIpAddress = ko.observable(r.vhrIpAddress); 
		this.vhrName = ko.observable(r.vhrName);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrCode = ko.observable(""); 
		this.vhrIpAddress = ko.observable(""); 
		this.vhrName = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
//	viewModel.getCompanies();
//	viewModel.getStores();
//	viewModel.getDefaultStatuses();
});

var viewModel = {
	// Array
	oatbluommasters: ko.observableArray([]),
	oacompanies : ko.observableArray([]),
	oastores : ko.observableArray([]),
	// Selected and Revert 
	selectedtbluommaster: ko.observable(),
	revertedtbluommaster: ko.observable(),
	// Search
	searchTexttbluommaster: ko.observable(''),
	searchtbluommasters: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtbluommasters: function() {
		viewModel.searchTexttbluommaster(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	/*getCompanies: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacompanies, output, oCompany);});
	},
	getStores: function() {
		FormFuncs.getAjax('../tblstores_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oastores, output, oStore);});
	},*/
	
	
	// Add and Edit
	posttbluommaster: function() {
		var record = ko.toJSON(this.selectedtbluommaster());
		FormFuncs.postAjax(
			'../tbluommasters_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtbluommaster().IUD() == 1) {
						NotifitMessage("Unit Of Measure saved successfully.", "success");
					}
					else {
						NotifitMessage("Unit Of Measure updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittbluommaster: function(data) {
		if (data == null)
			this.selectedtbluommaster(new otbluommaster());
		else
			this.selectedtbluommaster(data);
		
		FormFuncs.makeCopy(this.selectedtbluommaster, this.revertedtbluommaster, otbluommaster);
	},
	closeAddEdit: function(revert) {
		$('#modal-tbluommaster').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtbluommaster, this.selectedtbluommaster);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetbluommasterPaging(null);
		}
	},
	// Delete
	deletetbluommaster: function() {
		var record = ko.toJSON(this.selectedtbluommaster());
		FormFuncs.postAjax(
			'../tbluommasters_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Unit Of Measure deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttbluommasters: ko.observable(0),
	currentPagetbluommasters: ko.observable(0),
	noOfPagestbluommasters: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tbluommasters_Servlet',
			viewModel.counttbluommasters,
			viewModel.noOfPagestbluommasters,
			viewModel.pageSize,
			viewModel.searchTexttbluommaster());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tbluommasters_Servlet',
			viewModel.counttbluommasters,
			viewModel.noOfPagestbluommasters,
			viewModel.pageSize);
	},
	pagetbluommasterPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetbluommasters(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tbluommasters_Servlet',
			type,
			viewModel.oatbluommasters,
			otbluommaster,
			viewModel.currentPagetbluommasters,
			viewModel.noOfPagestbluommasters,
			viewModel.pageSize,
			viewModel.searchTexttbluommaster());
	},
	pagetbluommasterPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetbluommasters(-1); // force refresh
		
		FormFuncs.pagingPages('../tbluommasters_Servlet',
			type, 
			viewModel.oatbluommasters,
			otbluommaster,
			viewModel.currentPagetbluommasters,
			viewModel.noOfPagestbluommasters,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttbluommaster() == '')
			viewModel.pagetbluommasterPaging('f');
		else
			viewModel.pagetbluommasterPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttbluommaster() == '')
			viewModel.pagetbluommasterPaging('p');
		else
			viewModel.pagetbluommasterPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttbluommaster() == '')
			viewModel.pagetbluommasterPaging('n');
		else
			viewModel.pagetbluommasterPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttbluommaster() == '')
			viewModel.pagetbluommasterPaging('l');
		else
			viewModel.pagetbluommasterPagingSearch('l');
	}
};