$(".main-menu").each(function() {
	if (this.id == "Vendors") {
		$(this).addClass("active");
	}
});
function otblvendormaster(r) {
	this.selected = ko.observable(false);
	
	if (r) {
		this.chrRecCode = ko.observable(r.chrRecCode);
		this.chrInsertUser = ko.observable(r.chrInsertUser); 
		this.chrIpAddress = ko.observable(r.chrIpAddress); 
		this.chrUpdateUser = ko.observable(r.chrUpdateUser); 
		this.dtmInsertDate = ko.observable(r.dtmInsertDate); 
		this.dtmUpdateDate = ko.observable(r.dtmUpdateDate); 
		this.intPinCode = ko.observable(r.intPinCode); 
		this.tinActiveFlag = ko.observable(r.tinActiveFlag); 
		this.tinIsDirty = ko.observable(r.tinIsDirty); 
		this.vhrAddress1 = ko.observable(r.vhrAddress1); 
		this.vhrAddress2 = ko.observable(r.vhrAddress2); 
		this.vhrAddress3 = ko.observable(r.vhrAddress3); 
		this.vhrAddress4 = ko.observable(r.vhrAddress4); 
		this.vhrCode = ko.observable(r.vhrCode); 
		this.vhrCSTNo = ko.observable(r.vhrCSTNo); 
		this.vhrName = ko.observable(r.vhrName); 
		this.vhrPANNo = ko.observable(r.vhrPANNo); 
		this.vhrServiceTaxNo = ko.observable(r.vhrServiceTaxNo); 
		this.vhrTel1 = ko.observable(r.vhrTel1); 
		this.vhrTel2 = ko.observable(r.vhrTel2); 
		this.vhrVATNo = ko.observable(r.vhrVATNo);
		this.IUD = ko.observable(0);
	}
	else {
		this.chrRecCode = ko.revertableObservable(FormFuncs.getUUID().toString());
		this.chrInsertUser = ko.observable(""); 
		this.chrIpAddress = ko.observable(""); 
		this.chrUpdateUser = ko.observable(""); 
		this.dtmInsertDate = ko.observable(""); 
		this.dtmUpdateDate = ko.observable(""); 
		this.intPinCode = ko.observable(""); 
		this.tinActiveFlag = ko.observable(1); 
		this.tinIsDirty = ko.observable(0); 
		this.vhrAddress1 = ko.observable(""); 
		this.vhrAddress2 = ko.observable(""); 
		this.vhrAddress3 = ko.observable(""); 
		this.vhrAddress4 = ko.observable(""); 
		this.vhrCode = ko.observable(""); 
		this.vhrCSTNo = ko.observable(""); 
		this.vhrName = ko.observable(""); 
		this.vhrPANNo = ko.observable(""); 
		this.vhrServiceTaxNo = ko.observable(""); 
		this.vhrTel1 = ko.observable(""); 
		this.vhrTel2 = ko.observable(""); 
		this.vhrVATNo = ko.observable("");
		this.IUD = ko.observable(1);
	}
}

function oCompany(r){
	this.chrParentCode = r.chrRecCode;
	this.vhrName = r.vhrName;
}


$(function() {
	ko.applyBindings(viewModel);
	viewModel.pageCount();
	viewModel.firstPage();
//	viewModel.getCompanies();
	/*viewModel.getStores();*/
//	viewModel.getDefaultStatuses();
});

var viewModel = {
	// Array
	oatblvendormasters: ko.observableArray([]),
	oacompanies : ko.observableArray([]),
	oastores : ko.observableArray([]),
	// Selected and Revert 
	selectedtblvendormaster: ko.observable(),
	revertedtblvendormaster: ko.observable(),
	// Search
	searchTexttblvendormaster: ko.observable(''),
	searchtblvendormasters: function() {
		viewModel.pageCountSearch();
		viewModel.firstPage();
	},
	// Refresh
	refreshtblvendormasters: function() {
		viewModel.searchTexttblvendormaster(''),
		viewModel.pageCount();
		viewModel.firstPage();
	},
	
	/*getCompanies: function() {
		FormFuncs.getAjax('../tblcompanies_Servlet', {Type : 'all'}, true, function(output) {FormFuncs.pushRs(viewModel.oacompanies, output, oCompany);});
	},*/
	
	
	// Add and Edit
	posttblvendormaster: function() {
		var record = ko.toJSON(this.selectedtblvendormaster());
		FormFuncs.postAjax(
			'../tblvendormasters_Servlet',
			{Type: 'post', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					if(viewModel.selectedtblvendormaster().IUD() == 1) {
						NotifitMessage("Vendor saved successfully.", "success");
					}
					else {
						NotifitMessage("Vendor updated successfully.", "success");
					} 
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	addEdittblvendormaster: function(data) {
		if (data == null)
			this.selectedtblvendormaster(new otblvendormaster());
		else
			this.selectedtblvendormaster(data);
		
		FormFuncs.makeCopy(this.selectedtblvendormaster, this.revertedtblvendormaster, otblvendormaster);
	},
	closeAddEdit: function(revert) {
		$('#modal-tblvendormaster').modal('hide');
		if (revert) {
			FormFuncs.revertFromCopy(this.revertedtblvendormaster, this.selectedtblvendormaster);
		}
		else {
			viewModel.pageCount();
			viewModel.pagetblvendormasterPaging(null);
		}
	},
	// Delete
	deletetblvendormaster: function() {
		var record = ko.toJSON(this.selectedtblvendormaster());
		FormFuncs.postAjax(
			'../tblvendormasters_Servlet',
			{Type: 'delete', 
			 Record: record},
			true,
			function(output) {
				if (output.Success == true) {
					NotifitMessage("Vendor deleted successfully.", "success");
					viewModel.closeAddEdit(false);
				}
				else if (output.Success == false) {
					NotifitMessage(output.Message, "error");
				}
			}
		);
	},
	// Paging 
	counttblvendormasters: ko.observable(0),
	currentPagetblvendormasters: ko.observable(0),
	noOfPagestblvendormasters: ko.observable(0),
	pageSize: 20,
	pageCountSearch: function() {
		FormFuncs.pageCountSearch('../tblvendormasters_Servlet',
			viewModel.counttblvendormasters,
			viewModel.noOfPagestblvendormasters,
			viewModel.pageSize,
			viewModel.searchTexttblvendormaster());
	},
	pageCount: function() {
		FormFuncs.pageCount('../tblvendormasters_Servlet',
			viewModel.counttblvendormasters,
			viewModel.noOfPagestblvendormasters,
			viewModel.pageSize);
	},
	pagetblvendormasterPagingSearch: function(type) {
		if (type == 'f')
			viewModel.currentPagetblvendormasters(-1); // force refresh
		
		FormFuncs.pagingPagesSearch('../tblvendormasters_Servlet',
			type,
			viewModel.oatblvendormasters,
			otblvendormaster,
			viewModel.currentPagetblvendormasters,
			viewModel.noOfPagestblvendormasters,
			viewModel.pageSize,
			viewModel.searchTexttblvendormaster());
	},
	pagetblvendormasterPaging: function(type) {
		if (type == 'f')
			viewModel.currentPagetblvendormasters(-1); // force refresh
		
		FormFuncs.pagingPages('../tblvendormasters_Servlet',
			type, 
			viewModel.oatblvendormasters,
			otblvendormaster,
			viewModel.currentPagetblvendormasters,
			viewModel.noOfPagestblvendormasters,
			viewModel.pageSize);
	},
	firstPage: function() {
		if (viewModel.searchTexttblvendormaster() == '')
			viewModel.pagetblvendormasterPaging('f');
		else
			viewModel.pagetblvendormasterPagingSearch('f');
	},
	prevPage: function() {
		if (viewModel.searchTexttblvendormaster() == '')
			viewModel.pagetblvendormasterPaging('p');
		else
			viewModel.pagetblvendormasterPagingSearch('p');
	},
	nextPage: function() {
		if (viewModel.searchTexttblvendormaster() == '')
			viewModel.pagetblvendormasterPaging('n');
		else
			viewModel.pagetblvendormasterPagingSearch('n');
	},
	lastPage: function() {
		if (viewModel.searchTexttblvendormaster() == '')
			viewModel.pagetblvendormasterPaging('l');
		else
			viewModel.pagetblvendormasterPagingSearch('l');
	}
};

