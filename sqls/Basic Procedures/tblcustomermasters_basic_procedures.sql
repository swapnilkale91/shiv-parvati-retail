delimiter $$

drop procedure if exists USP_tblcustomermasters_insert $$

delimiter $$
create procedure USP_tblcustomermasters_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intPinCode int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrAddress1 varchar(100),
	p_vhrAddress2 varchar(100),
	p_vhrAddress3 varchar(100),
	p_vhrAddress4 varchar(100),
	p_vhrCode varchar(20),
	p_vhrName varchar(100),
	p_vhrPANNo varchar(100),
	p_vhrTel1 varchar(20),
	p_vhrTel2 varchar(20)
)
begin
	insert into tblcustomermasters(
		chrInsertUser,
		chrIpAddress,
		chrRecCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		intPinCode,
		tinActiveFlag,
		tinIsDirty,
		vhrAddress1,
		vhrAddress2,
		vhrAddress3,
		vhrAddress4,
		vhrCode,
		vhrName,
		vhrPANNo,
		vhrTel1,
		vhrTel2
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrRecCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intPinCode,
		p_tinActiveFlag,
		p_tinIsDirty,
		p_vhrAddress1,
		p_vhrAddress2,
		p_vhrAddress3,
		p_vhrAddress4,
		p_vhrCode,
		p_vhrName,
		p_vhrPANNo,
		p_vhrTel1,
		p_vhrTel2
	);
end$$

delimiter $$

drop procedure if exists USP_tblcustomermasters_update $$

delimiter $$
create procedure USP_tblcustomermasters_update(
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_intPinCode int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrAddress1 varchar(100),
	p_vhrAddress2 varchar(100),
	p_vhrAddress3 varchar(100),
	p_vhrAddress4 varchar(100),
	p_vhrCode varchar(20),
	p_vhrName varchar(100),
	p_vhrPANNo varchar(100),
	p_vhrTel1 varchar(20),
	p_vhrTel2 varchar(20)
)
begin
	update tblcustomermasters set
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		intPinCode = p_intPinCode,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty,
		vhrAddress1 = p_vhrAddress1,
		vhrAddress2 = p_vhrAddress2,
		vhrAddress3 = p_vhrAddress3,
		vhrAddress4 = p_vhrAddress4,
		vhrCode = p_vhrCode,
		vhrName = p_vhrName,
		vhrPANNo = p_vhrPANNo,
		vhrTel1 = p_vhrTel1,
		vhrTel2 = p_vhrTel2
	where chrRecCode = p_chrRecCode;
end$$
