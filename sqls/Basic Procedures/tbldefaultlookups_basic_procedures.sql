delimiter $$

drop procedure if exists USP_tbldefaultlookups_insert $$

delimiter $$
create procedure USP_tbldefaultlookups_insert(
	p_chrRecCode char(36),
	p_LookupTypeCode int,
	p_TheName varchar(100)
)
begin
	insert into tbldefaultlookups(
		chrRecCode,
		LookupTypeCode,
		TheName
	) values (
		p_chrRecCode,
		p_LookupTypeCode,
		p_TheName
	);
end$$

delimiter $$

drop procedure if exists USP_tbldefaultlookups_update $$

delimiter $$
create procedure USP_tbldefaultlookups_update(
	p_chrRecCode char(36),
	p_LookupTypeCode int,
	p_TheName varchar(100)
)
begin
	update tbldefaultlookups set
		chrRecCode = p_chrRecCode,
		LookupTypeCode = p_LookupTypeCode,
		TheName = p_TheName
	where chrRecCode = p_chrRecCode;
end$$
