delimiter $$

drop procedure if exists USP_tblemployeecategories_insert $$

delimiter $$
create procedure USP_tblemployeecategories_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrCode varchar(20),
	p_vhrName varchar(100)
)
begin
	insert into tblemployeecategories(
		chrInsertUser,
		chrIpAddress,
		chrRecCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		tinActiveFlag,
		tinIsDirty,
		vhrCode,
		vhrName
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrRecCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_tinActiveFlag,
		p_tinIsDirty,
		p_vhrCode,
		p_vhrName
	);
end$$

delimiter $$

drop procedure if exists USP_tblemployeecategories_update $$

delimiter $$
create procedure USP_tblemployeecategories_update(
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrCode varchar(20),
	p_vhrName varchar(100)
)
begin
	update tblemployeecategories set
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty,
		vhrCode = p_vhrCode,
		vhrName = p_vhrName
	where chrRecCode = p_chrRecCode;
end$$
