delimiter $$

drop procedure if exists USP_tblemployeemasters_insert $$

delimiter $$
create procedure USP_tblemployeemasters_insert(
	p_chrCompanyCode varchar(36),
	p_chrEmployeeCategoryCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intPinCode int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_tinLockUser int,
	p_vhrAddress1 varchar(100),
	p_vhrAddress2 varchar(100),
	p_vhrAddress3 varchar(100),
	p_vhrAddress4 varchar(100),
	p_vhrCode varchar(20),
	p_vhrName varchar(100),
	p_vhrPANNo varchar(100),
	p_vhrPassword varchar(100),
	p_vhrTel1 varchar(20),
	p_vhrTel2 varchar(20),
	p_vhrUserName varchar(100)
)
begin
	insert into tblemployeemasters(
		chrCompanyCode,
		chrEmployeeCategoryCode,
		chrInsertUser,
		chrIpAddress,
		chrRecCode,
		chrStoreCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		intPinCode,
		tinActiveFlag,
		tinIsDirty,
		tinLockUser,
		vhrAddress1,
		vhrAddress2,
		vhrAddress3,
		vhrAddress4,
		vhrCode,
		vhrName,
		vhrPANNo,
		vhrPassword,
		vhrTel1,
		vhrTel2,
		vhrUserName
	) values (
		p_chrCompanyCode,
		p_chrEmployeeCategoryCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intPinCode,
		p_tinActiveFlag,
		p_tinIsDirty,
		p_tinLockUser,
		p_vhrAddress1,
		p_vhrAddress2,
		p_vhrAddress3,
		p_vhrAddress4,
		p_vhrCode,
		p_vhrName,
		p_vhrPANNo,
		p_vhrPassword,
		p_vhrTel1,
		p_vhrTel2,
		p_vhrUserName
	);
end$$

delimiter $$

drop procedure if exists USP_tblemployeemasters_update $$

delimiter $$
create procedure USP_tblemployeemasters_update(
	p_chrCompanyCode varchar(36),
	p_chrEmployeeCategoryCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_intPinCode int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_tinLockUser int,
	p_vhrAddress1 varchar(100),
	p_vhrAddress2 varchar(100),
	p_vhrAddress3 varchar(100),
	p_vhrAddress4 varchar(100),
	p_vhrCode varchar(20),
	p_vhrName varchar(100),
	p_vhrPANNo varchar(100),
	p_vhrPassword varchar(100),
	p_vhrTel1 varchar(20),
	p_vhrTel2 varchar(20),
	p_vhrUserName varchar(100)
)
begin
	update tblemployeemasters set
		chrCompanyCode = p_chrCompanyCode,
		chrEmployeeCategoryCode = p_chrEmployeeCategoryCode,
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		intPinCode = p_intPinCode,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty,
		tinLockUser = p_tinLockUser,
		vhrAddress1 = p_vhrAddress1,
		vhrAddress2 = p_vhrAddress2,
		vhrAddress3 = p_vhrAddress3,
		vhrAddress4 = p_vhrAddress4,
		vhrCode = p_vhrCode,
		vhrName = p_vhrName,
		vhrPANNo = p_vhrPANNo,
		vhrPassword = p_vhrPassword,
		vhrTel1 = p_vhrTel1,
		vhrTel2 = p_vhrTel2,
		vhrUserName = p_vhrUserName
	where chrRecCode = p_chrRecCode;
end$$
