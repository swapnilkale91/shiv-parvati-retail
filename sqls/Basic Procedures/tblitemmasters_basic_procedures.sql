delimiter $$

drop procedure if exists USP_tblitemmasters_insert $$

delimiter $$
create procedure USP_tblitemmasters_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCategoryCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUnitOfMeasureCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intReOrderQuantity int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_tinLoseSold int,
	p_vhrBarcode varchar(100),
	p_vhrCode varchar(20),
	p_vhrDescription varchar(1000),
	p_vhrName varchar(100),
	p_vhrShortName varchar(20)
)
begin
	insert into tblitemmasters(
		chrInsertUser,
		chrIpAddress,
		chrItemCategoryCode,
		chrRecCode,
		chrUnitOfMeasureCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		intReOrderQuantity,
		tinActiveFlag,
		tinIsDirty,
		tinLoseSold,
		vhrBarcode,
		vhrCode,
		vhrDescription,
		vhrName,
		vhrShortName
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrItemCategoryCode,
		p_chrRecCode,
		p_chrUnitOfMeasureCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intReOrderQuantity,
		p_tinActiveFlag,
		p_tinIsDirty,
		p_tinLoseSold,
		p_vhrBarcode,
		p_vhrCode,
		p_vhrDescription,
		p_vhrName,
		p_vhrShortName
	);
end$$

delimiter $$

drop procedure if exists USP_tblitemmasters_update $$

delimiter $$
create procedure USP_tblitemmasters_update(
	p_chrIpAddress varchar(100),
	p_chrItemCategoryCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUnitOfMeasureCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_intReOrderQuantity int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_tinLoseSold int,
	p_vhrBarcode varchar(100),
	p_vhrCode varchar(20),
	p_vhrDescription varchar(1000),
	p_vhrName varchar(100),
	p_vhrShortName varchar(20)
)
begin
	update tblitemmasters set
		chrIpAddress = p_chrIpAddress,
		chrItemCategoryCode = p_chrItemCategoryCode,
		chrRecCode = p_chrRecCode,
		chrUnitOfMeasureCode = p_chrUnitOfMeasureCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		intReOrderQuantity = p_intReOrderQuantity,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty,
		tinLoseSold = p_tinLoseSold,
		vhrBarcode = p_vhrBarcode,
		vhrCode = p_vhrCode,
		vhrDescription = p_vhrDescription,
		vhrName = p_vhrName,
		vhrShortName = p_vhrShortName
	where chrRecCode = p_chrRecCode;
end$$
