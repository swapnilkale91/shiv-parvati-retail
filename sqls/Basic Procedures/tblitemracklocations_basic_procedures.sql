delimiter $$

drop procedure if exists USP_tblitemracklocations_insert $$

delimiter $$
create procedure USP_tblitemracklocations_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrRackLocationCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_tinIsDirty int,
	p_tinStatus int
)
begin
	insert into tblitemracklocations(
		chrInsertUser,
		chrIpAddress,
		chrItemCode,
		chrRackLocationCode,
		chrRecCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		tinIsDirty,
		tinStatus
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrItemCode,
		p_chrRackLocationCode,
		p_chrRecCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_tinIsDirty,
		p_tinStatus
	);
end$$

delimiter $$

drop procedure if exists USP_tblitemracklocations_update $$

delimiter $$
create procedure USP_tblitemracklocations_update(
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrRackLocationCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_tinIsDirty int,
	p_tinStatus int
)
begin
	update tblitemracklocations set
		chrIpAddress = p_chrIpAddress,
		chrItemCode = p_chrItemCode,
		chrRackLocationCode = p_chrRackLocationCode,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		tinIsDirty = p_tinIsDirty,
		tinStatus = p_tinStatus
	where chrRecCode = p_chrRecCode;
end$$
