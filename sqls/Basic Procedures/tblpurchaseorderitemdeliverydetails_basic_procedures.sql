delimiter $$

drop procedure if exists USP_tblpurchaseorderitemdeliverydetails_insert $$

delimiter $$
create procedure USP_tblpurchaseorderitemdeliverydetails_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreLocationCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decQuantity decimal(20,2),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_tinIsDirty int
)
begin
	insert into tblpurchaseorderitemdeliverydetails(
		chrInsertUser,
		chrIpAddress,
		chrParentCode,
		chrRecCode,
		chrStoreLocationCode,
		chrUpdateUser,
		decQuantity,
		dtmInsertDate,
		dtmUpdateDate,
		tinIsDirty
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrParentCode,
		p_chrRecCode,
		p_chrStoreLocationCode,
		p_chrUpdateUser,
		p_decQuantity,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_tinIsDirty
	);
end$$

delimiter $$

drop procedure if exists USP_tblpurchaseorderitemdeliverydetails_update $$

delimiter $$
create procedure USP_tblpurchaseorderitemdeliverydetails_update(
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreLocationCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decQuantity decimal(20,2),
	p_dtmUpdateDate datetime,
	p_tinIsDirty int
)
begin
	update tblpurchaseorderitemdeliverydetails set
		chrIpAddress = p_chrIpAddress,
		chrParentCode = p_chrParentCode,
		chrRecCode = p_chrRecCode,
		chrStoreLocationCode = p_chrStoreLocationCode,
		chrUpdateUser = p_chrUpdateUser,
		decQuantity = p_decQuantity,
		dtmUpdateDate = p_dtmUpdateDate,
		tinIsDirty = p_tinIsDirty
	where chrRecCode = p_chrRecCode;
end$$
