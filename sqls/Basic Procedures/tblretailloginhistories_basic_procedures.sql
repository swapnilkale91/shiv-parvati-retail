delimiter $$

drop procedure if exists USP_tblretailloginhistories_insert $$

delimiter $$
create procedure USP_tblretailloginhistories_insert(
	p_chrCustomerCode char(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode char(36),
	p_dteLogInTime datetime,
	p_dteLogoutTime datetime,
	p_tinActiveFlag int,
	p_tinIsDirty int
)
begin
	insert into tblretailloginhistories(
		chrCustomerCode,
		chrIpAddress,
		chrRecCode,
		dteLogInTime,
		dteLogoutTime,
		tinActiveFlag,
		tinIsDirty
	) values (
		p_chrCustomerCode,
		p_chrIpAddress,
		p_chrRecCode,
		p_dteLogInTime,
		p_dteLogoutTime,
		p_tinActiveFlag,
		p_tinIsDirty
	);
end$$

delimiter $$

drop procedure if exists USP_tblretailloginhistories_update $$

delimiter $$
create procedure USP_tblretailloginhistories_update(
	p_chrCustomerCode char(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode char(36),
	p_dteLogInTime datetime,
	p_dteLogoutTime datetime,
	p_tinActiveFlag int,
	p_tinIsDirty int
)
begin
	update tblretailloginhistories set
		chrCustomerCode = p_chrCustomerCode,
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		dteLogInTime = p_dteLogInTime,
		dteLogoutTime = p_dteLogoutTime,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty
	where chrRecCode = p_chrRecCode;
end$$
