delimiter $$

drop procedure if exists USP_tblsales_insert $$

delimiter $$
create procedure USP_tblsales_Insert(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrCustomerCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrOperatorCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decDiscountTotal decimal(20,2),
	p_decItemTotal decimal(20,2),
	p_decTotalAmount decimal(20,2),
	p_decVATTaxTotal decimal(20,2),
	p_dtmAuthorizationDateOne datetime,
	p_dtmAuthorizationDateThree datetime,
	p_dtmAuthorizationDateTwo datetime,
	p_dtmBillDate datetime,
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intAuthorizationLevel int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrBillNo varchar(100)
)
begin
	insert into tblsales(
		chrAuthorizationUserOne,
		chrAuthorizationUserThree,
		chrAuthorizationUserTwo,
		chrCompanyCode,
		chrCustomerCode,
		chrInsertUser,
		chrIpAddress,
		chrOperatorCode,
		chrRecCode,
		chrStoreCode,
		chrUpdateUser,
		decDiscountTotal,
		decItemTotal,
		decTotalAmount,
		decVATTaxTotal,
		dtmAuthorizationDateOne,
		dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo,
		dtmBillDate,
		dtmInsertDate,
		dtmUpdateDate,
		intAuthorizationLevel,
		intStatus,
		tinIsDirty,
		vhrBillNo
	) values (
		p_chrAuthorizationUserOne,
		p_chrAuthorizationUserThree,
		p_chrAuthorizationUserTwo,
		p_chrCompanyCode,
		p_chrCustomerCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrOperatorCode,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrUpdateUser,
		p_decDiscountTotal,
		p_decItemTotal,
		p_decTotalAmount,
		p_decVATTaxTotal,
		p_dtmAuthorizationDateOne,
		p_dtmAuthorizationDateThree,
		p_dtmAuthorizationDateTwo,
		p_dtmBillDate,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intAuthorizationLevel,
		p_intStatus,
		p_tinIsDirty,
		p_vhrBillNo
	);
end$$

delimiter $$

drop procedure if exists USP_tblsales_update $$

delimiter $$
create procedure USP_tblsales_update(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrCustomerCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrOperatorCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decDiscountTotal decimal(20,2),
	p_decItemTotal decimal(20,2),
	p_decTotalAmount decimal(20,2),
	p_decVATTaxTotal decimal(20,2),
	p_dtmAuthorizationDateOne datetime,
	p_dtmAuthorizationDateThree datetime,
	p_dtmAuthorizationDateTwo datetime,
	p_dtmBillDate datetime,
	p_dtmUpdateDate datetime,
	p_intAuthorizationLevel int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrBillNo varchar(100)
)
begin
	update tblsales set
		chrAuthorizationUserOne = p_chrAuthorizationUserOne,
		chrAuthorizationUserThree = p_chrAuthorizationUserThree,
		chrAuthorizationUserTwo = p_chrAuthorizationUserTwo,
		chrCompanyCode = p_chrCompanyCode,
		chrCustomerCode = p_chrCustomerCode,
		chrIpAddress = p_chrIpAddress,
		chrOperatorCode = p_chrOperatorCode,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrUpdateUser = p_chrUpdateUser,
		decDiscountTotal = p_decDiscountTotal,
		decItemTotal = p_decItemTotal,
		decTotalAmount = p_decTotalAmount,
		decVATTaxTotal = p_decVATTaxTotal,
		dtmAuthorizationDateOne = p_dtmAuthorizationDateOne,
		dtmAuthorizationDateThree = p_dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo = p_dtmAuthorizationDateTwo,
		dtmBillDate = p_dtmBillDate,
		dtmUpdateDate = p_dtmUpdateDate,
		intAuthorizationLevel = p_intAuthorizationLevel,
		intStatus = p_intStatus,
		tinIsDirty = p_tinIsDirty,
		vhrBillNo = p_vhrBillNo
	where chrRecCode = p_chrRecCode;
end$$
