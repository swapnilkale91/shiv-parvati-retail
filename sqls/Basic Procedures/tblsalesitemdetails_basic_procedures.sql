delimiter $$

drop procedure if exists USP_tblsalesitemdetails_insert $$

delimiter $$
create procedure USP_tblsalesitemdetails_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_chrVatTaxRecCode varchar(36),
	p_decCustomerDiscount decimal(20,2),
	p_decCustomerRate decimal(20,2),
	p_decItemCost decimal(20,2),
	p_decMRP decimal(20,2),
	p_decQuantity decimal(20,2),
	p_decVatTaxAmount decimal(20,2),
	p_decVatTaxPercentage decimal(20,2),
	p_dtmExpiryDate datetime,
	p_dtmInsertDate datetime,
	p_dtmManufactureDate datetime,
	p_dtmUpdateDate datetime,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrBatchNo varchar(100),
	p_vhrDescription varchar(100),
	p_vhrOurGeneratedBarCode varchar(100),
	p_vhrShortName varchar(20)
)
begin
	insert into tblsalesitemdetails(
		chrInsertUser,
		chrIpAddress,
		chrItemCode,
		chrParentCode,
		chrRecCode,
		chrUpdateUser,
		chrVatTaxRecCode,
		decCustomerDiscount,
		decCustomerRate,
		decItemCost,
		decMRP,
		decQuantity,
		decVatTaxAmount,
		decVatTaxPercentage,
		dtmExpiryDate,
		dtmInsertDate,
		dtmManufactureDate,
		dtmUpdateDate,
		intStatus,
		tinIsDirty,
		vhrBatchNo,
		vhrDescription,
		vhrOurGeneratedBarCode,
		vhrShortName
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrItemCode,
		p_chrParentCode,
		p_chrRecCode,
		p_chrUpdateUser,
		p_chrVatTaxRecCode,
		p_decCustomerDiscount,
		p_decCustomerRate,
		p_decItemCost,
		p_decMRP,
		p_decQuantity,
		p_decVatTaxAmount,
		p_decVatTaxPercentage,
		p_dtmExpiryDate,
		p_dtmInsertDate,
		p_dtmManufactureDate,
		p_dtmUpdateDate,
		p_intStatus,
		p_tinIsDirty,
		p_vhrBatchNo,
		p_vhrDescription,
		p_vhrOurGeneratedBarCode,
		p_vhrShortName
	);
end$$

delimiter $$

drop procedure if exists USP_tblsalesitemdetails_update $$

delimiter $$
create procedure USP_tblsalesitemdetails_update(
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_chrVatTaxRecCode varchar(36),
	p_decCustomerDiscount decimal(20,2),
	p_decCustomerRate decimal(20,2),
	p_decItemCost decimal(20,2),
	p_decMRP decimal(20,2),
	p_decQuantity decimal(20,2),
	p_decVatTaxAmount decimal(20,2),
	p_decVatTaxPercentage decimal(20,2),
	p_dtmExpiryDate datetime,
	p_dtmManufactureDate datetime,
	p_dtmUpdateDate datetime,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrBatchNo varchar(100),
	p_vhrDescription varchar(100),
	p_vhrOurGeneratedBarCode varchar(100),
	p_vhrShortName varchar(20)
)
begin
	update tblsalesitemdetails set
		chrIpAddress = p_chrIpAddress,
		chrItemCode = p_chrItemCode,
		chrParentCode = p_chrParentCode,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		chrVatTaxRecCode = p_chrVatTaxRecCode,
		decCustomerDiscount = p_decCustomerDiscount,
		decCustomerRate = p_decCustomerRate,
		decItemCost = p_decItemCost,
		decMRP = p_decMRP,
		decQuantity = p_decQuantity,
		decVatTaxAmount = p_decVatTaxAmount,
		decVatTaxPercentage = p_decVatTaxPercentage,
		dtmExpiryDate = p_dtmExpiryDate,
		dtmManufactureDate = p_dtmManufactureDate,
		dtmUpdateDate = p_dtmUpdateDate,
		intStatus = p_intStatus,
		tinIsDirty = p_tinIsDirty,
		vhrBatchNo = p_vhrBatchNo,
		vhrDescription = p_vhrDescription,
		vhrOurGeneratedBarCode = p_vhrOurGeneratedBarCode,
		vhrShortName = p_vhrShortName
	where chrRecCode = p_chrRecCode;
end$$
