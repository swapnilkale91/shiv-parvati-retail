delimiter $$

drop procedure if exists USP_tblsalespaymentdetails_insert $$

delimiter $$
create procedure USP_tblsalespaymentdetails_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrPaymentModeCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decAmount decimal(20,2),
	p_decBalance decimal(20,2),
	p_decCashReceived decimal(20,2),
	p_dtmCouponDenomination datetime,
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_tinIsDirty int,
	p_vhrCardDetails varchar(36)
)
begin
	insert into tblsalespaymentdetails(
		chrInsertUser,
		chrIpAddress,
		chrParentCode,
		chrPaymentModeCode,
		chrRecCode,
		chrUpdateUser,
		decAmount,
		decBalance,
		decCashReceived,
		dtmCouponDenomination,
		dtmInsertDate,
		dtmUpdateDate,
		tinIsDirty,
		vhrCardDetails
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrParentCode,
		p_chrPaymentModeCode,
		p_chrRecCode,
		p_chrUpdateUser,
		p_decAmount,
		p_decBalance,
		p_decCashReceived,
		p_dtmCouponDenomination,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_tinIsDirty,
		p_vhrCardDetails
	);
end$$

delimiter $$

drop procedure if exists USP_tblsalespaymentdetails_update $$

delimiter $$
create procedure USP_tblsalespaymentdetails_update(
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrPaymentModeCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decAmount decimal(20,2),
	p_decBalance decimal(20,2),
	p_decCashReceived decimal(20,2),
	p_dtmCouponDenomination datetime,
	p_dtmUpdateDate datetime,
	p_tinIsDirty int,
	p_vhrCardDetails varchar(36)
)
begin
	update tblsalespaymentdetails set
		chrIpAddress = p_chrIpAddress,
		chrParentCode = p_chrParentCode,
		chrPaymentModeCode = p_chrPaymentModeCode,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		decAmount = p_decAmount,
		decBalance = p_decBalance,
		decCashReceived = p_decCashReceived,
		dtmCouponDenomination = p_dtmCouponDenomination,
		dtmUpdateDate = p_dtmUpdateDate,
		tinIsDirty = p_tinIsDirty,
		vhrCardDetails = p_vhrCardDetails
	where chrRecCode = p_chrRecCode;
end$$
