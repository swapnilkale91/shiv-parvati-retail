delimiter $$

drop procedure if exists USP_tblstockadjustmentitemdetails_insert $$

delimiter $$
create procedure USP_tblstockadjustmentitemdetails_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decQuantity decimal(20,2),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intAdjustmentType int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrDescription varchar(100)
)
begin
	insert into tblstockadjustmentitemdetails(
		chrInsertUser,
		chrIpAddress,
		chrItemCode,
		chrParentCode,
		chrRecCode,
		chrUpdateUser,
		decQuantity,
		dtmInsertDate,
		dtmUpdateDate,
		intAdjustmentType,
		intStatus,
		tinIsDirty,
		vhrDescription
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrItemCode,
		p_chrParentCode,
		p_chrRecCode,
		p_chrUpdateUser,
		p_decQuantity,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intAdjustmentType,
		p_intStatus,
		p_tinIsDirty,
		p_vhrDescription
	);
end$$

delimiter $$

drop procedure if exists USP_tblstockadjustmentitemdetails_update $$

delimiter $$
create procedure USP_tblstockadjustmentitemdetails_update(
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decQuantity decimal(20,2),
	p_dtmUpdateDate datetime,
	p_intAdjustmentType int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrDescription varchar(100)
)
begin
	update tblstockadjustmentitemdetails set
		chrIpAddress = p_chrIpAddress,
		chrItemCode = p_chrItemCode,
		chrParentCode = p_chrParentCode,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		decQuantity = p_decQuantity,
		dtmUpdateDate = p_dtmUpdateDate,
		intAdjustmentType = p_intAdjustmentType,
		intStatus = p_intStatus,
		tinIsDirty = p_tinIsDirty,
		vhrDescription = p_vhrDescription
	where chrRecCode = p_chrRecCode;
end$$
