delimiter $$

drop procedure if exists USP_tblstockadjustments_insert $$

delimiter $$
create procedure USP_tblstockadjustments_insert(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmAdjustmentDate datetime,
	p_dtmAuthorizationDateOne datetime,
	p_dtmAuthorizationDateThree datetime,
	p_dtmAuthorizationDateTwo datetime,
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intAuthorizationLevel int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrAdjustmentNo varchar(100)
)
begin
	insert into tblstockadjustments(
		chrAuthorizationUserOne,
		chrAuthorizationUserThree,
		chrAuthorizationUserTwo,
		chrCompanyCode,
		chrInsertUser,
		chrIpAddress,
		chrRecCode,
		chrStoreCode,
		chrUpdateUser,
		dtmAdjustmentDate,
		dtmAuthorizationDateOne,
		dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo,
		dtmInsertDate,
		dtmUpdateDate,
		intAuthorizationLevel,
		intStatus,
		tinIsDirty,
		vhrAdjustmentNo
	) values (
		p_chrAuthorizationUserOne,
		p_chrAuthorizationUserThree,
		p_chrAuthorizationUserTwo,
		p_chrCompanyCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrUpdateUser,
		p_dtmAdjustmentDate,
		p_dtmAuthorizationDateOne,
		p_dtmAuthorizationDateThree,
		p_dtmAuthorizationDateTwo,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intAuthorizationLevel,
		p_intStatus,
		p_tinIsDirty,
		p_vhrAdjustmentNo
	);
end$$

delimiter $$

drop procedure if exists USP_tblstockadjustments_update $$

delimiter $$
create procedure USP_tblstockadjustments_update(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmAdjustmentDate datetime,
	p_dtmAuthorizationDateOne datetime,
	p_dtmAuthorizationDateThree datetime,
	p_dtmAuthorizationDateTwo datetime,
	p_dtmUpdateDate datetime,
	p_intAuthorizationLevel int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrAdjustmentNo varchar(100)
)
begin
	update tblstockadjustments set
		chrAuthorizationUserOne = p_chrAuthorizationUserOne,
		chrAuthorizationUserThree = p_chrAuthorizationUserThree,
		chrAuthorizationUserTwo = p_chrAuthorizationUserTwo,
		chrCompanyCode = p_chrCompanyCode,
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmAdjustmentDate = p_dtmAdjustmentDate,
		dtmAuthorizationDateOne = p_dtmAuthorizationDateOne,
		dtmAuthorizationDateThree = p_dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo = p_dtmAuthorizationDateTwo,
		dtmUpdateDate = p_dtmUpdateDate,
		intAuthorizationLevel = p_intAuthorizationLevel,
		intStatus = p_intStatus,
		tinIsDirty = p_tinIsDirty,
		vhrAdjustmentNo = p_vhrAdjustmentNo
	where chrRecCode = p_chrRecCode;
end$$
