delimiter $$

drop procedure if exists USP_tblstocks_insert $$

delimiter $$
create procedure USP_tblstocks_insert(
	p_chrCompanyCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decQuantity decimal(20,2),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intStockType int,
	p_tinIsDirty int
)
begin
	insert into tblstocks(
		chrCompanyCode,
		chrInsertUser,
		chrIpAddress,
		chrItemCode,
		chrRecCode,
		chrStoreCode,
		chrUpdateUser,
		decQuantity,
		dtmInsertDate,
		dtmUpdateDate,
		intStockType,
		tinIsDirty
	) values (
		p_chrCompanyCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrItemCode,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrUpdateUser,
		p_decQuantity,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intStockType,
		p_tinIsDirty
	);
end$$

delimiter $$

drop procedure if exists USP_tblstocks_update $$

delimiter $$
create procedure USP_tblstocks_update(
	p_chrCompanyCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decQuantity decimal(20,2),
	p_dtmUpdateDate datetime,
	p_intStockType int,
	p_tinIsDirty int
)
begin
	update tblstocks set
		chrCompanyCode = p_chrCompanyCode,
		chrIpAddress = p_chrIpAddress,
		chrItemCode = p_chrItemCode,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrUpdateUser = p_chrUpdateUser,
		decQuantity = p_decQuantity,
		dtmUpdateDate = p_dtmUpdateDate,
		intStockType = p_intStockType,
		tinIsDirty = p_tinIsDirty
	where chrRecCode = p_chrRecCode;
end$$
