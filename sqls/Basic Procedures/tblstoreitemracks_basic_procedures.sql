delimiter $$

drop procedure if exists USP_tblstoreitemracks_Insert $$

delimiter $$
create procedure USP_tblstoreitemracks_Insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intStoreLocationCategory int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrCode varchar(20),
	p_vhrName varchar(100)
)
begin
	insert into tblstoreitemracks(
		chrInsertUser,
		chrIpAddress,
		chrParentCode,
		chrRecCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		intStoreLocationCategory,
		tinActiveFlag,
		tinIsDirty,
		vhrCode,
		vhrName
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrParentCode,
		p_chrRecCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intStoreLocationCategory,
		p_tinActiveFlag,
		p_tinIsDirty,
		p_vhrCode,
		p_vhrName
	);
end$$

delimiter $$

drop procedure if exists USP_tblstoreitemracks_Update $$

delimiter $$
create procedure USP_tblstoreitemracks_Update(
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_intStoreLocationCategory int,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrCode varchar(20),
	p_vhrName varchar(100)
)
begin
	update tblstoreitemracks set
		chrIpAddress = p_chrIpAddress,
		chrParentCode = p_chrParentCode,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		intStoreLocationCategory = p_intStoreLocationCategory,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty,
		vhrCode = p_vhrCode,
		vhrName = p_vhrName
	where chrRecCode = p_chrRecCode;
end$$
