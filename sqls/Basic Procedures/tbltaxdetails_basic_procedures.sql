delimiter $$

drop procedure if exists USP_tbltaxdetails_insert $$

delimiter $$
create procedure USP_tbltaxdetails_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decPercentage decimal(10,2),
	p_dtmEffectiveDate datetime,
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrName varchar(100)
)
begin
	insert into tbltaxdetails(
		chrInsertUser,
		chrIpAddress,
		chrParentCode,
		chrRecCode,
		chrUpdateUser,
		decPercentage,
		dtmEffectiveDate,
		dtmInsertDate,
		dtmUpdateDate,
		tinActiveFlag,
		tinIsDirty,
		vhrName
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrParentCode,
		p_chrRecCode,
		p_chrUpdateUser,
		p_decPercentage,
		UFn_UTCDate(p_dtmEffectiveDate),
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_tinActiveFlag,
		p_tinIsDirty,
		p_vhrName
	);
end$$

delimiter $$

drop procedure if exists USP_tbltaxdetails_update $$

delimiter $$
create procedure USP_tbltaxdetails_update(
	p_chrIpAddress varchar(100),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_decPercentage decimal(10,2),
	p_dtmEffectiveDate datetime,
	p_dtmUpdateDate datetime,
	p_tinActiveFlag int,
	p_tinIsDirty int,
	p_vhrName varchar(100)
)
begin
	update tbltaxdetails set
		chrIpAddress = p_chrIpAddress,
		chrParentCode = p_chrParentCode,
		chrRecCode = p_chrRecCode,
		chrUpdateUser = p_chrUpdateUser,
		decPercentage = p_decPercentage,
		dtmEffectiveDate = UFn_UTCDate(p_dtmEffectiveDate),
		dtmUpdateDate = p_dtmUpdateDate,
		tinActiveFlag = p_tinActiveFlag,
		tinIsDirty = p_tinIsDirty,
		vhrName = p_vhrName
	where chrRecCode = p_chrRecCode;
end$$
