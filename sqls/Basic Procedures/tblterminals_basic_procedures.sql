delimiter $$

drop procedure if exists USP_tblterminals_insert $$

delimiter $$
create procedure USP_tblterminals_insert(
	p_chrCompanyCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrTerminalCode varchar(100),
	p_chrUpdateUser varchar(36),
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_tinIsDirty int,
	p_tinStatus int
)
begin
	insert into tblterminals(
		chrCompanyCode,
		chrInsertUser,
		chrIpAddress,
		chrRecCode,
		chrStoreCode,
		chrTerminalCode,
		chrUpdateUser,
		dtmInsertDate,
		dtmUpdateDate,
		tinIsDirty,
		tinStatus
	) values (
		p_chrCompanyCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrTerminalCode,
		p_chrUpdateUser,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_tinIsDirty,
		p_tinStatus
	);
end$$

delimiter $$

drop procedure if exists USP_tblterminals_update $$

delimiter $$
create procedure USP_tblterminals_update(
	p_chrCompanyCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrTerminalCode varchar(100),
	p_chrUpdateUser varchar(36),
	p_dtmUpdateDate datetime,
	p_tinIsDirty int,
	p_tinStatus int
)
begin
	update tblterminals set
		chrCompanyCode = p_chrCompanyCode,
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrTerminalCode = p_chrTerminalCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmUpdateDate = p_dtmUpdateDate,
		tinIsDirty = p_tinIsDirty,
		tinStatus = p_tinStatus
	where chrRecCode = p_chrRecCode;
end$$
