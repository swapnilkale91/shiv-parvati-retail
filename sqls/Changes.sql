-- -----------------

-- change by jaydeep 9th oct 2015

ALTER TABLE `dtb_retail`.`tbltaxdetails` 
DROP INDEX `UK_tblTaxDetails` ,
ADD UNIQUE INDEX `UK_tblTaxDetails` (`chrParentCode` ASC, `dtmEffectiveDate` ASC);


 -- ----------------------------------------------------------------------------------

-- change by aniket 9th oct 2015

DELIMITER $$
CREATE FUNCTION `UFn_UTCDate`(p_date varchar(50)) RETURNS datetime
BEGIN 
	DECLARE fdate varchar(20); 
	DECLARE formatedDate datetime;	
	
	if(p_date != '' || p_date != null) Then
		SET fdate = (SELECT SUBSTRING_INDEX(p_date, '.', 1));
		SET formatedDate = (select convert_tz(fdate,'-05:30','+00:00') from dual);
		Return formatedDate;
	else
		return null;
end if;
END$$
DELIMITER ;


-- ----------------------------------------------------------------------------------

--- change by aniket 9th oct 2015

delimiter $$

drop procedure if exists USP_tblstockadjustments_insert $$

delimiter $$
create procedure USP_tblstockadjustments_insert(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmAdjustmentDate char(30),
	p_dtmAuthorizationDateOne datetime,
	p_dtmAuthorizationDateThree datetime,
	p_dtmAuthorizationDateTwo datetime,
	p_dtmInsertDate datetime,
	p_dtmUpdateDate datetime,
	p_intAuthorizationLevel int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrAdjustmentNo varchar(100)
)
begin
	insert into tblstockadjustments(
		chrAuthorizationUserOne,
		chrAuthorizationUserThree,
		chrAuthorizationUserTwo,
		chrCompanyCode,
		chrInsertUser,
		chrIpAddress,
		chrRecCode,
		chrStoreCode,
		chrUpdateUser,
		dtmAdjustmentDate,
		dtmAuthorizationDateOne,
		dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo,
		dtmInsertDate,
		dtmUpdateDate,
		intAuthorizationLevel,
		intStatus,
		tinIsDirty,
		vhrAdjustmentNo
	) values (
		p_chrAuthorizationUserOne,
		p_chrAuthorizationUserThree,
		p_chrAuthorizationUserTwo,
		p_chrCompanyCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrUpdateUser,
		UFn_UTCDate(p_dtmAdjustmentDate),
		p_dtmAuthorizationDateOne,
		p_dtmAuthorizationDateThree,
		p_dtmAuthorizationDateTwo,
		p_dtmInsertDate,
		p_dtmUpdateDate,
		p_intAuthorizationLevel,
		p_intStatus,
		p_tinIsDirty,
		p_vhrAdjustmentNo
	);
end$$

delimiter $$

drop procedure if exists USP_tblstockadjustments_update $$

delimiter $$
create procedure USP_tblstockadjustments_update(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_dtmAdjustmentDate char(30),
	p_dtmAuthorizationDateOne datetime,
	p_dtmAuthorizationDateThree datetime,
	p_dtmAuthorizationDateTwo datetime,
	p_dtmUpdateDate datetime,
	p_intAuthorizationLevel int,
	p_intStatus int,
	p_tinIsDirty int,
	p_vhrAdjustmentNo varchar(100)
)
begin
	update tblstockadjustments set
		chrAuthorizationUserOne = p_chrAuthorizationUserOne,
		chrAuthorizationUserThree = p_chrAuthorizationUserThree,
		chrAuthorizationUserTwo = p_chrAuthorizationUserTwo,
		chrCompanyCode = p_chrCompanyCode,
		chrIpAddress = p_chrIpAddress,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrUpdateUser = p_chrUpdateUser,
		dtmAdjustmentDate = UFn_UTCDate(p_dtmAdjustmentDate),
		dtmAuthorizationDateOne = p_dtmAuthorizationDateOne,
		dtmAuthorizationDateThree = p_dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo = p_dtmAuthorizationDateTwo,
		dtmUpdateDate = p_dtmUpdateDate,
		intAuthorizationLevel = p_intAuthorizationLevel,
		intStatus = p_intStatus,
		tinIsDirty = p_tinIsDirty,
		vhrAdjustmentNo = p_vhrAdjustmentNo
	where chrRecCode = p_chrRecCode;
end$$


------------------------------------------------------------------------------------------

ALTER TABLE `dtb_retail`.`tblstockadjustmentitemdetails` 
DROP FOREIGN KEY `FK_tblStockAdjustmentItemDetails_chrParentCode_tblSales`;
ALTER TABLE `dtb_retail`.`tblstockadjustmentitemdetails` 
ADD CONSTRAINT `FK_tblStockAdjustmentItemDetails_chrParentCode_tblSA`
  FOREIGN KEY (`chrParentCode`)
  REFERENCES `dtb_retail`.`tblstockadjustments` (`chrRecCode`);



ALTER TABLE `dtb_retail`.`tbltaxdetails` 
DROP INDEX `UK_tblTaxDetails` ,
ADD UNIQUE INDEX `UK_tblTaxDetails` (`chrParentCode` ASC, `dtmEffectiveDate` ASC);



-- Column added on 21-10-2015 Changed column chrPaymentModeCode (datatype varchar(36) to intPaymentModeCode (datatype int(10)))
CREATE TABLE `tblsalespaymentdetails` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrParentCode` varchar(36) NOT NULL,
	 `intPaymentModeCode` Int(10) NOT NULL,
	 `decAmount` decimal(20,2) NOT NULL,
	 `decCashReceived` decimal(20,2) NOT NULL,
	 `decBalance` decimal(20,2) NOT NULL,
	 `dtmCouponDenomination` datetime NOT NULL,
	 `vhrCardDetails` varchar(36) NOT NULL,
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `chrIpAddress` varchar(100) NOT NULL,
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblSalesPaymentDetails` PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblSalesPaymentDetails_chrParentCode_tblSales` FOREIGN KEY (`chrParentCode`) REFERENCES `tblsales` (`chrRecCode`)
);


/* Name     :     tblDefaultlookUp
Description :     Table is used to store default values that are used in all forms
*/
CREATE TABLE `tbldefaultlookups` (
	`intRecCode` int(10) NOT NULL,
    `vhrName` varchar(100) NOT NULL,
    `LookupTypeCode` int(10) NOT NULL,
    CONSTRAINT `PK_tblDefaultLookUps` PRIMARY KEY (`intRecCode`)
);

---------------------------------------------------------------------------------------------------------------------------------------------------
-- inward UTC date change on 12th oct 2015 

delimiter $$
drop procedure if exists USP_tblinwarditemdetails_insert $$
delimiter $$
create procedure USP_tblinwarditemdetails_insert(
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreRackCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_chrVATTaxRecCode varchar(36),
	p_decCustomerDiscountAmount decimal(20,2),
	p_decCustomerDiscountPercentage decimal(20,2),
	p_decCustomerItemRate decimal(20,2),
	p_decFreeQuantity decimal(20,2),
	p_decItemAmount decimal(20,2),
	p_decItemAmountAfterTax decimal(20,2),
	p_decItemDiscountAmount decimal(20,2),
	p_decMarginAmount decimal(20,2),
	p_decMarginPercentage decimal(20,2),
	p_decMRP decimal(20,2),
	p_decQuantity decimal(20,2),
	p_decSchemeDiscount decimal(20,2),
	p_decSchemeDiscountAmount decimal(20,2),
	p_decVATTaxAmount decimal(20,2),
	p_decVATTaxPercentage decimal(20,2),
	p_decVendorDiscount1 decimal(20,2),
	p_decVendorDiscount2 decimal(20,2),
	p_decVendorDiscountAmount1 decimal(20,2),
	p_decVendorDiscountAmount2 decimal(20,2),
	p_dtmExpiryDate char(30),
	p_dtmInsertDate char(30),
	p_dtmManufactureDate char(30),
	p_dtmUpdateDate char(30),
	p_tinIsDirty int,
	p_tinPrintLabel int,
	p_vhrBarCode varchar(100),
	p_vhrBatchNo varchar(100),
	p_vhrDescription varchar(1000),
	p_vhrGeneratedBarcode varchar(100),
	p_vhrPacking varchar(100)
)
begin
	insert into tblinwarditemdetails(
		chrInsertUser,
		chrIpAddress,
		chrItemCode,
		chrParentCode,
		chrRecCode,
		chrStoreRackCode,
		chrUpdateUser,
		chrVATTaxRecCode,
		decCustomerDiscountAmount,
		decCustomerDiscountPercentage,
		decCustomerItemRate,
		decFreeQuantity,
		decItemAmount,
		decItemAmountAfterTax,
		decItemDiscountAmount,
		decMarginAmount,
		decMarginPercentage,
		decMRP,
		decQuantity,
		decSchemeDiscount,
		decSchemeDiscountAmount,
		decVATTaxAmount,
		decVATTaxPercentage,
		decVendorDiscount1,
		decVendorDiscount2,
		decVendorDiscountAmount1,
		decVendorDiscountAmount2,
		dtmExpiryDate,
		dtmInsertDate,
		dtmManufactureDate,
		dtmUpdateDate,
		tinIsDirty,
		tinPrintLabel,
		vhrBarCode,
		vhrBatchNo,
		vhrDescription,
		vhrGeneratedBarcode,
		vhrPacking
	) values (
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrItemCode,
		p_chrParentCode,
		p_chrRecCode,
		p_chrStoreRackCode,
		p_chrUpdateUser,
		p_chrVATTaxRecCode,
		p_decCustomerDiscountAmount,
		p_decCustomerDiscountPercentage,
		p_decCustomerItemRate,
		p_decFreeQuantity,
		p_decItemAmount,
		p_decItemAmountAfterTax,
		p_decItemDiscountAmount,
		p_decMarginAmount,
		p_decMarginPercentage,
		p_decMRP,
		p_decQuantity,
		p_decSchemeDiscount,
		p_decSchemeDiscountAmount,
		p_decVATTaxAmount,
		p_decVATTaxPercentage,
		p_decVendorDiscount1,
		p_decVendorDiscount2,
		p_decVendorDiscountAmount1,
		p_decVendorDiscountAmount2,
		UFn_UTCDate(p_dtmExpiryDate),
		UFn_UTCDate(p_dtmInsertDate),
		UFn_UTCDate(p_dtmManufactureDate),
		UFn_UTCDate(p_dtmUpdateDate),
		p_tinIsDirty,
		p_tinPrintLabel,
		p_vhrBarCode,
		p_vhrBatchNo,
		p_vhrDescription,
		p_vhrGeneratedBarcode,
		p_vhrPacking
	);
end$$

---------------------------------------------------------------------------------------------------------------------------------------------------

delimiter $$
drop procedure if exists USP_tblinwarditemdetails_update $$
delimiter $$
create procedure USP_tblinwarditemdetails_update(
	p_chrIpAddress varchar(100),
	p_chrItemCode varchar(36),
	p_chrParentCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreRackCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_chrVATTaxRecCode varchar(36),
	p_decCustomerDiscountAmount decimal(20,2),
	p_decCustomerDiscountPercentage decimal(20,2),
	p_decCustomerItemRate decimal(20,2),
	p_decFreeQuantity decimal(20,2),
	p_decItemAmount decimal(20,2),
	p_decItemAmountAfterTax decimal(20,2),
	p_decItemDiscountAmount decimal(20,2),
	p_decMarginAmount decimal(20,2),
	p_decMarginPercentage decimal(20,2),
	p_decMRP decimal(20,2),
	p_decQuantity decimal(20,2),
	p_decSchemeDiscount decimal(20,2),
	p_decSchemeDiscountAmount decimal(20,2),
	p_decVATTaxAmount decimal(20,2),
	p_decVATTaxPercentage decimal(20,2),
	p_decVendorDiscount1 decimal(20,2),
	p_decVendorDiscount2 decimal(20,2),
	p_decVendorDiscountAmount1 decimal(20,2),
	p_decVendorDiscountAmount2 decimal(20,2),
	p_dtmExpiryDate char(30),
	p_dtmManufactureDate char(30),
	p_dtmUpdateDate char(30),
	p_tinIsDirty int,
	p_tinPrintLabel int,
	p_vhrBarCode varchar(100),
	p_vhrBatchNo varchar(100),
	p_vhrDescription varchar(1000),
	p_vhrGeneratedBarcode varchar(100),
	p_vhrPacking varchar(100)
)
begin
	update tblinwarditemdetails set
		chrIpAddress = p_chrIpAddress,
		chrItemCode = p_chrItemCode,
		chrParentCode = p_chrParentCode,
		chrRecCode = p_chrRecCode,
		chrStoreRackCode = p_chrStoreRackCode,
		chrUpdateUser = p_chrUpdateUser,
		chrVATTaxRecCode = p_chrVATTaxRecCode,
		decCustomerDiscountAmount = p_decCustomerDiscountAmount,
		decCustomerDiscountPercentage = p_decCustomerDiscountPercentage,
		decCustomerItemRate = p_decCustomerItemRate,
		decFreeQuantity = p_decFreeQuantity,
		decItemAmount = p_decItemAmount,
		decItemAmountAfterTax = p_decItemAmountAfterTax,
		decItemDiscountAmount = p_decItemDiscountAmount,
		decMarginAmount = p_decMarginAmount,
		decMarginPercentage = p_decMarginPercentage,
		decMRP = p_decMRP,
		decQuantity = p_decQuantity,
		decSchemeDiscount = p_decSchemeDiscount,
		decSchemeDiscountAmount = p_decSchemeDiscountAmount,
		decVATTaxAmount = p_decVATTaxAmount,
		decVATTaxPercentage = p_decVATTaxPercentage,
		decVendorDiscount1 = p_decVendorDiscount1,
		decVendorDiscount2 = p_decVendorDiscount2,
		decVendorDiscountAmount1 = p_decVendorDiscountAmount1,
		decVendorDiscountAmount2 = p_decVendorDiscountAmount2,
		dtmExpiryDate = UFn_UTCDate(p_dtmExpiryDate),
		dtmManufactureDate = UFn_UTCDate(p_dtmManufactureDate),
		dtmUpdateDate = UFn_UTCDate(p_dtmUpdateDate),
		tinIsDirty = p_tinIsDirty,
		tinPrintLabel = p_tinPrintLabel,
		vhrBarCode = p_vhrBarCode,
		vhrBatchNo = p_vhrBatchNo,
		vhrDescription = p_vhrDescription,
		vhrGeneratedBarcode = p_vhrGeneratedBarcode,
		vhrPacking = p_vhrPacking
	where chrRecCode = p_chrRecCode;
end$$

---------------------------------------------------------------------------------------------------------------------------------------------------

delimiter $$

drop procedure if exists USP_tblinwards_insert $$

delimiter $$
create procedure USP_tblinwards_insert(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrInsertUser varchar(36),
	p_chrIpAddress varchar(100),
	p_chrPOCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_chrVendorCode varchar(36),
	p_decItemDiscountTotal decimal(20,2),
	p_decItemSchemeTotal decimal(20,2),
	p_decItemTotal decimal(20,2),
	p_decVATTaxTotal decimal(20,2),
	p_decVendorPayableAmount decimal(20,2),
	p_dtmAuthorizationDateOne char(30),
	p_dtmAuthorizationDateThree char(30),
	p_dtmAuthorizationDateTwo char(30),
	p_dtmDueDate char(30),
	p_dtmInsertDate char(30),
	p_dtmInwarddate char(30),
	p_dtmUpdateDate char(30),
	p_intCreditPeriod int,
	p_tinAuthorizationLevel int,
	p_tinIsDirty int,
	p_tinStatus int,
	p_vhrChallanNo varchar(100),
	p_vhrInwardNo varchar(100),
	p_vhrLorryNo varchar(100)
)
begin
	insert into tblinwards(
		chrAuthorizationUserOne,
		chrAuthorizationUserThree,
		chrAuthorizationUserTwo,
		chrCompanyCode,
		chrInsertUser,
		chrIpAddress,
		chrPOCode,
		chrRecCode,
		chrStoreCode,
		chrUpdateUser,
		chrVendorCode,
		decItemDiscountTotal,
		decItemSchemeTotal,
		decItemTotal,
		decVATTaxTotal,
		decVendorPayableAmount,
		dtmAuthorizationDateOne,
		dtmAuthorizationDateThree,
		dtmAuthorizationDateTwo,
		dtmDueDate,
		dtmInsertDate,
		dtmInwarddate,
		dtmUpdateDate,
		intCreditPeriod,
		tinAuthorizationLevel,
		tinIsDirty,
		tinStatus,
		vhrChallanNo,
		vhrInwardNo,
		vhrLorryNo
	) values (
		p_chrAuthorizationUserOne,
		p_chrAuthorizationUserThree,
		p_chrAuthorizationUserTwo,
		p_chrCompanyCode,
		p_chrInsertUser,
		p_chrIpAddress,
		p_chrPOCode,
		p_chrRecCode,
		p_chrStoreCode,
		p_chrUpdateUser,
		p_chrVendorCode,
		p_decItemDiscountTotal,
		p_decItemSchemeTotal,
		p_decItemTotal,
		p_decVATTaxTotal,
		p_decVendorPayableAmount,
		UFn_UTCDate(p_dtmAuthorizationDateOne),
		UFn_UTCDate(p_dtmAuthorizationDateThree),
		UFn_UTCDate(p_dtmAuthorizationDateTwo),
		UFn_UTCDate(p_dtmDueDate),
		UFn_UTCDate(p_dtmInsertDate),
		UFn_UTCDate(p_dtmInwarddate),
		UFn_UTCDate(p_dtmUpdateDate),
		p_intCreditPeriod,
		p_tinAuthorizationLevel,
		p_tinIsDirty,
		p_tinStatus,
		p_vhrChallanNo,
		p_vhrInwardNo,
		p_vhrLorryNo
	);
end$$

---------------------------------------------------------------------------------------------------------------------------------------------------

delimiter $$
drop procedure if exists USP_tblinwards_update $$
delimiter $$
create procedure USP_tblinwards_update(
	p_chrAuthorizationUserOne varchar(36),
	p_chrAuthorizationUserThree varchar(36),
	p_chrAuthorizationUserTwo varchar(36),
	p_chrCompanyCode varchar(36),
	p_chrIpAddress varchar(100),
	p_chrPOCode varchar(36),
	p_chrRecCode varchar(36),
	p_chrStoreCode varchar(36),
	p_chrUpdateUser varchar(36),
	p_chrVendorCode varchar(36),
	p_decItemDiscountTotal decimal(20,2),
	p_decItemSchemeTotal decimal(20,2),
	p_decItemTotal decimal(20,2),
	p_decVATTaxTotal decimal(20,2),
	p_decVendorPayableAmount decimal(20,2),
	p_dtmAuthorizationDateOne char(30),
	p_dtmAuthorizationDateThree char(30),
	p_dtmAuthorizationDateTwo char(30),
	p_dtmDueDate char(30),
	p_dtmInwarddate char(30),
	p_dtmUpdateDate char(30),
	p_intCreditPeriod int,
	p_tinAuthorizationLevel int,
	p_tinIsDirty int,
	p_tinStatus int,
	p_vhrChallanNo varchar(100),
	p_vhrInwardNo varchar(100),
	p_vhrLorryNo varchar(100)
)
begin
	update tblinwards set
		chrAuthorizationUserOne = p_chrAuthorizationUserOne,
		chrAuthorizationUserThree = p_chrAuthorizationUserThree,
		chrAuthorizationUserTwo = p_chrAuthorizationUserTwo,
		chrCompanyCode = p_chrCompanyCode,
		chrIpAddress = p_chrIpAddress,
		chrPOCode = p_chrPOCode,
		chrRecCode = p_chrRecCode,
		chrStoreCode = p_chrStoreCode,
		chrUpdateUser = p_chrUpdateUser,
		chrVendorCode = p_chrVendorCode,
		decItemDiscountTotal = p_decItemDiscountTotal,
		decItemSchemeTotal = p_decItemSchemeTotal,
		decItemTotal = p_decItemTotal,
		decVATTaxTotal = p_decVATTaxTotal,
		decVendorPayableAmount = p_decVendorPayableAmount,
		dtmAuthorizationDateOne = UFn_UTCDate(p_dtmAuthorizationDateOne),
		dtmAuthorizationDateThree = UFn_UTCDate(p_dtmAuthorizationDateThree),
		dtmAuthorizationDateTwo = UFn_UTCDate(p_dtmAuthorizationDateTwo),
		dtmDueDate = UFn_UTCDate(p_dtmDueDate),
		dtmInwarddate = UFn_UTCDate(p_dtmInwarddate),
		dtmUpdateDate = UFn_UTCDate(p_dtmUpdateDate),
		intCreditPeriod = p_intCreditPeriod,
		tinAuthorizationLevel = p_tinAuthorizationLevel,
		tinIsDirty = p_tinIsDirty,
		tinStatus = p_tinStatus,
		vhrChallanNo = p_vhrChallanNo,
		vhrInwardNo = p_vhrInwardNo,
		vhrLorryNo = p_vhrLorryNo
	where chrRecCode = p_chrRecCode;
end$$
