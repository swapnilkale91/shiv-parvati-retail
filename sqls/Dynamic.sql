delimiter $$
drop procedure if exists USP_Select_Columns_Dynamic $$
delimiter $$
create procedure USP_Select_Columns_Dynamic(p_TableName varchar(500), p_Columns varchar(1000), p_Where varchar(1500), p_OrderBy varchar(100))
begin
	set @sql = concat("select ", p_Columns, " from ", p_TableName);
	if (p_Where <> '') then
		set @sql = concat(@sql, " where ", p_Where);
	end if;
	if (p_OrderBy <> '') then
		set @sql = concat(@sql, " order by ", p_OrderBy);
	end if;
	
	prepare stmt from @sql;
	execute stmt;
	deallocate prepare stmt;
end $$

delimiter $$
drop procedure if exists USP_Select_Columns_Dynamic_Page $$
delimiter $$
create procedure USP_Select_Columns_Dynamic_Page(p_TableName varchar(500), p_Columns varchar(1000), p_Where varchar(1500), p_OrderBy varchar(100), p_Offset int, p_PageSize int)
begin
	set @sql = concat("select ", p_Columns, " from ", p_TableName);
	if (p_Where <> '') then
		set @sql = concat(@sql, " where ", p_Where);
	end if;
	if (p_OrderBy <> '') then
		set @sql = concat(@sql, " order by ", p_OrderBy);
	end if;
	set @sql = concat(@sql, " limit ", cast(p_Offset as char), ", ", cast(p_PageSize as char));
	
	prepare stmt from @sql;
	execute stmt;
	deallocate prepare stmt;
end $$

drop procedure if exists USP_Count_Dynamic $$
delimiter $$
create procedure USP_Count_Dynamic(p_TableName varchar(500), p_Where varchar(1500), p_OrderBy varchar(100))
begin
	set @sql = concat("select count(*) from ", p_TableName);
	if (p_Where <> '') then
		set @sql = concat(@sql, " where ", p_Where);
	end if;
	if (p_OrderBy <> '') then
		set @sql = concat(@sql, " order by ", p_OrderBy);
	end if;
	
	prepare stmt from @sql;
	execute stmt;
	deallocate prepare stmt;
end $$

delimiter $$
drop procedure if exists USP_Count_Dynamic_Page $$
delimiter $$
create procedure USP_Count_Dynamic_Page(p_TableName varchar(500), p_Where varchar(1500), p_OrderBy varchar(100), p_Offset int, p_PageSize int)
begin
	set @sql = concat("select count(*) from ", p_TableName);
	if (p_Where <> '') then
		set @sql = concat(@sql, " where ", p_Where);
	end if;
	if (p_OrderBy <> '') then
		set @sql = concat(@sql, " order by ", p_OrderBy);
	end if;
	set @sql = concat(@sql, " limit ", cast(p_Offset as char), ", ", cast(p_PageSize as char));
	
	prepare stmt from @sql;
	execute stmt;
	deallocate prepare stmt;
end $$

delimiter $$
drop procedure if exists USP_Select_For_Email_Password $$
delimiter $$
create procedure USP_Select_For_Email_Password(
	p_Email varchar(100),
	p_Password varchar(20))
begin
	select
		a.chrRecCode,
		a.vhrEmail,
		a.vhrPassword
	from tbl_pmscustomers a
	where a.vhrEmail = p_Email
	and b.vhrPassword = p_Password;
end $$


delimiter $$
drop procedure if exists USP_Delete_Dynamic $$
delimiter $$
create procedure USP_Delete_Dynamic(p_TableName varchar(500), p_Where varchar(1500))
begin
	set @sql = concat("delete from ", p_TableName);
	if (p_Where <> '') then
		set @sql = concat(@sql, " where ", p_Where);
	end if;
	/*set @sql = concat(@sql, " limit ", cast(p_Offset as char), ", ", cast(p_PageSize as char));*/
	
	prepare stmt from @sql;
	execute stmt;
	deallocate prepare stmt;
end $$