CREATE DATABASE `dtb_retail` ;
use dtb_retail;

/* Name 	: 	tblcompanies
Description : 	Table to store information about the company. 
*/
CREATE  TABLE `tblcompanies` (
  `chrRecCode` varchar(36) NOT NULL,
  `vhrCode` varchar(20) NOT NULL,
  `vhrName` varchar(100) NOT NULL,
  `vhrAddress1` varchar(100) NOT NULL,
  `vhrAddress2` varchar(100) DEFAULT NULL,
  `vhrAddress3` varchar(100) DEFAULT NULL,
  `vhrAddress4` varchar(100) DEFAULT NULL,
  `intPinCode` int(10) NOT NULL,
  `vhrTel1` varchar(20) DEFAULT NULL,
  `vhrTel2` varchar(20) DEFAULT NULL,
  `vhrPANNo` varchar(100) NOT NULL,
  `vhrServiceTaxNo` varchar(100) NOT NULL,
  `vhrCSTNo` varchar(100) NOT NULL,
  `vhrVATNo` varchar(100) NOT NULL,
  `tinActiveFlag` int NOT NULL DEFAULT '1',
  `dtmInsertDate` datetime NOT NULL,
  `chrInsertUser` varchar(36) NOT NULL,
  `dtmUpdateDate` datetime NOT NULL,
  `chrUpdateUser` varchar(36) NOT NULL,
  `chrIpAddress` varchar(100) NOT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
  CONSTRAINT `PK_tblcompanies` PRIMARY KEY (`chrRecCode`),
  CONSTRAINT `UK_tblcompanies_vhrName` UNIQUE KEY (`vhrName`),
  CONSTRAINT `UK_tblcompanies_vhrCode` UNIQUE KEY (`vhrCode`)
  
);

/* Name 	: 	tblstores
Description : 	Table to store information of the different locations where the company has stores. 
*/
CREATE  TABLE `tblstores` (
  `chrRecCode` varchar(36) NOT NULL,
  `chrParentCode` varchar(36) NOT NULL,
  `vhrCode` varchar(20) NOT NULL,
  `vhrName` varchar(100) NOT NULL,
  `vhrAddress1` varchar(100) NOT NULL,
  `vhrAddress2` varchar(100) DEFAULT NULL,
  `vhrAddress3` varchar(100) DEFAULT NULL,
  `vhrAddress4` varchar(100) DEFAULT NULL,
  `intPinCode` int(10) NOT NULL,
  `vhrTel1` varchar(20) DEFAULT NULL,
  `vhrTel2` varchar(20) DEFAULT NULL,
  `vhrPANNo` varchar(100) NOT NULL,
  `vhrServiceTaxNo` varchar(100) NOT NULL,
  `vhrCSTNo` varchar(100) NOT NULL,
  `vhrVATNo` varchar(100) NOT NULL,
  `tinActiveFlag` int NOT NULL DEFAULT '1',
  `dtmInsertDate` datetime NOT NULL,
  `chrInsertUser` varchar(36) NOT NULL,
  `dtmUpdateDate` datetime NOT NULL,
  `chrUpdateUser` varchar(36) NOT NULL,
  `chrIpAddress` varchar(100) NOT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
  CONSTRAINT `PK_tblstores` PRIMARY KEY (`chrRecCode`),
  CONSTRAINT `UK_tblstores_vhrName` UNIQUE KEY (`vhrName`),
  CONSTRAINT `UK_tblstores_vhrCode` UNIQUE KEY (`vhrCode`),
  CONSTRAINT `FK_tblstores_chrParentCode_tblcompanies` FOREIGN KEY (`chrParentCode`) REFERENCES `tblcompanies` (`chrRecCode`)
);


/* Name 	: 	tblstoreitemracks
Description : 	Table to store information of the different racks / locations inside the store 
				where the items will be put on display or godown. 
*/
CREATE  TABLE `tblstoreitemracks` (
  `chrRecCode` varchar(36) NOT NULL,
  `chrParentCode` varchar(36) NOT NULL,
  `intStoreLocationCategory` int(11) NOT NULL,
  `vhrName` varchar(100) NOT NULL,
  `vhrCode` varchar(20) NOT NULL,
  `tinActiveFlag` int NOT NULL DEFAULT '1',
  `dtmInsertDate` datetime NOT NULL,
  `chrInsertUser` varchar(36) NOT NULL,
  `dtmUpdateDate` datetime NOT NULL,
  `chrUpdateUser` varchar(36) NOT NULL,
  `chrIpAddress` varchar(100) NOT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
  CONSTRAINT `PK_tblstoreitemracks` PRIMARY KEY (`chrRecCode`),
  CONSTRAINT `UK_tblstoreitemracks_vhrName` UNIQUE KEY (`vhrName`),
  CONSTRAINT `UK_tblstoreitemracks_vhrCode` UNIQUE KEY (`vhrCode`),
  CONSTRAINT `FK_tblstoreitemracks_chrParentCode_tblstores` FOREIGN KEY (`chrParentCode`) REFERENCES `tblstores` (`chrRecCode`) 
);
  
  
  /* Name 	: 	tblEmployeeCategory
Description : 	Table to store information of the different employee categories.
*/
CREATE  TABLE `tblemployeecategories` (
	`chrRecCode` varchar(36) NOT NULL,
	`vhrName` varchar(100) NOT NULL,
	`vhrCode` varchar(20) NOT NULL,
    `tinActiveFlag` int NOT NULL DEFAULT '1',
	`dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
    CONSTRAINT `PK_tblEmployeeCategories` PRIMARY KEY (`chrRecCode`),
    CONSTRAINT `UK_tblEmployeeCategories_vhrName` UNIQUE KEY (`vhrName`),
	CONSTRAINT `UK_tblEmployeeCategories_vhrCode` UNIQUE KEY (`vhrCode`)
    
);


  /* Name 	: 	tblEmployeeMaster
Description : 	Table to store information of employees working for the company and store.
*/
CREATE  TABLE `tblemployeemasters` (
	`chrRecCode` varchar(36) NOT NULL,
	`chrCompanyCode` varchar(36) NOT NULL,
	`chrEmployeeCategoryCode` varchar(36) NOT NULL,
    `chrStoreCode` varchar(36) NOT NULL,
    `vhrCode` varchar(20) NOT NULL,
    `vhrName` varchar(100) NOT NULL,
    `vhrAddress1` varchar(100) NOT NULL,
	`vhrAddress2` varchar(100) DEFAULT NULL,
	`vhrAddress3` varchar(100) DEFAULT NULL,
	`vhrAddress4` varchar(100) DEFAULT NULL,
	`intPinCode` int(10) NOT NULL,
    `vhrTel1` varchar(20) DEFAULT NULL,
	`vhrTel2` varchar(20) DEFAULT NULL,
    `vhrPANNo` varchar(100) NOT NULL,
	`vhrUserName` varchar(100) NOT NULL,
    `vhrPassword` varchar(100) NOT NULL,
    `tinLockUser` int NOT NULL DEFAULT '0',
    `tinActiveFlag` int NOT NULL DEFAULT '1',
	`dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
    CONSTRAINT `PK_tblEmployeeMasters` PRIMARY KEY (`chrRecCode`),
	CONSTRAINT `UK_tblEmployeeMasters_vhrName` UNIQUE KEY (`vhrName`),
	CONSTRAINT `UK_tblEmployeeMasters_vhrCode` UNIQUE KEY (`vhrCode`),
    CONSTRAINT `Fk_tblEmployeeMasters_chrCompanyCode_tblCompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
	CONSTRAINT `Fk_tblEMs_chrEmployeeCategoryCode_tblEmployeeCategories` FOREIGN KEY (`chrEmployeeCategoryCode`) REFERENCES `tblemployeecategories` (`chrRecCode`),
	CONSTRAINT `Fk_tblEmployeeMasters_chrStoreCode_tblstores` FOREIGN KEY (`chrStoreCode`) REFERENCES `tblstores` (`chrRecCode`)
);


 /* Name 	: 	tblVendorMaster
Description : 	Table to store information of vendors that supply items to the company and its stores.
*/
CREATE  TABLE `tblvendormasters` (
  `chrRecCode` varchar(36) NOT NULL,
  `vhrCode` varchar(20) NOT NULL,
  `vhrName` varchar(100) NOT NULL,
  `vhrAddress1` varchar(100) NOT NULL,
  `vhrAddress2` varchar(100) DEFAULT NULL,
  `vhrAddress3` varchar(100) DEFAULT NULL,
  `vhrAddress4` varchar(100) DEFAULT NULL,
  `intPinCode` int(10) NOT NULL,
  `vhrTel1` varchar(20) DEFAULT NULL,
  `vhrTel2` varchar(20) DEFAULT NULL,
  `vhrPANNo` varchar(100) NOT NULL,
  `vhrServiceTaxNo` varchar(100) NOT NULL,
  `vhrCSTNo` varchar(100) NOT NULL,
  `vhrVATNo` varchar(100) NOT NULL,
  `tinActiveFlag` int NOT NULL DEFAULT '1',
  `dtmInsertDate` datetime NOT NULL,
  `chrInsertUser` varchar(36) NOT NULL,
  `dtmUpdateDate` datetime NOT NULL,
  `chrUpdateUser` varchar(36) NOT NULL,
  `chrIpAddress` varchar(100) NOT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
  CONSTRAINT `PK_tblVendorMasters` PRIMARY KEY (`chrRecCode`),
  CONSTRAINT `UK_tblVendorMasters_vhrName` UNIQUE KEY (`vhrName`),
  CONSTRAINT `UK_tblVendorMasters_vhrCode` UNIQUE KEY (`vhrCode`)
  
);

/* Name 	: 	tblCustomerMaster
Description : 	Table to store information of customers that buy or purchase items to the company and its stores.
*/
CREATE TABLE `tblcustomermasters` (
	`chrRecCode` varchar(36) NOT NULL,
	`vhrCode` varchar(20) NOT NULL,
    `vhrName` varchar(100) NOT NULL,
    `vhrAddress1` varchar(100) NOT NULL,
	`vhrAddress2` varchar(100) DEFAULT NULL,
	`vhrAddress3` varchar(100) DEFAULT NULL,
	`vhrAddress4` varchar(100) DEFAULT NULL,
	`intPinCode` int(10) NOT NULL,
    `vhrTel1` varchar(20) DEFAULT NULL,
	`vhrTel2` varchar(20) DEFAULT NULL,
    `vhrPANNo` varchar(100) NOT NULL,
	`tinActiveFlag` int NOT NULL DEFAULT '1',
	`dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
	CONSTRAINT `PK_tblCustomerMasters` PRIMARY KEY (`chrRecCode`),
    CONSTRAINT `UK_tblCustomerMasters_vhrName` UNIQUE KEY (`vhrName`),
    CONSTRAINT `UK_tblCustomerMasters_vhrCode` UNIQUE KEY (`vhrCode`)
     
);

/* Name 	: 	tblTaxMaster
Description : 	Table to store information of different taxes.
*/
CREATE TABLE `tbltaxmasters` (
  `chrRecCode` varchar(36) NOT NULL,
  `vhrName` varchar(100) NOT NULL,
  `vhrCode` varchar(20) NOT NULL,
  `tinActiveFlag` int NOT NULL DEFAULT '1',
  `dtmInsertDate` datetime NOT NULL,
  `chrInsertUser` varchar(36) NOT NULL,
  `dtmUpdateDate` datetime NOT NULL,
  `chrUpdateUser` varchar(36) NOT NULL,
  `chrIpAddress` varchar(100) NOT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
  CONSTRAINT `PK_tblTaxMasters`  PRIMARY KEY (`chrRecCode`),
  CONSTRAINT `UK_tblTaxMasters_vhrName` UNIQUE KEY (`vhrName`),
  CONSTRAINT `UK_tblTaxMasters_vhrCode` UNIQUE KEY (`vhrCode`)
	
);

/* Name 	: 	tblTaxDetails
Description : 	Table to store information of different taxes percentage with effective dates.
*/
CREATE TABLE `tbltaxdetails` (
  `chrRecCode` varchar(36) NOT NULL,
  `chrParentCode` varchar(36) NOT NULL,
  `vhrName` varchar(100) NOT NULL,
  `dtmEffectiveDate` datetime NOT NULL,
  `decPercentage` decimal(10,2) NOT NULL,
  `tinActiveFlag` int NOT NULL DEFAULT '1',
  `dtmInsertDate` datetime NOT NULL,
  `chrInsertUser` varchar(36) NOT NULL,
  `dtmUpdateDate` datetime NOT NULL,
  `chrUpdateUser` varchar(36) NOT NULL,
  `chrIpAddress` varchar(100) NOT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
   CONSTRAINT `PK_tblTaxDetails` PRIMARY KEY (`chrRecCode`),
   UNIQUE `UK_tblTaxDetails`(`chrParentCode`, `dtmEffectiveDate`, `decPercentage`),
   CONSTRAINT `FK_tblTaxDetails_chrParentCode_tblTaxMasters` FOREIGN KEY (`chrParentCode`) REFERENCES `tbltaxmasters` (`chrRecCode`) 
);

/* Name 	: 	tblUOMMasters
Description : 	Table to store information of different unit of measures.
*/
CREATE TABLE `tbluommasters` (
	`chrRecCode` varchar(36) NOT NULL,
	`vhrName` varchar(100) NOT NULL,
    `vhrCode` varchar(20) NOT NULL,
    `tinActiveFlag` int NOT NULL DEFAULT '1',
	`dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
	CONSTRAINT `PK_tblUOMMasters` PRIMARY KEY (`chrRecCode`),
    CONSTRAINT `UK_tblUOMMasters_vhrName` UNIQUE KEY (`vhrName`),
    CONSTRAINT `UK_tblUOMMasters_vhrCode` UNIQUE KEY (`vhrCode`)
);

 /* Name 	: 	tblItemCategory
Description : 	Table to store information of the different employee categories.
*/
CREATE  TABLE `tblitemcategories` (
	`chrRecCode` varchar(36) NOT NULL,
	`vhrName` varchar(100) NOT NULL,
	`vhrCode` varchar(20) NOT NULL,
    `tinActiveFlag` int NOT NULL DEFAULT '1',
	`dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
    CONSTRAINT `PK_tblItemCategories` PRIMARY KEY (`chrRecCode`),
    CONSTRAINT `UK_tblItemCategories_vhrName` UNIQUE KEY (`vhrName`),
	CONSTRAINT `UK_tblItemCategories_vhrCode` UNIQUE KEY (`vhrCode`)
    
);

/* Name 	: 	tblItemMasters
Description : 	Table to store information of different items.
*/
CREATE TABLE `tblitemmasters` (
	`chrRecCode` varchar(36) NOT NULL,
    `chrItemCategoryCode` varchar(36) NOT NULL,
    `vhrName` varchar(100) NOT NULL,
    `vhrShortName` varchar(20) NOT NULL,
    `vhrCode` varchar(20) NOT NULL,
    `chrUnitOfMeasureCode` varchar(36) NOT NULL,
    `vhrDescription` varchar(1000) NOT NULL,
    `tinLoseSold` int NOT NULL,
    `vhrBarcode` varchar(100) NOT NULL,	
    `intReOrderQuantity` int(20) NOT NULL,
	`tinActiveFlag` int NOT NULL DEFAULT '1',
	`dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
     CONSTRAINT `PK_tblItemMasters` PRIMARY KEY (`chrRecCode`),
     CONSTRAINT `UK_tblItemMasters_vhrName` UNIQUE KEY (`vhrName`),
     CONSTRAINT `UK_tblItemMasters_vhrCode` UNIQUE KEY (`vhrCode`),
     CONSTRAINT `FK_tblItemMasters_chrUnitOfMeasureCode_tblUOMMasters` FOREIGN KEY (`chrUnitOfMeasureCode`) REFERENCES `tbluommasters` (`chrRecCode`),
     CONSTRAINT `FK_tblItemMasters_chrItemCategoryCode_tblItemCategories` FOREIGN KEY (`chrItemCategoryCode`) REFERENCES `tblitemcategories` (`chrRecCode`)  
);

/* Name 	: 	tblItemRackLocations
Description : 	Table to store information of items that will be stored on different racks.
*/
CREATE TABLE `tblitemracklocations` (
	`chrRecCode` varchar(36) NOT NULL,
    `chrItemCode` varchar(36) NOT NULL,
	`chrRackLocationCode` varchar(36) NOT NULL,
    `tinStatus` int NOT NULL DEFAULT '0',
    `dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
	CONSTRAINT `PK_tblItemRackLocations` PRIMARY KEY (`chrRecCode`),
	CONSTRAINT `FK_tblItemRackLocations_chrItemCode_tblItemMasters` FOREIGN KEY (`chrItemCode`) REFERENCES `tblItemMasters` (`chrRecCode`),  
	CONSTRAINT `FK_tblItemRackLocations_tblstoreitemracks` FOREIGN KEY (`chrRackLocationCode`) REFERENCES `tblstoreitemracks` (`chrRecCode`)  
);

/* Name 	: 	tbTerminals
Description : 	Table to maintain details of each PC/Terminal for the store.
*/
CREATE TABLE `tblterminals` (
	`chrRecCode` varchar(36) NOT NULL,
    `chrCompanyCode` varchar(36) NOT NULL,
    `chrStoreCode` varchar(36) NOT NULL,
    `chrTerminalCode` varchar(100) NOT NULL,
    `tinStatus` int NOT NULL DEFAULT '0',
    `dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
	CONSTRAINT `PK_tblTerminals` PRIMARY KEY (`chrRecCode`),
    CONSTRAINT `FK_tblTerminals_chrCompanyCode_tblCompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
    CONSTRAINT `FK_tblTerminals_chrStoreCode_tblstores` FOREIGN KEY (`chrStoreCode`) REFERENCES `tblstores` (`chrRecCode`)	
);

/* Name 	: 	tblPurchaseOrder
Description : 	Table to store information of items that has been purchased from a vendor.
*/
CREATE TABLE `tblpurchaseorders` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrCompanyCode` varchar(36) NOT NULL,
	 `chrPONo` varchar(100) NOT NULL,
	 `dtmPODate` datetime NOT NULL,
	 `chrVendorCode` varchar(36) NOT NULL,
	 `intCreditPeriod` int(10) NOT NULL,
	 `decItemTotal` decimal(20,2) NOT NULL,
	 `decMarginTotal` decimal(20,2) NOT NULL,
	 `decSchemeTotal` decimal(20,2) NOT NULL,
	 `decDiscountTotal` decimal(20,2) NOT NULL,
	 `decVatTaxTotal` decimal(20,2) NOT NULL,
	 `decPOAmt` decimal (20,2) NOT NULL,
	 `tinStatus` int NOT NULL DEFAULT '0',
	 `tinAuthorizationLevel` int NOT NULL DEFAULT '0',
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `dtmAuthorizationDateOne` datetime ,
	 `chrAuthorizationUserOne` varchar(36),
	 `dtmAuthorizationDateTwo` datetime,
	 `chrAuthorizationUserTwo` varchar(36),
	 `dtmAuthorizationDateThree` datetime,
	 `chrAuthorizationUserThree` varchar(36),
	 `chrIpAddress` varchar(100),
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	 CONSTRAINT `PK_tblPurchaseOrder` PRIMARY KEY (`chrRecCode`),
	 CONSTRAINT `FK_tblPurchaseOrder_chrCompanyCode_tblcompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
	 CONSTRAINT `FK_tblPurchaseOrder_chrVendorCode_tblVendorMasters` FOREIGN KEY (`chrVendorCode`) REFERENCES `tblvendormasters` (`chrRecCode`)
);

/* Name     :     tblPurchaseOrderItemDetails
Description :     Table is used store information of items that has been purchased from a vendor.
*/
 CREATE TABLE `tblpurchaseorderitemdetails` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrParentCode` varchar(36) NOT NULL,
	 `chrItemCode` varchar(36) NOT NULL,
	 `chrItemDescription` varchar(100),
	 `decQuantity` decimal(20,2) NOT NULL,
	 `chrUOMCode` varchar(36) NOT NULL,
	 `decRate` decimal(20,2) NOT NULL,
	 `decItemAmount` decimal(20,2) NOT NULL,
	 `decMarginPercent` decimal(20,2) NOT NULL,
	 `decMarginAmount` decimal(20,2) NOT NULL,
	 `decSchemeDiscountPercent` decimal(20,2) NOT NULL,
	 `decSchemeDiscountAmount` decimal(20,2) NOT NULL,
	 `decVendorDiscoutPercent1` decimal(20,2) NOT NULL,
	 `decVendorAmount1` decimal(20,2) NOT NULL,
	 `decVendorDiscountPercent2` decimal(20,2) NOT NULL,
	 `decVendorDiscountAmount2` decimal(20,2) NOT NULL,
	 `decItemDiscount` decimal(20,2) NOT NULL,
	 `chrAmount` varchar(36) NOT NULL,
	 `chrVatTaxRecCode` varchar(36) NOT NULL,
	 `decVatTaxPercent` decimal(20,2),
	 `decVatTaxAmount` decimal(20,2),
	 `decItemAmountAfterTax` decimal(20,2),
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `chrIpAddress` varchar(100) NOT NULL,
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblPOItemDetails` PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblPOItemDetails_chrParentCode_tblPurchaseOrder` FOREIGN KEY (`chrParentCode`) REFERENCES `tblpurchaseorders` (`chrRecCode`),
	  CONSTRAINT `FK_tblPOItemDetails_chrItemCode_tblItemMasters` FOREIGN KEY (`chrItemCode`) REFERENCES `tblitemmasters` (`chrRecCode`),
      CONSTRAINT `FK_tblPOItemDetails_chrUOMCode_tblUOMMasters` FOREIGN KEY (`chrUOMCode`) REFERENCES `tbluommasters` (`chrRecCode`),
      CONSTRAINT `FK_tblPOItemDetails_chrVatTaxRecCode_tblTaxDetails` FOREIGN KEY (`chrVatTaxRecCode`) REFERENCES `tbltaxdetails` (`chrRecCode`)
      
  );

/* Name     :     tblPurchaseOrderItemDeliveryDetails
Description :     Table is used store information of items that has to be delivered at which store location by the vendor.
*/
 CREATE TABLE `tblpurchaseorderitemdeliverydetails` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrParentCode` varchar(36) NOT NULL,
	 `chrStoreLocationCode` varchar(36) NOT NULL,
	 `decQuantity` decimal(20,2) NOT NULL,
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `chrIpAddress` varchar(100) NOT NULL,
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblPOIDDs` PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblPOIDDs_chrParentCode_tblPOItemsDetails` FOREIGN KEY (`chrParentCode`) REFERENCES `tblpurchaseorderitemdetails` (`chrRecCode`),
	  CONSTRAINT `FK_tblPOIDDs_chrStoreLocationCode_tblstores` FOREIGN KEY (`chrStoreLocationCode`) REFERENCES `tblstores` (`chrRecCode`)

);

/* Name 	: 	tblInwards
Description : 	Table to store information of items that come to the store location based on PO.
*/
CREATE TABLE `tblinwards` (
	`chrRecCode` varchar(36) NOT NULL,
	`chrCompanyCode` varchar(36) NOT NULL,
    `chrStoreCode` varchar(36) NOT NULL,
    `vhrInwardNo` varchar(100) NOT NULL,
    `dtmInwarddate` datetime NOT NULL,
    `chrPOCode` varchar(36) NOT NULL,
	`chrVendorCode` varchar(36) NOT NULL,
    `intCreditPeriod` int(10) NOT NULL,
    `dtmDueDate` datetime NOT NULL,
	`vhrLorryNo` varchar(100) NOT NULL,
    `vhrChallanNo` varchar(100) NOT NULL,
    `decItemTotal` decimal(20,2) NOT NULL,
    `decItemSchemeTotal` decimal(20,2) NOT NULL,
    `decItemDiscountTotal` decimal(20,2) NOT NULL,
	`decVATTaxTotal` decimal(20,2) NOT NULL,
    `decVendorPayableAmount` decimal(20,2) NOT NULL,
    `tinStatus` int NOT NULL DEFAULT '0',
    `tinAuthorizationLevel` int NOT NULL DEFAULT '0',
    `dtmInsertDate` datetime NOT NULL,
    `chrInsertUser` varchar(36) NOT NULL,
    `dtmUpdateDate` datetime NOT NULL,
    `chrUpdateUser` varchar(36) NOT NULL,
    `dtmAuthorizationDateOne` datetime ,
    `chrAuthorizationUserOne` varchar(36),
    `dtmAuthorizationDateTwo` datetime,
    `chrAuthorizationUserTwo` varchar(36),
    `dtmAuthorizationDateThree` datetime,
    `chrAuthorizationUserThree` varchar(36),
    `chrIpAddress` varchar(100),
    `tinIsDirty` int NOT NULL DEFAULT '0',
     CONSTRAINT `PK_tblInwards` PRIMARY KEY (`chrRecCode`),
	 CONSTRAINT `FK_tblInwards_chrCompanyCode_tblcompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
     CONSTRAINT `FK_tblInwards_chrStoreCode_tblEmployeeMasters` FOREIGN KEY (`chrStoreCode`) REFERENCES `tblemployeemasters` (`chrStoreCode`)
);

/* Name 	: 	tblInwardItemDetails
Description : 	Table to store information of items that has been purchased from a vendor.
*/
CREATE TABLE `tblinwarditemdetails` (
	`chrRecCode` varchar(36) NOT NULL,
	`chrParentCode` varchar(36) NOT NULL,
    `chrItemCode` varchar(36) NOT NULL,
	`vhrDescription` varchar(1000) NOT NULL,
	`chrStoreRackCode` varchar(36) NOT NULL,
    `vhrPacking` varchar(100) NOT NULL,
    `vhrBarCode` varchar(100) NOT NULL,
    `decQuantity` decimal(20,2) NOT NULL,
    `decFreeQuantity` decimal(20,2) NOT NULL,
    `dtmManufactureDate` datetime ,
	`dtmExpiryDate` datetime ,
    `vhrBatchNo` varchar(100) NOT NULL,
	`vhrGeneratedBarcode` varchar(100) NOT NULL,
    `tinPrintLabel` int NOT NULL,
    `decMRP` decimal(20,2) NOT NULL,
    `decItemAmount` decimal(20,2) NOT NULL,
    `decMarginPercentage` decimal(20,2) NOT NULL,
    `decMarginAmount` decimal(20,2) NOT NULL,
    `decSchemeDiscount` decimal(20,2) NOT NULL,
    `decSchemeDiscountAmount` decimal(20,2) NOT NULL,
    `decVendorDiscount1` decimal(20,2) NOT NULL,
    `decVendorDiscountAmount1` decimal(20,2) NOT NULL,
    `decVendorDiscount2` decimal(20,2) NOT NULL,
    `decVendorDiscountAmount2` decimal(20,2) NOT NULL,
	`decItemDiscountAmount` decimal(20,2) NOT NULL,
    `chrVATTaxRecCode` varchar(36) NOT NULL,
    `decVATTaxPercentage` decimal(20,2) NOT NULL,
    `decVATTaxAmount` decimal(20,2) NOT NULL,
	`decItemAmountAfterTax` decimal(20,2) NOT NULL,
    `decCustomerDiscountPercentage` decimal(20,2) NOT NULL,
	`decCustomerDiscountAmount` decimal(20,2) NOT NULL,
    `decCustomerItemRate` decimal(20,2) NOT NULL,
    `dtmInsertDate` datetime NOT NULL,
    `chrInsertUser` varchar(36) NOT NULL,
    `dtmUpdateDate` datetime NOT NULL,
    `chrUpdateUser` varchar(36) NOT NULL,
    `chrIpAddress` varchar(100) NOT NULL,
	`tinIsDirty` int NOT NULL DEFAULT '0',
	 CONSTRAINT `PK_tblInwardItemDetails` PRIMARY KEY (`chrRecCode`),
     CONSTRAINT `FK_tblInwardItemDetails_chrParentCode_tblInwards` FOREIGN KEY (`chrParentCode`) REFERENCES `tblinwards` (`chrRecCode`),
     CONSTRAINT `FK_tblInwardItemDetails_chrItemCode_tblItemMasters` FOREIGN KEY (`chrItemCode`) REFERENCES `tblitemmasters` (`chrRecCode`),  
	 CONSTRAINT `FK_tblInwardItemDetails_chrStoreRackCode_tblstoreitemracks` FOREIGN KEY (`chrStoreRackCode`) REFERENCES `tblstoreitemracks` (`chrRecCode`),
	 CONSTRAINT `FK_tblInwardItemDetails_chrVATTaxRecCode_tblTaxDetails` FOREIGN KEY (`chrVATTaxRecCode`) REFERENCES `tbltaxdetails` (`chrRecCode`)
      
);


/* Name     :     tblSales
Description :     Table is used store information of items sold from the store location to a customer.
*/
 CREATE TABLE `tblsales` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrCompanyCode` varchar(36) NOT NULL,
	 `chrStoreCode` varchar(36) NOT NULL,
	 `chrOperatorCode` varchar(36) NOT NULL,
	 `vhrBillNo` varchar(100) NOT NULL,
	 `dtmBillDate` datetime NOT NULL,
	 `chrCustomerCode` varchar(36) NOT NULL,
	 `decItemTotal` decimal(20,2) NOT NULL,     
	 `decDiscountTotal` decimal(20,2) NOT NULL,
	 `decVATTaxTotal` decimal(20,2) NOT NULL,
	 `decTotalAmount` decimal(20,2) NOT NULL,
	 `intStatus` int NOT NULL DEFAULT '0',
	 `intAuthorizationLevel` int(1) NOT NULL DEFAULT '0',
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `dtmAuthorizationDateOne` datetime ,
	 `chrAuthorizationUserOne` varchar(36),
	 `dtmAuthorizationDateTwo` datetime ,
	 `chrAuthorizationUserTwo` varchar(36),
	 `dtmAuthorizationDateThree` datetime ,
	 `chrAuthorizationUserThree` varchar(36),  
     `chrIpAddress` varchar(100) NOT NULL ,
     `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblSales` PRIMARY KEY (`chrRecCode`),							
	  CONSTRAINT `FK_tblSales_chrCompanyCode_tblcompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
	  CONSTRAINT `FK_tblSales_chrStoreCode_tblstores` FOREIGN KEY (`chrStoreCode`) REFERENCES `tblstores` (`chrRecCode`),
	  CONSTRAINT `FK_tblSales_chrOperatorCode_tblEmployeeMasters` FOREIGN KEY (`chrOperatorCode`) REFERENCES `tblemployeemasters` (`chrRecCode`),
	  CONSTRAINT `FK_tblSales_chrCustomerCode_tblVendorMasters` FOREIGN KEY (`chrCustomerCode`) REFERENCES `tblvendormasters` (`chrRecCode`)
);


/* Name     :     tblSalesItemDetails
Description :     Table is used store information of items purchased by customer.
*/

CREATE TABLE `tblsalesitemdetails` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrParentCode` varchar(36) NOT NULL,
	 `chrItemCode` varchar(36) NOT NULL,
	 `vhrDescription` varchar(100) NOT NULL,
	 `vhrShortName` varchar(20) NOT NULL,
	 `vhrOurGeneratedBarCode` varchar(100) NOT NULL,
	 `decQuantity` decimal (20,2) NOT NULL,
	 `dtmManufactureDate` datetime NOT NULL,
	 `dtmExpiryDate` datetime NOT NULL,
	 `vhrBatchNo` varchar(100) NOT NULL,
	 `decMRP` decimal (20,2) NOT NULL,
	 `decCustomerDiscount` decimal(20,2) NOT NULL,
	 `decCustomerRate` decimal(20,2) NOT NULL,
	 `decVatTaxPercentage` decimal(20,2) NOT NULL,
	 `chrVatTaxRecCode` varchar(36) NOT NULL,
	 `decVatTaxAmount` decimal(20,2) NOT NULL,
	 `decItemCost` decimal(20,2) NOT NULL,
	 `intStatus` int NOT NULL DEFAULT '0',
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `chrIpAddress` varchar(100) NOT NULL,
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblSalesItemDetails` PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblSalesItemDetails_chrParentCode_tblSales` FOREIGN KEY (`chrParentCode`) REFERENCES `tblsales` (`chrRecCode`),
	  CONSTRAINT `FK_tblSalesItemDetails_chrItemCode_tblItemMasters` FOREIGN KEY (`chrItemCode`) REFERENCES `tblitemmasters` (`chrRecCode`),
	  CONSTRAINT `FK_tblSalesItemDetails_chrVatTaxRecCode_tblTaxDetails` FOREIGN KEY (`chrVatTaxRecCode`) REFERENCES `tbltaxdetails` (`chrRecCode`)
 );


/* Name     :     tblSalesPaymentDetails
Description :     Table is used store information of payment mode with denomination by the customer.
*/

CREATE TABLE `tblsalespaymentdetails` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrParentCode` varchar(36) NOT NULL,
	 `intPaymentModeCode` Int(10) NOT NULL,
	 `decAmount` decimal(20,2) NOT NULL,
	 `decCashReceived` decimal(20,2) NOT NULL,
	 `decBalance` decimal(20,2) NOT NULL,
	 `dtmCouponDenomination` datetime NOT NULL,
	 `vhrCardDetails` varchar(36) NOT NULL,
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `chrIpAddress` varchar(100) NOT NULL,
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblSalesPaymentDetails` PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblSalesPaymentDetails_chrParentCode_tblSales` FOREIGN KEY (`chrParentCode`) REFERENCES `tblsales` (`chrRecCode`)
);

/* Name     :     tblStockAdjustment
Description :     Table is used to adjust the stock.
*/

CREATE TABLE `tblstockadjustments` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrCompanyCode` varchar(36) NOT NULL,
	 `chrStoreCode` varchar(36) NOT NULL,
	 `vhrAdjustmentNo` varchar(100) NOT NULL,
	 `dtmAdjustmentDate` datetime NOT NULL,
	 `intStatus` int NOT NULL DEFAULT '0',
	 `intAuthorizationLevel` int(1) NOT NULL DEFAULT '0',
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `dtmAuthorizationDateOne` datetime ,
	 `chrAuthorizationUserOne` varchar(36),
	 `dtmAuthorizationDateTwo` datetime,
	 `chrAuthorizationUserTwo` varchar(36),
	 `dtmAuthorizationDateThree` datetime,
	 `chrAuthorizationUserThree` varchar(36),
	 `chrIpAddress` varchar(100),
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblStockAdjustments`  PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblStockAdjustments_chrCompanyCode_tblcompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
	  CONSTRAINT `FK_tblStockAdjustments_chrStoreCode_tblstores` FOREIGN KEY (`chrStoreCode`) REFERENCES `tblstores` (`chrRecCode`)
);

/* Name     :     tblStockAdjustmentItemDetails
Description :     Table is used store information of items purchased by customer.
*/

CREATE TABLE `tblstockadjustmentitemdetails` (
	 `chrRecCode` varchar(36) NOT NULL,
	 `chrParentCode` varchar(36) NOT NULL,
	 `chrItemCode` varchar(36) NOT NULL,
	 `vhrDescription` varchar(100) NOT NULL,
	 `intAdjustmentType` int NOT NULL,
	 `decQuantity` decimal(20,2) NOT NULL,
	 `intStatus` int NOT NULL DEFAULT '0',
	 `dtmInsertDate` datetime NOT NULL,
	 `chrInsertUser` varchar(36) NOT NULL,
	 `dtmUpdateDate` datetime NOT NULL,
	 `chrUpdateUser` varchar(36) NOT NULL,
	 `chrIpAddress` varchar(100),
	 `tinIsDirty` int NOT NULL DEFAULT '0',
	  CONSTRAINT `PK_tblStockAdjustmentItemDetails` PRIMARY KEY (`chrRecCode`),
	  CONSTRAINT `FK_tblStockAdjustmentItemDetails_chrParentCode_tblSales` FOREIGN KEY (`chrParentCode`) REFERENCES `tblsales` (`chrRecCode`),
	  CONSTRAINT `FK_tblStockAdjustmentItemDetails_chrItemCode_tblItemMasters` FOREIGN KEY (`chrItemCode`) REFERENCES `tblitemmasters` (`chrRecCode`)
);

/* Name     :     tblStocks
Description :     Table is used store inventory of items.
*/

CREATE TABLE `tblstocks` (
	`chrRecCode` varchar(36) NOT NULL,
	`chrCompanyCode` varchar(36) NOT NULL,
	`chrStoreCode` varchar(36) NOT NULL,
	`chrItemCode` varchar(36) NOT NULL,
    `intStockType` int NOT NULL,
    `decQuantity` decimal(20,2) NOT NULL,
    `dtmInsertDate` datetime NOT NULL,
	`chrInsertUser` varchar(36) NOT NULL,
	`dtmUpdateDate` datetime NOT NULL,
	`chrUpdateUser` varchar(36) NOT NULL,
	`chrIpAddress` varchar(100),
	`tinIsDirty` int NOT NULL DEFAULT '0',
	CONSTRAINT `PK_tblStocks` PRIMARY KEY (`chrRecCode`),
	CONSTRAINT `FK_tblStocks_chrCompanyCode_tblcompanies` FOREIGN KEY (`chrCompanyCode`) REFERENCES `tblcompanies` (`chrRecCode`),
	CONSTRAINT `FK_tblStocks_chrStoreCode_tblstores` FOREIGN KEY (`chrStoreCode`) REFERENCES `tblstores` (`chrRecCode`),
	CONSTRAINT `FK_tblStocks_chrItemCode_tblItemMasters` FOREIGN KEY (`chrItemCode`) REFERENCES `tblitemmasters` (`chrRecCode`)

);

/* Name     :     tblRetailLoginHistories
Description :     Table is used to store login history
*/
CREATE TABLE `tblretailloginhistories` (
  `chrRecCode` char(36) NOT NULL,
  `chrCustomerCode` char(36) NOT NULL,
  `dteLogInTime` datetime DEFAULT NULL,
  `dteLogoutTime` datetime DEFAULT NULL,
  `tinIsDirty` int NOT NULL DEFAULT '0',
  `tinActiveFlag` int DEFAULT NULL,
  `chrIpAddress` varchar(100) DEFAULT NULL,
  CONSTRAINT `PK_tblRetailLoginHistories` PRIMARY KEY (`chrRecCode`),
  CONSTRAINT `FK_tblRetailLoginHistories_chrCustomerCode_tblCustomerMasters` FOREIGN KEY (`chrCustomerCode`) REFERENCES `tblcustomermasters` (`chrRecCode`)
  
);

/* Name     :     tblDefaultlookUp
Description :     Table is used to store default values that are used in all forms
*/
CREATE TABLE `tbldefaultlookups` (
	`intRecCode` int(10) NOT NULL,
    `vhrName` varchar(100) NOT NULL,
    `LookupTypeCode` int(10) NOT NULL,
    CONSTRAINT `PK_tblDefaultLookUps` PRIMARY KEY (`intRecCode`)
);

 
