-- Insert statement for  tblcompanies
INSERT INTO `dtb_retail`.`tblcompanies`
(`chrRecCode`,
`vhrCode`,
`vhrName`,
`vhrAddress1`,
`vhrAddress2`,
`vhrAddress3`,
`vhrAddress4`,
`intPinCode`,
`vhrTel1`,
`vhrTel2`,
`vhrPANNo`,
`vhrServiceTaxNo`,
`vhrCSTNo`,
`vhrVATNo`,
`tinActiveFlag`,
`dtmInsertDate`,
`chrInsertUser`,
`dtmUpdateDate`,
`chrUpdateUser`,
`chrIpAddress`,
`tinIsDirty`)
VALUES
('00000000-0000-0000-0000-000000000001',
'SPR001',
'Shiv Parvati ',
'Mumbai',
'',
'',
'',
421201,
null,
null,
'PAN1234',
'STN1234',
'CSTN1234',
'VAT1234',
1,
now(),
'00000000-0000-0000-0000-000000000001',
now(),
'00000000-0000-0000-0000-000000000001',
'192.168.1.22',
1);


-- Insert statement for  tblstores
INSERT INTO `dtb_retail`.`tblstores`
(`chrRecCode`,
`chrParentCode`,
`vhrCode`,
`vhrName`,
`vhrAddress1`,
`vhrAddress2`,
`vhrAddress3`,
`vhrAddress4`,
`intPinCode`,
`vhrTel1`,
`vhrTel2`,
`vhrPANNo`,
`vhrServiceTaxNo`,
`vhrCSTNo`,
`vhrVATNo`,
`tinActiveFlag`,
`dtmInsertDate`,
`chrInsertUser`,
`dtmUpdateDate`,
`chrUpdateUser`,
`chrIpAddress`,
`tinIsDirty`)
VALUES
('00000000-0000-0000-0000-000000000001',
'00000000-0000-0000-0000-000000000001',
'MS0001',
'Mega Store',
'Mumbai',
null,
null,
null,
421204,
null,
null,
'PAN1234',
'STN1234',
'CSTN1234',
'VAT1234',
1,
now(),
'00000000-0000-0000-0000-000000000001',
now(),
'00000000-0000-0000-0000-000000000001',
'192.168.1.22',
1);


-- Insert statement for  tblEmployeeCategories
INSERT INTO `dtb_retail`.`tblEmployeeCategories`
(`chrRecCode`,
`vhrName`,
`vhrCode`,
`tinActiveFlag`,
`dtmInsertDate`,
`chrInsertUser`,
`dtmUpdateDate`,
`chrUpdateUser`,
`chrIpAddress`,
`tinIsDirty`)
VALUES
('00000000-0000-0000-0000-000000000001',
'Sales Person',
'SP',
1,
now(),
'00000000-0000-0000-0000-000000000001',
now(),
'00000000-0000-0000-0000-000000000001',
'192.168.1.22',
1);



INSERT INTO `dtb_retail`.`tblEmployeeCategories`
(`chrRecCode`,
`vhrName`,
`vhrCode`,
`tinActiveFlag`,
`dtmInsertDate`,
`chrInsertUser`,
`dtmUpdateDate`,
`chrUpdateUser`,
`chrIpAddress`,
`tinIsDirty`)
VALUES
('00000000-0000-0000-0000-000000000002',
'Branch Manager',
'BM',
1,
now(),
'00000000-0000-0000-0000-000000000001',
now(),
'00000000-0000-0000-0000-000000000001',
'192.168.1.22',
1);



-- Insert statement for  tblEmployeeMasters
INSERT INTO `dtb_retail`.`tblEmployeeMasters`
(`chrRecCode`,
`chrCompanyCode`,
`chrEmployeeCategoryCode`,
`chrStoreCode`,
`vhrCode`,
`vhrName`,
`vhrAddress1`,
`vhrAddress2`,
`vhrAddress3`,
`vhrAddress4`,
`intPinCode`,
`vhrTel1`,
`vhrTel2`,
`vhrPANNo`,
`vhrUserName`,
`vhrPassword`,
`tinLockUser`,
`tinActiveFlag`,
`dtmInsertDate`,
`chrInsertUser`,
`dtmUpdateDate`,
`chrUpdateUser`,
`chrIpAddress`,
`tinIsDirty`)
VALUES
('00000000-0000-0000-0000-000000000001',
'00000000-0000-0000-0000-000000000001',
'00000000-0000-0000-0000-000000000002',
'00000000-0000-0000-0000-000000000001',
'SP001',
'JAYDEEEP B',
'DOMBIVLI',
NULL,
null,
null,
380001,
null,
null,
'PAN1234',
'jaydeep',
'hits345ht',
1,
1,
now(),
'00000000-0000-0000-0000-000000000001',
now(),
'00000000-0000-0000-0000-000000000001',
'192.168.1.22',
1);


INSERT INTO `dtb_retail`.`tblEmployeeMasters`
(`chrRecCode`,
`chrCompanyCode`,
`chrEmployeeCategoryCode`,
`chrStoreCode`,
`vhrCode`,
`vhrName`,
`vhrAddress1`,
`vhrAddress2`,
`vhrAddress3`,
`vhrAddress4`,
`intPinCode`,
`vhrTel1`,
`vhrTel2`,
`vhrPANNo`,
`vhrUserName`,
`vhrPassword`,
`tinLockUser`,
`tinActiveFlag`,
`dtmInsertDate`,
`chrInsertUser`,
`dtmUpdateDate`,
`chrUpdateUser`,
`chrIpAddress`,
`tinIsDirty`)
VALUES
('00000000-0000-0000-0000-000000000002',
'00000000-0000-0000-0000-000000000001',
'00000000-0000-0000-0000-000000000002',
'00000000-0000-0000-0000-000000000001',
'BM001',
'ANIKET P',
'THANE',
NULL,
null,
null,
380401,
null,
null,
'PAN1234',
'aniket',
'hits345bt',
1,
1,
now(),
'00000000-0000-0000-0000-000000000001',
now(),
'00000000-0000-0000-0000-000000000001',
'192.168.1.22',
1);


UPDATE tblEmployeeMasters SET vhrPassword = 'iQSMgklN4Z24MvA9rb2HuA==' where chrRecCode = '00000000-0000-0000-0000-000000000002';

UPDATE tblEmployeeMasters SET vhrPassword = 'TqQj7dGYhd1eOSKyajJpSg==' where chrRecCode = '00000000-0000-0000-0000-000000000001';


-- jaydeep
-- TqQj7dGYhd1eOSKyajJpSg==

-- aniket
-- iQSMgklN4Z24MvA9rb2HuA==


-- 09/10/2015

INSERT INTO `dtb_retail`.`tbldefaultlookups`
(`intRecCode`,
`vhrName`,
`LookupTypeCode`)
VALUES
(1,
'YES',
1);



INSERT INTO `dtb_retail`.`tbldefaultlookups`
(`intRecCode`,
`vhrName`,
`LookupTypeCode`)
VALUES
(2,
'NO',
1);

-- 10/10/2015
INSERT INTO `dtb_retail`.`tbldefaultlookups`
(`intRecCode`,
`vhrName`,
`LookupTypeCode`)
VALUES
(5,
'ADD',
3);


INSERT INTO `dtb_retail`.`tbldefaultlookups`
(`intRecCode`,
`vhrName`,
`LookupTypeCode`)
VALUES
(6,
'REMOVE',
3);
