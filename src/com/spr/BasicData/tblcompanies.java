package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblcompany;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblcompanies extends com.heptanesia.Data.BasicData {
	public tblcompanies() {
	}
	
	public tblcompanies(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblcompaniesColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcompanies", columns, where, orderBy);
		return json;
	}

	public String gettblcompaniesAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcompanies", "*", where, orderBy);
		return json;
	}

	public String gettblcompaniesAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcompanies", "*", "", "");
		return json;
	}
	
	public String gettblcompaniesForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcompanies", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblcompaniesForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcompanies", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblcompaniesForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblcompanies", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblcompaniesInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblcompanies", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblcompany JSONToClass(String record) {
		Gson gson = new Gson();
		tblcompany r = gson.fromJson(record, tblcompany.class);
		
		return r;
	}

	public ArrayList<tblcompany> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblcompany>>(){}.getType();
		ArrayList<tblcompany> rs = new ArrayList<tblcompany>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblcompany(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblcompany r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblcompany(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblcompany(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblcompany(Connection localConnection, String record) {
		tblcompany r = this.JSONToClass(record);
		
		return this.inserttblcompany(localConnection, r);
	}
	
	public boolean inserttblcompany(Connection localConnection, tblcompany r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrcstno = r.getvhrCSTNo(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrservicetaxno = r.getvhrServiceTaxNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrvatno = r.getvhrVATNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblcompanies_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblcompany(Connection localConnection, String record) {
		tblcompany r = this.JSONToClass(record);
		
		return this.updatetblcompany(localConnection, r);
	}
	
	public boolean updatetblcompany(Connection localConnection, tblcompany r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrcstno = r.getvhrCSTNo(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrservicetaxno = r.getvhrServiceTaxNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrvatno = r.getvhrVATNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblcompanies_update",
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblcompany(String record) throws SQLException {
		boolean returnValue = false;
		tblcompany r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblcompanies",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
