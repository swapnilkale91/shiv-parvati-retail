package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblcustomermaster;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblcustomermasters extends com.heptanesia.Data.BasicData {
	public tblcustomermasters() {
	}
	
	public tblcustomermasters(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblcustomermastersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcustomermasters", columns, where, orderBy);
		return json;
	}

	public String gettblcustomermastersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcustomermasters", "*", where, orderBy);
		return json;
	}

	public String gettblcustomermastersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcustomermasters", "*", "", "");
		return json;
	}
	
	public String gettblcustomermastersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcustomermasters", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblcustomermastersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblcustomermasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblcustomermastersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblcustomermasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblcustomermastersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblcustomermasters", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblcustomermaster JSONToClass(String record) {
		Gson gson = new Gson();
		tblcustomermaster r = gson.fromJson(record, tblcustomermaster.class);
		
		return r;
	}

	public ArrayList<tblcustomermaster> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblcustomermaster>>(){}.getType();
		ArrayList<tblcustomermaster> rs = new ArrayList<tblcustomermaster>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblcustomermaster(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblcustomermaster r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblcustomermaster(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblcustomermaster(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblcustomermaster(Connection localConnection, String record) {
		tblcustomermaster r = this.JSONToClass(record);
		
		return this.inserttblcustomermaster(localConnection, r);
	}
	
	public boolean inserttblcustomermaster(Connection localConnection, tblcustomermaster r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblcustomermasters_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrtel1,
				vhrtel2
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrtel1,
				vhrtel2
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblcustomermaster(Connection localConnection, String record) {
		tblcustomermaster r = this.JSONToClass(record);
		
		return this.updatetblcustomermaster(localConnection, r);
	}
	
	public boolean updatetblcustomermaster(Connection localConnection, tblcustomermaster r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblcustomermasters_update",
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrtel1,
				vhrtel2
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrtel1,
				vhrtel2
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblcustomermaster(String record) throws SQLException {
		boolean returnValue = false;
		tblcustomermaster r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblcustomermasters",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
