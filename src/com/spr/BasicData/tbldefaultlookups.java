package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tbldefaultlookup;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tbldefaultlookups extends com.heptanesia.Data.BasicData {
	public tbldefaultlookups() {
	}
	
	public tbldefaultlookups(Connection connection) {
		this.connection = connection;
	}
	
	public String gettbldefaultlookupsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbldefaultlookups", columns, where, orderBy);
		return json;
	}

	public String gettbldefaultlookupsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbldefaultlookups", "*", where, orderBy);
		return json;
	}

	public String gettbldefaultlookupsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbldefaultlookups", "*", "", "");
		return json;
	}
	
	public String gettbldefaultlookupsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbldefaultlookups", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettbldefaultlookupsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbldefaultlookups", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettbldefaultlookupsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tbldefaultlookups", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettbldefaultlookupsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tbldefaultlookups", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tbldefaultlookup JSONToClass(String record) {
		Gson gson = new Gson();
		tbldefaultlookup r = gson.fromJson(record, tbldefaultlookup.class);
		
		return r;
	}

	public ArrayList<tbldefaultlookup> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tbldefaultlookup>>(){}.getType();
		ArrayList<tbldefaultlookup> rs = new ArrayList<tbldefaultlookup>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetbldefaultlookup(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tbldefaultlookup r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttbldefaultlookup(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetbldefaultlookup(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttbldefaultlookup(Connection localConnection, String record) {
		tbldefaultlookup r = this.JSONToClass(record);
		
		return this.inserttbldefaultlookup(localConnection, r);
	}
	
	public boolean inserttbldefaultlookup(Connection localConnection, tbldefaultlookup r) {
		boolean result = false;

		String intreccode = r.getintRecCode(); 
		int lookuptypecode = r.getLookupTypeCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbldefaultlookups_insert",
				intreccode,
				lookuptypecode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				intreccode,
				lookuptypecode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetbldefaultlookup(Connection localConnection, String record) {
		tbldefaultlookup r = this.JSONToClass(record);
		
		return this.updatetbldefaultlookup(localConnection, r);
	}
	
	public boolean updatetbldefaultlookup(Connection localConnection, tbldefaultlookup r) {
		boolean result = false;

		String intreccode = r.getintRecCode(); 
		int lookuptypecode = r.getLookupTypeCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbldefaultlookups_update",
				intreccode,
				lookuptypecode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				intreccode,
				lookuptypecode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetbldefaultlookup(String record) throws SQLException {
		boolean returnValue = false;
		tbldefaultlookup r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tbldefaultlookups",r.getintRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
