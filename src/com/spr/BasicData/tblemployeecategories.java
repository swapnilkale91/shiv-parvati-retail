package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblemployeecategory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblemployeecategories extends com.heptanesia.Data.BasicData {
	public tblemployeecategories() {
	}
	
	public tblemployeecategories(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblemployeecategoriesColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblemployeecategories", columns, where, orderBy);
		return json;
	}

	public String gettblemployeecategoriesAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblemployeecategories", "*", where, orderBy);
		return json;
	}

	public String gettblemployeecategoriesAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblemployeecategories", "*", "", "");
		return json;
	}
	
	public String gettblemployeecategoriesForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblemployeecategories", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblemployeecategoriesForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblemployeecategories", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblemployeecategoriesForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblemployeecategories", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblemployeecategoriesInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblemployeecategories", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblemployeecategory JSONToClass(String record) {
		Gson gson = new Gson();
		tblemployeecategory r = gson.fromJson(record, tblemployeecategory.class);
		
		return r;
	}

	public ArrayList<tblemployeecategory> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblemployeecategory>>(){}.getType();
		ArrayList<tblemployeecategory> rs = new ArrayList<tblemployeecategory>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblemployeecategory(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblemployeecategory r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblemployeecategory(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblemployeecategory(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblemployeecategory(Connection localConnection, String record) {
		tblemployeecategory r = this.JSONToClass(record);
		
		return this.inserttblemployeecategory(localConnection, r);
	}
	
	public boolean inserttblemployeecategory(Connection localConnection, tblemployeecategory r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblemployeecategories_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblemployeecategory(Connection localConnection, String record) {
		tblemployeecategory r = this.JSONToClass(record);
		
		return this.updatetblemployeecategory(localConnection, r);
	}
	
	public boolean updatetblemployeecategory(Connection localConnection, tblemployeecategory r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblemployeecategories_update",
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblemployeecategory(String record) throws SQLException {
		boolean returnValue = false;
		tblemployeecategory r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblemployeecategories",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
