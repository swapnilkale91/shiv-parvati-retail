package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblemployeemaster;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblemployeemasters extends com.heptanesia.Data.BasicData {
	public tblemployeemasters() {
	}
	
	public tblemployeemasters(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblemployeemastersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblEmployeeMasters", columns, where, orderBy);
		return json;
	}

	public String gettblemployeemastersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblEmployeeMasters", "*", where, orderBy);
		return json;
	}

	public String gettblemployeemastersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblEmployeeMasters", "*", "", "");
		return json;
	}
	
	public String gettblemployeemastersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblEmployeeMasters", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblemployeemastersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblEmployeeMasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblemployeemastersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblEmployeeMasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblemployeemastersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblEmployeeMasters", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblemployeemaster JSONToClass(String record) {
		Gson gson = new Gson();
		tblemployeemaster r = gson.fromJson(record, tblemployeemaster.class);
		
		return r;
	}

	public ArrayList<tblemployeemaster> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblemployeemaster>>(){}.getType();
		ArrayList<tblemployeemaster> rs = new ArrayList<tblemployeemaster>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblemployeemaster(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblemployeemaster r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblemployeemaster(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblemployeemaster(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblemployeemaster(Connection localConnection, String record) {
		tblemployeemaster r = this.JSONToClass(record);
		
		return this.inserttblemployeemaster(localConnection, r);
	}
	
	public boolean inserttblemployeemaster(Connection localConnection, tblemployeemaster r) {
		boolean result = false;

		String chrcompanycode = r.getchrCompanyCode(); 
		String chremployeecategorycode = r.getchrEmployeeCategoryCode(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinlockuser = r.gettinLockUser(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrpassword = r.getvhrPassword(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrusername = r.getvhrUserName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblEmployeeMasters_insert",
				chrcompanycode,
				chremployeecategorycode,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				tinlockuser,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrpassword,
				vhrtel1,
				vhrtel2,
				vhrusername
			));

			this.common.setParameters(callableStatement,
				chrcompanycode,
				chremployeecategorycode,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				tinlockuser,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrpassword,
				vhrtel1,
				vhrtel2,
				vhrusername
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblemployeemaster(Connection localConnection, String record) {
		tblemployeemaster r = this.JSONToClass(record);
		
		return this.updatetblemployeemaster(localConnection, r);
	}
	
	public boolean updatetblemployeemaster(Connection localConnection, tblemployeemaster r) {
		boolean result = false;

		String chrcompanycode = r.getchrCompanyCode(); 
		String chremployeecategorycode = r.getchrEmployeeCategoryCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinlockuser = r.gettinLockUser(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrpassword = r.getvhrPassword(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrusername = r.getvhrUserName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblEmployeeMasters_update",
				chrcompanycode,
				chremployeecategorycode,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				tinlockuser,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrpassword,
				vhrtel1,
				vhrtel2,
				vhrusername
			));

			this.common.setParameters(callableStatement,
				chrcompanycode,
				chremployeecategorycode,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				tinlockuser,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrname,
				vhrpanno,
				vhrpassword,
				vhrtel1,
				vhrtel2,
				vhrusername
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblemployeemaster(String record) throws SQLException {
		boolean returnValue = false;
		tblemployeemaster r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblEmployeeMasters",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
