package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblinwarditemdetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblinwarditemdetails extends com.heptanesia.Data.BasicData {
	public tblinwarditemdetails() {
	}
	
	public tblinwarditemdetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblinwarditemdetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwarditemdetails", columns, where, orderBy);
		return json;
	}

	public String gettblinwarditemdetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwarditemdetails", "*", where, orderBy);
		return json;
	}

	public String gettblinwarditemdetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwarditemdetails", "*", "", "");
		return json;
	}
	
	public String gettblinwarditemdetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwarditemdetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblinwarditemdetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwarditemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblinwarditemdetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblinwarditemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblinwarditemdetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblinwarditemdetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblinwarditemdetail JSONToClass(String record) {
		Gson gson = new Gson();
		tblinwarditemdetail r = gson.fromJson(record, tblinwarditemdetail.class);
		
		return r;
	}

	public ArrayList<tblinwarditemdetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblinwarditemdetail>>(){}.getType();
		ArrayList<tblinwarditemdetail> rs = new ArrayList<tblinwarditemdetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblinwarditemdetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblinwarditemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblinwarditemdetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblinwarditemdetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblinwarditemdetail(Connection localConnection, String record) {
		tblinwarditemdetail r = this.JSONToClass(record);
		
		return this.inserttblinwarditemdetail(localConnection, r);
	}
	
	public boolean inserttblinwarditemdetail(Connection localConnection, tblinwarditemdetail r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorerackcode = r.getchrStoreRackCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvattaxreccode = r.getchrVATTaxRecCode(); 
		float deccustomerdiscountamount = r.getdecCustomerDiscountAmount(); 
		float deccustomerdiscountpercentage = r.getdecCustomerDiscountPercentage(); 
		float deccustomeritemrate = r.getdecCustomerItemRate(); 
		float decfreequantity = r.getdecFreeQuantity(); 
		float decitemamount = r.getdecItemAmount(); 
		float decitemamountaftertax = r.getdecItemAmountAfterTax(); 
		float decitemdiscountamount = r.getdecItemDiscountAmount(); 
		float decmarginamount = r.getdecMarginAmount(); 
		float decmarginpercentage = r.getdecMarginPercentage(); 
		float decmrp = r.getdecMRP(); 
		float decquantity = r.getdecQuantity(); 
		float decschemediscount = r.getdecSchemeDiscount(); 
		float decschemediscountamount = r.getdecSchemeDiscountAmount(); 
		float decvattaxamount = r.getdecVATTaxAmount(); 
		float decvattaxpercentage = r.getdecVATTaxPercentage(); 
		float decvendordiscount1 = r.getdecVendorDiscount1(); 
		float decvendordiscount2 = r.getdecVendorDiscount2(); 
		float decvendordiscountamount1 = r.getdecVendorDiscountAmount1(); 
		float decvendordiscountamount2 = r.getdecVendorDiscountAmount2(); 
		String dtmexpirydate = r.getdtmExpiryDate(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmmanufacturedate = r.getdtmManufactureDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinprintlabel = r.gettinPrintLabel(); 
		String vhrbarcode = r.getvhrBarCode(); 
		String vhrbatchno = r.getvhrBatchNo(); 
		String vhrdescription = r.getvhrDescription(); 
		String vhrgeneratedbarcode = r.getvhrGeneratedBarcode(); 
		String vhrpacking = r.getvhrPacking();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblinwarditemdetails_insert",
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrstorerackcode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscountamount,
				deccustomerdiscountpercentage,
				deccustomeritemrate,
				decfreequantity,
				decitemamount,
				decitemamountaftertax,
				decitemdiscountamount,
				decmarginamount,
				decmarginpercentage,
				decmrp,
				decquantity,
				decschemediscount,
				decschemediscountamount,
				decvattaxamount,
				decvattaxpercentage,
				decvendordiscount1,
				decvendordiscount2,
				decvendordiscountamount1,
				decvendordiscountamount2,
				dtmexpirydate,
				dtminsertdate,
				dtmmanufacturedate,
				dtmupdatedate,
				tinisdirty,
				tinprintlabel,
				vhrbarcode,
				vhrbatchno,
				vhrdescription,
				vhrgeneratedbarcode,
				vhrpacking
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrstorerackcode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscountamount,
				deccustomerdiscountpercentage,
				deccustomeritemrate,
				decfreequantity,
				decitemamount,
				decitemamountaftertax,
				decitemdiscountamount,
				decmarginamount,
				decmarginpercentage,
				decmrp,
				decquantity,
				decschemediscount,
				decschemediscountamount,
				decvattaxamount,
				decvattaxpercentage,
				decvendordiscount1,
				decvendordiscount2,
				decvendordiscountamount1,
				decvendordiscountamount2,
				dtmexpirydate,
				dtminsertdate,
				dtmmanufacturedate,
				dtmupdatedate,
				tinisdirty,
				tinprintlabel,
				vhrbarcode,
				vhrbatchno,
				vhrdescription,
				vhrgeneratedbarcode,
				vhrpacking
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblinwarditemdetail(Connection localConnection, String record) {
		tblinwarditemdetail r = this.JSONToClass(record);
		
		return this.updatetblinwarditemdetail(localConnection, r);
	}
	
	public boolean updatetblinwarditemdetail(Connection localConnection, tblinwarditemdetail r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorerackcode = r.getchrStoreRackCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvattaxreccode = r.getchrVATTaxRecCode(); 
		float deccustomerdiscountamount = r.getdecCustomerDiscountAmount(); 
		float deccustomerdiscountpercentage = r.getdecCustomerDiscountPercentage(); 
		float deccustomeritemrate = r.getdecCustomerItemRate(); 
		float decfreequantity = r.getdecFreeQuantity(); 
		float decitemamount = r.getdecItemAmount(); 
		float decitemamountaftertax = r.getdecItemAmountAfterTax(); 
		float decitemdiscountamount = r.getdecItemDiscountAmount(); 
		float decmarginamount = r.getdecMarginAmount(); 
		float decmarginpercentage = r.getdecMarginPercentage(); 
		float decmrp = r.getdecMRP(); 
		float decquantity = r.getdecQuantity(); 
		float decschemediscount = r.getdecSchemeDiscount(); 
		float decschemediscountamount = r.getdecSchemeDiscountAmount(); 
		float decvattaxamount = r.getdecVATTaxAmount(); 
		float decvattaxpercentage = r.getdecVATTaxPercentage(); 
		float decvendordiscount1 = r.getdecVendorDiscount1(); 
		float decvendordiscount2 = r.getdecVendorDiscount2(); 
		float decvendordiscountamount1 = r.getdecVendorDiscountAmount1(); 
		float decvendordiscountamount2 = r.getdecVendorDiscountAmount2(); 
		String dtmexpirydate = r.getdtmExpiryDate(); 
		String dtmmanufacturedate = r.getdtmManufactureDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinprintlabel = r.gettinPrintLabel(); 
		String vhrbarcode = r.getvhrBarCode(); 
		String vhrbatchno = r.getvhrBatchNo(); 
		String vhrdescription = r.getvhrDescription(); 
		String vhrgeneratedbarcode = r.getvhrGeneratedBarcode(); 
		String vhrpacking = r.getvhrPacking();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblinwarditemdetails_update",
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrstorerackcode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscountamount,
				deccustomerdiscountpercentage,
				deccustomeritemrate,
				decfreequantity,
				decitemamount,
				decitemamountaftertax,
				decitemdiscountamount,
				decmarginamount,
				decmarginpercentage,
				decmrp,
				decquantity,
				decschemediscount,
				decschemediscountamount,
				decvattaxamount,
				decvattaxpercentage,
				decvendordiscount1,
				decvendordiscount2,
				decvendordiscountamount1,
				decvendordiscountamount2,
				dtmexpirydate,
				dtmmanufacturedate,
				dtmupdatedate,
				tinisdirty,
				tinprintlabel,
				vhrbarcode,
				vhrbatchno,
				vhrdescription,
				vhrgeneratedbarcode,
				vhrpacking
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrstorerackcode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscountamount,
				deccustomerdiscountpercentage,
				deccustomeritemrate,
				decfreequantity,
				decitemamount,
				decitemamountaftertax,
				decitemdiscountamount,
				decmarginamount,
				decmarginpercentage,
				decmrp,
				decquantity,
				decschemediscount,
				decschemediscountamount,
				decvattaxamount,
				decvattaxpercentage,
				decvendordiscount1,
				decvendordiscount2,
				decvendordiscountamount1,
				decvendordiscountamount2,
				dtmexpirydate,
				dtmmanufacturedate,
				dtmupdatedate,
				tinisdirty,
				tinprintlabel,
				vhrbarcode,
				vhrbatchno,
				vhrdescription,
				vhrgeneratedbarcode,
				vhrpacking
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblinwarditemdetail(String record) throws SQLException {
		boolean returnValue = false;
		tblinwarditemdetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblinwarditemdetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
