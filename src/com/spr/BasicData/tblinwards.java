package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblinward;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblinwards extends com.heptanesia.Data.BasicData {
	public tblinwards() {
	}
	
	public tblinwards(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblinwardsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwards", columns, where, orderBy);
		return json;
	}

	public String gettblinwardsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwards", "*", where, orderBy);
		return json;
	}

	public String gettblinwardsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwards", "*", "", "");
		return json;
	}
	
	public String gettblinwardsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwards", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblinwardsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblinwards", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblinwardsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblinwards", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblinwardsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblinwards", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblinward JSONToClass(String record) {
		Gson gson = new Gson();
		tblinward r = gson.fromJson(record, tblinward.class);
		
		return r;
	}

	public ArrayList<tblinward> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblinward>>(){}.getType();
		ArrayList<tblinward> rs = new ArrayList<tblinward>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblinward(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblinward r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblinward(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblinward(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblinward(Connection localConnection, String record) {
		tblinward r = this.JSONToClass(record);
		
		return this.inserttblinward(localConnection, r);
	}
	
	public boolean inserttblinward(Connection localConnection, tblinward r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrpocode = r.getchrPOCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvendorcode = r.getchrVendorCode(); 
		float decitemdiscounttotal = r.getdecItemDiscountTotal(); 
		float decitemschemetotal = r.getdecItemSchemeTotal(); 
		float decitemtotal = r.getdecItemTotal(); 
		float decvattaxtotal = r.getdecVATTaxTotal(); 
		float decvendorpayableamount = r.getdecVendorPayableAmount(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtmduedate = r.getdtmDueDate(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtminwarddate = r.getdtmInwarddate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intcreditperiod = r.getintCreditPeriod(); 
		int tinauthorizationlevel = r.gettinAuthorizationLevel(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus(); 
		String vhrchallanno = r.getvhrChallanNo(); 
		String vhrinwardno = r.getvhrInwardNo(); 
		String vhrlorryno = r.getvhrLorryNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblinwards_insert",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrpocode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				chrvendorcode,
				decitemdiscounttotal,
				decitemschemetotal,
				decitemtotal,
				decvattaxtotal,
				decvendorpayableamount,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmduedate,
				dtminsertdate,
				dtminwarddate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus,
				vhrchallanno,
				vhrinwardno,
				vhrlorryno
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrpocode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				chrvendorcode,
				decitemdiscounttotal,
				decitemschemetotal,
				decitemtotal,
				decvattaxtotal,
				decvendorpayableamount,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmduedate,
				dtminsertdate,
				dtminwarddate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus,
				vhrchallanno,
				vhrinwardno,
				vhrlorryno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblinward(Connection localConnection, String record) {
		tblinward r = this.JSONToClass(record);
		
		return this.updatetblinward(localConnection, r);
	}
	
	public boolean updatetblinward(Connection localConnection, tblinward r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrpocode = r.getchrPOCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvendorcode = r.getchrVendorCode(); 
		float decitemdiscounttotal = r.getdecItemDiscountTotal(); 
		float decitemschemetotal = r.getdecItemSchemeTotal(); 
		float decitemtotal = r.getdecItemTotal(); 
		float decvattaxtotal = r.getdecVATTaxTotal(); 
		float decvendorpayableamount = r.getdecVendorPayableAmount(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtmduedate = r.getdtmDueDate(); 
		String dtminwarddate = r.getdtmInwarddate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intcreditperiod = r.getintCreditPeriod(); 
		int tinauthorizationlevel = r.gettinAuthorizationLevel(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus(); 
		String vhrchallanno = r.getvhrChallanNo(); 
		String vhrinwardno = r.getvhrInwardNo(); 
		String vhrlorryno = r.getvhrLorryNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblinwards_update",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chripaddress,
				chrpocode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				chrvendorcode,
				decitemdiscounttotal,
				decitemschemetotal,
				decitemtotal,
				decvattaxtotal,
				decvendorpayableamount,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmduedate,
				dtminwarddate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus,
				vhrchallanno,
				vhrinwardno,
				vhrlorryno
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chripaddress,
				chrpocode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				chrvendorcode,
				decitemdiscounttotal,
				decitemschemetotal,
				decitemtotal,
				decvattaxtotal,
				decvendorpayableamount,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmduedate,
				dtminwarddate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus,
				vhrchallanno,
				vhrinwardno,
				vhrlorryno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblinward(String record) throws SQLException {
		boolean returnValue = false;
		tblinward r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblinwards",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
