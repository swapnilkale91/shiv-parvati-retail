package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblitemcategory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblitemcategories extends com.heptanesia.Data.BasicData {
	public tblitemcategories() {
	}
	
	public tblitemcategories(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblitemcategoriesColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemcategories", columns, where, orderBy);
		return json;
	}

	public String gettblitemcategoriesAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemcategories", "*", where, orderBy);
		return json;
	}

	public String gettblitemcategoriesAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemcategories", "*", "", "");
		return json;
	}
	
	public String gettblitemcategoriesForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemcategories", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblitemcategoriesForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemcategories", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblitemcategoriesForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblitemcategories", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblitemcategoriesInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblitemcategories", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblitemcategory JSONToClass(String record) {
		Gson gson = new Gson();
		tblitemcategory r = gson.fromJson(record, tblitemcategory.class);
		
		return r;
	}

	public ArrayList<tblitemcategory> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblitemcategory>>(){}.getType();
		ArrayList<tblitemcategory> rs = new ArrayList<tblitemcategory>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblitemcategory(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblitemcategory r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblitemcategory(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblitemcategory(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblitemcategory(Connection localConnection, String record) {
		tblitemcategory r = this.JSONToClass(record);
		
		return this.inserttblitemcategory(localConnection, r);
	}
	
	public boolean inserttblitemcategory(Connection localConnection, tblitemcategory r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblitemcategories_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblitemcategory(Connection localConnection, String record) {
		tblitemcategory r = this.JSONToClass(record);
		
		return this.updatetblitemcategory(localConnection, r);
	}
	
	public boolean updatetblitemcategory(Connection localConnection, tblitemcategory r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblitemcategories_update",
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblitemcategory(String record) throws SQLException {
		boolean returnValue = false;
		tblitemcategory r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblitemcategories",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
