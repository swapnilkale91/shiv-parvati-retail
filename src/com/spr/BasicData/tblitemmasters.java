package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblitemmaster;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblitemmasters extends com.heptanesia.Data.BasicData {
	public tblitemmasters() {
	}
	
	public tblitemmasters(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblitemmastersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemmasters", columns, where, orderBy);
		return json;
	}

	public String gettblitemmastersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemmasters", "*", where, orderBy);
		return json;
	}

	public String gettblitemmastersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemmasters", "*", "", "");
		return json;
	}
	
	public String gettblitemmastersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemmasters", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblitemmastersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemmasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblitemmastersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblitemmasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblitemmastersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblitemmasters", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblitemmaster JSONToClass(String record) {
		Gson gson = new Gson();
		tblitemmaster r = gson.fromJson(record, tblitemmaster.class);
		
		return r;
	}

	public ArrayList<tblitemmaster> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblitemmaster>>(){}.getType();
		ArrayList<tblitemmaster> rs = new ArrayList<tblitemmaster>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblitemmaster(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblitemmaster r = this.JSONToClass(record);
		boolean returnValue = false;

		System.out.println("iud check : " + r.getIUD());
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblitemmaster(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblitemmaster(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblitemmaster(Connection localConnection, String record) {
		tblitemmaster r = this.JSONToClass(record);
		
		return this.inserttblitemmaster(localConnection, r);
	}
	
	public boolean inserttblitemmaster(Connection localConnection, tblitemmaster r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcategorycode = r.getchrItemCategoryCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrunitofmeasurecode = r.getchrUnitOfMeasureCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intreorderquantity = r.getintReOrderQuantity(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinlosesold = r.gettinLoseSold(); 
		String vhrbarcode = r.getvhrBarcode(); 
		String vhrcode = r.getvhrCode(); 
		String vhrdescription = r.getvhrDescription(); 
		String vhrname = r.getvhrName(); 
		String vhrshortname = r.getvhrShortName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblitemmasters_insert",
				chrinsertuser,
				chripaddress,
				chritemcategorycode,
				chrreccode,
				chrunitofmeasurecode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intreorderquantity,
				tinactiveflag,
				tinisdirty,
				tinlosesold,
				vhrbarcode,
				vhrcode,
				vhrdescription,
				vhrname,
				vhrshortname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chritemcategorycode,
				chrreccode,
				chrunitofmeasurecode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intreorderquantity,
				tinactiveflag,
				tinisdirty,
				tinlosesold,
				vhrbarcode,
				vhrcode,
				vhrdescription,
				vhrname,
				vhrshortname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblitemmaster(Connection localConnection, String record) {
		tblitemmaster r = this.JSONToClass(record);
		
		return this.updatetblitemmaster(localConnection, r);
	}
	
	public boolean updatetblitemmaster(Connection localConnection, tblitemmaster r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chritemcategorycode = r.getchrItemCategoryCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrunitofmeasurecode = r.getchrUnitOfMeasureCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intreorderquantity = r.getintReOrderQuantity(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinlosesold = r.gettinLoseSold(); 
		String vhrbarcode = r.getvhrBarcode(); 
		String vhrcode = r.getvhrCode(); 
		String vhrdescription = r.getvhrDescription(); 
		String vhrname = r.getvhrName(); 
		String vhrshortname = r.getvhrShortName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblitemmasters_update",
				chripaddress,
				chritemcategorycode,
				chrreccode,
				chrunitofmeasurecode,
				chrupdateuser,
				dtmupdatedate,
				intreorderquantity,
				tinactiveflag,
				tinisdirty,
				tinlosesold,
				vhrbarcode,
				vhrcode,
				vhrdescription,
				vhrname,
				vhrshortname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chritemcategorycode,
				chrreccode,
				chrunitofmeasurecode,
				chrupdateuser,
				dtmupdatedate,
				intreorderquantity,
				tinactiveflag,
				tinisdirty,
				tinlosesold,
				vhrbarcode,
				vhrcode,
				vhrdescription,
				vhrname,
				vhrshortname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblitemmaster(String record) throws SQLException {
		boolean returnValue = false;
		tblitemmaster r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblitemmasters",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
