package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblitemracklocation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblitemracklocations extends com.heptanesia.Data.BasicData {
	public tblitemracklocations() {
	}
	
	public tblitemracklocations(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblitemracklocationsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemracklocations", columns, where, orderBy);
		return json;
	}

	public String gettblitemracklocationsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemracklocations", "*", where, orderBy);
		return json;
	}

	public String gettblitemracklocationsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemracklocations", "*", "", "");
		return json;
	}
	
	public String gettblitemracklocationsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemracklocations", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblitemracklocationsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblitemracklocations", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblitemracklocationsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblitemracklocations", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblitemracklocationsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblitemracklocations", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblitemracklocation JSONToClass(String record) {
		Gson gson = new Gson();
		tblitemracklocation r = gson.fromJson(record, tblitemracklocation.class);
		
		return r;
	}

	public ArrayList<tblitemracklocation> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblitemracklocation>>(){}.getType();
		ArrayList<tblitemracklocation> rs = new ArrayList<tblitemracklocation>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblitemracklocation(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblitemracklocation r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblitemracklocation(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblitemracklocation(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblitemracklocation(Connection localConnection, String record) {
		tblitemracklocation r = this.JSONToClass(record);
		
		return this.inserttblitemracklocation(localConnection, r);
	}
	
	public boolean inserttblitemracklocation(Connection localConnection, tblitemracklocation r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrracklocationcode = r.getchrRackLocationCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblitemracklocations_insert",
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrracklocationcode,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrracklocationcode,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblitemracklocation(Connection localConnection, String record) {
		tblitemracklocation r = this.JSONToClass(record);
		
		return this.updatetblitemracklocation(localConnection, r);
	}
	
	public boolean updatetblitemracklocation(Connection localConnection, tblitemracklocation r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrracklocationcode = r.getchrRackLocationCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblitemracklocations_update",
				chripaddress,
				chritemcode,
				chrracklocationcode,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chritemcode,
				chrracklocationcode,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblitemracklocation(String record) throws SQLException {
		boolean returnValue = false;
		tblitemracklocation r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblitemracklocations",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
