package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblpurchaseorderitemdeliverydetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblpurchaseorderitemdeliverydetails extends com.heptanesia.Data.BasicData {
	public tblpurchaseorderitemdeliverydetails() {
	}
	
	public tblpurchaseorderitemdeliverydetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblpurchaseorderitemdeliverydetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdeliverydetails", columns, where, orderBy);
		return json;
	}

	public String gettblpurchaseorderitemdeliverydetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdeliverydetails", "*", where, orderBy);
		return json;
	}

	public String gettblpurchaseorderitemdeliverydetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdeliverydetails", "*", "", "");
		return json;
	}
	
	public String gettblpurchaseorderitemdeliverydetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdeliverydetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblpurchaseorderitemdeliverydetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdeliverydetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblpurchaseorderitemdeliverydetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblpurchaseorderitemdeliverydetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblpurchaseorderitemdeliverydetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblpurchaseorderitemdeliverydetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblpurchaseorderitemdeliverydetail JSONToClass(String record) {
		Gson gson = new Gson();
		tblpurchaseorderitemdeliverydetail r = gson.fromJson(record, tblpurchaseorderitemdeliverydetail.class);
		
		return r;
	}

	public ArrayList<tblpurchaseorderitemdeliverydetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblpurchaseorderitemdeliverydetail>>(){}.getType();
		ArrayList<tblpurchaseorderitemdeliverydetail> rs = new ArrayList<tblpurchaseorderitemdeliverydetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblpurchaseorderitemdeliverydetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblpurchaseorderitemdeliverydetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblpurchaseorderitemdeliverydetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblpurchaseorderitemdeliverydetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblpurchaseorderitemdeliverydetail(Connection localConnection, String record) {
		tblpurchaseorderitemdeliverydetail r = this.JSONToClass(record);
		
		return this.inserttblpurchaseorderitemdeliverydetail(localConnection, r);
	}
	
	public boolean inserttblpurchaseorderitemdeliverydetail(Connection localConnection, tblpurchaseorderitemdeliverydetail r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorelocationcode = r.getchrStoreLocationCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decquantity = r.getdecQuantity(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblpurchaseorderitemdeliverydetails_insert",
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrstorelocationcode,
				chrupdateuser,
				decquantity,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrstorelocationcode,
				chrupdateuser,
				decquantity,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblpurchaseorderitemdeliverydetail(Connection localConnection, String record) {
		tblpurchaseorderitemdeliverydetail r = this.JSONToClass(record);
		
		return this.updatetblpurchaseorderitemdeliverydetail(localConnection, r);
	}
	
	public boolean updatetblpurchaseorderitemdeliverydetail(Connection localConnection, tblpurchaseorderitemdeliverydetail r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorelocationcode = r.getchrStoreLocationCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decquantity = r.getdecQuantity(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblpurchaseorderitemdeliverydetails_update",
				chripaddress,
				chrparentcode,
				chrreccode,
				chrstorelocationcode,
				chrupdateuser,
				decquantity,
				dtmupdatedate,
				tinisdirty
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrstorelocationcode,
				chrupdateuser,
				decquantity,
				dtmupdatedate,
				tinisdirty
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblpurchaseorderitemdeliverydetail(String record) throws SQLException {
		boolean returnValue = false;
		tblpurchaseorderitemdeliverydetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblpurchaseorderitemdeliverydetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
