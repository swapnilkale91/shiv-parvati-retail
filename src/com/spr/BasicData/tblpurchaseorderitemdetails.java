package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblpurchaseorderitemdetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblpurchaseorderitemdetails extends com.heptanesia.Data.BasicData {
	public tblpurchaseorderitemdetails() {
	}
	
	public tblpurchaseorderitemdetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblpurchaseorderitemdetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdetails", columns, where, orderBy);
		return json;
	}

	public String gettblpurchaseorderitemdetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdetails", "*", where, orderBy);
		return json;
	}

	public String gettblpurchaseorderitemdetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdetails", "*", "", "");
		return json;
	}
	
	public String gettblpurchaseorderitemdetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblpurchaseorderitemdetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorderitemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblpurchaseorderitemdetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblpurchaseorderitemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblpurchaseorderitemdetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblpurchaseorderitemdetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblpurchaseorderitemdetail JSONToClass(String record) {
		Gson gson = new Gson();
		tblpurchaseorderitemdetail r = gson.fromJson(record, tblpurchaseorderitemdetail.class);
		
		return r;
	}

	public ArrayList<tblpurchaseorderitemdetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblpurchaseorderitemdetail>>(){}.getType();
		ArrayList<tblpurchaseorderitemdetail> rs = new ArrayList<tblpurchaseorderitemdetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblpurchaseorderitemdetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblpurchaseorderitemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblpurchaseorderitemdetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblpurchaseorderitemdetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblpurchaseorderitemdetail(Connection localConnection, String record) {
		tblpurchaseorderitemdetail r = this.JSONToClass(record);
		
		return this.inserttblpurchaseorderitemdetail(localConnection, r);
	}
	
	public boolean inserttblpurchaseorderitemdetail(Connection localConnection, tblpurchaseorderitemdetail r) {
		boolean result = false;

		String chramount = r.getchrAmount(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chritemdescription = r.getchrItemDescription(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chruomcode = r.getchrUOMCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvattaxreccode = r.getchrVatTaxRecCode(); 
		float decitemamount = r.getdecItemAmount(); 
		float decitemamountaftertax = r.getdecItemAmountAfterTax(); 
		float decitemdiscount = r.getdecItemDiscount(); 
		float decmarginamount = r.getdecMarginAmount(); 
		float decmarginpercent = r.getdecMarginPercent(); 
		float decquantity = r.getdecQuantity(); 
		float decrate = r.getdecRate(); 
		float decschemediscountamount = r.getdecSchemeDiscountAmount(); 
		float decschemediscountpercent = r.getdecSchemeDiscountPercent(); 
		float decvattaxamount = r.getdecVatTaxAmount(); 
		float decvattaxpercent = r.getdecVatTaxPercent(); 
		float decvendoramount1 = r.getdecVendorAmount1(); 
		float decvendordiscountamount2 = r.getdecVendorDiscountAmount2(); 
		float decvendordiscountpercent2 = r.getdecVendorDiscountPercent2(); 
		float decvendordiscoutpercent1 = r.getdecVendorDiscoutPercent1(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblpurchaseorderitemdetails_insert",
				chramount,
				chrinsertuser,
				chripaddress,
				chritemcode,
				chritemdescription,
				chrparentcode,
				chrreccode,
				chruomcode,
				chrupdateuser,
				chrvattaxreccode,
				decitemamount,
				decitemamountaftertax,
				decitemdiscount,
				decmarginamount,
				decmarginpercent,
				decquantity,
				decrate,
				decschemediscountamount,
				decschemediscountpercent,
				decvattaxamount,
				decvattaxpercent,
				decvendoramount1,
				decvendordiscountamount2,
				decvendordiscountpercent2,
				decvendordiscoutpercent1,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty
			));

			this.common.setParameters(callableStatement,
				chramount,
				chrinsertuser,
				chripaddress,
				chritemcode,
				chritemdescription,
				chrparentcode,
				chrreccode,
				chruomcode,
				chrupdateuser,
				chrvattaxreccode,
				decitemamount,
				decitemamountaftertax,
				decitemdiscount,
				decmarginamount,
				decmarginpercent,
				decquantity,
				decrate,
				decschemediscountamount,
				decschemediscountpercent,
				decvattaxamount,
				decvattaxpercent,
				decvendoramount1,
				decvendordiscountamount2,
				decvendordiscountpercent2,
				decvendordiscoutpercent1,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblpurchaseorderitemdetail(Connection localConnection, String record) {
		tblpurchaseorderitemdetail r = this.JSONToClass(record);
		
		return this.updatetblpurchaseorderitemdetail(localConnection, r);
	}
	
	public boolean updatetblpurchaseorderitemdetail(Connection localConnection, tblpurchaseorderitemdetail r) {
		boolean result = false;

		String chramount = r.getchrAmount(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chritemdescription = r.getchrItemDescription(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chruomcode = r.getchrUOMCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvattaxreccode = r.getchrVatTaxRecCode(); 
		float decitemamount = r.getdecItemAmount(); 
		float decitemamountaftertax = r.getdecItemAmountAfterTax(); 
		float decitemdiscount = r.getdecItemDiscount(); 
		float decmarginamount = r.getdecMarginAmount(); 
		float decmarginpercent = r.getdecMarginPercent(); 
		float decquantity = r.getdecQuantity(); 
		float decrate = r.getdecRate(); 
		float decschemediscountamount = r.getdecSchemeDiscountAmount(); 
		float decschemediscountpercent = r.getdecSchemeDiscountPercent(); 
		float decvattaxamount = r.getdecVatTaxAmount(); 
		float decvattaxpercent = r.getdecVatTaxPercent(); 
		float decvendoramount1 = r.getdecVendorAmount1(); 
		float decvendordiscountamount2 = r.getdecVendorDiscountAmount2(); 
		float decvendordiscountpercent2 = r.getdecVendorDiscountPercent2(); 
		float decvendordiscoutpercent1 = r.getdecVendorDiscoutPercent1(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblpurchaseorderitemdetails_update",
				chramount,
				chripaddress,
				chritemcode,
				chritemdescription,
				chrparentcode,
				chrreccode,
				chruomcode,
				chrupdateuser,
				chrvattaxreccode,
				decitemamount,
				decitemamountaftertax,
				decitemdiscount,
				decmarginamount,
				decmarginpercent,
				decquantity,
				decrate,
				decschemediscountamount,
				decschemediscountpercent,
				decvattaxamount,
				decvattaxpercent,
				decvendoramount1,
				decvendordiscountamount2,
				decvendordiscountpercent2,
				decvendordiscoutpercent1,
				dtmupdatedate,
				tinisdirty
			));

			this.common.setParameters(callableStatement,
				chramount,
				chripaddress,
				chritemcode,
				chritemdescription,
				chrparentcode,
				chrreccode,
				chruomcode,
				chrupdateuser,
				chrvattaxreccode,
				decitemamount,
				decitemamountaftertax,
				decitemdiscount,
				decmarginamount,
				decmarginpercent,
				decquantity,
				decrate,
				decschemediscountamount,
				decschemediscountpercent,
				decvattaxamount,
				decvattaxpercent,
				decvendoramount1,
				decvendordiscountamount2,
				decvendordiscountpercent2,
				decvendordiscoutpercent1,
				dtmupdatedate,
				tinisdirty
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblpurchaseorderitemdetail(String record) throws SQLException {
		boolean returnValue = false;
		tblpurchaseorderitemdetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblpurchaseorderitemdetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
