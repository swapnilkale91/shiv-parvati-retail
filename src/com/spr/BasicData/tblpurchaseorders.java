package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblpurchaseorder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblpurchaseorders extends com.heptanesia.Data.BasicData {
	public tblpurchaseorders() {
	}
	
	public tblpurchaseorders(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblpurchaseordersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorders", columns, where, orderBy);
		return json;
	}

	public String gettblpurchaseordersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorders", "*", where, orderBy);
		return json;
	}

	public String gettblpurchaseordersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorders", "*", "", "");
		return json;
	}
	
	public String gettblpurchaseordersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorders", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblpurchaseordersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblpurchaseorders", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblpurchaseordersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblpurchaseorders", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblpurchaseordersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblpurchaseorders", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblpurchaseorder JSONToClass(String record) {
		Gson gson = new Gson();
		tblpurchaseorder r = gson.fromJson(record, tblpurchaseorder.class);
		
		return r;
	}

	public ArrayList<tblpurchaseorder> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblpurchaseorder>>(){}.getType();
		ArrayList<tblpurchaseorder> rs = new ArrayList<tblpurchaseorder>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblpurchaseorder(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblpurchaseorder r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblpurchaseorder(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblpurchaseorder(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblpurchaseorder(Connection localConnection, String record) {
		tblpurchaseorder r = this.JSONToClass(record);
		
		return this.inserttblpurchaseorder(localConnection, r);
	}
	
	public boolean inserttblpurchaseorder(Connection localConnection, tblpurchaseorder r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrpono = r.getchrPONo(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvendorcode = r.getchrVendorCode(); 
		float decdiscounttotal = r.getdecDiscountTotal(); 
		float decitemtotal = r.getdecItemTotal(); 
		float decmargintotal = r.getdecMarginTotal(); 
		float decpoamt = r.getdecPOAmt(); 
		float decschemetotal = r.getdecSchemeTotal(); 
		float decvattaxtotal = r.getdecVatTaxTotal(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmpodate = r.getdtmPODate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intcreditperiod = r.getintCreditPeriod(); 
		int tinauthorizationlevel = r.gettinAuthorizationLevel(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblpurchaseorders_insert",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrpono,
				chrreccode,
				chrupdateuser,
				chrvendorcode,
				decdiscounttotal,
				decitemtotal,
				decmargintotal,
				decpoamt,
				decschemetotal,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtminsertdate,
				dtmpodate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrpono,
				chrreccode,
				chrupdateuser,
				chrvendorcode,
				decdiscounttotal,
				decitemtotal,
				decmargintotal,
				decpoamt,
				decschemetotal,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtminsertdate,
				dtmpodate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblpurchaseorder(Connection localConnection, String record) {
		tblpurchaseorder r = this.JSONToClass(record);
		
		return this.updatetblpurchaseorder(localConnection, r);
	}
	
	public boolean updatetblpurchaseorder(Connection localConnection, tblpurchaseorder r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrpono = r.getchrPONo(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvendorcode = r.getchrVendorCode(); 
		float decdiscounttotal = r.getdecDiscountTotal(); 
		float decitemtotal = r.getdecItemTotal(); 
		float decmargintotal = r.getdecMarginTotal(); 
		float decpoamt = r.getdecPOAmt(); 
		float decschemetotal = r.getdecSchemeTotal(); 
		float decvattaxtotal = r.getdecVatTaxTotal(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtmpodate = r.getdtmPODate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intcreditperiod = r.getintCreditPeriod(); 
		int tinauthorizationlevel = r.gettinAuthorizationLevel(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblpurchaseorders_update",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chripaddress,
				chrpono,
				chrreccode,
				chrupdateuser,
				chrvendorcode,
				decdiscounttotal,
				decitemtotal,
				decmargintotal,
				decpoamt,
				decschemetotal,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmpodate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chripaddress,
				chrpono,
				chrreccode,
				chrupdateuser,
				chrvendorcode,
				decdiscounttotal,
				decitemtotal,
				decmargintotal,
				decpoamt,
				decschemetotal,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmpodate,
				dtmupdatedate,
				intcreditperiod,
				tinauthorizationlevel,
				tinisdirty,
				tinstatus
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblpurchaseorder(String record) throws SQLException {
		boolean returnValue = false;
		tblpurchaseorder r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblpurchaseorders",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
