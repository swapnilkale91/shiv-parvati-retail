package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblsale;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblsales extends com.heptanesia.Data.BasicData {
	public tblsales() {
	}
	
	public tblsales(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblsalesColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsales", columns, where, orderBy);
		return json;
	}

	public String gettblsalesAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsales", "*", where, orderBy);
		return json;
	}

	public String gettblsalesAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsales", "*", "", "");
		return json;
	}
	
	public String gettblsalesForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsales", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblsalesForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsales", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblsalesForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblsales", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblsalesInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblsales", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblsale JSONToClass(String record) {
		Gson gson = new Gson();
		tblsale r = gson.fromJson(record, tblsale.class);
		
		return r;
	}

	public ArrayList<tblsale> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblsale>>(){}.getType();
		ArrayList<tblsale> rs = new ArrayList<tblsale>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblsale(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblsale r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		/*r.setchrCompanyCode(companyCode);
		r.setchrStoreCode(storeCode);
		r.setchrOperatorCode(employeeCode);*/
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblsale(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblsale(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblsale(Connection localConnection, String record) {
		tblsale r = this.JSONToClass(record);
		
		return this.inserttblsale(localConnection, r);
	}
	
	public boolean inserttblsale(Connection localConnection, tblsale r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		System.out.println("chrcompanycode ="+chrcompanycode);
		String chrcustomercode = r.getchrCustomerCode(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chroperatorcode = r.getchrOperatorCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		System.out.println("chrstorecode ="+chrstorecode);
		String chrupdateuser = r.getchrUpdateUser(); 
		float decdiscounttotal = r.getdecDiscountTotal(); 
		float decitemtotal = r.getdecItemTotal(); 
		float dectotalamount = r.getdecTotalAmount(); 
		float decvattaxtotal = r.getdecVATTaxTotal(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtmbilldate = r.getdtmBillDate(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intauthorizationlevel = r.getintAuthorizationLevel(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrbillno = r.getvhrBillNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblsales_insert",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrcustomercode,
				chrinsertuser,
				chripaddress,
				chroperatorcode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				decdiscounttotal,
				decitemtotal,
				dectotalamount,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmbilldate,
				dtminsertdate,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhrbillno
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrcustomercode,
				chrinsertuser,
				chripaddress,
				chroperatorcode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				decdiscounttotal,
				decitemtotal,
				dectotalamount,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmbilldate,
				dtminsertdate,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhrbillno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblsale(Connection localConnection, String record) {
		tblsale r = this.JSONToClass(record);
		
		return this.updatetblsale(localConnection, r);
	}
	
	public boolean updatetblsale(Connection localConnection, tblsale r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chrcustomercode = r.getchrCustomerCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String chroperatorcode = r.getchrOperatorCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decdiscounttotal = r.getdecDiscountTotal(); 
		float decitemtotal = r.getdecItemTotal(); 
		float dectotalamount = r.getdecTotalAmount(); 
		float decvattaxtotal = r.getdecVATTaxTotal(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtmbilldate = r.getdtmBillDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intauthorizationlevel = r.getintAuthorizationLevel(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrbillno = r.getvhrBillNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblsales_update",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrcustomercode,
				chripaddress,
				chroperatorcode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				decdiscounttotal,
				decitemtotal,
				dectotalamount,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmbilldate,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhrbillno
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrcustomercode,
				chripaddress,
				chroperatorcode,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				decdiscounttotal,
				decitemtotal,
				dectotalamount,
				decvattaxtotal,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmbilldate,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhrbillno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblsale(String record) throws SQLException {
		boolean returnValue = false;
		tblsale r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblsales",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
