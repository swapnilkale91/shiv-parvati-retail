package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblsalesitemdetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblsalesitemdetails extends com.heptanesia.Data.BasicData {
	public tblsalesitemdetails() {
	}
	
	public tblsalesitemdetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblsalesitemdetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalesitemdetails", columns, where, orderBy);
		return json;
	}

	public String gettblsalesitemdetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalesitemdetails", "*", where, orderBy);
		return json;
	}

	public String gettblsalesitemdetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalesitemdetails", "*", "", "");
		return json;
	}
	
	public String gettblsalesitemdetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalesitemdetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblsalesitemdetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalesitemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblsalesitemdetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblsalesitemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblsalesitemdetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblsalesitemdetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblsalesitemdetail JSONToClass(String record) {
		Gson gson = new Gson();
		tblsalesitemdetail r = gson.fromJson(record, tblsalesitemdetail.class);
		
		return r;
	}

	public ArrayList<tblsalesitemdetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblsalesitemdetail>>(){}.getType();
		ArrayList<tblsalesitemdetail> rs = new ArrayList<tblsalesitemdetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblsalesitemdetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblsalesitemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblsalesitemdetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblsalesitemdetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblsalesitemdetail(Connection localConnection, String record) {
		tblsalesitemdetail r = this.JSONToClass(record);
		
		return this.inserttblsalesitemdetail(localConnection, r);
	}
	
	public boolean inserttblsalesitemdetail(Connection localConnection, tblsalesitemdetail r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvattaxreccode = r.getchrVatTaxRecCode(); 
		float deccustomerdiscount = r.getdecCustomerDiscount(); 
		float deccustomerrate = r.getdecCustomerRate(); 
		float decitemcost = r.getdecItemCost(); 
		float decmrp = r.getdecMRP(); 
		float decquantity = r.getdecQuantity(); 
		float decvattaxamount = r.getdecVatTaxAmount(); 
		float decvattaxpercentage = r.getdecVatTaxPercentage(); 
		String dtmexpirydate = r.getdtmExpiryDate(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmmanufacturedate = r.getdtmManufactureDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrbatchno = r.getvhrBatchNo(); 
		String vhrdescription = r.getvhrDescription(); 
		String vhrourgeneratedbarcode = r.getvhrOurGeneratedBarCode(); 
		String vhrshortname = r.getvhrShortName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblsalesitemdetails_insert",
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscount,
				deccustomerrate,
				decitemcost,
				decmrp,
				decquantity,
				decvattaxamount,
				decvattaxpercentage,
				dtmexpirydate,
				dtminsertdate,
				dtmmanufacturedate,
				dtmupdatedate,
				intstatus,
				tinisdirty,
				vhrbatchno,
				vhrdescription,
				vhrourgeneratedbarcode,
				vhrshortname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscount,
				deccustomerrate,
				decitemcost,
				decmrp,
				decquantity,
				decvattaxamount,
				decvattaxpercentage,
				dtmexpirydate,
				dtminsertdate,
				dtmmanufacturedate,
				dtmupdatedate,
				intstatus,
				tinisdirty,
				vhrbatchno,
				vhrdescription,
				vhrourgeneratedbarcode,
				vhrshortname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblsalesitemdetail(Connection localConnection, String record) {
		tblsalesitemdetail r = this.JSONToClass(record);
		
		return this.updatetblsalesitemdetail(localConnection, r);
	}
	
	public boolean updatetblsalesitemdetail(Connection localConnection, tblsalesitemdetail r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String chrvattaxreccode = r.getchrVatTaxRecCode(); 
		float deccustomerdiscount = r.getdecCustomerDiscount(); 
		float deccustomerrate = r.getdecCustomerRate(); 
		float decitemcost = r.getdecItemCost(); 
		float decmrp = r.getdecMRP(); 
		float decquantity = r.getdecQuantity(); 
		float decvattaxamount = r.getdecVatTaxAmount(); 
		float decvattaxpercentage = r.getdecVatTaxPercentage(); 
		String dtmexpirydate = r.getdtmExpiryDate(); 
		String dtmmanufacturedate = r.getdtmManufactureDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrbatchno = r.getvhrBatchNo(); 
		String vhrdescription = r.getvhrDescription(); 
		String vhrourgeneratedbarcode = r.getvhrOurGeneratedBarCode(); 
		String vhrshortname = r.getvhrShortName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblsalesitemdetails_update",
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscount,
				deccustomerrate,
				decitemcost,
				decmrp,
				decquantity,
				decvattaxamount,
				decvattaxpercentage,
				dtmexpirydate,
				dtmmanufacturedate,
				dtmupdatedate,
				intstatus,
				tinisdirty,
				vhrbatchno,
				vhrdescription,
				vhrourgeneratedbarcode,
				vhrshortname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				chrvattaxreccode,
				deccustomerdiscount,
				deccustomerrate,
				decitemcost,
				decmrp,
				decquantity,
				decvattaxamount,
				decvattaxpercentage,
				dtmexpirydate,
				dtmmanufacturedate,
				dtmupdatedate,
				intstatus,
				tinisdirty,
				vhrbatchno,
				vhrdescription,
				vhrourgeneratedbarcode,
				vhrshortname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblsalesitemdetail(String record) throws SQLException {
		boolean returnValue = false;
		tblsalesitemdetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblsalesitemdetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
