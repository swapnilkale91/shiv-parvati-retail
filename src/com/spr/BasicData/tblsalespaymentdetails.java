package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblsalespaymentdetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblsalespaymentdetails extends com.heptanesia.Data.BasicData {
	public tblsalespaymentdetails() {
	}
	
	public tblsalespaymentdetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblsalespaymentdetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalespaymentdetails", columns, where, orderBy);
		return json;
	}

	public String gettblsalespaymentdetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalespaymentdetails", "*", where, orderBy);
		return json;
	}

	public String gettblsalespaymentdetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalespaymentdetails", "*", "", "");
		return json;
	}
	
	public String gettblsalespaymentdetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalespaymentdetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblsalespaymentdetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblsalespaymentdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblsalespaymentdetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblsalespaymentdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblsalespaymentdetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblsalespaymentdetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblsalespaymentdetail JSONToClass(String record) {
		Gson gson = new Gson();
		tblsalespaymentdetail r = gson.fromJson(record, tblsalespaymentdetail.class);
		
		return r;
	}

	public ArrayList<tblsalespaymentdetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblsalespaymentdetail>>(){}.getType();
		ArrayList<tblsalespaymentdetail> rs = new ArrayList<tblsalespaymentdetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblsalespaymentdetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblsalespaymentdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblsalespaymentdetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblsalespaymentdetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblsalespaymentdetail(Connection localConnection, String record) {
		tblsalespaymentdetail r = this.JSONToClass(record);
		
		return this.inserttblsalespaymentdetail(localConnection, r);
	}
	
	public boolean inserttblsalespaymentdetail(Connection localConnection, tblsalespaymentdetail r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrpaymentmodecode = r.getintPaymentModeCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decamount = r.getdecAmount(); 
		float decbalance = r.getdecBalance(); 
		float deccashreceived = r.getdecCashReceived(); 
		String dtmcoupondenomination = r.getdtmCouponDenomination(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcarddetails = r.getvhrCardDetails();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblsalespaymentdetails_insert",
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrpaymentmodecode,
				chrreccode,
				chrupdateuser,
				decamount,
				decbalance,
				deccashreceived,
				dtmcoupondenomination,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty,
				vhrcarddetails
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrpaymentmodecode,
				chrreccode,
				chrupdateuser,
				decamount,
				decbalance,
				deccashreceived,
				dtmcoupondenomination,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty,
				vhrcarddetails
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblsalespaymentdetail(Connection localConnection, String record) {
		tblsalespaymentdetail r = this.JSONToClass(record);
		
		return this.updatetblsalespaymentdetail(localConnection, r);
	}
	
	public boolean updatetblsalespaymentdetail(Connection localConnection, tblsalespaymentdetail r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrpaymentmodecode = r.getintPaymentModeCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decamount = r.getdecAmount(); 
		float decbalance = r.getdecBalance(); 
		float deccashreceived = r.getdecCashReceived(); 
		String dtmcoupondenomination = r.getdtmCouponDenomination(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcarddetails = r.getvhrCardDetails();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblsalespaymentdetails_update",
				chripaddress,
				chrparentcode,
				chrpaymentmodecode,
				chrreccode,
				chrupdateuser,
				decamount,
				decbalance,
				deccashreceived,
				dtmcoupondenomination,
				dtmupdatedate,
				tinisdirty,
				vhrcarddetails
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrparentcode,
				chrpaymentmodecode,
				chrreccode,
				chrupdateuser,
				decamount,
				decbalance,
				deccashreceived,
				dtmcoupondenomination,
				dtmupdatedate,
				tinisdirty,
				vhrcarddetails
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblsalespaymentdetail(String record) throws SQLException {
		boolean returnValue = false;
		tblsalespaymentdetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblsalespaymentdetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
