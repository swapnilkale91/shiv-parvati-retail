package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblstockadjustmentitemdetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblstockadjustmentitemdetails extends com.heptanesia.Data.BasicData {
	public tblstockadjustmentitemdetails() {
	}
	
	public tblstockadjustmentitemdetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblstockadjustmentitemdetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustmentitemdetails", columns, where, orderBy);
		return json;
	}

	public String gettblstockadjustmentitemdetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustmentitemdetails", "*", where, orderBy);
		return json;
	}

	public String gettblstockadjustmentitemdetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustmentitemdetails", "*", "", "");
		return json;
	}
	
	public String gettblstockadjustmentitemdetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustmentitemdetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblstockadjustmentitemdetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustmentitemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblstockadjustmentitemdetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblstockadjustmentitemdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblstockadjustmentitemdetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblstockadjustmentitemdetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblstockadjustmentitemdetail JSONToClass(String record) {
		Gson gson = new Gson();
		tblstockadjustmentitemdetail r = gson.fromJson(record, tblstockadjustmentitemdetail.class);
		
		return r;
	}

	public ArrayList<tblstockadjustmentitemdetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblstockadjustmentitemdetail>>(){}.getType();
		ArrayList<tblstockadjustmentitemdetail> rs = new ArrayList<tblstockadjustmentitemdetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblstockadjustmentitemdetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblstockadjustmentitemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblstockadjustmentitemdetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblstockadjustmentitemdetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblstockadjustmentitemdetail(Connection localConnection, String record) {
		tblstockadjustmentitemdetail r = this.JSONToClass(record);
		
		return this.inserttblstockadjustmentitemdetail(localConnection, r);
	}
	
	public boolean inserttblstockadjustmentitemdetail(Connection localConnection, tblstockadjustmentitemdetail r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decquantity = r.getdecQuantity(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intadjustmenttype = r.getintAdjustmentType(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrdescription = r.getvhrDescription();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstockadjustmentitemdetails_insert",
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decquantity,
				dtminsertdate,
				dtmupdatedate,
				intadjustmenttype,
				intstatus,
				tinisdirty,
				vhrdescription
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decquantity,
				dtminsertdate,
				dtmupdatedate,
				intadjustmenttype,
				intstatus,
				tinisdirty,
				vhrdescription
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblstockadjustmentitemdetail(Connection localConnection, String record) {
		tblstockadjustmentitemdetail r = this.JSONToClass(record);
		
		return this.updatetblstockadjustmentitemdetail(localConnection, r);
	}
	
	public boolean updatetblstockadjustmentitemdetail(Connection localConnection, tblstockadjustmentitemdetail r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chritemcode = r.getchrItemCode(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decquantity = r.getdecQuantity(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intadjustmenttype = r.getintAdjustmentType(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrdescription = r.getvhrDescription();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstockadjustmentitemdetails_update",
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decquantity,
				dtmupdatedate,
				intadjustmenttype,
				intstatus,
				tinisdirty,
				vhrdescription
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chritemcode,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decquantity,
				dtmupdatedate,
				intadjustmenttype,
				intstatus,
				tinisdirty,
				vhrdescription
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblstockadjustmentitemdetail(String record) throws SQLException {
		boolean returnValue = false;
		tblstockadjustmentitemdetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblstockadjustmentitemdetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
