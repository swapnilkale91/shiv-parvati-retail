package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblstockadjustment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblstockadjustments extends com.heptanesia.Data.BasicData {
	public tblstockadjustments() {
	}
	
	public tblstockadjustments(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblstockadjustmentsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustments", columns, where, orderBy);
		return json;
	}

	public String gettblstockadjustmentsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustments", "*", where, orderBy);
		return json;
	}

	public String gettblstockadjustmentsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustments", "*", "", "");
		return json;
	}
	
	public String gettblstockadjustmentsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustments", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblstockadjustmentsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstockadjustments", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblstockadjustmentsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblstockadjustments", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblstockadjustmentsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblstockadjustments", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblstockadjustment JSONToClass(String record) {
		Gson gson = new Gson();
		tblstockadjustment r = gson.fromJson(record, tblstockadjustment.class);
		
		return r;
	}

	public ArrayList<tblstockadjustment> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblstockadjustment>>(){}.getType();
		ArrayList<tblstockadjustment> rs = new ArrayList<tblstockadjustment>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblstockadjustment(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblstockadjustment r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblstockadjustment(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblstockadjustment(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblstockadjustment(Connection localConnection, String record) {
		tblstockadjustment r = this.JSONToClass(record);
		
		return this.inserttblstockadjustment(localConnection, r);
	}
	
	public boolean inserttblstockadjustment(Connection localConnection, tblstockadjustment r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmadjustmentdate = r.getdtmAdjustmentDate(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intauthorizationlevel = r.getintAuthorizationLevel(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhradjustmentno = r.getvhrAdjustmentNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstockadjustments_insert",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtmadjustmentdate,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtminsertdate,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhradjustmentno
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtmadjustmentdate,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtminsertdate,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhradjustmentno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblstockadjustment(Connection localConnection, String record) {
		tblstockadjustment r = this.JSONToClass(record);
		
		return this.updatetblstockadjustment(localConnection, r);
	}
	
	public boolean updatetblstockadjustment(Connection localConnection, tblstockadjustment r) {
		boolean result = false;

		String chrauthorizationuserone = r.getchrAuthorizationUserOne(); 
		String chrauthorizationuserthree = r.getchrAuthorizationUserThree(); 
		String chrauthorizationusertwo = r.getchrAuthorizationUserTwo(); 
		String chrcompanycode = r.getchrCompanyCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmadjustmentdate = r.getdtmAdjustmentDate(); 
		String dtmauthorizationdateone = r.getdtmAuthorizationDateOne(); 
		String dtmauthorizationdatethree = r.getdtmAuthorizationDateThree(); 
		String dtmauthorizationdatetwo = r.getdtmAuthorizationDateTwo(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intauthorizationlevel = r.getintAuthorizationLevel(); 
		int intstatus = r.getintStatus(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhradjustmentno = r.getvhrAdjustmentNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstockadjustments_update",
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtmadjustmentdate,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhradjustmentno
			));

			this.common.setParameters(callableStatement,
				chrauthorizationuserone,
				chrauthorizationuserthree,
				chrauthorizationusertwo,
				chrcompanycode,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrupdateuser,
				dtmadjustmentdate,
				dtmauthorizationdateone,
				dtmauthorizationdatethree,
				dtmauthorizationdatetwo,
				dtmupdatedate,
				intauthorizationlevel,
				intstatus,
				tinisdirty,
				vhradjustmentno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblstockadjustment(String record) throws SQLException {
		boolean returnValue = false;
		tblstockadjustment r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblstockadjustments",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
