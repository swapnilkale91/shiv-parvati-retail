package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblstoreitemrack;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblstoreitemracks extends com.heptanesia.Data.BasicData {
	public tblstoreitemracks() {
	}
	
	public tblstoreitemracks(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblstoreitemracksColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstoreitemracks", columns, where, orderBy);
		return json;
	}

	public String gettblstoreitemracksAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstoreitemracks", "*", where, orderBy);
		return json;
	}

	public String gettblstoreitemracksAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstoreitemracks", "*", "", "");
		return json;
	}
	
	public String gettblstoreitemracksForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstoreitemracks", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblstoreitemracksForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstoreitemracks", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblstoreitemracksForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblstoreitemracks", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblstoreitemracksInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblstoreitemracks", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblstoreitemrack JSONToClass(String record) {
		Gson gson = new Gson();
		tblstoreitemrack r = gson.fromJson(record, tblstoreitemrack.class);
		
		return r;
	}

	public ArrayList<tblstoreitemrack> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblstoreitemrack>>(){}.getType();
		ArrayList<tblstoreitemrack> rs = new ArrayList<tblstoreitemrack>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblstoreitemrack(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblstoreitemrack r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblstoreitemrack(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblstoreitemrack(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblstoreitemrack(Connection localConnection, String record) {
		tblstoreitemrack r = this.JSONToClass(record);
		
		return this.inserttblstoreitemrack(localConnection, r);
	}
	
	public boolean inserttblstoreitemrack(Connection localConnection, tblstoreitemrack r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intstorelocationcategory = r.getintStoreLocationCategory(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstoreitemracks_insert",
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intstorelocationcategory,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intstorelocationcategory,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblstoreitemrack(Connection localConnection, String record) {
		tblstoreitemrack r = this.JSONToClass(record);
		
		return this.updatetblstoreitemrack(localConnection, r);
	}
	
	public boolean updatetblstoreitemrack(Connection localConnection, tblstoreitemrack r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intstorelocationcategory = r.getintStoreLocationCategory(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstoreitemracks_update",
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intstorelocationcategory,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intstorelocationcategory,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblstoreitemrack(String record) throws SQLException {
		boolean returnValue = false;
		tblstoreitemrack r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblstoreitemracks",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
