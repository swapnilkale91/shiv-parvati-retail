package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblstore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblstores extends com.heptanesia.Data.BasicData {
	public tblstores() {
	}
	
	public tblstores(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblstoresColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstores", columns, where, orderBy);
		return json;
	}

	public String gettblstoresAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstores", "*", where, orderBy);
		return json;
	}

	public String gettblstoresAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstores", "*", "", "");
		return json;
	}
	
	public String gettblstoresForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstores", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblstoresForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblstores", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblstoresForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblstores", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblstoresInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblstores", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblstore JSONToClass(String record) {
		Gson gson = new Gson();
		tblstore r = gson.fromJson(record, tblstore.class);
		
		return r;
	}

	public ArrayList<tblstore> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblstore>>(){}.getType();
		ArrayList<tblstore> rs = new ArrayList<tblstore>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblstore(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblstore r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblstore(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblstore(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblstore(Connection localConnection, String record) {
		tblstore r = this.JSONToClass(record);
		
		return this.inserttblstore(localConnection, r);
	}
	
	public boolean inserttblstore(Connection localConnection, tblstore r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrcstno = r.getvhrCSTNo(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrservicetaxno = r.getvhrServiceTaxNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrvatno = r.getvhrVATNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstores_insert",
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblstore(Connection localConnection, String record) {
		tblstore r = this.JSONToClass(record);
		
		return this.updatetblstore(localConnection, r);
	}
	
	public boolean updatetblstore(Connection localConnection, tblstore r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrcstno = r.getvhrCSTNo(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrservicetaxno = r.getvhrServiceTaxNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrvatno = r.getvhrVATNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblstores_update",
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblstore(String record) throws SQLException {
		boolean returnValue = false;
		tblstore r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblstores",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
