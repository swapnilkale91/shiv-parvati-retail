package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tbltaxdetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tbltaxdetails extends com.heptanesia.Data.BasicData {
	public tbltaxdetails() {
	}
	
	public tbltaxdetails(Connection connection) {
		this.connection = connection;
	}
	
	public String gettbltaxdetailsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxdetails", columns, where, orderBy);
		return json;
	}

	public String gettbltaxdetailsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxdetails", "*", where, orderBy);
		return json;
	}

	public String gettbltaxdetailsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxdetails", "*", "", "");
		return json;
	}
	
	public String gettbltaxdetailsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxdetails", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettbltaxdetailsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettbltaxdetailsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tbltaxdetails", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettbltaxdetailsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tbltaxdetails", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tbltaxdetail JSONToClass(String record) {
		Gson gson = new Gson();
		tbltaxdetail r = gson.fromJson(record, tbltaxdetail.class);
		
		return r;
	}

	public ArrayList<tbltaxdetail> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tbltaxdetail>>(){}.getType();
		ArrayList<tbltaxdetail> rs = new ArrayList<tbltaxdetail>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetbltaxdetail(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tbltaxdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttbltaxdetail(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetbltaxdetail(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttbltaxdetail(Connection localConnection, String record) {
		tbltaxdetail r = this.JSONToClass(record);
		
		return this.inserttbltaxdetail(localConnection, r);
	}
	
	public boolean inserttbltaxdetail(Connection localConnection, tbltaxdetail r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decpercentage = r.getdecPercentage(); 
		String dtmeffectivedate = r.getdtmEffectiveDate(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbltaxdetails_insert",
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decpercentage,
				dtmeffectivedate,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decpercentage,
				dtmeffectivedate,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetbltaxdetail(Connection localConnection, String record) {
		tbltaxdetail r = this.JSONToClass(record);
		
		return this.updatetbltaxdetail(localConnection, r);
	}
	
	public boolean updatetbltaxdetail(Connection localConnection, tbltaxdetail r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrparentcode = r.getchrParentCode(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		float decpercentage = r.getdecPercentage(); 
		String dtmeffectivedate = r.getdtmEffectiveDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbltaxdetails_update",
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decpercentage,
				dtmeffectivedate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrparentcode,
				chrreccode,
				chrupdateuser,
				decpercentage,
				dtmeffectivedate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetbltaxdetail(String record) throws SQLException {
		boolean returnValue = false;
		tbltaxdetail r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tbltaxdetails",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
