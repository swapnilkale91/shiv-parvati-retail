package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tbltaxmaster;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tbltaxmasters extends com.heptanesia.Data.BasicData {
	public tbltaxmasters() {
	}
	
	public tbltaxmasters(Connection connection) {
		this.connection = connection;
	}
	
	public String gettbltaxmastersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxmasters", columns, where, orderBy);
		return json;
	}

	public String gettbltaxmastersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxmasters", "*", where, orderBy);
		return json;
	}

	public String gettbltaxmastersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxmasters", "*", "", "");
		return json;
	}
	
	public String gettbltaxmastersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxmasters", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettbltaxmastersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbltaxmasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettbltaxmastersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tbltaxmasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettbltaxmastersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tbltaxmasters", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tbltaxmaster JSONToClass(String record) {
		Gson gson = new Gson();
		tbltaxmaster r = gson.fromJson(record, tbltaxmaster.class);
		
		return r;
	}

	public ArrayList<tbltaxmaster> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tbltaxmaster>>(){}.getType();
		ArrayList<tbltaxmaster> rs = new ArrayList<tbltaxmaster>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetbltaxmaster(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tbltaxmaster r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttbltaxmaster(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetbltaxmaster(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttbltaxmaster(Connection localConnection, String record) {
		tbltaxmaster r = this.JSONToClass(record);
		
		return this.inserttbltaxmaster(localConnection, r);
	}
	
	public boolean inserttbltaxmaster(Connection localConnection, tbltaxmaster r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbltaxmasters_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetbltaxmaster(Connection localConnection, String record) {
		tbltaxmaster r = this.JSONToClass(record);
		
		return this.updatetbltaxmaster(localConnection, r);
	}
	
	public boolean updatetbltaxmaster(Connection localConnection, tbltaxmaster r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbltaxmasters_update",
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetbltaxmaster(String record) throws SQLException {
		boolean returnValue = false;
		tbltaxmaster r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tbltaxmasters",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
