package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblterminal;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblterminals extends com.heptanesia.Data.BasicData {
	public tblterminals() {
	}
	
	public tblterminals(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblterminalsColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblterminals", columns, where, orderBy);
		return json;
	}

	public String gettblterminalsAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblterminals", "*", where, orderBy);
		return json;
	}

	public String gettblterminalsAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblterminals", "*", "", "");
		return json;
	}
	
	public String gettblterminalsForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblterminals", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblterminalsForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblterminals", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblterminalsForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblterminals", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblterminalsInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblterminals", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblterminal JSONToClass(String record) {
		Gson gson = new Gson();
		tblterminal r = gson.fromJson(record, tblterminal.class);
		
		return r;
	}

	public ArrayList<tblterminal> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblterminal>>(){}.getType();
		ArrayList<tblterminal> rs = new ArrayList<tblterminal>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblterminal(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblterminal r = this.JSONToClass(record);
		boolean returnValue = false;
		System.out.println("insertUpdatetblterminal ipAddress  ="+ipAddress);
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblterminal(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblterminal(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblterminal(Connection localConnection, String record) {
		tblterminal r = this.JSONToClass(record);
		
		return this.inserttblterminal(localConnection, r);
	}
		
	public boolean inserttblterminal(Connection localConnection, tblterminal r) {
		boolean result = false;

		String chrcompanycode = r.getchrCompanyCode(); 
		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrterminalcode = r.getchrTerminalCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblterminals_insert",
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrterminalcode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			));

			this.common.setParameters(callableStatement,
				chrcompanycode,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrterminalcode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblterminal(Connection localConnection, String record) {
		tblterminal r = this.JSONToClass(record);
		
		return this.updatetblterminal(localConnection, r);
	}
	
	public boolean updatetblterminal(Connection localConnection, tblterminal r) {
		boolean result = false;

		String chrcompanycode = r.getchrCompanyCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrstorecode = r.getchrStoreCode(); 
		String chrterminalcode = r.getchrTerminalCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinisdirty = r.gettinIsDirty(); 
		int tinstatus = r.gettinStatus();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblterminals_update",
				chrcompanycode,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrterminalcode,
				chrupdateuser,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			));

			this.common.setParameters(callableStatement,
				chrcompanycode,
				chripaddress,
				chrreccode,
				chrstorecode,
				chrterminalcode,
				chrupdateuser,
				dtmupdatedate,
				tinisdirty,
				tinstatus
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblterminal(String record) throws SQLException {
		boolean returnValue = false;
		tblterminal r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblterminals",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
