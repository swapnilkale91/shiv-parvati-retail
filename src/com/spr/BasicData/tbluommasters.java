package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tbluommaster;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tbluommasters extends com.heptanesia.Data.BasicData {
	public tbluommasters() {
	}
	
	public tbluommasters(Connection connection) {
		this.connection = connection;
	}
	
	public String gettbluommastersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbluommasters", columns, where, orderBy);
		return json;
	}

	public String gettbluommastersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbluommasters", "*", where, orderBy);
		return json;
	}

	public String gettbluommastersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbluommasters", "*", "", "");
		return json;
	}
	
	public String gettbluommastersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbluommasters", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettbluommastersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tbluommasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettbluommastersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tbluommasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettbluommastersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tbluommasters", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tbluommaster JSONToClass(String record) {
		Gson gson = new Gson();
		tbluommaster r = gson.fromJson(record, tbluommaster.class);
		
		return r;
	}

	public ArrayList<tbluommaster> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tbluommaster>>(){}.getType();
		ArrayList<tbluommaster> rs = new ArrayList<tbluommaster>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetbluommaster(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tbluommaster r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttbluommaster(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetbluommaster(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttbluommaster(Connection localConnection, String record) {
		tbluommaster r = this.JSONToClass(record);
		
		return this.inserttbluommaster(localConnection, r);
	}
	
	public boolean inserttbluommaster(Connection localConnection, tbluommaster r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		System.out.println("Basic data dtmupdatedate ="+dtmupdatedate);
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbluommasters_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetbluommaster(Connection localConnection, String record) {
		tbluommaster r = this.JSONToClass(record);
		
		return this.updatetbluommaster(localConnection, r);
	}
	
	public boolean updatetbluommaster(Connection localConnection, tbluommaster r) {
		boolean result = false;

		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhrcode = r.getvhrCode(); 
		String chripaddress = r.getchrIpAddress(); 
		String vhrname = r.getvhrName();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tbluommasters_update",
				chrreccode,
				chripaddress,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			));

			this.common.setParameters(callableStatement,
				chrreccode,
				chripaddress,
				chrupdateuser,
				dtmupdatedate,
				tinactiveflag,
				tinisdirty,
				vhrcode,
				vhrname
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetbluommaster(String record) throws SQLException {
		boolean returnValue = false;
		tbluommaster r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tbluommasters",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
