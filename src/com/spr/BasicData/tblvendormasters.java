package com.spr.BasicData;

import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.spr.Objects.tblvendormaster;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class tblvendormasters extends com.heptanesia.Data.BasicData {
	public tblvendormasters() {
	}
	
	public tblvendormasters(Connection connection) {
		this.connection = connection;
	}
	
	public String gettblvendormastersColumnsAnyJSON(String columns, String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblvendormasters", columns, where, orderBy);
		return json;
	}

	public String gettblvendormastersAnyJSON(String where, String orderBy) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblvendormasters", "*", where, orderBy);
		return json;
	}

	public String gettblvendormastersAllJSON() throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblvendormasters", "*", "", "");
		return json;
	}
	
	public String gettblvendormastersForRecCodeJSON(String chrRecCode) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblvendormasters", "*", "chrRecCode = '" + chrRecCode + "'", "");
		return json;
	}
	
	public String gettblvendormastersForInsertUserJSON(String chrInsertUser) throws SQLException {
		String json = this.getTableColumnsAnyJSON("tblvendormasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "");
		return json;
	}
	
	public String gettblvendormastersForInsertUserPageJSON(String chrInsertUser, int offset, int pageSize) throws SQLException {
		String json = this.getTableColumnsAnyPageJSON("tblvendormasters", "*", "chrInsertUser = '" + chrInsertUser + "'", "", offset, pageSize);
		return json;
	}
	
	public String gettblvendormastersInsertUserCountJSON(String chrInsertUser) throws SQLException {
		return this.getCountJSON("tblvendormasters", "chrInsertUser = '" + chrInsertUser + "'", "");
	}
	
	public tblvendormaster JSONToClass(String record) {
		Gson gson = new Gson();
		tblvendormaster r = gson.fromJson(record, tblvendormaster.class);
		
		return r;
	}

	public ArrayList<tblvendormaster> JSONSetToClass(String record) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<tblvendormaster>>(){}.getType();
		ArrayList<tblvendormaster> rs = new ArrayList<tblvendormaster>();
		// Record may be a JSON Array or a JSON Object
		if (record.startsWith("[")) {
			rs = gson.fromJson(record, type);
		}
		else {
			rs.add(this.JSONToClass(record));
		}
		
		return rs;
	}
	
	public boolean insertUpdatetblvendormaster(Connection localConnection, String record, String userRecCode, String ipAddress) throws SQLException {
		tblvendormaster r = this.JSONToClass(record);
		boolean returnValue = false;
		
		r.setchrIpAddress(ipAddress);
		if (r.getIUD() == 1) {
			r.setchrInsertUser(userRecCode);
			r.setchrUpdateUser(userRecCode);
			r.setdtmInsertDate(this.common.getCurrentDateTime());
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.inserttblvendormaster(localConnection, r);
		}
		else if (r.getIUD() == 0) {
			r.setchrUpdateUser(userRecCode);
			r.setdtmUpdateDate(this.common.getCurrentDateTime());
			returnValue = this.updatetblvendormaster(localConnection, r);
		}
		
		return returnValue;
	}

	public boolean inserttblvendormaster(Connection localConnection, String record) {
		tblvendormaster r = this.JSONToClass(record);
		
		return this.inserttblvendormaster(localConnection, r);
	}
	
	public boolean inserttblvendormaster(Connection localConnection, tblvendormaster r) {
		boolean result = false;

		String chrinsertuser = r.getchrInsertUser(); 
		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtminsertdate = r.getdtmInsertDate(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrcstno = r.getvhrCSTNo(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrservicetaxno = r.getvhrServiceTaxNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrvatno = r.getvhrVATNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblvendormasters_insert",
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			));

			this.common.setParameters(callableStatement,
				chrinsertuser,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtminsertdate,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updatetblvendormaster(Connection localConnection, String record) {
		tblvendormaster r = this.JSONToClass(record);
		
		return this.updatetblvendormaster(localConnection, r);
	}
	
	public boolean updatetblvendormaster(Connection localConnection, tblvendormaster r) {
		boolean result = false;

		String chripaddress = r.getchrIpAddress(); 
		String chrreccode = r.getchrRecCode(); 
		String chrupdateuser = r.getchrUpdateUser(); 
		String dtmupdatedate = r.getdtmUpdateDate(); 
		int intpincode = r.getintPinCode(); 
		int tinactiveflag = r.gettinActiveFlag(); 
		int tinisdirty = r.gettinIsDirty(); 
		String vhraddress1 = r.getvhrAddress1(); 
		String vhraddress2 = r.getvhrAddress2(); 
		String vhraddress3 = r.getvhrAddress3(); 
		String vhraddress4 = r.getvhrAddress4(); 
		String vhrcode = r.getvhrCode(); 
		String vhrcstno = r.getvhrCSTNo(); 
		String vhrname = r.getvhrName(); 
		String vhrpanno = r.getvhrPANNo(); 
		String vhrservicetaxno = r.getvhrServiceTaxNo(); 
		String vhrtel1 = r.getvhrTel1(); 
		String vhrtel2 = r.getvhrTel2(); 
		String vhrvatno = r.getvhrVATNo();

		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_tblvendormasters_update",
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			));

			this.common.setParameters(callableStatement,
				chripaddress,
				chrreccode,
				chrupdateuser,
				dtmupdatedate,
				intpincode,
				tinactiveflag,
				tinisdirty,
				vhraddress1,
				vhraddress2,
				vhraddress3,
				vhraddress4,
				vhrcode,
				vhrcstno,
				vhrname,
				vhrpanno,
				vhrservicetaxno,
				vhrtel1,
				vhrtel2,
				vhrvatno
			);

			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblvendormaster(String record) throws SQLException {
		boolean returnValue = false;
		tblvendormaster r = this.JSONToClass(record);
		this.connection = this.common.getConnection(this.connection);
		
		returnValue =  this.deleteByRecCodeJSON("tblvendormasters",r.getchrRecCode());
		
		this.common.closeConnection(this.connection);
		return returnValue;
	}

}
