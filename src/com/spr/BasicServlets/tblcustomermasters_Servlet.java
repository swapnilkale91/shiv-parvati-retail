package com.spr.BasicServlets;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spr.Data.tblcustomermasters;
import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblcustomermasters_Servlet
 */
public class tblcustomermasters_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected Message message = new Message();
	protected tblcustomermasters tblcustomermasters = new tblcustomermasters();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblcustomermasters_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("all")) {
				returnValue = this.tblcustomermasters.gettblcustomermastersAllJSON();
			}
			else if (callType.equals("recCode")) {
				String recCode = request.getParameter("RecCode");
				returnValue = this.tblcustomermasters.gettblcustomermastersForRecCodeJSON(recCode);
			}
			else if (callType.equals("insertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				returnValue = this.tblcustomermasters.gettblcustomermastersForInsertUserJSON(insertUser);
			}
			else if (callType.equals("pageInsertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblcustomermasters.gettblcustomermastersForInsertUserPageJSON(insertUser, offset, pageSize);
			}
			else if (callType.equals("countInsertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				returnValue = this.tblcustomermasters.gettblcustomermastersInsertUserCountJSON(insertUser);
			}
			
			response.getWriter().print(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String callType = request.getParameter("Type");
			String record =  request.getParameter("Record");
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			String ipAddress = request.getRemoteAddr();
			boolean success = false;
			
			if (this.message.getSuccess() == true) {
				if (callType.equals("post")) {
					com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
					Connection localConnection = dataCommon.getConnection();

					success = this.tblcustomermasters.insertUpdatetblcustomermaster(localConnection , record, userRecCode, ipAddress);
					if (success == false) {
						this.message = new Message("SAVE", "Error in save.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				else if (callType.equals("delete")) {
					success = this.tblcustomermasters.deletetblcustomermaster(record);
					if (success == false) {
						this.message = new Message("DELETE", "Error in delete.", false);
					}
				}
			}
			
			response.getWriter().print(this.tblcustomermasters.messageToJSON(this.message));
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
