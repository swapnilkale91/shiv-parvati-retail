package com.spr.BasicServlets;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spr.Data.tblinwarditemdetails;
import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblinwarditemdetails_Servlet
 */
public class tblinwarditemdetails_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected Message message = new Message();
	protected tblinwarditemdetails tblinwarditemdetails = new tblinwarditemdetails();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblinwarditemdetails_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("all")) {
				returnValue = this.tblinwarditemdetails.gettblinwarditemdetailsAllJSON();
			}
			else if (callType.equals("recCode")) {
				String recCode = request.getParameter("RecCode");
				returnValue = this.tblinwarditemdetails.gettblinwarditemdetailsForRecCodeJSON(recCode);
			}
			else if (callType.equals("insertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				returnValue = this.tblinwarditemdetails.gettblinwarditemdetailsForInsertUserJSON(insertUser);
			}
			else if (callType.equals("pageInsertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblinwarditemdetails.gettblinwarditemdetailsForInsertUserPageJSON(insertUser, offset, pageSize);
			}
			else if (callType.equals("countInsertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				returnValue = this.tblinwarditemdetails.gettblinwarditemdetailsInsertUserCountJSON(insertUser);
			}
			
			response.getWriter().print(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String callType = request.getParameter("Type");
			String record =  request.getParameter("Record");
			String userRecCode = request.getSession().getAttribute("RecCode").toString();
			String ipAddress = request.getRemoteAddr();
			boolean success = false;
			
			if (this.message.getSuccess() == true) {
				if (callType.equals("post")) {
					com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
					Connection localConnection = dataCommon.getConnection();

					success = this.tblinwarditemdetails.insertUpdatetblinwarditemdetail(localConnection , record, userRecCode, ipAddress);
					if (success == false) {
						this.message = new Message("SAVE", "Error in save.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				else if (callType.equals("delete")) {
					success = this.tblinwarditemdetails.deletetblinwarditemdetail(record);
					if (success == false) {
						this.message = new Message("DELETE", "Error in delete.", false);
					}
				}
			}
			
			response.getWriter().print(this.tblinwarditemdetails.messageToJSON(this.message));
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
