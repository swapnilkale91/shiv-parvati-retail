package com.spr.BasicServlets;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spr.Data.tblsales;
import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblsales_Servlet
 */
public class tblsales_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected Message message = new Message();
	protected tblsales tblsales = new tblsales();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblsales_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("all")) {
				returnValue = this.tblsales.gettblsalesAllJSON();
			}
			else if (callType.equals("recCode")) {
				String recCode = request.getParameter("RecCode");
				returnValue = this.tblsales.gettblsalesForRecCodeJSON(recCode);
			}
			else if (callType.equals("insertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				returnValue = this.tblsales.gettblsalesForInsertUserJSON(insertUser);
			}
			else if (callType.equals("pageInsertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblsales.gettblsalesForInsertUserPageJSON(insertUser, offset, pageSize);
			}
			else if (callType.equals("countInsertUser")) {
				String insertUser = request.getSession().getAttribute("RecCode").toString();
				returnValue = this.tblsales.gettblsalesInsertUserCountJSON(insertUser);
			}
			
			response.getWriter().print(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String callType = request.getParameter("Type");
			String record =  request.getParameter("Record");
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			/*String companyCode = request.getSession().getAttribute("CompanyCode").toString();
			System.out.println("sale in basic post companyCode ="+companyCode);
			String storeCode = request.getSession().getAttribute("StoreCode").toString();
			System.out.println("sale in basic post storeCode ="+storeCode);
			String employeeCode = request.getSession().getAttribute("EmployeeCode").toString();
			System.out.println("sale in basic post employeeCode ="+employeeCode);*/
			String ipAddress = request.getRemoteAddr();
			boolean success = false;
			
			if (this.message.getSuccess() == true) {
				if (callType.equals("post")) {
					com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
					Connection localConnection = dataCommon.getConnection();

					success = this.tblsales.insertUpdatetblsale(localConnection , record, userRecCode, ipAddress);
					if (success == false) {
						this.message = new Message("SAVE", "Error in save.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				else if (callType.equals("delete")) {
					success = this.tblsales.deletetblsale(record);
					if (success == false) {
						this.message = new Message("DELETE", "Error in delete.", false);
					}
				}
			}
			
			response.getWriter().print(this.tblsales.messageToJSON(this.message));
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
