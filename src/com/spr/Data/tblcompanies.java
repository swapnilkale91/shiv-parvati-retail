package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblcompanies extends com.spr.BasicData.tblcompanies {
	
	public tblcompanies() {
		super();
	}
	public tblcompanies(Connection connection) {
		super(connection);
	}
	public String gettblcompaniesForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblcompanies";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblcompaniesCountJSON() throws SQLException {
		String table = "tblcompanies";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblcompaniesForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblcompanies";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblcompaniesCountSearchJSON(String searchText) throws SQLException {
		String table = "tblcompanies";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
