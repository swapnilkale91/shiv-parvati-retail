package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblcustomermasters extends com.spr.BasicData.tblcustomermasters {
	
	public tblcustomermasters() {
		super();
	}
	public tblcustomermasters(Connection connection) {
		super(connection);
	}
	public String gettblcustomermastersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblcustomermasters";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblcustomermastersCountJSON() throws SQLException {
		String table = "tblcustomermasters";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblcustomermastersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblcustomermasters";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblcustomermastersCountSearchJSON(String searchText) throws SQLException {
		String table = "tblcustomermasters";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
