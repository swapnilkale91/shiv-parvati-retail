package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tbldefaultlookups extends com.spr.BasicData.tbldefaultlookups {
	
	public tbldefaultlookups() {
		super();
	}
	public tbldefaultlookups(Connection connection) {
		super(connection);
	}
	public String gettbldefaultlookupsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tbldefaultlookups";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbldefaultlookupsCountJSON() throws SQLException {
		String table = "tbldefaultlookups";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettbldefaultlookupsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tbldefaultlookups";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbldefaultlookupsCountSearchJSON(String searchText) throws SQLException {
		String table = "tbldefaultlookups";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	public String getDefaultLookUpForLookTypeCodeJSON(int LookupTypeCode) throws SQLException {
		String table = "tbldefaultlookups";	
		String columns = "intRecCode,LookupTypeCode,vhrName";
		String where = "LookupTypeCode = '"+LookupTypeCode+"'";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyJSON(table, columns, where, orderBy);
		return json;
	}
}
