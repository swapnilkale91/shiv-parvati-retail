package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblemployeecategories extends com.spr.BasicData.tblemployeecategories {
	
	public tblemployeecategories() {
		super();
	}
	public tblemployeecategories(Connection connection) {
		super(connection);
	}
	public String gettblemployeecategoriesForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblemployeecategories";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblemployeecategoriesCountJSON() throws SQLException {
		String table = "tblemployeecategories";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblemployeecategoriesForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblemployeecategories";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblemployeecategoriesCountSearchJSON(String searchText) throws SQLException {
		String table = "tblemployeecategories";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
