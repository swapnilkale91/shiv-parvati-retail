package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblemployeemasters extends com.spr.BasicData.tblemployeemasters {
	
	public tblemployeemasters() {
		super();
	}
	public tblemployeemasters(Connection connection) {
		super(connection);
	}
	public String gettblemployeemastersForvhrEmailAndvhrPasswordJSON(String vhrUserName, String vhrPassword) {
		String json = "[]";
		try {
			System.out.println("email : " + vhrUserName + "password : " + vhrPassword);
			String table = "tblEmployeeMasters";
			String columns = "*";
			String where = "vhrUserName = '" + vhrUserName + "' and vhrPassword = '" + vhrPassword + "'";
			System.out.println("where" + where);
			json = this.getTableColumnsAnyJSON(table, columns, where, "");
			System.out.println(json+"json");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return json;
	}
	public String gettblemployeemastersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblEmployeeMasters";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblemployeemastersCountJSON() throws SQLException {
		String table = "tblEmployeeMasters";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblemployeemastersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblEmployeeMasters";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblemployeemastersCountSearchJSON(String searchText) throws SQLException {
		String table = "tblEmployeeMasters";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
