package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblinwarditemdetails extends com.spr.BasicData.tblinwarditemdetails {
	
	public tblinwarditemdetails() {
		super();
	}
	public tblinwarditemdetails(Connection connection) {
		super(connection);
	}
	public String gettblinwarditemdetailsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblinwarditemdetails";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblinwarditemdetailsCountJSON() throws SQLException {
		String table = "tblinwarditemdetails";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblinwarditemdetailsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblinwarditemdetails";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblinwarditemdetailsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblinwarditemdetails";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	
	public String gettblinwarddetailsForInwardCodeJSON(String inwardcode) throws SQLException {
		String tableName = "tblinwarditemdetails";	
		String columns = "*";
		String where = "chrParentCode = '" + inwardcode + "'";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyJSON(tableName, columns, where, orderBy);
		return json;
	}

}
