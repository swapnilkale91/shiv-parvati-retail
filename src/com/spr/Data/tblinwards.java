package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblinwards extends com.spr.BasicData.tblinwards {
	
	public tblinwards() {
		super();
	}
	public tblinwards(Connection connection) {
		super(connection);
	}
	public String gettblinwardsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblinwards";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblinwardsCountJSON() throws SQLException {
		String table = "tblinwards";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblinwardsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblinwards";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblinwardsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblinwards";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
