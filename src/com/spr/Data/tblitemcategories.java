package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblitemcategories extends com.spr.BasicData.tblitemcategories {
	
	public tblitemcategories() {
		super();
	}
	public tblitemcategories(Connection connection) {
		super(connection);
	}
	public String gettblitemcategoriesForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblitemcategories";	
		String columns = "*";
		String where = "";	
		String orderBy = "vhrName";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblitemcategoriesCountJSON() throws SQLException {
		String table = "tblitemcategories";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblitemcategoriesForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblitemcategories";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%' OR vhrCode like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblitemcategoriesCountSearchJSON(String searchText) throws SQLException {
		String table = "tblitemcategories";	
		String where = "vhrName like '%"+searchText+"%' OR vhrCode like '%"+searchText+"%'";
		return this.getCountJSON(table, where, "");
	}
}
