package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblitemmasters extends com.spr.BasicData.tblitemmasters {
	
	public tblitemmasters() {
		super();
	}
	public tblitemmasters(Connection connection) {
		super(connection);
	}
	public String gettblitemmastersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblitemmasters";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblitemmastersCountJSON() throws SQLException {
		String table = "tblitemmasters";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblitemmastersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblitemmasters";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%' OR vhrCode like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblitemmastersCountSearchJSON(String searchText) throws SQLException {
		String table = "tblitemmasters";	
		String where = "vhrName like '%"+searchText+"%' OR vhrCode like '%"+searchText+"%'";
		return this.getCountJSON(table, where, "");
	}
}
