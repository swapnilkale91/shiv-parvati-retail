package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblitemracklocations extends com.spr.BasicData.tblitemracklocations {
	
	public tblitemracklocations() {
		super();
	}
	public tblitemracklocations(Connection connection) {
		super(connection);
	}
	public String gettblitemracklocationsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblitemracklocations a";	
		String columns = "a.*,"
				+	"(select vhrName from tblitemmasters where chrRecCode = chrItemCode) as vhrItem,"
				+   "(select vhrName from tblstoreitemracks where chrRecCode = chrRackLocationCode) as vhrRackLocation";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblitemracklocationsCountJSON() throws SQLException {
		String table = "tblitemracklocations";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblitemracklocationsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblitemracklocations";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblitemracklocationsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblitemracklocations";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
