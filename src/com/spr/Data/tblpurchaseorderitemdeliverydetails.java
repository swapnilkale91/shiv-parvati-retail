package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.spr.Objects.tblpurchaseorderitemdeliverydetail;

public class tblpurchaseorderitemdeliverydetails extends com.spr.BasicData.tblpurchaseorderitemdeliverydetails {
	
	public tblpurchaseorderitemdeliverydetails() {
		super();
	}
	public tblpurchaseorderitemdeliverydetails(Connection connection) {
		super(connection);
	}
	public String gettblpurchaseorderitemdeliverydetailsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblpurchaseorderitemdeliverydetails";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblpurchaseorderitemdeliverydetailsCountJSON() throws SQLException {
		String table = "tblpurchaseorderitemdeliverydetails";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblpurchaseorderitemdeliverydetailsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblpurchaseorderitemdeliverydetails";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblpurchaseorderitemdeliverydetailsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblpurchaseorderitemdeliverydetails";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblpurchaseorderitemdeliverydetailsCodeJSON(String purchaseorderreccode) throws SQLException {
		String table = "tblpurchaseorderitemdeliverydetails a , tblpurchaseorderitemdetails b, tblpurchaseorders c";
		String column = "a.*";
		String where = " a.chrParentCode = b.chrRecCode and b.chrParentCode = c.chrRecCode and c.chrRecCode = '"+purchaseorderreccode+"'";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyJSON(table, column, where, orderBy);
		return json;
	}
	
	public boolean deleteByRecCodeJSON(Connection localConnection, String tableName, String where) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", tableName, where));
			this.common.setParameters(callableStatement, tableName, where);
			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletetblpurchaseorderitemdeliverydetailByRecCodeJSON(Connection localConnection, String record) throws SQLException {
		tblpurchaseorderitemdeliverydetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblpurchaseorderitemdeliverydetails", "chrRecCode = '"+r.getchrRecCode()+"'"));
			this.common.setParameters(callableStatement, "tblpurchaseorderitemdeliverydetails", "chrRecCode = '"+r.getchrRecCode()+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			returnValue = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
}

