package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.spr.Objects.tblpurchaseorderitemdetail;

public class tblpurchaseorderitemdetails extends com.spr.BasicData.tblpurchaseorderitemdetails {
	
	public tblpurchaseorderitemdetails() {
		super();
	}
	public tblpurchaseorderitemdetails(Connection connection) {
		super(connection);
	}
	public String gettblpurchaseorderitemdetailsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblpurchaseorderitemdetails a";	
		String columns = "a.*,"
				+"(select vhrName from tblitemmasters where chrRecCode = a.chrItemCode) as vhrItemName,"
				+"(select vhrName from tbluommasters where chrRecCode = a.chrUOMCode) as vhrUMOName,"
				+"(select vhrName from tbltaxdetails where chrRecCode = a.chrVatTaxRecCode) as vhrVatTaxName";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblpurchaseorderitemdetailsCountJSON() throws SQLException {
		String table = "tblpurchaseorderitemdetails";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblpurchaseorderitemdetailsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblpurchaseorderitemdetails";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblpurchaseorderitemdetailsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblpurchaseorderitemdetails";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblpurchaseorderitemdetailsCodeJSON(String chrParentCode) throws SQLException {
		String table = "tblpurchaseorderitemdetails a , tblpurchaseorders b";
		String column = "a.*,"
				+"(select vhrName from tblitemmasters where chrRecCode = a.chrItemCode) as vhrItemName,"
				+"(select vhrName from tbluommasters where chrRecCode = a.chrUOMCode) as vhrUMOName,"
				+"(select vhrName from tbltaxdetails where chrRecCode = a.chrVatTaxRecCode) as vhrVatTaxName";
		String where = " a.chrParentCode = b.chrRecCode and a.chrParentCode = '"+chrParentCode+"'";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyJSON(table, column, where, orderBy);
		return json;
	}
	
	public boolean deleteByRecCodeJSON(Connection localConnection, String tableName, String where) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", tableName, where));
			this.common.setParameters(callableStatement, tableName, where);
			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public boolean deletepurchaseorderitemdetailByRecCodeJSON(Connection localConnection, String record) throws SQLException {
		tblpurchaseorderitemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblpurchaseorderitemdetails", "chrRecCode = '"+r.getchrRecCode()+"'"));
			this.common.setParameters(callableStatement, "tblpurchaseorderitemdetails", "chrRecCode = '"+r.getchrRecCode()+"'");
			callableStatement.execute();
			System.out.println("deleteseamindentificationdetailByRecCodeJSON success ="+ r.getchrRecCode());
			//this.common.closeConnection(localConnection);
			returnValue = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	
	public boolean deletepurchaseorderitemdeliverydetailByRecCodeJSON(Connection localConnection, String record) throws SQLException {
		tblpurchaseorderitemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblpurchaseorderitemdeliverydetails", "chrParentCode = '"+r.getchrRecCode()+"'"));
			this.common.setParameters(callableStatement, "tblpurchaseorderitemdeliverydetails", "chrParentCode = '"+r.getchrRecCode()+"'");
			callableStatement.execute();
			System.out.println("deletereportdetailByRecCodeJSON ="+r.getchrRecCode());
			//this.common.closeConnection(localConnection);
			returnValue = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
}
