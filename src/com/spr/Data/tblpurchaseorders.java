package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class tblpurchaseorders extends com.spr.BasicData.tblpurchaseorders {
	
	public tblpurchaseorders() {
		super();
	}
	public tblpurchaseorders(Connection connection) {
		super(connection);
	}
	public String gettblpurchaseordersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblpurchaseorders a";	
		String columns = "a.* ,"
				+"(select vhrName from tblcompanies where chrRecCode = a.chrCompanyCode) as vhrCompanyName";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblpurchaseordersCountJSON() throws SQLException {
		String table = "tblpurchaseorders";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblpurchaseordersForComboBox() throws SQLException {
		String table = "tblpurchaseorders";	
		String where = "";	
		String columns = "chrPONo, chrRecCode";	
		String orderBy = "";	
		return this.getTableColumnsAnyJSON(table, columns, where, orderBy);
	}
	public String gettblpurchaseordersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblpurchaseorders";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblpurchaseordersCountSearchJSON(String searchText) throws SQLException {
		String table = "tblpurchaseorders";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	
	public boolean delete(Connection localConnection, String chrParentCode) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblpurchaseorders", "chrRecCode = '"+chrParentCode+"'"));
			this.common.setParameters(callableStatement, "tblpurchaseorders", "chrRecCode = '"+chrParentCode+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
