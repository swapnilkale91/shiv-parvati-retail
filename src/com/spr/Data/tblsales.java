package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class tblsales extends com.spr.BasicData.tblsales {
	
	public tblsales() {
		super();
	}
	public tblsales(Connection connection) {
		super(connection);
	}
	public String gettblsalesForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblsales";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblsalesCountJSON() throws SQLException {
		String table = "tblsales";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblsalesForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblsales";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblsalesCountSearchJSON(String searchText) throws SQLException {
		String table = "tblsales";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	
	public boolean delete(Connection localConnection, String chrParentCode) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblpurchaseorders", "chrRecCode = '"+chrParentCode+"'"));
			this.common.setParameters(callableStatement, "tblpurchaseorders", "chrRecCode = '"+chrParentCode+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
