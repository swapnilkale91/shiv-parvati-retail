package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class tblsalesitemdetails extends com.spr.BasicData.tblsalesitemdetails {
	
	public tblsalesitemdetails() {
		super();
	}
	public tblsalesitemdetails(Connection connection) {
		super(connection);
	}
	public String gettblsalesitemdetailsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblsalesitemdetails";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblsalesitemdetailsCountJSON() throws SQLException {
		String table = "tblsalesitemdetails";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblsalesitemdetailsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblsalesitemdetails";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblsalesitemdetailsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblsalesitemdetails";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	
	public boolean deleteByRecCodeJSON(Connection localConnection, String tableName, String where) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", tableName, where));
			this.common.setParameters(callableStatement, tableName, where);
			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
