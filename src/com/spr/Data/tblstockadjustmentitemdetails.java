package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.spr.Objects.tblstockadjustmentitemdetail;

public class tblstockadjustmentitemdetails extends com.spr.BasicData.tblstockadjustmentitemdetails {
	
	public tblstockadjustmentitemdetails() {
		super();
	}
	public tblstockadjustmentitemdetails(Connection connection) {
		super(connection);
	}
	public String gettblstockadjustmentitemdetailsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblstockadjustmentitemdetails";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstockadjustmentitemdetailsCountJSON() throws SQLException {
		String table = "tblstockadjustmentitemdetails";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblstockadjustmentitemdetailsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblstockadjustmentitemdetails";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstockadjustmentitemdetailsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblstockadjustmentitemdetails";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	
	public boolean deleteByRecCodeJSON(Connection localConnection, String tableName, String where) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", tableName, where));
			this.common.setParameters(callableStatement, tableName, where);
			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean delete(Connection localConnection, String chrRecCode) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblstockadjustmentitemdetails", "RecCode = '"+chrRecCode+"'"));
			this.common.setParameters(callableStatement, "tblstockadjustmentitemdetails", "RecCode = '"+chrRecCode+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean deletelhstubedetailByRecCodeJSON(Connection localConnection, String record) throws SQLException {
		System.out.println("record ; " + record);
		tblstockadjustmentitemdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblstockadjustmentitemdetails", "RecCode = '"+r.getchrRecCode()+"'"));
			this.common.setParameters(callableStatement, "tblstockadjustmentitemdetails", "RecCode = '"+r.getchrRecCode()+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			returnValue = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
}

