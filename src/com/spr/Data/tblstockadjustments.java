package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class tblstockadjustments extends com.spr.BasicData.tblstockadjustments {
	
	public tblstockadjustments() {
		super();
	}
	public tblstockadjustments(Connection connection) {
		super(connection);
	}
	public String gettblstockadjustmentsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblstockadjustments a";	
		String columns = "a.* ,"
			+"(select vhrName from tblcompanies where chrRecCode = a.chrCompanyCode) as vhrCompanyName,"
			+"(select vhrName from tblcompanies where chrRecCode = a.chrStoreCode) as vhrStoreName";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstockadjustmentsCountJSON() throws SQLException {
		String table = "tblstockadjustments";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblstockadjustmentsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblstockadjustments";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstockadjustmentsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblstockadjustments";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblstockadjustmentdetailsJSON(String chrParentCode) throws SQLException {
		String table = "tblstockadjustmentitemdetails  wp, tblstockadjustments je";	
		String columns = "wp.*, je.*,"
				+"(select vhrName from tblitemmasters where chrRecCode = wp.chrItemCode) as vhrItemName";
		String where = "wp.chrParentCode = je.chrRecCode and chrParentCode = '"+chrParentCode+"'";
		String json = this.getTableColumnsAnyJSON(table, columns, where, "");
		System.out.println("getLHSTubeDimensionCountJobNoJSON json : " + json);
		return json;
	}
	
	public boolean delete(Connection localConnection, String chrRecCode) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tblstockadjustments", "chrRecCode = '"+chrRecCode+"'"));
			this.common.setParameters(callableStatement, "LHSTubeDimension", "chrRecCode = '"+chrRecCode+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
