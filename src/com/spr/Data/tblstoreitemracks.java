package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblstoreitemracks extends com.spr.BasicData.tblstoreitemracks {
	
	public tblstoreitemracks() {
		super();
	}
	public tblstoreitemracks(Connection connection) {
		super(connection);
	}
	public String gettblstoreitemracksForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblstoreitemracks";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstoreitemracksCountJSON() throws SQLException {
		String table = "tblstoreitemracks";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblstoreitemracksForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblstoreitemracks";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstoreitemracksCountSearchJSON(String searchText) throws SQLException {
		String table = "tblstoreitemracks";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
