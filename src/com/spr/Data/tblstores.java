package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblstores extends com.spr.BasicData.tblstores {
	
	public tblstores() {
		super();
	}
	public tblstores(Connection connection) {
		super(connection);
	}
	public String gettblstoresForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblstores";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstoresCountJSON() throws SQLException {
		String table = "tblstores";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblstoresForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblstores";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblstoresCountSearchJSON(String searchText) throws SQLException {
		String table = "tblstores";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
