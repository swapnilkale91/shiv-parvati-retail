package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.spr.Objects.tbltaxdetail;

public class tbltaxdetails extends com.spr.BasicData.tbltaxdetails {
	
	public tbltaxdetails() {
		super();
	}
	public tbltaxdetails(Connection connection) {
		super(connection);
	}
	public String gettbltaxdetailsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tbltaxdetails";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbltaxdetailsCountJSON() throws SQLException {
		String table = "tbltaxdetails";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettbltaxdetailsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tbltaxdetails";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbltaxdetailsCountSearchJSON(String searchText) throws SQLException {
		String table = "tbltaxdetails";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
	
	public String getTaxDetailsJSON(String chrParentCode) throws SQLException {
		String table = "tbltaxdetails";	
		String columns = "*";
		String where = "chrParentCode = '"+chrParentCode+"'";
		String orderBy = "";
		return this.getTableColumnsAnyJSON(table, columns, where, orderBy);
	}
	
	public boolean deletetbltaxdetailByRecCodeJSON(Connection localConnection, String record) throws SQLException {
		System.out.println("Inside delete record : " + record);
		tbltaxdetail r = this.JSONToClass(record);
		boolean returnValue = false;
		
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tbltaxdetails", "chrRecCode = '"+r.getchrRecCode()+"'"));
			this.common.setParameters(callableStatement, "tbltaxdetails", "chrRecCode = '"+r.getchrRecCode()+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			returnValue = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public boolean deleteByRecCodeJSON(Connection localConnection, String tableName, String where) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", tableName, where));
			this.common.setParameters(callableStatement, tableName, where);
			callableStatement.execute();
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public String getTaxPercentageByTaxCodeAndPODate(String taxcode, String podate) throws SQLException {
		String tableName = "tbltaxdetails";	
		String columns = "*";
		String where = "chrParentCode = '" + taxcode + "'";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyJSON(tableName, columns, where, orderBy);
		return json;
	}
}
