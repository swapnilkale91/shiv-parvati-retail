package com.spr.Data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class tbltaxmasters extends com.spr.BasicData.tbltaxmasters {
	
	public tbltaxmasters() {
		super();
	}
	public tbltaxmasters(Connection connection) {
		super(connection);
	}
	public String gettbltaxmastersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tbltaxmasters";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbltaxmastersCountJSON() throws SQLException {
		String table = "tbltaxmasters";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettbltaxmastersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tbltaxmasters";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%' OR vhrCode like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbltaxmastersCountSearchJSON(String searchText) throws SQLException {
		String table = "tbltaxmasters";	
		String where = "vhrName like '%"+searchText+"%' OR vhrCode like '%"+searchText+"%'";
		return this.getCountJSON(table, where, "");
	}
	
	public boolean delete(Connection localConnection, String chrRecCode) throws SQLException {
		boolean result = false;
		try {
			CallableStatement callableStatement = localConnection.prepareCall(this.common.setStatement("USP_Delete_Dynamic", "tbltaxmasters", "chrRecCode = '"+chrRecCode+"'"));
			this.common.setParameters(callableStatement, "tbltaxmasters", "chrRecCode = '"+chrRecCode+"'");
			callableStatement.execute();
			
			//this.common.closeConnection(localConnection);
			result = true;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
