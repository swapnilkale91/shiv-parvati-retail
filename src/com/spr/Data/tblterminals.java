package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblterminals extends com.spr.BasicData.tblterminals {
	
	public tblterminals() {
		super();
	}
	public tblterminals(Connection connection) {
		super(connection);
	}
	public String gettblterminalsForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblterminals a";	
		String columns = "a.*,"
				+"(select vhrName from tblcompanies where chrRecCode = a.chrCompanyCode) as vhrCompanyName,"
				+"(select vhrName from tblcompanies where chrRecCode = a.chrStoreCode) as vhrStoreName";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblterminalsCountJSON() throws SQLException {
		String table = "tblterminals";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblterminalsForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblterminals";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblterminalsCountSearchJSON(String searchText) throws SQLException {
		String table = "tblterminals";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
