package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tbluommasters extends com.spr.BasicData.tbluommasters {
	
	public tbluommasters() {
		super();
	}
	public tbluommasters(Connection connection) {
		super(connection);
	}
	public String gettbluommastersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tbluommasters";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbluommastersCountJSON() throws SQLException {
		String table = "tbluommasters";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettbluommastersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tbluommasters";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettbluommastersCountSearchJSON(String searchText) throws SQLException {
		String table = "tbluommasters";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
