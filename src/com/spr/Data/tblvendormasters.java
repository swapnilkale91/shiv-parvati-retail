package com.spr.Data;

import java.sql.Connection;
import java.sql.SQLException;

public class tblvendormasters extends com.spr.BasicData.tblvendormasters {
	
	public tblvendormasters() {
		super();
	}
	public tblvendormasters(Connection connection) {
		super(connection);
	}
	public String gettblvendormastersForPageJSON(int offset, int pageSize) throws SQLException {
		String table = "tblvendormasters";	
		String columns = "*";
		String where = "";	
		String orderBy = "";	
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblvendormastersCountJSON() throws SQLException {
		String table = "tblvendormasters";	
		String where = "";	
		return this.getCountJSON(table, where, "");
	}
	public String gettblvendormastersForPageSearchJSON(int offset, int pageSize, String searchText) throws SQLException {
		String table = "tblvendormasters";	
		String columns = "*";
		String where = "vhrName like '%"+searchText+"%'";
		String orderBy = "vhrName";
		String json = this.getTableColumnsAnyPageJSON(table, columns, where, orderBy, offset, pageSize);
		return json;
	}
	public String gettblvendormastersCountSearchJSON(String searchText) throws SQLException {
		String table = "tblvendormasters";	
		String where = "vhrName like '%"+searchText+"%'";	
		return this.getCountJSON(table, where, "");
	}
}
