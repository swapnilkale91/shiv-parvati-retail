package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tbldefaultlookup extends Common {
	public tbldefaultlookup() {
	}

	private String intRecCode; 
	private int LookupTypeCode; 
	private String vhrName;
	public void setintRecCode(String intreccode) { this.intRecCode = intreccode; } 
	public void setLookupTypeCode(int lookuptypecode) { this.LookupTypeCode = lookuptypecode; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; }
	public String getintRecCode() { return this.intRecCode; } 
	public int getLookupTypeCode() { return this.LookupTypeCode; } 
	public String getvhrName() { return this.vhrName; }
}
