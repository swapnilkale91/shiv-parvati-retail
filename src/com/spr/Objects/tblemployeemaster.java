package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblemployeemaster extends Common {
	public tblemployeemaster() {
	}

	//private String chrCompanyCode; 
	private String chrEmployeeCategoryCode; 
	private String chrRecCode; 
	private String chrStoreCode; 
	private int intPinCode; 
	private int tinLockUser; 
	private String vhrAddress1; 
	private String vhrAddress2; 
	private String vhrAddress3; 
	private String vhrAddress4; 
	private String vhrCode; 
	private String vhrName; 
	private String vhrPANNo; 
	private String vhrPassword; 
	private String vhrTel1; 
	private String vhrTel2; 
	private String vhrUserName;
//	public void setchrCompanyCode(String chrcompanycode) { this.chrCompanyCode = chrcompanycode; } 
	public void setchrEmployeeCategoryCode(String chremployeecategorycode) { this.chrEmployeeCategoryCode = chremployeecategorycode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreCode(String chrstorecode) { this.chrStoreCode = chrstorecode; } 
	public void setintPinCode(int intpincode) { this.intPinCode = intpincode; } 
	public void settinLockUser(int tinlockuser) { this.tinLockUser = tinlockuser; } 
	public void setvhrAddress1(String vhraddress1) { this.vhrAddress1 = vhraddress1; } 
	public void setvhrAddress2(String vhraddress2) { this.vhrAddress2 = vhraddress2; } 
	public void setvhrAddress3(String vhraddress3) { this.vhrAddress3 = vhraddress3; } 
	public void setvhrAddress4(String vhraddress4) { this.vhrAddress4 = vhraddress4; } 
	public void setvhrCode(String vhrcode) { this.vhrCode = vhrcode; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; } 
	public void setvhrPANNo(String vhrpanno) { this.vhrPANNo = vhrpanno; } 
	public void setvhrPassword(String vhrpassword) { this.vhrPassword = vhrpassword; } 
	public void setvhrTel1(String vhrtel1) { this.vhrTel1 = vhrtel1; } 
	public void setvhrTel2(String vhrtel2) { this.vhrTel2 = vhrtel2; } 
	public void setvhrUserName(String vhrusername) { this.vhrUserName = vhrusername; }
//	public String getchrCompanyCode() { return this.chrCompanyCode; } 
	public String getchrEmployeeCategoryCode() { return this.chrEmployeeCategoryCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreCode() { return this.chrStoreCode; } 
	public int getintPinCode() { return this.intPinCode; } 
	public int gettinLockUser() { return this.tinLockUser; } 
	public String getvhrAddress1() { return this.vhrAddress1; } 
	public String getvhrAddress2() { return this.vhrAddress2; } 
	public String getvhrAddress3() { return this.vhrAddress3; } 
	public String getvhrAddress4() { return this.vhrAddress4; } 
	public String getvhrCode() { return this.vhrCode; } 
	public String getvhrName() { return this.vhrName; } 
	public String getvhrPANNo() { return this.vhrPANNo; } 
	public String getvhrPassword() { return this.vhrPassword; } 
	public String getvhrTel1() { return this.vhrTel1; } 
	public String getvhrTel2() { return this.vhrTel2; } 
	public String getvhrUserName() { return this.vhrUserName; }
}
