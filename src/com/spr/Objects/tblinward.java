package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblinward extends Common {
	public tblinward() {
		
	}
	
	private String chrAuthorizationUserOne; 
	private String chrAuthorizationUserThree; 
	private String chrAuthorizationUserTwo; 
//	private String chrCompanyCode; 
	private String chrPOCode; 
	private String chrRecCode; 
	private String chrStoreCode; 
	private String chrVendorCode; 
	private float decItemDiscountTotal; 
	private float decItemSchemeTotal; 
	private float decItemTotal; 
	private float decVATTaxTotal; 
	private float decVendorPayableAmount; 
	private String dtmAuthorizationDateOne; 
	private String dtmAuthorizationDateThree; 
	private String dtmAuthorizationDateTwo; 
	private String dtmDueDate; 
	private String dtmInwarddate; 
	private int intCreditPeriod; 
	private int tinAuthorizationLevel; 
	private int tinStatus; 
	private String vhrChallanNo; 
	private String vhrInwardNo; 
	private String vhrLorryNo;
	public void setchrAuthorizationUserOne(String chrauthorizationuserone) { this.chrAuthorizationUserOne = chrauthorizationuserone; } 
	public void setchrAuthorizationUserThree(String chrauthorizationuserthree) { this.chrAuthorizationUserThree = chrauthorizationuserthree; } 
	public void setchrAuthorizationUserTwo(String chrauthorizationusertwo) { this.chrAuthorizationUserTwo = chrauthorizationusertwo; } 
//	public void setchrCompanyCode(String chrcompanycode) { this.chrCompanyCode = chrcompanycode; } 
	public void setchrPOCode(String chrpocode) { this.chrPOCode = chrpocode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreCode(String chrstorecode) { this.chrStoreCode = chrstorecode; } 
	public void setchrVendorCode(String chrvendorcode) { this.chrVendorCode = chrvendorcode; } 
	public void setdecItemDiscountTotal(float decitemdiscounttotal) { this.decItemDiscountTotal = decitemdiscounttotal; } 
	public void setdecItemSchemeTotal(float decitemschemetotal) { this.decItemSchemeTotal = decitemschemetotal; } 
	public void setdecItemTotal(float decitemtotal) { this.decItemTotal = decitemtotal; } 
	public void setdecVATTaxTotal(float decvattaxtotal) { this.decVATTaxTotal = decvattaxtotal; } 
	public void setdecVendorPayableAmount(float decvendorpayableamount) { this.decVendorPayableAmount = decvendorpayableamount; } 
	public void setdtmAuthorizationDateOne(String dtmauthorizationdateone) { this.dtmAuthorizationDateOne = dtmauthorizationdateone; } 
	public void setdtmAuthorizationDateThree(String dtmauthorizationdatethree) { this.dtmAuthorizationDateThree = dtmauthorizationdatethree; } 
	public void setdtmAuthorizationDateTwo(String dtmauthorizationdatetwo) { this.dtmAuthorizationDateTwo = dtmauthorizationdatetwo; } 
	public void setdtmDueDate(String dtmduedate) { this.dtmDueDate = dtmduedate; } 
	public void setdtmInwarddate(String dtminwarddate) { this.dtmInwarddate = dtminwarddate; } 
	public void setintCreditPeriod(int intcreditperiod) { this.intCreditPeriod = intcreditperiod; } 
	public void settinAuthorizationLevel(int tinauthorizationlevel) { this.tinAuthorizationLevel = tinauthorizationlevel; } 
	public void settinStatus(int tinstatus) { this.tinStatus = tinstatus; } 
	public void setvhrChallanNo(String vhrchallanno) { this.vhrChallanNo = vhrchallanno; } 
	public void setvhrInwardNo(String vhrinwardno) { this.vhrInwardNo = vhrinwardno; } 
	public void setvhrLorryNo(String vhrlorryno) { this.vhrLorryNo = vhrlorryno; }
	public String getchrAuthorizationUserOne() { return this.chrAuthorizationUserOne; } 
	public String getchrAuthorizationUserThree() { return this.chrAuthorizationUserThree; } 
	public String getchrAuthorizationUserTwo() { return this.chrAuthorizationUserTwo; } 
//	public String getchrCompanyCode() { return this.chrCompanyCode; } 
	public String getchrPOCode() { return this.chrPOCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreCode() { return this.chrStoreCode; } 
	public String getchrVendorCode() { return this.chrVendorCode; } 
	public float getdecItemDiscountTotal() { return this.decItemDiscountTotal; } 
	public float getdecItemSchemeTotal() { return this.decItemSchemeTotal; } 
	public float getdecItemTotal() { return this.decItemTotal; } 
	public float getdecVATTaxTotal() { return this.decVATTaxTotal; } 
	public float getdecVendorPayableAmount() { return this.decVendorPayableAmount; } 
	public String getdtmAuthorizationDateOne() { return this.dtmAuthorizationDateOne; } 
	public String getdtmAuthorizationDateThree() { return this.dtmAuthorizationDateThree; } 
	public String getdtmAuthorizationDateTwo() { return this.dtmAuthorizationDateTwo; } 
	public String getdtmDueDate() { return this.dtmDueDate; } 
	public String getdtmInwarddate() { return this.dtmInwarddate; } 
	public int getintCreditPeriod() { return this.intCreditPeriod; } 
	public int gettinAuthorizationLevel() { return this.tinAuthorizationLevel; } 
	public int gettinStatus() { return this.tinStatus; } 
	public String getvhrChallanNo() { return this.vhrChallanNo; } 
	public String getvhrInwardNo() { return this.vhrInwardNo; } 
	public String getvhrLorryNo() { return this.vhrLorryNo; }
}
