package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblinwarditemdetail extends Common {
	public tblinwarditemdetail() {
	}

	private String chrItemCode; 
	private String chrParentCode; 
	private String chrRecCode; 
	private String chrStoreRackCode; 
	private String chrVATTaxRecCode; 
	private float decCustomerDiscountAmount; 
	private float decCustomerDiscountPercentage; 
	private float decCustomerItemRate; 
	private float decFreeQuantity; 
	private float decItemAmount; 
	private float decItemAmountAfterTax; 
	private float decItemDiscountAmount; 
	private float decMarginAmount; 
	private float decMarginPercentage; 
	private float decMRP; 
	private float decQuantity; 
	private float decSchemeDiscount; 
	private float decSchemeDiscountAmount; 
	private float decVATTaxAmount; 
	private float decVATTaxPercentage; 
	private float decVendorDiscount1; 
	private float decVendorDiscount2; 
	private float decVendorDiscountAmount1; 
	private float decVendorDiscountAmount2; 
	private String dtmExpiryDate; 
	private String dtmManufactureDate; 
	private int tinPrintLabel; 
	private String vhrBarCode; 
	private String vhrBatchNo; 
	private String vhrDescription; 
	private String vhrGeneratedBarcode; 
	private String vhrPacking;
	public void setchrItemCode(String chritemcode) { this.chrItemCode = chritemcode; } 
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreRackCode(String chrstorerackcode) { this.chrStoreRackCode = chrstorerackcode; } 
	public void setchrVATTaxRecCode(String chrvattaxreccode) { this.chrVATTaxRecCode = chrvattaxreccode; } 
	public void setdecCustomerDiscountAmount(float deccustomerdiscountamount) { this.decCustomerDiscountAmount = deccustomerdiscountamount; } 
	public void setdecCustomerDiscountPercentage(float deccustomerdiscountpercentage) { this.decCustomerDiscountPercentage = deccustomerdiscountpercentage; } 
	public void setdecCustomerItemRate(float deccustomeritemrate) { this.decCustomerItemRate = deccustomeritemrate; } 
	public void setdecFreeQuantity(float decfreequantity) { this.decFreeQuantity = decfreequantity; } 
	public void setdecItemAmount(float decitemamount) { this.decItemAmount = decitemamount; } 
	public void setdecItemAmountAfterTax(float decitemamountaftertax) { this.decItemAmountAfterTax = decitemamountaftertax; } 
	public void setdecItemDiscountAmount(float decitemdiscountamount) { this.decItemDiscountAmount = decitemdiscountamount; } 
	public void setdecMarginAmount(float decmarginamount) { this.decMarginAmount = decmarginamount; } 
	public void setdecMarginPercentage(float decmarginpercentage) { this.decMarginPercentage = decmarginpercentage; } 
	public void setdecMRP(float decmrp) { this.decMRP = decmrp; } 
	public void setdecQuantity(float decquantity) { this.decQuantity = decquantity; } 
	public void setdecSchemeDiscount(float decschemediscount) { this.decSchemeDiscount = decschemediscount; } 
	public void setdecSchemeDiscountAmount(float decschemediscountamount) { this.decSchemeDiscountAmount = decschemediscountamount; } 
	public void setdecVATTaxAmount(float decvattaxamount) { this.decVATTaxAmount = decvattaxamount; } 
	public void setdecVATTaxPercentage(float decvattaxpercentage) { this.decVATTaxPercentage = decvattaxpercentage; } 
	public void setdecVendorDiscount1(float decvendordiscount1) { this.decVendorDiscount1 = decvendordiscount1; } 
	public void setdecVendorDiscount2(float decvendordiscount2) { this.decVendorDiscount2 = decvendordiscount2; } 
	public void setdecVendorDiscountAmount1(float decvendordiscountamount1) { this.decVendorDiscountAmount1 = decvendordiscountamount1; } 
	public void setdecVendorDiscountAmount2(float decvendordiscountamount2) { this.decVendorDiscountAmount2 = decvendordiscountamount2; } 
	public void setdtmExpiryDate(String dtmexpirydate) { this.dtmExpiryDate = dtmexpirydate; } 
	public void setdtmManufactureDate(String dtmmanufacturedate) { this.dtmManufactureDate = dtmmanufacturedate; } 
	public void settinPrintLabel(int tinprintlabel) { this.tinPrintLabel = tinprintlabel; } 
	public void setvhrBarCode(String vhrbarcode) { this.vhrBarCode = vhrbarcode; } 
	public void setvhrBatchNo(String vhrbatchno) { this.vhrBatchNo = vhrbatchno; } 
	public void setvhrDescription(String vhrdescription) { this.vhrDescription = vhrdescription; } 
	public void setvhrGeneratedBarcode(String vhrgeneratedbarcode) { this.vhrGeneratedBarcode = vhrgeneratedbarcode; } 
	public void setvhrPacking(String vhrpacking) { this.vhrPacking = vhrpacking; }
	public String getchrItemCode() { return this.chrItemCode; } 
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreRackCode() { return this.chrStoreRackCode; } 
	public String getchrVATTaxRecCode() { return this.chrVATTaxRecCode; } 
	public float getdecCustomerDiscountAmount() { return this.decCustomerDiscountAmount; } 
	public float getdecCustomerDiscountPercentage() { return this.decCustomerDiscountPercentage; } 
	public float getdecCustomerItemRate() { return this.decCustomerItemRate; } 
	public float getdecFreeQuantity() { return this.decFreeQuantity; } 
	public float getdecItemAmount() { return this.decItemAmount; } 
	public float getdecItemAmountAfterTax() { return this.decItemAmountAfterTax; } 
	public float getdecItemDiscountAmount() { return this.decItemDiscountAmount; } 
	public float getdecMarginAmount() { return this.decMarginAmount; } 
	public float getdecMarginPercentage() { return this.decMarginPercentage; } 
	public float getdecMRP() { return this.decMRP; } 
	public float getdecQuantity() { return this.decQuantity; } 
	public float getdecSchemeDiscount() { return this.decSchemeDiscount; } 
	public float getdecSchemeDiscountAmount() { return this.decSchemeDiscountAmount; } 
	public float getdecVATTaxAmount() { return this.decVATTaxAmount; } 
	public float getdecVATTaxPercentage() { return this.decVATTaxPercentage; } 
	public float getdecVendorDiscount1() { return this.decVendorDiscount1; } 
	public float getdecVendorDiscount2() { return this.decVendorDiscount2; } 
	public float getdecVendorDiscountAmount1() { return this.decVendorDiscountAmount1; } 
	public float getdecVendorDiscountAmount2() { return this.decVendorDiscountAmount2; } 
	public String getdtmExpiryDate() { return this.dtmExpiryDate; } 
	public String getdtmManufactureDate() { return this.dtmManufactureDate; } 
	public int gettinPrintLabel() { return this.tinPrintLabel; } 
	public String getvhrBarCode() { return this.vhrBarCode; } 
	public String getvhrBatchNo() { return this.vhrBatchNo; } 
	public String getvhrDescription() { return this.vhrDescription; } 
	public String getvhrGeneratedBarcode() { return this.vhrGeneratedBarcode; } 
	public String getvhrPacking() { return this.vhrPacking; }
}
