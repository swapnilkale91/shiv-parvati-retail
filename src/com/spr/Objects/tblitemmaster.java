package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblitemmaster extends Common {
	public tblitemmaster() {
	}

	private String chrItemCategoryCode; 
	private String chrRecCode; 
	private String chrUnitOfMeasureCode; 
	private int intReOrderQuantity; 
	private int tinLoseSold; 
	private String vhrBarcode; 
	private String vhrCode; 
	private String vhrDescription; 
	private String vhrName; 
	private String vhrShortName;
	public void setchrItemCategoryCode(String chritemcategorycode) { this.chrItemCategoryCode = chritemcategorycode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrUnitOfMeasureCode(String chrunitofmeasurecode) { this.chrUnitOfMeasureCode = chrunitofmeasurecode; } 
	public void setintReOrderQuantity(int intreorderquantity) { this.intReOrderQuantity = intreorderquantity; } 
	public void settinLoseSold(int tinlosesold) { this.tinLoseSold = tinlosesold; } 
	public void setvhrBarcode(String vhrbarcode) { this.vhrBarcode = vhrbarcode; } 
	public void setvhrCode(String vhrcode) { this.vhrCode = vhrcode; } 
	public void setvhrDescription(String vhrdescription) { this.vhrDescription = vhrdescription; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; } 
	public void setvhrShortName(String vhrshortname) { this.vhrShortName = vhrshortname; }
	public String getchrItemCategoryCode() { return this.chrItemCategoryCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrUnitOfMeasureCode() { return this.chrUnitOfMeasureCode; } 
	public int getintReOrderQuantity() { return this.intReOrderQuantity; } 
	public int gettinLoseSold() { return this.tinLoseSold; } 
	public String getvhrBarcode() { return this.vhrBarcode; } 
	public String getvhrCode() { return this.vhrCode; } 
	public String getvhrDescription() { return this.vhrDescription; } 
	public String getvhrName() { return this.vhrName; } 
	public String getvhrShortName() { return this.vhrShortName; }
}
