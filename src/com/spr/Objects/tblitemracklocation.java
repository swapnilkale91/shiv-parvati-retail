package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblitemracklocation extends Common {
	public tblitemracklocation() {
	}

	private String chrItemCode; 
	private String chrRackLocationCode; 
	private String chrRecCode; 
	private int tinStatus;
	public void setchrItemCode(String chritemcode) { this.chrItemCode = chritemcode; } 
	public void setchrRackLocationCode(String chrracklocationcode) { this.chrRackLocationCode = chrracklocationcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void settinStatus(int tinstatus) { this.tinStatus = tinstatus; }
	public String getchrItemCode() { return this.chrItemCode; } 
	public String getchrRackLocationCode() { return this.chrRackLocationCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public int gettinStatus() { return this.tinStatus; }
}
