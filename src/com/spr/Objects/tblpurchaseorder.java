package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblpurchaseorder extends Common {
	public tblpurchaseorder() {
	}

	private String chrAuthorizationUserOne; 
	private String chrAuthorizationUserThree; 
	private String chrAuthorizationUserTwo; 
//	private String chrCompanyCode; 
	private String chrPONo; 
	private String chrRecCode; 
	private String chrVendorCode; 
	private float decDiscountTotal; 
	private float decItemTotal; 
	private float decMarginTotal; 
	private float decPOAmt; 
	private float decSchemeTotal; 
	private float decVatTaxTotal; 
	private String dtmAuthorizationDateOne; 
	private String dtmAuthorizationDateThree; 
	private String dtmAuthorizationDateTwo; 
	private String dtmPODate; 
	private int intCreditPeriod; 
	private int tinAuthorizationLevel; 
	private int tinStatus;
	public void setchrAuthorizationUserOne(String chrauthorizationuserone) { this.chrAuthorizationUserOne = chrauthorizationuserone; } 
	public void setchrAuthorizationUserThree(String chrauthorizationuserthree) { this.chrAuthorizationUserThree = chrauthorizationuserthree; } 
	public void setchrAuthorizationUserTwo(String chrauthorizationusertwo) { this.chrAuthorizationUserTwo = chrauthorizationusertwo; } 
//	public void setchrCompanyCode(String chrcompanycode) { this.chrCompanyCode = chrcompanycode; } 
	public void setchrPONo(String chrpono) { this.chrPONo = chrpono; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrVendorCode(String chrvendorcode) { this.chrVendorCode = chrvendorcode; } 
	public void setdecDiscountTotal(float decdiscounttotal) { this.decDiscountTotal = decdiscounttotal; } 
	public void setdecItemTotal(float decitemtotal) { this.decItemTotal = decitemtotal; } 
	public void setdecMarginTotal(float decmargintotal) { this.decMarginTotal = decmargintotal; } 
	public void setdecPOAmt(float decpoamt) { this.decPOAmt = decpoamt; } 
	public void setdecSchemeTotal(float decschemetotal) { this.decSchemeTotal = decschemetotal; } 
	public void setdecVatTaxTotal(float decvattaxtotal) { this.decVatTaxTotal = decvattaxtotal; } 
	public void setdtmAuthorizationDateOne(String dtmauthorizationdateone) { this.dtmAuthorizationDateOne = dtmauthorizationdateone; } 
	public void setdtmAuthorizationDateThree(String dtmauthorizationdatethree) { this.dtmAuthorizationDateThree = dtmauthorizationdatethree; } 
	public void setdtmAuthorizationDateTwo(String dtmauthorizationdatetwo) { this.dtmAuthorizationDateTwo = dtmauthorizationdatetwo; } 
	public void setdtmPODate(String dtmpodate) { this.dtmPODate = dtmpodate; } 
	public void setintCreditPeriod(int intcreditperiod) { this.intCreditPeriod = intcreditperiod; } 
	public void settinAuthorizationLevel(int tinauthorizationlevel) { this.tinAuthorizationLevel = tinauthorizationlevel; } 
	public void settinStatus(int tinstatus) { this.tinStatus = tinstatus; }
	public String getchrAuthorizationUserOne() { return this.chrAuthorizationUserOne; } 
	public String getchrAuthorizationUserThree() { return this.chrAuthorizationUserThree; } 
	public String getchrAuthorizationUserTwo() { return this.chrAuthorizationUserTwo; } 
//	public String getchrCompanyCode() { return this.chrCompanyCode; } 
	public String getchrPONo() { return this.chrPONo; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrVendorCode() { return this.chrVendorCode; } 
	public float getdecDiscountTotal() { return this.decDiscountTotal; } 
	public float getdecItemTotal() { return this.decItemTotal; } 
	public float getdecMarginTotal() { return this.decMarginTotal; } 
	public float getdecPOAmt() { return this.decPOAmt; } 
	public float getdecSchemeTotal() { return this.decSchemeTotal; } 
	public float getdecVatTaxTotal() { return this.decVatTaxTotal; } 
	public String getdtmAuthorizationDateOne() { return this.dtmAuthorizationDateOne; } 
	public String getdtmAuthorizationDateThree() { return this.dtmAuthorizationDateThree; } 
	public String getdtmAuthorizationDateTwo() { return this.dtmAuthorizationDateTwo; } 
	public String getdtmPODate() { return this.dtmPODate; } 
	public int getintCreditPeriod() { return this.intCreditPeriod; } 
	public int gettinAuthorizationLevel() { return this.tinAuthorizationLevel; } 
	public int gettinStatus() { return this.tinStatus; }
}
