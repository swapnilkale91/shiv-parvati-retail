package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblpurchaseorderitemdeliverydetail extends Common {
	public tblpurchaseorderitemdeliverydetail() {
	}

	private String chrParentCode; 
	private String chrRecCode; 
	private String chrStoreLocationCode; 
	private float decQuantity; 

	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreLocationCode(String chrstorelocationcode) { this.chrStoreLocationCode = chrstorelocationcode; } 
	public void setdecQuantity(float decquantity) { this.decQuantity = decquantity; } 

	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreLocationCode() { return this.chrStoreLocationCode; } 
	public float getdecQuantity() { return this.decQuantity; } 

}
