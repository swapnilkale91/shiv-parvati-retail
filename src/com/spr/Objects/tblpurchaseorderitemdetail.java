package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblpurchaseorderitemdetail extends Common {
	public tblpurchaseorderitemdetail() {
	}

	private String chrAmount; 
	private String chrItemCode; 
	private String chrItemDescription; 
	private String chrParentCode; 
	private String chrRecCode; 
	private String chrUOMCode; 
	private String chrVatTaxRecCode; 
	private float decItemAmount; 
	private float decItemAmountAfterTax; 
	private float decItemDiscount; 
	private float decMarginAmount; 
	private float decMarginPercent; 
	private float decQuantity; 
	private float decRate; 
	private float decSchemeDiscountAmount; 
	private float decSchemeDiscountPercent; 
	private float decVatTaxAmount; 
	private float decVatTaxPercent; 
	private float decVendorAmount1; 
	private float decVendorDiscountAmount2; 
	private float decVendorDiscountPercent2; 
	private float decVendorDiscoutPercent1; 

	public void setchrAmount(String chramount) { this.chrAmount = chramount; } 
	public void setchrItemCode(String chritemcode) { this.chrItemCode = chritemcode; } 
	public void setchrItemDescription(String chritemdescription) { this.chrItemDescription = chritemdescription; } 
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrUOMCode(String chruomcode) { this.chrUOMCode = chruomcode; } 
	public void setchrVatTaxRecCode(String chrvattaxreccode) { this.chrVatTaxRecCode = chrvattaxreccode; } 
	public void setdecItemAmount(float decitemamount) { this.decItemAmount = decitemamount; } 
	public void setdecItemAmountAfterTax(float decitemamountaftertax) { this.decItemAmountAfterTax = decitemamountaftertax; } 
	public void setdecItemDiscount(float decitemdiscount) { this.decItemDiscount = decitemdiscount; } 
	public void setdecMarginAmount(float decmarginamount) { this.decMarginAmount = decmarginamount; } 
	public void setdecMarginPercent(float decmarginpercent) { this.decMarginPercent = decmarginpercent; } 
	public void setdecQuantity(float decquantity) { this.decQuantity = decquantity; } 
	public void setdecRate(float decrate) { this.decRate = decrate; } 
	public void setdecSchemeDiscountAmount(float decschemediscountamount) { this.decSchemeDiscountAmount = decschemediscountamount; } 
	public void setdecSchemeDiscountPercent(float decschemediscountpercent) { this.decSchemeDiscountPercent = decschemediscountpercent; } 
	public void setdecVatTaxAmount(float decvattaxamount) { this.decVatTaxAmount = decvattaxamount; } 
	public void setdecVatTaxPercent(float decvattaxpercent) { this.decVatTaxPercent = decvattaxpercent; } 
	public void setdecVendorAmount1(float decvendoramount1) { this.decVendorAmount1 = decvendoramount1; } 
	public void setdecVendorDiscountAmount2(float decvendordiscountamount2) { this.decVendorDiscountAmount2 = decvendordiscountamount2; } 
	public void setdecVendorDiscountPercent2(float decvendordiscountpercent2) { this.decVendorDiscountPercent2 = decvendordiscountpercent2; } 
	public void setdecVendorDiscoutPercent1(float decvendordiscoutpercent1) { this.decVendorDiscoutPercent1 = decvendordiscoutpercent1; } 

	public String getchrAmount() { return this.chrAmount; } 
	public String getchrItemCode() { return this.chrItemCode; } 
	public String getchrItemDescription() { return this.chrItemDescription; } 
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrUOMCode() { return this.chrUOMCode; } 
	public String getchrVatTaxRecCode() { return this.chrVatTaxRecCode; } 
	public float getdecItemAmount() { return this.decItemAmount; } 
	public float getdecItemAmountAfterTax() { return this.decItemAmountAfterTax; } 
	public float getdecItemDiscount() { return this.decItemDiscount; } 
	public float getdecMarginAmount() { return this.decMarginAmount; } 
	public float getdecMarginPercent() { return this.decMarginPercent; } 
	public float getdecQuantity() { return this.decQuantity; } 
	public float getdecRate() { return this.decRate; } 
	public float getdecSchemeDiscountAmount() { return this.decSchemeDiscountAmount; } 
	public float getdecSchemeDiscountPercent() { return this.decSchemeDiscountPercent; } 
	public float getdecVatTaxAmount() { return this.decVatTaxAmount; } 
	public float getdecVatTaxPercent() { return this.decVatTaxPercent; } 
	public float getdecVendorAmount1() { return this.decVendorAmount1; } 
	public float getdecVendorDiscountAmount2() { return this.decVendorDiscountAmount2; } 
	public float getdecVendorDiscountPercent2() { return this.decVendorDiscountPercent2; } 
	public float getdecVendorDiscoutPercent1() { return this.decVendorDiscoutPercent1; } 

}
