package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblsale extends Common {
	public tblsale() {
	}

	private String chrAuthorizationUserOne; 
	private String chrAuthorizationUserThree; 
	private String chrAuthorizationUserTwo; 
//	private String chrCompanyCode; 
	private String chrCustomerCode; 
	private String chrOperatorCode; 
	private String chrRecCode; 
	private String chrStoreCode; 
	private float decDiscountTotal; 
	private float decItemTotal; 
	private float decTotalAmount; 
	private float decVATTaxTotal; 
	private String dtmAuthorizationDateOne; 
	private String dtmAuthorizationDateThree; 
	private String dtmAuthorizationDateTwo; 
	private String dtmBillDate; 
	private int intAuthorizationLevel; 
	private int intStatus; 
	private String vhrBillNo;
	public void setchrAuthorizationUserOne(String chrauthorizationuserone) { this.chrAuthorizationUserOne = chrauthorizationuserone; } 
	public void setchrAuthorizationUserThree(String chrauthorizationuserthree) { this.chrAuthorizationUserThree = chrauthorizationuserthree; } 
	public void setchrAuthorizationUserTwo(String chrauthorizationusertwo) { this.chrAuthorizationUserTwo = chrauthorizationusertwo; } 
//	public void setchrCompanyCode(String chrcompanycode) { this.chrCompanyCode = chrcompanycode; } 
	public void setchrCustomerCode(String chrcustomercode) { this.chrCustomerCode = chrcustomercode; } 
	public void setchrOperatorCode(String chroperatorcode) { this.chrOperatorCode = chroperatorcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreCode(String chrstorecode) { this.chrStoreCode = chrstorecode; } 
	public void setdecDiscountTotal(float decdiscounttotal) { this.decDiscountTotal = decdiscounttotal; } 
	public void setdecItemTotal(float decitemtotal) { this.decItemTotal = decitemtotal; } 
	public void setdecTotalAmount(float dectotalamount) { this.decTotalAmount = dectotalamount; } 
	public void setdecVATTaxTotal(float decvattaxtotal) { this.decVATTaxTotal = decvattaxtotal; } 
	public void setdtmAuthorizationDateOne(String dtmauthorizationdateone) { this.dtmAuthorizationDateOne = dtmauthorizationdateone; } 
	public void setdtmAuthorizationDateThree(String dtmauthorizationdatethree) { this.dtmAuthorizationDateThree = dtmauthorizationdatethree; } 
	public void setdtmAuthorizationDateTwo(String dtmauthorizationdatetwo) { this.dtmAuthorizationDateTwo = dtmauthorizationdatetwo; } 
	public void setdtmBillDate(String dtmbilldate) { this.dtmBillDate = dtmbilldate; } 
	public void setintAuthorizationLevel(int intauthorizationlevel) { this.intAuthorizationLevel = intauthorizationlevel; } 
	public void setintStatus(int intstatus) { this.intStatus = intstatus; } 
	public void setvhrBillNo(String vhrbillno) { this.vhrBillNo = vhrbillno; }
	public String getchrAuthorizationUserOne() { return this.chrAuthorizationUserOne; } 
	public String getchrAuthorizationUserThree() { return this.chrAuthorizationUserThree; } 
	public String getchrAuthorizationUserTwo() { return this.chrAuthorizationUserTwo; } 
//	public String getchrCompanyCode() { return this.chrCompanyCode; } 
	public String getchrCustomerCode() { return this.chrCustomerCode; } 
	public String getchrOperatorCode() { return this.chrOperatorCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreCode() { return this.chrStoreCode; } 
	public float getdecDiscountTotal() { return this.decDiscountTotal; } 
	public float getdecItemTotal() { return this.decItemTotal; } 
	public float getdecTotalAmount() { return this.decTotalAmount; } 
	public float getdecVATTaxTotal() { return this.decVATTaxTotal; } 
	public String getdtmAuthorizationDateOne() { return this.dtmAuthorizationDateOne; } 
	public String getdtmAuthorizationDateThree() { return this.dtmAuthorizationDateThree; } 
	public String getdtmAuthorizationDateTwo() { return this.dtmAuthorizationDateTwo; } 
	public String getdtmBillDate() { return this.dtmBillDate; } 
	public int getintAuthorizationLevel() { return this.intAuthorizationLevel; } 
	public int getintStatus() { return this.intStatus; } 
	public String getvhrBillNo() { return this.vhrBillNo; }
}
