package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblsalesitemdetail extends Common {
	public tblsalesitemdetail() {
	}

	private String chrItemCode; 
	private String chrParentCode; 
	private String chrRecCode; 
	private String chrVatTaxRecCode; 
	private float decCustomerDiscount; 
	private float decCustomerRate; 
	private float decItemCost; 
	private float decMRP; 
	private float decQuantity; 
	private float decVatTaxAmount; 
	private float decVatTaxPercentage; 
	private String dtmExpiryDate; 
	private String dtmManufactureDate; 
	private int intStatus; 
	private String vhrBatchNo; 
	private String vhrDescription; 
	private String vhrOurGeneratedBarCode; 
	private String vhrShortName;
	public void setchrItemCode(String chritemcode) { this.chrItemCode = chritemcode; } 
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrVatTaxRecCode(String chrvattaxreccode) { this.chrVatTaxRecCode = chrvattaxreccode; } 
	public void setdecCustomerDiscount(float deccustomerdiscount) { this.decCustomerDiscount = deccustomerdiscount; } 
	public void setdecCustomerRate(float deccustomerrate) { this.decCustomerRate = deccustomerrate; } 
	public void setdecItemCost(float decitemcost) { this.decItemCost = decitemcost; } 
	public void setdecMRP(float decmrp) { this.decMRP = decmrp; } 
	public void setdecQuantity(float decquantity) { this.decQuantity = decquantity; } 
	public void setdecVatTaxAmount(float decvattaxamount) { this.decVatTaxAmount = decvattaxamount; } 
	public void setdecVatTaxPercentage(float decvattaxpercentage) { this.decVatTaxPercentage = decvattaxpercentage; } 
	public void setdtmExpiryDate(String dtmexpirydate) { this.dtmExpiryDate = dtmexpirydate; } 
	public void setdtmManufactureDate(String dtmmanufacturedate) { this.dtmManufactureDate = dtmmanufacturedate; } 
	public void setintStatus(int intstatus) { this.intStatus = intstatus; } 
	public void setvhrBatchNo(String vhrbatchno) { this.vhrBatchNo = vhrbatchno; } 
	public void setvhrDescription(String vhrdescription) { this.vhrDescription = vhrdescription; } 
	public void setvhrOurGeneratedBarCode(String vhrourgeneratedbarcode) { this.vhrOurGeneratedBarCode = vhrourgeneratedbarcode; } 
	public void setvhrShortName(String vhrshortname) { this.vhrShortName = vhrshortname; }
	public String getchrItemCode() { return this.chrItemCode; } 
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrVatTaxRecCode() { return this.chrVatTaxRecCode; } 
	public float getdecCustomerDiscount() { return this.decCustomerDiscount; } 
	public float getdecCustomerRate() { return this.decCustomerRate; } 
	public float getdecItemCost() { return this.decItemCost; } 
	public float getdecMRP() { return this.decMRP; } 
	public float getdecQuantity() { return this.decQuantity; } 
	public float getdecVatTaxAmount() { return this.decVatTaxAmount; } 
	public float getdecVatTaxPercentage() { return this.decVatTaxPercentage; } 
	public String getdtmExpiryDate() { return this.dtmExpiryDate; } 
	public String getdtmManufactureDate() { return this.dtmManufactureDate; } 
	public int getintStatus() { return this.intStatus; } 
	public String getvhrBatchNo() { return this.vhrBatchNo; } 
	public String getvhrDescription() { return this.vhrDescription; } 
	public String getvhrOurGeneratedBarCode() { return this.vhrOurGeneratedBarCode; } 
	public String getvhrShortName() { return this.vhrShortName; }
}
