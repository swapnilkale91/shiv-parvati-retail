package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblsalespaymentdetail extends Common {
	public tblsalespaymentdetail() {
	}

	private String chrParentCode; 
	private String intPaymentModeCode; 
	private String chrRecCode; 
	private float decAmount; 
	private float decBalance; 
	private float decCashReceived; 
	private String dtmCouponDenomination; 
	private String vhrCardDetails;
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrPaymentModeCode(String intpaymentmodecode) { this.intPaymentModeCode = intpaymentmodecode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setdecAmount(float decamount) { this.decAmount = decamount; } 
	public void setdecBalance(float decbalance) { this.decBalance = decbalance; } 
	public void setdecCashReceived(float deccashreceived) { this.decCashReceived = deccashreceived; } 
	public void setdtmCouponDenomination(String dtmcoupondenomination) { this.dtmCouponDenomination = dtmcoupondenomination; } 
	public void setvhrCardDetails(String vhrcarddetails) { this.vhrCardDetails = vhrcarddetails; }
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getintPaymentModeCode() { return this.intPaymentModeCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public float getdecAmount() { return this.decAmount; } 
	public float getdecBalance() { return this.decBalance; } 
	public float getdecCashReceived() { return this.decCashReceived; } 
	public String getdtmCouponDenomination() { return this.dtmCouponDenomination; } 
	public String getvhrCardDetails() { return this.vhrCardDetails; }
}
