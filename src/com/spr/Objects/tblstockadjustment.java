package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblstockadjustment extends Common {
	public tblstockadjustment() {
	}

	private String chrAuthorizationUserOne; 
	private String chrAuthorizationUserThree; 
	private String chrAuthorizationUserTwo; 
//	private String chrCompanyCode; 
	private String chrRecCode; 
	private String chrStoreCode; 
	private String dtmAdjustmentDate; 
	private String dtmAuthorizationDateOne; 
	private String dtmAuthorizationDateThree; 
	private String dtmAuthorizationDateTwo; 
	private int intAuthorizationLevel; 
	private int intStatus; 
	private String vhrAdjustmentNo;
	public void setchrAuthorizationUserOne(String chrauthorizationuserone) { this.chrAuthorizationUserOne = chrauthorizationuserone; } 
	public void setchrAuthorizationUserThree(String chrauthorizationuserthree) { this.chrAuthorizationUserThree = chrauthorizationuserthree; } 
	public void setchrAuthorizationUserTwo(String chrauthorizationusertwo) { this.chrAuthorizationUserTwo = chrauthorizationusertwo; } 
//	public void setchrCompanyCode(String chrcompanycode) { this.chrCompanyCode = chrcompanycode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreCode(String chrstorecode) { this.chrStoreCode = chrstorecode; } 
	public void setdtmAdjustmentDate(String dtmadjustmentdate) { this.dtmAdjustmentDate = dtmadjustmentdate; } 
	public void setdtmAuthorizationDateOne(String dtmauthorizationdateone) { this.dtmAuthorizationDateOne = dtmauthorizationdateone; } 
	public void setdtmAuthorizationDateThree(String dtmauthorizationdatethree) { this.dtmAuthorizationDateThree = dtmauthorizationdatethree; } 
	public void setdtmAuthorizationDateTwo(String dtmauthorizationdatetwo) { this.dtmAuthorizationDateTwo = dtmauthorizationdatetwo; } 
	public void setintAuthorizationLevel(int intauthorizationlevel) { this.intAuthorizationLevel = intauthorizationlevel; } 
	public void setintStatus(int intstatus) { this.intStatus = intstatus; } 
	public void setvhrAdjustmentNo(String vhradjustmentno) { this.vhrAdjustmentNo = vhradjustmentno; }
	public String getchrAuthorizationUserOne() { return this.chrAuthorizationUserOne; } 
	public String getchrAuthorizationUserThree() { return this.chrAuthorizationUserThree; } 
	public String getchrAuthorizationUserTwo() { return this.chrAuthorizationUserTwo; } 
//	public String getchrCompanyCode() { return this.chrCompanyCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreCode() { return this.chrStoreCode; } 
	public String getdtmAdjustmentDate() { return this.dtmAdjustmentDate; } 
	public String getdtmAuthorizationDateOne() { return this.dtmAuthorizationDateOne; } 
	public String getdtmAuthorizationDateThree() { return this.dtmAuthorizationDateThree; } 
	public String getdtmAuthorizationDateTwo() { return this.dtmAuthorizationDateTwo; } 
	public int getintAuthorizationLevel() { return this.intAuthorizationLevel; } 
	public int getintStatus() { return this.intStatus; } 
	public String getvhrAdjustmentNo() { return this.vhrAdjustmentNo; }
}
