package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblstockadjustmentitemdetail extends Common {
	public tblstockadjustmentitemdetail() {
	}

	private String chrItemCode; 
	private String chrParentCode; 
	private String chrRecCode; 
	private float decQuantity; 
	private int intAdjustmentType; 
	private int intStatus; 
	private String vhrDescription;
	public void setchrItemCode(String chritemcode) { this.chrItemCode = chritemcode; } 
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setdecQuantity(float decquantity) { this.decQuantity = decquantity; } 
	public void setintAdjustmentType(int intadjustmenttype) { this.intAdjustmentType = intadjustmenttype; } 
	public void setintStatus(int intstatus) { this.intStatus = intstatus; } 
	public void setvhrDescription(String vhrdescription) { this.vhrDescription = vhrdescription; }
	public String getchrItemCode() { return this.chrItemCode; } 
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public float getdecQuantity() { return this.decQuantity; } 
	public int getintAdjustmentType() { return this.intAdjustmentType; } 
	public int getintStatus() { return this.intStatus; } 
	public String getvhrDescription() { return this.vhrDescription; }
}
