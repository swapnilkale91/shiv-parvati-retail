package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblstoreitemrack extends Common {
	public tblstoreitemrack() {
	}

	private String chrParentCode; 
	private String chrRecCode; 
	private int intStoreLocationCategory; 
	private String vhrCode; 
	private String vhrName;
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setintStoreLocationCategory(int intstorelocationcategory) { this.intStoreLocationCategory = intstorelocationcategory; } 
	public void setvhrCode(String vhrcode) { this.vhrCode = vhrcode; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; }
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public int getintStoreLocationCategory() { return this.intStoreLocationCategory; } 
	public String getvhrCode() { return this.vhrCode; } 
	public String getvhrName() { return this.vhrName; }
}
