package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tbltaxdetail extends Common {
	public tbltaxdetail() {
	}

	private String chrParentCode; 
	private String chrRecCode; 
	private float decPercentage; 
	private String dtmEffectiveDate; 
	private String vhrName;
	public void setchrParentCode(String chrparentcode) { this.chrParentCode = chrparentcode; } 
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setdecPercentage(float decpercentage) { this.decPercentage = decpercentage; } 
	public void setdtmEffectiveDate(String dtmeffectivedate) { this.dtmEffectiveDate = dtmeffectivedate; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; }
	public String getchrParentCode() { return this.chrParentCode; } 
	public String getchrRecCode() { return this.chrRecCode; } 
	public float getdecPercentage() { return this.decPercentage; } 
	public String getdtmEffectiveDate() { return this.dtmEffectiveDate; } 
	public String getvhrName() { return this.vhrName; }
}
