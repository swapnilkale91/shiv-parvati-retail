package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tbltaxmaster extends Common {
	public tbltaxmaster() {
	}

	private String chrRecCode; 
	private String vhrCode; 
	private String vhrName;
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setvhrCode(String vhrcode) { this.vhrCode = vhrcode; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; }
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getvhrCode() { return this.vhrCode; } 
	public String getvhrName() { return this.vhrName; }
}
