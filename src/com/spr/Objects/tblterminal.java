package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblterminal extends Common {
	public tblterminal() {
	}

	private String chrRecCode; 
	private String chrStoreCode; 
	private String chrTerminalCode; 
	private int tinStatus;
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setchrStoreCode(String chrstorecode) { this.chrStoreCode = chrstorecode; } 
	public void setchrTerminalCode(String chrterminalcode) { this.chrTerminalCode = chrterminalcode; } 
	public void settinStatus(int tinstatus) { this.tinStatus = tinstatus; }
	public String getchrRecCode() { return this.chrRecCode; } 
	public String getchrStoreCode() { return this.chrStoreCode; } 
	public String getchrTerminalCode() { return this.chrTerminalCode; } 
	public int gettinStatus() { return this.tinStatus; }
}
