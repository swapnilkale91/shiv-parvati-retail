package com.spr.Objects;

import com.heptanesia.Objects.Common;

public class tblvendormaster extends Common {
	public tblvendormaster() {
	}

	private String chrRecCode; 
	private int intPinCode; 
	private String vhrAddress1; 
	private String vhrAddress2; 
	private String vhrAddress3; 
	private String vhrAddress4; 
	private String vhrCode; 
	private String vhrCSTNo; 
	private String vhrName; 
	private String vhrPANNo; 
	private String vhrServiceTaxNo; 
	private String vhrTel1; 
	private String vhrTel2; 
	private String vhrVATNo;
	public void setchrRecCode(String chrreccode) { this.chrRecCode = chrreccode; } 
	public void setintPinCode(int intpincode) { this.intPinCode = intpincode; } 
	public void setvhrAddress1(String vhraddress1) { this.vhrAddress1 = vhraddress1; } 
	public void setvhrAddress2(String vhraddress2) { this.vhrAddress2 = vhraddress2; } 
	public void setvhrAddress3(String vhraddress3) { this.vhrAddress3 = vhraddress3; } 
	public void setvhrAddress4(String vhraddress4) { this.vhrAddress4 = vhraddress4; } 
	public void setvhrCode(String vhrcode) { this.vhrCode = vhrcode; } 
	public void setvhrCSTNo(String vhrcstno) { this.vhrCSTNo = vhrcstno; } 
	public void setvhrName(String vhrname) { this.vhrName = vhrname; } 
	public void setvhrPANNo(String vhrpanno) { this.vhrPANNo = vhrpanno; } 
	public void setvhrServiceTaxNo(String vhrservicetaxno) { this.vhrServiceTaxNo = vhrservicetaxno; } 
	public void setvhrTel1(String vhrtel1) { this.vhrTel1 = vhrtel1; } 
	public void setvhrTel2(String vhrtel2) { this.vhrTel2 = vhrtel2; } 
	public void setvhrVATNo(String vhrvatno) { this.vhrVATNo = vhrvatno; }
	public String getchrRecCode() { return this.chrRecCode; } 
	public int getintPinCode() { return this.intPinCode; } 
	public String getvhrAddress1() { return this.vhrAddress1; } 
	public String getvhrAddress2() { return this.vhrAddress2; } 
	public String getvhrAddress3() { return this.vhrAddress3; } 
	public String getvhrAddress4() { return this.vhrAddress4; } 
	public String getvhrCode() { return this.vhrCode; } 
	public String getvhrCSTNo() { return this.vhrCSTNo; } 
	public String getvhrName() { return this.vhrName; } 
	public String getvhrPANNo() { return this.vhrPANNo; } 
	public String getvhrServiceTaxNo() { return this.vhrServiceTaxNo; } 
	public String getvhrTel1() { return this.vhrTel1; } 
	public String getvhrTel2() { return this.vhrTel2; } 
	public String getvhrVATNo() { return this.vhrVATNo; }
}
