package com.spr.Servlets;

import javax.crypto.Cipher;  
import javax.crypto.SecretKey;  
import javax.crypto.spec.SecretKeySpec;  

/**
 * The encrpytion method
 * @author Dhvani Nagda
 * @version 1.0
 * @category 
 */

public class encrytText {  
    private Cipher ecipher;  
    private Cipher dcipher;  
  
    public encrytText(String mykey) {
    	//String mykey ="grahakcrmgrahakc";     //Key length should be 16 characters
        SecretKey key = new SecretKeySpec(mykey.getBytes(), "AES"); 
        try {
			ecipher = Cipher.getInstance("AES");
            dcipher = Cipher.getInstance("AES");  
            ecipher.init(Cipher.ENCRYPT_MODE, key);  
            dcipher.init(Cipher.DECRYPT_MODE, key);  
	    } catch (Exception e) {
			e.printStackTrace();
		}  
    }  
  
    public String encrypt(String str) {  
        try {  
            byte[] utf8 = str.getBytes("UTF-8");  
            byte[] enc = ecipher.doFinal(utf8);  
              
            return new sun.misc.BASE64Encoder().encode(enc);  
        } catch (Exception e) {  
        	e.printStackTrace();
        }  
        return null;  
    }  
  
    public String decrypt(String str) {  
        try {  
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);  
  
            byte[] utf8 = dcipher.doFinal(dec);  
  
            return new String(utf8, "UTF-8");  
        } catch (Exception e) {  
        	e.printStackTrace();
        }  
        return null;  
    }  
    
//    public static void main(String[] args) {  
//        encrpytText encrypter = new encrpytText();  
//        
//        //Encrpytion
//        String encryptText = "admin";  
//        System.out.println("Before Encryption : " + encryptText);  
//        String encrypted = encrypter.encrypt(encryptText);  
//        System.out.println("After Encryption : " + encrypted); 
//        
//        //Decryption
//        String decryptText = "pRwwRMmoKZy4xSQ+o6CleA==";  
//        System.out.println("Before Decryption : " + decryptText);  
//        String decrypted = encrypter.decrypt(decryptText);  
//        System.out.println("After Decryption : " + decrypted);  
//    }
}  