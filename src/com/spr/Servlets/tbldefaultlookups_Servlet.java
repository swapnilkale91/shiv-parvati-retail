package com.spr.Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tbldefaultlookups_Servlet
 */
@WebServlet("/tbldefaultlookups_Servlet")
public class tbldefaultlookups_Servlet extends com.spr.BasicServlets.tbldefaultlookups_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tbldefaultlookups tbldefaultlookupsBL = new com.spr.BusinessRules.tbldefaultlookups();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tbldefaultlookups_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tbldefaultlookups = new com.spr.Data.tbldefaultlookups();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tbldefaultlookups.gettbldefaultlookupsForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tbldefaultlookups.gettbldefaultlookupsCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tbldefaultlookups.gettbldefaultlookupsForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tbldefaultlookups.gettbldefaultlookupsCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("LookupTypeCode")) {
				
				int LookupTypeCode = Integer.parseInt(request.getParameter("LookupTypeCode"));
				returnValue = this.tbldefaultlookups.getDefaultLookUpForLookTypeCodeJSON(LookupTypeCode);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tbldefaultlookups = new com.spr.Data.tbldefaultlookups();
			response.setContentType("application/json");
			String record = request.getParameter("Record");
			String callType = request.getParameter("Type");

			if (callType.equals("post")) {
	//			this.message = this.tbldefaultlookupsBL.validateInsertUpdateJSON(record);
			}
			else if (callType.equals("delete")) {
				//this.message = this.tbldefaultlookupsBL.validateDeleteJSON(record);
				this.message = new Message();
			}
	
			if (this.message.getSuccess() == true) {
				super.doPost(request, response);
			}
			else {
				response.getWriter().print(this.tbldefaultlookups.messageToJSON(this.message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
