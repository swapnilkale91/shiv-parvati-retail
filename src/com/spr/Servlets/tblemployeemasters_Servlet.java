package com.spr.Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.heptanesia.Data.Common;
import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblemployeemasters_Servlet
 */
@WebServlet("/tblemployeemasters_Servlet")
public class tblemployeemasters_Servlet extends com.spr.BasicServlets.tblemployeemasters_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.ret.BusinessRules.tblemployeemasters tblemployeemastersBL = new com.ret.BusinessRules.tblemployeemasters();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblemployeemasters_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*String UserName = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(UserName.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}*/

		try {
			this.tblemployeemasters = new com.spr.Data.tblemployeemasters();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			
			if (callType.equals("login")) {
				
				String vhrUserName = request.getParameter("vhrUserName");
				String vhrPassword = request.getParameter("vhrPassword");
				System.out.println("Login : vhrUserName :" + vhrUserName+ "vhrPassword :" + vhrPassword);
				
				String encryptKey = "HeptanesiaRetail"; 
				
				com.spr.Servlets.encrytText en = new com.spr.Servlets.encrytText(encryptKey);
				String Password = en.encrypt(vhrPassword);
				System.out.println("vhrPassword : " + Password);
			
				returnValue = tblemployeemasters.gettblemployeemastersForvhrEmailAndvhrPasswordJSON(vhrUserName, Password);
				
				System.out.println("1");
				if(!returnValue.equalsIgnoreCase("[]"))
				{
					
					ArrayList<com.spr.Objects.tblemployeemaster> usermaster = this.tblemployeemasters.JSONSetToClass(returnValue);
				//	String LoginHistoryCode = UUID.randomUUID().toString();
					com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
					Connection localConnection = dataCommon.getConnection();
					
					//com.erp.Data.pns.crmusergroupmappings datacrmusergroupmappings = new com.erp.Data.pns.crmusergroupmappings(Common.getDomainName(request));
					//ArrayList<String> modules = datacrmusergroupmappings.getcrmusergroupmappingsForchrUserCodeJSON(localConnection, usermaster.get(0).getchrRecCode());
				
					HttpSession session = request.getSession();
					session.setAttribute("chrRecCode", usermaster.get(0).getchrRecCode());
					session.setAttribute("vhrName", usermaster.get(0).getvhrName());
					session.setAttribute("chrCompanyCode", usermaster.get(0).getchrCompanyCode());
				//	session.setAttribute("LoginHistoryCode", LoginHistoryCode);
					session.setAttribute("vhrUserName", usermaster.get(0).getvhrUserName());
					session.setAttribute("chrStoreCode", usermaster.get(0).getchrStoreCode());
					//session.setAttribute("UserRole", usermaster.get(0).getUserRole());
				//	session.setAttribute("ManagerUser", usermaster.get(0).getManagerUser());
					//session.setAttribute("ModuleAccess", modules);
					
				/*	com.erp.Data.pns.tblLoginHistories datatblLoginHistories = new com.erp.Data.pns.tblLoginHistories(localConnection);
					com.erp.Objects.pns.tblLoginHistory otblLoginHistory = new com.erp.Objects.pns.tblLoginHistory();*/
					
					/*otblLoginHistory.setchrRecCode(LoginHistoryCode);
					otblLoginHistory.setchrUserCode(usermaster.get(0).getRecCode());
					otblLoginHistory.setdtmLogInTime(dataCommon.getCurrentDateTime());
					//otblLoginHistory.setdteLogoutTime(null);
					otblLoginHistory.settinIsDirty((byte)1);
					otblLoginHistory.settinActiveFlag((byte)1);
					otblLoginHistory.setIUD((short)1);
					datatblLoginHistories.inserttblLoginHistory(localConnection, otblLoginHistory);
					dataCommon.closeConnection(localConnection);*/
				}
				response.getWriter().print(returnValue);
				return;
			}		
			else if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblemployeemasters.gettblemployeemastersForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tblemployeemasters.gettblemployeemastersCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tblemployeemasters.gettblemployeemastersForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tblemployeemasters.gettblemployeemastersCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			
			
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tblemployeemasters = new com.spr.Data.tblemployeemasters();
			response.setContentType("application/json");
			String record = request.getParameter("Record");
			String callType = request.getParameter("Type");

			if (callType.equals("post")) {
				//this.message = this.tblemployeemastersBL.validateInsertUpdateJSON(record);
			}
			else if (callType.equals("delete")) {
				//this.message = this.tblemployeemastersBL.validateDeleteJSON(record);
				this.message = new Message();
			}
	
			if (this.message.getSuccess() == true) {
				super.doPost(request, response);
			}
			else {
				response.getWriter().print(this.tblemployeemasters.messageToJSON(this.message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
