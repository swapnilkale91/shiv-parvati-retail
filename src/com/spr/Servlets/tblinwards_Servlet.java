package com.spr.Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.heptanesia.Objects.Message;
import com.spr.Objects.tblinward;
import com.spr.Objects.tblinwarditemdetail;

/**
 * Servlet implementation class tblinwards_Servlet
 */
@WebServlet("/tblinwards_Servlet")
public class tblinwards_Servlet extends com.spr.BasicServlets.tblinwards_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tblinwards tblinwardsBL = new com.spr.BusinessRules.tblinwards();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblinwards_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tblinwards = new com.spr.Data.tblinwards();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblinwards.gettblinwardsForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tblinwards.gettblinwardsCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tblinwards.gettblinwardsForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tblinwards.gettblinwardsCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tblinwards = new com.spr.Data.tblinwards();
			response.setContentType("application/json");		
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			String callType = request.getParameter("Type");
			String ipAddress = request.getRemoteAddr();
			Message message = new Message();
		//	String record = request.getParameter("Record");
			boolean success = true;

			if (callType.equals("post")) {
	//			this.message = this.tblinwardsBL.validateInsertUpdateJSON(record);
			}
			else if (callType.equals("postDetails")) {
				String inwarddetails = request.getParameter("inwarddetails");
				String inward = request.getParameter("inward");
				Gson gson = new Gson();
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				
				com.spr.Data.tblinwards datatblinward = new com.spr.Data.tblinwards(localConnection);
				com.spr.Data.tblinwarditemdetails datatblinwarddetail = new com.spr.Data.tblinwarditemdetails(localConnection); // use same connection
				
				ArrayList<tblinwarditemdetail> atblinwarddetail = datatblinwarddetail.JSONSetToClass(inwarddetails);
				
				//Validation
			//	message = this.tblasnsBL.validateInsertUpdateJSON(inward);
				
			//	if (message.getSuccess() == true) {
					
					try {
						localConnection.setAutoCommit(false);
						
						//Add in ASN
						if(success)
						{
							success = datatblinward.insertUpdatetblinward(localConnection, inward, userRecCode, ipAddress);
						//	com.spr.BusinessRules.tblasndetails tblasndetailsBL = new com.wms.BusinessRules.tblasndetails();
							if (message.getSuccess() == true) {
								for(tblinwarditemdetail ca: atblinwarddetail) {
							//		message = tblasndetailsBL.validateInsertUpdateJSON(gson.toJson(ca));
									if (message.getSuccess() == true){								
										success = datatblinwarddetail.insertUpdatetblinwarditemdetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
										if (success == false)
											break;
									} else {
										success = false;
										break;
									}
								}
							}
							else
								success = false;
						}
						
						if(success)
							localConnection.commit();
						else
						{
							localConnection.rollback();
							//message = new Message("SAVE", "Error in save.", false);
						}
						localConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						message = new Message("SAVE", "Error in save.", false);
					}		
		//		}				
				response.getWriter().print(datatblinwarddetail.messageToJSON(message));
				return;
			}
			else if (callType.equals("delete")) {
				//this.message = this.tblinwardsBL.validateDeleteJSON(record);
				this.message = new Message();
			}
	
			if (this.message.getSuccess() == true) {
				super.doPost(request, response);
			}
			else {
				response.getWriter().print(this.tblinwards.messageToJSON(this.message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
