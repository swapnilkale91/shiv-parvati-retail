package com.spr.Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblpurchaseorderitemdeliverydetails_Servlet
 */
@WebServlet("/tblpurchaseorderitemdeliverydetails_Servlet")
public class tblpurchaseorderitemdeliverydetails_Servlet extends com.spr.BasicServlets.tblpurchaseorderitemdeliverydetails_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tblpurchaseorderitemdeliverydetails tblpurchaseorderitemdeliverydetailsBL = new com.spr.BusinessRules.tblpurchaseorderitemdeliverydetails();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblpurchaseorderitemdeliverydetails_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tblpurchaseorderitemdeliverydetails = new com.spr.Data.tblpurchaseorderitemdeliverydetails();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblpurchaseorderitemdeliverydetails.gettblpurchaseorderitemdeliverydetailsForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tblpurchaseorderitemdeliverydetails.gettblpurchaseorderitemdeliverydetailsCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tblpurchaseorderitemdeliverydetails.gettblpurchaseorderitemdeliverydetailsForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tblpurchaseorderitemdeliverydetails.gettblpurchaseorderitemdeliverydetailsCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("purchaseorderitemdeliverydetail")) {
				String chrParentCode = request.getParameter("chrParentCode");
				returnValue = this.tblpurchaseorderitemdeliverydetails.gettblpurchaseorderitemdeliverydetailsCodeJSON(chrParentCode);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tblpurchaseorderitemdeliverydetails = new com.spr.Data.tblpurchaseorderitemdeliverydetails();
			response.setContentType("application/json");
			String record = request.getParameter("Record");
			String callType = request.getParameter("Type");

			if (callType.equals("post")) {
//				this.message = this.tblpurchaseorderitemdeliverydetailsBL.validateInsertUpdateJSON(record);
			}
			else if (callType.equals("delete")) {
				//this.message = this.tblpurchaseorderitemdeliverydetailsBL.validateDeleteJSON(record);
				this.message = new Message();
			}
	
			if (this.message.getSuccess() == true) {
				super.doPost(request, response);
			}
			else {
				response.getWriter().print(this.tblpurchaseorderitemdeliverydetails.messageToJSON(this.message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
