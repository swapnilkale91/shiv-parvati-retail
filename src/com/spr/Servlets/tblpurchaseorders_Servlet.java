package com.spr.Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spr.Objects.tblpurchaseorder;
import com.spr.Objects.tblpurchaseorderitemdetail;
import com.spr.Objects.tblpurchaseorderitemdeliverydetail;
import com.google.gson.Gson;
import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblpurchaseorders_Servlet
 */
@WebServlet("/tblpurchaseorders_Servlet")
public class tblpurchaseorders_Servlet extends com.spr.BasicServlets.tblpurchaseorders_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tblpurchaseorders tblpurchaseordersBL = new com.spr.BusinessRules.tblpurchaseorders();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblpurchaseorders_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tblpurchaseorders = new com.spr.Data.tblpurchaseorders();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblpurchaseorders.gettblpurchaseordersForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tblpurchaseorders.gettblpurchaseordersCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			if (callType.equals("combobox")) {
				returnValue = this.tblpurchaseorders.gettblpurchaseordersForComboBox();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tblpurchaseorders.gettblpurchaseordersForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tblpurchaseorders.gettblpurchaseordersCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tblpurchaseorders = new com.spr.Data.tblpurchaseorders();
			response.setContentType("application/json");
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			String callType = request.getParameter("Type");
			String ipAddress = request.getRemoteAddr();
		/*	String companyCode = request.getSession().getAttribute("CompanyCode").toString();
			System.out.println("doPost companyCode ="+companyCode);*/
			Message message = new Message();
			boolean success = false;

			if (callType.equals("postDetails")) {
				String PurchaseOderItemDeliveryDetails = request.getParameter("PurchaseOderItemDeliveryDetails");
				String PurchaseOderItemDetails = request.getParameter("PurchaseOderItemDetails");
				String PurchaseOrder = request.getParameter("PurchaseOrder");
//				String DeletePurchaseOderItemDetails = request.getParameter("DeletePurchaseOderItemDetails");
//				System.out.println("DeletePurchaseOderItemDetails = "+DeletePurchaseOderItemDetails);
//				String Deletetblpurchaseorderitemdeliverydetails = request.getParameter("Deletetblpurchaseorderitemdeliverydetails");
///				System.out.println("Deletetblpurchaseorderitemdeliverydetails = "+Deletetblpurchaseorderitemdeliverydetails);
				Gson gson = new Gson();
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				
				com.spr.Data.tblpurchaseorders datatblpurchaseorders = new com.spr.Data.tblpurchaseorders(localConnection);
				com.spr.Data.tblpurchaseorderitemdetails datatblpurchaseorderitemdetails = new com.spr.Data.tblpurchaseorderitemdetails(localConnection); // use same connection
				com.spr.Data.tblpurchaseorderitemdeliverydetails datatblpurchaseorderitemdeliverydetails = new com.spr.Data.tblpurchaseorderitemdeliverydetails(localConnection); // use same connection
				com.spr.Data.tblpurchaseorderitemdetails datadeletePurchaseOderItemDetails = new com.spr.Data.tblpurchaseorderitemdetails(localConnection); // use same connection
				com.spr.Data.tblpurchaseorderitemdeliverydetails datadeletetblpurchaseorderitemdeliverydetails = new com.spr.Data.tblpurchaseorderitemdeliverydetails(localConnection); // use same connection
				
				ArrayList<tblpurchaseorderitemdetail> atblpurchaseorderitemdetail = datatblpurchaseorderitemdetails.JSONSetToClass(PurchaseOderItemDetails);
				ArrayList<tblpurchaseorderitemdeliverydetail> atblpurchaseorderitemdeliverydetail = datatblpurchaseorderitemdeliverydetails.JSONSetToClass(PurchaseOderItemDeliveryDetails);
				
				//delete vendor
	//			ArrayList<tblpurchaseorderitemdetail> adeleteseamindentificationdetails = datadeletePurchaseOderItemDetails.JSONSetToClass(DeletePurchaseOderItemDetails);
	//			ArrayList<tblpurchaseorderitemdeliverydetail> adeletereportdetails = datadeletetblpurchaseorderitemdeliverydetails.JSONSetToClass(Deletetblpurchaseorderitemdeliverydetails);

				
				//delete Vendor details
	/*			if(DeletePurchaseOderItemDetails != ""){
					for(tblpurchaseorderitemdetail ca1:adeleteseamindentificationdetails){
						
						success = datadeletePurchaseOderItemDetails.deletepurchaseorderitemdeliverydetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						System.out.println("after datadeletePurchaseOderItemDetails success="+success);
						if(success == true){
							//success = adeleteseamindentificationdetails.deletereportdetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						//	System.out.println("datadeletePurchaseOderItemDetails success ="+ success);
							success = datadeletePurchaseOderItemDetails.deletepurchaseorderitemdetailByRecCodeJSON(localConnection, gson.toJson(ca1));
								if(success == false)
									break;
						}
						else {
							break;
							 }
						}
				}*/
				
	/*			if(Deletetblpurchaseorderitemdeliverydetails != ""){
					for(tblpurchaseorderitemdeliverydetail ca1:adeletereportdetails){
						success = datadeletetblpurchaseorderitemdeliverydetails.deletetblpurchaseorderitemdeliverydetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						if (success == false)
							break;
					}
				}*/
				
				//Validation
				//message = this.tblasnsBL.validateInsertUpdateJSON(asn);
				
				//if (message.getSuccess() == true) {
					try {
						localConnection.setAutoCommit(false);
						
						//Add in ASN
					
						success = datatblpurchaseorders.insertUpdatetblpurchaseorder(localConnection, PurchaseOrder, userRecCode, ipAddress);
						System.out.println("success ="+success);
						//com.wms.BusinessRules.tblasndetails tblasndetailsBL = new com.wms.BusinessRules.tblasndetails();
						
						if (success == true) {
							for(tblpurchaseorderitemdetail ca:atblpurchaseorderitemdetail) {
								//message = tblasndetailsBL.validateInsertUpdateJSON(gson.toJson(ca));
								//if (message.getSuccess() == true){
									System.out.println("inside if success ="+success);
									/*System.out.println("inside if companyCode ="+companyCode);*/
									success = datatblpurchaseorderitemdetails.insertUpdatetblpurchaseorderitemdetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
									if (success == false)
										break;
							}
						}
						if (success) {
							//Add In order Product table
							for(tblpurchaseorderitemdeliverydetail ca:atblpurchaseorderitemdeliverydetail) {
								success = datatblpurchaseorderitemdeliverydetails.insertUpdatetblpurchaseorderitemdeliverydetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
								if (success == false)
									break;
							}
						}
						
						if(success)
							localConnection.commit();
						else
						{
							localConnection.rollback();
							if(message.getSuccess())
								message = new Message("SAVE", "Error in save.", false);
						}
						localConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						message = new Message("SAVE", "Error in save.", false);
					}		
				//}				
				response.getWriter().print(datatblpurchaseorderitemdetails.messageToJSON(message));
				return;
			}
			else if (callType.equals("deletedetails")) {
				String record = request.getParameter("Record");
				tblpurchaseorder r = tblpurchaseorders.JSONToClass(record);
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				try {
					localConnection.setAutoCommit(false);
					
					com.spr.Data.tblpurchaseorderitemdeliverydetails datatblpurchaseorderitemdeliverydetails = new com.spr.Data.tblpurchaseorderitemdeliverydetails(localConnection);
					com.spr.Data.tblpurchaseorderitemdetails datatblpurchaseorderitemdetails = new com.spr.Data.tblpurchaseorderitemdetails(localConnection);
					com.spr.Data.tblpurchaseorders datatblpurchaseorders = new com.spr.Data.tblpurchaseorders(localConnection);
					/*com.spr.Data.tblpurchaseorderitemdetails datadeletePurchaseOderItemDetails = new com.spr.Data.tblpurchaseorderitemdetails(localConnection);
					com.spr.Data.tblpurchaseorderitemdeliverydetails datadeletetblpurchaseorderitemdeliverydetails = new com.spr.Data.tblpurchaseorderitemdeliverydetails(localConnection);*/
					
					success = datatblpurchaseorderitemdeliverydetails.deleteByRecCodeJSON(localConnection, "tblpurchaseorderitemdeliverydetails", "tblpurchaseorderitemdetailsCode IN (Select RecCode From tblpurchaseorderitemdetails where RadiographyCode = '"+r.getchrRecCode()+"')");
					if(success) {
						success = datatblpurchaseorderitemdetails.deleteByRecCodeJSON(localConnection, "tblpurchaseorderitemdetails", "RadiographyCode = '"+r.getchrRecCode()+"'");
					}
					if(success) {
						success = datatblpurchaseorders.delete(localConnection, r.getchrRecCode());
					}
					if(success)
						localConnection.commit();
					else
					{
						localConnection.rollback();
						message = new Message("DELETE", "Record is in used.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				catch (SQLException e) {
					e.printStackTrace();
					message = new Message("DELETE", "Error in Delete.", false);
				}
				response.getWriter().print(this.tblpurchaseorders.messageToJSON(message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}