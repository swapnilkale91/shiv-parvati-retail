package com.spr.Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.heptanesia.Objects.Message;
import com.spr.Objects.tblsale;
import com.spr.Objects.tblsalesitemdetail;
import com.spr.Objects.tblsalespaymentdetail;


/**
 * Servlet implementation class tblsales_Servlet
 */
@WebServlet("/tblsales_Servlet")
public class tblsales_Servlet extends com.spr.BasicServlets.tblsales_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tblsales tblsalesBL = new com.spr.BusinessRules.tblsales();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblsales_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tblsales = new com.spr.Data.tblsales();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblsales.gettblsalesForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tblsales.gettblsalesCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tblsales.gettblsalesForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tblsales.gettblsalesCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tblsales = new com.spr.Data.tblsales();
			response.setContentType("application/json");
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			System.out.println("sale in post servlet userRecCode ="+userRecCode);
			/*String companyCode = request.getSession().getAttribute("chrCompanyCode").toString();
			System.out.println("sale in post servlet companyCode ="+companyCode);
			String storeCode = request.getSession().getAttribute("chrStoreCode").toString();
			System.out.println("sale in post servlet storeCode ="+storeCode);*/
			/*String employeeCode = request.getSession().getAttribute("chrOperatorCode").toString();
			System.out.println("sale in post servlet employeeCode ="+employeeCode);*/
			String callType = request.getParameter("Type");
			String ipAddress = request.getRemoteAddr();
		/*	String companyCode = request.getSession().getAttribute("CompanyCode").toString();
			System.out.println("doPost companyCode ="+companyCode);*/
			Message message = new Message();
			boolean success = false;

			if (callType.equals("postDetails")) {
				String Sales = request.getParameter("Sales");
				String SaleItemDetails = request.getParameter("SaleItemDetails");
				String SalePaymentDetails = request.getParameter("SalePaymentDetails");
//				String DeletePurchaseOderItemDetails = request.getParameter("DeletePurchaseOderItemDetails");
//				System.out.println("DeletePurchaseOderItemDetails = "+DeletePurchaseOderItemDetails);
//				String Deletetblsalespaymentdetails = request.getParameter("Deletetblsalespaymentdetails");
///				System.out.println("Deletetblsalespaymentdetails = "+Deletetblsalespaymentdetails);
				Gson gson = new Gson();
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				
				com.spr.Data.tblsales datatblsales = new com.spr.Data.tblsales(localConnection);
				com.spr.Data.tblsalesitemdetails datatblsalesitemdetails = new com.spr.Data.tblsalesitemdetails(localConnection); // use same connection
				com.spr.Data.tblsalespaymentdetails datatblsalespaymentdetails = new com.spr.Data.tblsalespaymentdetails(localConnection); // use same connection
				com.spr.Data.tblsalesitemdetails datadeletePurchaseOderItemDetails = new com.spr.Data.tblsalesitemdetails(localConnection); // use same connection
				com.spr.Data.tblsalespaymentdetails datadeletetblsalespaymentdetails = new com.spr.Data.tblsalespaymentdetails(localConnection); // use same connection
				
				ArrayList<tblsalesitemdetail> atblsalesitemdetail = datatblsalesitemdetails.JSONSetToClass(SaleItemDetails);
				ArrayList<tblsalespaymentdetail> atblsalespaymentdetail = datatblsalespaymentdetails.JSONSetToClass(SalePaymentDetails);
				
				//delete vendor
	//			ArrayList<tblsaleitemdetail> adeleteseamindentificationdetails = datadeletePurchaseOderItemDetails.JSONSetToClass(DeletePurchaseOderItemDetails);
	//			ArrayList<tblsaleitemdeliverydetail> adeletereportdetails = datadeletetblsalespaymentdetails.JSONSetToClass(Deletetblsalespaymentdetails);

				
				//delete Vendor details
	/*			if(DeletePurchaseOderItemDetails != ""){
					for(tblsaleitemdetail ca1:adeleteseamindentificationdetails){
						
						success = datadeletePurchaseOderItemDetails.deletepurchaseorderitemdeliverydetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						System.out.println("after datadeletePurchaseOderItemDetails success="+success);
						if(success == true){
							//success = adeleteseamindentificationdetails.deletereportdetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						//	System.out.println("datadeletePurchaseOderItemDetails success ="+ success);
							success = datadeletePurchaseOderItemDetails.deletepurchaseorderitemdetailByRecCodeJSON(localConnection, gson.toJson(ca1));
								if(success == false)
									break;
						}
						else {
							break;
							 }
						}
				}*/
				
	/*			if(Deletetblsalespaymentdetails != ""){
					for(tblsaleitemdeliverydetail ca1:adeletereportdetails){
						success = datadeletetblsalespaymentdetails.deletetblsaleitemdeliverydetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						if (success == false)
							break;
					}
				}*/
				
				//Validation
				//message = this.tblasnsBL.validateInsertUpdateJSON(asn);
				
				//if (message.getSuccess() == true) {
					try {
						localConnection.setAutoCommit(false);
						
						//Add in ASN
					
						success = datatblsales.insertUpdatetblsale(localConnection, Sales, userRecCode, ipAddress);
						System.out.println("success ="+success);
						//com.wms.BusinessRules.tblasndetails tblasndetailsBL = new com.wms.BusinessRules.tblasndetails();
						
						if (success == true) {
							for(tblsalesitemdetail ca:atblsalesitemdetail) {
								//message = tblasndetailsBL.validateInsertUpdateJSON(gson.toJson(ca));
								//if (message.getSuccess() == true){
									System.out.println("inside if success ="+success);
									/*System.out.println("inside if companyCode ="+companyCode);*/
									success = datatblsalesitemdetails.insertUpdatetblsalesitemdetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
									if (success == false)
										break;
							}
						}
						if (success) {
							//Add 
							for(tblsalespaymentdetail ca:atblsalespaymentdetail) {
								success = datatblsalespaymentdetails.insertUpdatetblsalespaymentdetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
								if (success == false)
									break;
							}
						}
						
						if(success)
							localConnection.commit();
						else
						{
							localConnection.rollback();
							if(message.getSuccess())
								message = new Message("SAVE", "Error in save.", false);
						}
						localConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						message = new Message("SAVE", "Error in save.", false);
					}		
				//}				
				response.getWriter().print(datatblsalesitemdetails.messageToJSON(message));
				return;
			}
			else if (callType.equals("deletedetails")) {
				String record = request.getParameter("Record");
				tblsale r = tblsales.JSONToClass(record);
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				try {
					localConnection.setAutoCommit(false);
					
					com.spr.Data.tblsalespaymentdetails datatblsalespaymentdetails = new com.spr.Data.tblsalespaymentdetails(localConnection);
					com.spr.Data.tblsalesitemdetails datatblsalesitemdetails = new com.spr.Data.tblsalesitemdetails(localConnection);
					com.spr.Data.tblsales datatblsales = new com.spr.Data.tblsales(localConnection);
					/*com.spr.Data.tblsalesitemdetails datadeletePurchaseOderItemDetails = new com.spr.Data.tblsalesitemdetails(localConnection);
					com.spr.Data.tblsalespaymentdetails datadeletetblsalespaymentdetails = new com.spr.Data.tblsalespaymentdetails(localConnection);*/
					
					success = datatblsalespaymentdetails.deleteByRecCodeJSON(localConnection, "tblsalespaymentdetails", "tblsalesitemdetailsCode IN (Select RecCode From tblsalesitemdetails where RadiographyCode = '"+r.getchrRecCode()+"')");
					if(success) {
						success = datatblsalesitemdetails.deleteByRecCodeJSON(localConnection, "tblsalesitemdetails", "RadiographyCode = '"+r.getchrRecCode()+"'");
					}
					if(success) {
						success = datatblsales.delete(localConnection, r.getchrRecCode());
					}
					if(success)
						localConnection.commit();
					else
					{
						localConnection.rollback();
						message = new Message("DELETE", "Record is in used.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				catch (SQLException e) {
					e.printStackTrace();
					message = new Message("DELETE", "Error in Delete.", false);
				}
				response.getWriter().print(this.tblsales.messageToJSON(message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
