package com.spr.Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.spr.Objects.tblstockadjustment;
import com.spr.Objects.tblstockadjustmentitemdetail;
import com.google.gson.Gson;
import com.heptanesia.Objects.Message;

/**
 * Servlet implementation class tblstockadjustments_Servlet
 */
@WebServlet("/tblstockadjustments_Servlet")
public class tblstockadjustments_Servlet extends com.spr.BasicServlets.tblstockadjustments_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tblstockadjustments tblstockadjustmentsBL = new com.spr.BusinessRules.tblstockadjustments();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tblstockadjustments_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tblstockadjustments = new com.spr.Data.tblstockadjustments();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tblstockadjustments.gettblstockadjustmentsForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tblstockadjustments.gettblstockadjustmentsCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tblstockadjustments.gettblstockadjustmentsForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tblstockadjustments.gettblstockadjustmentsCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("stockdetail")) {
				String chrParentCode = request.getParameter("chrParentCode");
				returnValue = this.tblstockadjustments.gettblstockadjustmentdetailsJSON(chrParentCode);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tblstockadjustments = new com.spr.Data.tblstockadjustments();
			response.setContentType("application/json");
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			String callType = request.getParameter("Type");
			String ipAddress = request.getRemoteAddr();
			Message message = new Message();
			boolean success = false;

			if (callType.equals("postDetails")) {
				String StockAdjustmentDetails = request.getParameter("StockAdjustmentDetails");
				String StockAdjustment = request.getParameter("StockAdjustment");
	//			String deletelhstubedetails = request.getParameter("DeleteLHSTubeDetails");
				Gson gson = new Gson();
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				
				com.spr.Data.tblstockadjustments datatblstockadjustments = new com.spr.Data.tblstockadjustments(localConnection);
				com.spr.Data.tblstockadjustmentitemdetails datatblstockadjustmentitemdetails = new com.spr.Data.tblstockadjustmentitemdetails(localConnection); // use same connection
				com.spr.Data.tblstockadjustmentitemdetails datadeletelhstubedetails = new com.spr.Data.tblstockadjustmentitemdetails(localConnection); // use same connection
				
				ArrayList<tblstockadjustmentitemdetail> atblstockadjustmentsdetail = datatblstockadjustmentitemdetails.JSONSetToClass(StockAdjustmentDetails);
				
				//Delete Vendor Item
	//			ArrayList<tblstockadjustmentitemdetail> adeletelhstubedetails = datadeletelhstubedetails.JSONSetToClass(deletelhstubedetails);
				
				//delete Vendor details
	/*			if(deletelhstubedetails != ""){
					for(tblstockadjustmentitemdetail ca1:adeletelhstubedetails){
						success = datadeletelhstubedetails.deletelhstubedetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						if (success == false)
							break;
					}
				}
			*/	
				//Validation
				//message = this.tblasnsBL.validateInsertUpdateJSON(asn);
				
				//if (message.getSuccess() == true) {
					try {
						localConnection.setAutoCommit(false);
						
						//Add in ASN
						success = datatblstockadjustments.insertUpdatetblstockadjustment(localConnection, StockAdjustment, userRecCode, ipAddress);
						//com.wms.BusinessRules.tblasndetails tblasndetailsBL = new com.wms.BusinessRules.tblasndetails();
						
						if (success == true) {
							for(tblstockadjustmentitemdetail ca:atblstockadjustmentsdetail) {
								//message = tblasndetailsBL.validateInsertUpdateJSON(gson.toJson(ca));
								//if (message.getSuccess() == true){
									success = datatblstockadjustmentitemdetails.insertUpdatetblstockadjustmentitemdetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
									if (success == false)
										break;
//								} else {
//									success = false;
//									break;
//								}
							}
						}
						
						if(success)
							localConnection.commit();
						else
						{
							localConnection.rollback();
							if(message.getSuccess())
								message = new Message("SAVE", "Error in save.", false);
						}
						localConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						message = new Message("SAVE", "Error in save.", false);
					}		
				//}				
				response.getWriter().print(datatblstockadjustmentitemdetails.messageToJSON(message));
				return;
			}
			else if (callType.equals("deletedetails")) {
				String record = request.getParameter("Record");
				tblstockadjustment r = tblstockadjustments.JSONToClass(record);
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				try {
					localConnection.setAutoCommit(false);
					
					com.spr.Data.tblstockadjustmentitemdetails datatblstockadjustmentitemdetails = new com.spr.Data.tblstockadjustmentitemdetails(localConnection);
					com.spr.Data.tblstockadjustments datatblstockadjustments = new com.spr.Data.tblstockadjustments(localConnection);
					
					success = datatblstockadjustmentitemdetails.deleteByRecCodeJSON(localConnection, "tblstockadjustmentitemdetails", "tblstockadjustmentsCode = '"+r.getchrRecCode()+"'");
					if(success) {
						success = datatblstockadjustments.delete(localConnection, r.getchrRecCode());
					}
					if(success)
						localConnection.commit();
					else
					{
						localConnection.rollback();
						message = new Message("DELETE", "Record is in used.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				catch (SQLException e) {
					e.printStackTrace();
					message = new Message("DELETE", "Error in Delete.", false);
				}
				response.getWriter().print(this.tblstockadjustments.messageToJSON(message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
