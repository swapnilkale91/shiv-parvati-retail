package com.spr.Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.heptanesia.Objects.Message;
import com.spr.Objects.tbltaxdetail;
import com.spr.Objects.tbltaxmaster;

/**
 * Servlet implementation class tbltaxmasters_Servlet
 */
@WebServlet("/tbltaxmasters_Servlet")
public class tbltaxmasters_Servlet extends com.spr.BasicServlets.tbltaxmasters_Servlet {
	private static final long serialVersionUID = 1L;
//	protected com.spr.BusinessRules.tbltaxmasters tbltaxmastersBL = new com.spr.BusinessRules.tbltaxmasters();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public tbltaxmasters_Servlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userCode = (String)request.getSession().getAttribute("chrRecCode");		
		try{
			if(userCode.equals(null))
				return;
		} catch(Exception e){
			response.getWriter().print("Error");
			return;
		}

		try {
			this.tbltaxmasters = new com.spr.Data.tbltaxmasters();
			response.setContentType("application/json");
			String callType = request.getParameter("Type");
			String returnValue = "";
			if (callType.equals("page")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				returnValue = this.tbltaxmasters.gettbltaxmastersForPageJSON(offset, pageSize);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("count")) {
				returnValue = this.tbltaxmasters.gettbltaxmastersCountJSON();
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("pagesearch")) {
				int offset = Integer.parseInt(request.getParameter("Offset"));
				int pageSize = Integer.parseInt(request.getParameter("PageSize"));
				String search = request.getParameter("SearchText");
				returnValue = this.tbltaxmasters.gettbltaxmastersForPageSearchJSON(offset, pageSize, search);
				response.getWriter().println(returnValue);
				return;
			}
			else if (callType.equals("countsearch")) {
				String search = request.getParameter("SearchText");
				returnValue = this.tbltaxmasters.gettbltaxmastersCountSearchJSON(search);
				response.getWriter().println(returnValue);
				return;
			}
			else
				super.doGet(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			this.tbltaxmasters = new com.spr.Data.tbltaxmasters();
			response.setContentType("application/json");
			String userRecCode = request.getSession().getAttribute("chrRecCode").toString();
			System.out.println("userRecCode : " + userRecCode);
			String callType = request.getParameter("Type");
			String ipAddress = request.getRemoteAddr();
			Message message = new Message();
			boolean success = false;
			
			if (callType.equals("postDetails")) {
				String DeleteTaxDetail = request.getParameter("DeleteTaxDetail");
				String TaxDetail = request.getParameter("TaxDetail");
				System.out.println("DeleteTaxDetail" + DeleteTaxDetail);
				String TaxMaster = request.getParameter("TaxMaster");
				
				Gson gson = new Gson();
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				
				com.spr.Data.tbltaxmasters datatbltaxmasters = new com.spr.Data.tbltaxmasters(localConnection);
				com.spr.Data.tbltaxdetails datatbltaxdetails = new com.spr.Data.tbltaxdetails(localConnection); // use same connection
				com.spr.Data.tbltaxdetails dataDeletetbltaxdetails = new com.spr.Data.tbltaxdetails(localConnection); // use same connection
				
				ArrayList<tbltaxdetail> atbltaxdetail = datatbltaxdetails.JSONSetToClass(TaxDetail);
				//Delete Alignment Report Detail
				ArrayList<tbltaxdetail> aDeleteAlignmentDetails = dataDeletetbltaxdetails.JSONSetToClass(DeleteTaxDetail);
				
				//delete Vendor details
				if(DeleteTaxDetail != ""){
					for(tbltaxdetail ca1:aDeleteAlignmentDetails){
						success = dataDeletetbltaxdetails.deletetbltaxdetailByRecCodeJSON(localConnection, gson.toJson(ca1));
						if (success == false)
							break;
					}
				}
				
					try {
						localConnection.setAutoCommit(false);
						
						//Add in ASN
						success = datatbltaxmasters.insertUpdatetbltaxmaster(localConnection, TaxMaster, userRecCode, ipAddress);
						//com.wms.BusinessRules.tblasndetails tblasndetailsBL = new com.wms.BusinessRules.tblasndetails();
						
						if (success == true) {
							for(tbltaxdetail ca:atbltaxdetail) {
								//message = tblasndetailsBL.validateInsertUpdateJSON(gson.toJson(ca));
								//if (message.getSuccess() == true){
									success = datatbltaxdetails.insertUpdatetbltaxdetail(localConnection, gson.toJson(ca), userRecCode, ipAddress);
									if (success == false)
										break;
//								} else {
//									success = false;
//									break;
//								}
							}
						}
						
						if(success)
							localConnection.commit();
						else
						{
							localConnection.rollback();
							if(message.getSuccess())
								message = new Message("SAVE", "Error in save.", false);
						}
						localConnection.close();
					} catch (SQLException e) {
						e.printStackTrace();
						message = new Message("SAVE", "Error in save.", false);
					}		
				//}				
				response.getWriter().print(datatbltaxdetails.messageToJSON(message));
				return;
			}
			else if (callType.equals("deletedetails")) {
				String record = request.getParameter("Record");
				tbltaxmaster r = tbltaxmasters.JSONToClass(record);
				
				com.heptanesia.Data.Common dataCommon = new com.heptanesia.Data.Common();
				Connection localConnection = dataCommon.getConnection();
				try {
					localConnection.setAutoCommit(false);
					
					com.spr.Data.tbltaxdetails datatbltaxdetails = new com.spr.Data.tbltaxdetails(localConnection);
					com.spr.Data.tbltaxmasters datatbltaxmasters = new com.spr.Data.tbltaxmasters(localConnection);
					
					success = datatbltaxdetails.deleteByRecCodeJSON(localConnection, "tbltaxdetails", "chrParentCode = '"+r.getchrRecCode()+"'");
					if(success) {
						success = datatbltaxmasters.delete(localConnection, r.getchrRecCode());
					}
					if(success)
						localConnection.commit();
					else
					{
						localConnection.rollback();
						message = new Message("DELETE", "Record is in used.", false);
					}
					dataCommon.closeConnection(localConnection);
				}
				catch (SQLException e) {
					e.printStackTrace();
					message = new Message("DELETE", "Error in Delete.", false);
				}
				response.getWriter().print(this.tbltaxmasters.messageToJSON(message));
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
