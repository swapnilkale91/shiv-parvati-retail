package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class AddButton extends SimpleTagSupport {

	private String databind;
	private String title;
	private String id;
	private String modalid;
	
   
	public void setdatabind(String databind) { this.databind = databind; }
	public void settitle(String title) { this.title = title; }
	public void setid(String id) { this.id = id; }
	public void setmodalid(String modalid) { this.modalid = modalid; }

	public void doTag() throws JspException, IOException {
       if (databind != null) {
    	   String message = "<a href=\"#"+ this.modalid +"\" data-toggle=\"modal\" data-backdrop=\"static\" class=\"btn btn-primary pull-right\"";
    	   if(id != null)
    		   message = message + "id=\""+ this.id +"\"";
    	   if(title != null)
    		   message = message + "title=\""+ this.title +"\"";
    	   message = message + " data-bind=\"click: function() { viewModel."+ this.databind +"(null); }\">"
    	   		+ "<i class=\"fa fa-plus fa-lg add-plus\"></i>Add</a>";
    	   JspWriter out = getJspContext().getOut();
    	   out.println(message);
       }
   }
}

//<a href="#modal-tblwmsvendor" data-toggle="modal" data-backdrop="static" class="btn btn-primary pull-right" data-bind="click: function() { viewModel.addEdittblwmsvendor(null);}">
//<i class="fa fa-plus fa-lg"></i> Add
//</a>