package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class AddEditModal extends SimpleTagSupport {

	private String postfn;
	private String closefn;
	private String modal;
	private String title;
	private String withbind;
	   
	public void setpostfn(String postfn) { this.postfn = postfn; }
	public void setclosefn(String closefn) { this.closefn = closefn; }
	public void setmodal(String modal) {
		this.modal = modal; }
	public void settitle(String title) { this.title = title; }
	public void setwithbind(String withbind) { this.withbind = withbind; }
	   
	StringWriter sw = new StringWriter();		

	public void doTag() throws JspException, IOException {
		if (this.postfn != null && this.closefn != null && this.modal != null && this.title != null) {
			String message = "<form role=\"form\" data-bind=\"submit: function() { viewModel."+ this.postfn +"() }\">";
			message = message + "<div class=\"modal fade\" id=\""+ this.modal +"\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\""+this.modal+"Label\" aria-hidden=\"true\">";
			message = message + "<div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\">";
			message = message + "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" data-bind=\"click: function() { viewModel."+ this.closefn +"(true); }\">";
			message = message + "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
			message = message + "<h4 class=\"modal-title\" id=\""+this.modal+"Label\"> "+ this.title +" </h4></div>";
			
			if(this.withbind != null)
				message = message + "<div class=\"modal-body\" data-bind=\"with: viewModel."+ this.withbind +"\">";
			else
				message = message + "<div class=\"modal-body\">";
			
			//Accessing the Tag Body
			getJspBody().invoke(sw);
			message = message + sw.toString();
			
			message = message + "</div><div class=\"modal-footer\">";
			message = message + "<button type=\"cancel\" class=\"btn btn-default\" data-dismiss=\"modal\" data-bind=\"click: function() { viewModel."+ this.closefn +"(true); }\">Cancel</button>";
			message = message + "<button type=\"submit\" class=\"btn btn-primary\">Save</button>";
			message = message + " </div> </div> </div> </div> </form>";

			JspWriter out = getJspContext().getOut();
			out.println(message);
		}
	}
}

//<form role="form" data-bind="submit: function() { viewModel.postCrmBrand() }">
//<div class="modal fade" id="modal-crmbrand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
//	<div class="modal-dialog">
//		<div class="modal-content">
//			<div class="modal-header">
//				<button type="button" class="close" data-dismiss="modal" data-bind="click: function() { viewModel.closeAddEdit(true); }">
//					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
//				</button>
//				<h4 class="modal-title" id="myModalLabel">Brand</h4>
//			</div>
//			<div class="modal-body" data-bind="with: viewModel.selectedCrmBrand">
//				//Tag body
//			</div>
//			<div class="modal-footer">
//				<button type="cancel" class="btn btn-default" data-dismiss="modal" data-bind="click: function() { viewModel.closeAddEdit(true); }">Cancel</button>
//				<button type="submit" class="btn btn-primary">Save Changes</button>
//			</div>
//		</div>
//	</div>
//</div>
//</form>