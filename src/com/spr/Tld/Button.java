package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;

import java.io.*;

public class Button extends SimpleTagSupport {

   private String databind;
   private String databindoption;
   private String title;
   private String id;
   private String modalid;
   private String icon;
   
   public void setdatabind(String databind) { this.databind = databind; }
   public void setdatabindoption(String databindoption) { this.databindoption = databindoption; }
   public void settitle(String title) { this.title = title; }
   public void setid(String id) { this.id = id; }
   public void setmodalid(String modalid) { this.modalid = modalid; }
   public void seticon(String icon) { this.icon = icon; }

   public void doTag() throws JspException, IOException
    {
       if (databind != null) 
       {
    	   String message = "<a class=\"table-link\""; 
    	   if(modalid != null)
    		   message = message + "href=\"#"+ this.modalid +"\" data-toggle=\"modal\" ";
    	   if(title != null)
    		   message = message + "title=\""+ this.title +"\"";
    	   message = message + "data-bind=\"click: function() { viewModel."+ this.databind +"($data); }";
		   if(databindoption != null)
			   message = message + ", "+ this.databindoption;
	   		message = message  + "\">"
	   		+ "<span class=\"fa-stack\"><i class=\"fa fa-square fa-stack-2x\"></i> "
			+ "<i class=\"fa "+this.icon+" fa-stack-1x fa-inverse\"";
			if(id != null)
    		   message = message + "id=\""+ this.id +"\""; 
			
			message = message +"></i></span></a>";
    	   JspWriter out = getJspContext().getOut();
    	 
    	   out.println(message);
       }
   }
}

//<a href="#modal-delete" data-toggle="modal" data-backdrop="static" class="table-link" title="Delete Department" data-bind="click: function() { viewModel.addEdittblwmsdepartment($data);}"> 
//<span class="fa-stack">	
//<i class="fa fa-square fa-stack-2x"></i> <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
//</span>
//</a>
