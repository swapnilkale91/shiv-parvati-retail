package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;

import java.io.*;

public class DeactivateButton extends SimpleTagSupport {

   private String databind;
   private String databindoption;
   private String title;
   private String id;
   private String modalid;
   
   public void setdatabind(String databind) { this.databind = databind; }
   public void setdatabindoption(String databindoption) { this.databindoption = databindoption; }
   public void settitle(String title) { this.title = title; }
   public void setid(String id) { this.id = id; }
   public void setmodalid(String modalid) { this.modalid = modalid; }

   public void doTag() throws JspException, IOException
    {
       if (databind != null) 
       {
    	   String message = "<a href=\"#"+ this.modalid +"\" data-toggle=\"modal\" class=\"table-link\""; 
    	   if(title != null)
    		   message = message + "title=\""+ this.title +"\"";
    	   message = message + "data-bind=\"click: function() { viewModel."+ this.databind +"($data); },visible : $data.tinActiveFlag() == 0";
		   if(databindoption != null)
			   message = message + ", "+ this.databindoption;
	   		message = message  + "\">"
	   		+ "<span class=\"fa-stack fa-deactivate\"><i class=\"fa fa-square fa-stack-2x\"></i> "
			+ "<i class=\"fa fa-close fa-stack-1x fa-inverse\"";
			if(id != null)
    		   message = message + "id=\""+ this.id +"\""; 
			
			message = message +"></i></span></a>";
    	   JspWriter out = getJspContext().getOut();
    	  
    	   out.println(message);
       }
   }
}

//<a href="#modal-delete" data-toggle="modal" data-backdrop="static" class="table-link" title="Delete Department" data-bind="click: function() { viewModel.addEdittblwmsdepartment($data);}"> 
//<span class="fa-stack">	
//<i class="fa fa-square fa-stack-2x"></i> <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
//</span>
//</a>
