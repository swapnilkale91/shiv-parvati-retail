package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class DeactivateModal extends SimpleTagSupport {

	private String databindfn;
	private String id;
	private String title;
	private String msg;

	public void setdatabindfn(String databindfn) { this.databindfn = databindfn; }
	public void setid(String id) { this.id = id; }
	public void settitle(String title) { this.title = title; }
	public void setmsg(String msg) { this.msg = msg; }

	public void doTag() throws JspException, IOException {
		if (databindfn != null) {
			String message = null;
			if(this.id == null)
				message = "<div class=\"modal fade\" id=\"modal-deactivate\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">";
			else
				message = "<div class=\"modal fade\" id=\""+this.id+"\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">";
			
			message = message + "<div class=\"modal-dialog\">";
			message = message + "<div class=\"modal-content\">";
			message = message + "<div class=\"modal-header\">";
			message = message + "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">";
			message = message + "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>";
			
			if(this.title == null)
				message = message + "<h4 class=\"modal-title\" id=\"myModalLabel\">Deactivate</h4>";
			else
				message = message + "<h4 class=\"modal-title\" id=\"myModalLabel\">"+this.title+"</h4>";
			
			message = message + "</div>";
			
			if(this.msg == null)
				message = message + "<div class=\"modal-body\"><span>Deactivate this record?</span></div>";
			else
				message = message + "<div class=\"modal-body\"><span>"+ this.msg +"</span></div>";	
			
			message = message + "<div class=\"modal-footer\">";
			message = message + "<button type=\"cancel\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>";
			message = message + "<button type=\"submit\" data-dismiss=\"modal\" class=\"btn btn-primary\" data-bind=\"click: function() { viewModel."+ this.databindfn +"(); }\">Ok</button>";
			message = message + "</div></div></div></div>";
			JspWriter out = getJspContext().getOut();
			out.println(message);
		}
	}
}

//<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
//<div class="modal-dialog">
//<div class="modal-content">
//<div class="modal-header">
//<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>";
//<h4 class="modal-title" id="myModalLabel">Delete</h4>
//</div>
//<div class="modal-body"><span>Delete the selected record?</span></div>			
//<div class="modal-footer">
//<button type="cancel" class="btn btn-default" data-dismiss="modal">Cancel</button>
//<button type="submit" data-dismiss="modal" class="btn btn-primary" data-bind="click: function() { viewModel.deleteCrmProductcategory(); }">Ok</button>
//</div></div></div></div>
