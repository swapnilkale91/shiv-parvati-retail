package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class Export extends SimpleTagSupport {

   private String databind;
   
   public void setdatabind(String databind) { this.databind = databind; }

   public void doTag() throws JspException, IOException
    {
	   String message = null;
       if (databind != null) 
    	   message = "<button class=\"btn btn-default\" data-bind=\"click: function() { viewModel."+ this.databind +"(); }\">Export</button>";
       else
    	   message = "<button class=\"btn btn-default\" data-bind=\"click: function() { viewModel.export(); }\">Export</button>";
	   JspWriter out = getJspContext().getOut();
	   out.println(message);
   }
}

//<button class="btn btn-default" data-bind="click: function() { viewModel.export(); }">Export</button>