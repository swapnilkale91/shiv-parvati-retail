package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class InputDatePicker extends SimpleTagSupport {

	private String label;
	private String id;
	private String databind;
	private String placeholder;
	private String required;
	private String datepickerOptions;
	private String visible;
	
	
	public void setlabel(String label) { this.label = label; }

	public void setid(String id) { this.id = id; }
	public void setdatabind(String databind) { this.databind = databind; }
	public void setplaceholder(String placeholder) { this.placeholder = placeholder; }
	public void setrequired(String required) { this.required = required; }
	public void setdatepickerOptions(String datepickerOptions) { this.datepickerOptions = datepickerOptions; }

	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	
	public void doTag() throws JspException, IOException
	{
		String message = "<div class=\"form-group\">";
		
		// Highlights if mandatory
		if(this.required != null)
		{
			if (this.required.equalsIgnoreCase("true"))
				message = "<div class=\"form-group has-error\">";
		}
		
		//<label> tag
		if(this.label != null)
			message = message + "<label for=\""+ this.id +"\">"+ this.label +"</label> ";
		
		//<input> tag
		message = message + "<input class=\"form-control datepicker\"";
		if(this.id != null)
			message = message + "id=\""+ this.id +"\"";
		if(this.databind != null)
		{
			message = message + "data-bind=\"datepicker: "+ this.databind;
			if(this.datepickerOptions == null)
				message = message + ", datepickerOptions: {format: 'dd/mm/yyyy'}";
			else 
				message = message + ", datepickerOptions: "+ this.datepickerOptions;
			
			message = message + "\"";
		}
	
		if(this.required != null)
			message = message + " required=\""+ this.required +"\"";
		if(this.placeholder != null)
			message = message + " placeholder=\""+ this.placeholder +"\"";
		message = message + "></input>";
		
		message = message + "</div>";
		JspWriter out = getJspContext().getOut();
		out.println(message);
	}
}

//<div class="form-group">
//<label for="dteBirthDate">Birth Date</label> 
//<input class="form-control datepicker" id="dteBirthDate" data-bind="datepicker: dteBirthDate, datepickerOptions: {format: 'dd/mm/yyyy'}" placeholder="Select Birth Date"/>
//</div>