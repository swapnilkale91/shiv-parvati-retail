package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class InputGroup extends SimpleTagSupport {

	private String label;
	private String modalid;
	private String databind;

	public void setlabel(String label) { this.label = label; }
	public void setmodalid(String modalid) { this.modalid = modalid; }
	public void setdatabind(String databind) { this.databind = databind; }
	
	StringWriter sw = new StringWriter();
	
	public void doTag() throws JspException, IOException
	{
		String message = "<div class=\"form-group\">";
		if (this.label != null)
			message = message + "<label for=\"chrParentCategory\">"+ this.label +"</label>";
		message = message + "<div class=\"input-group\">";
		
		//Tag Body
		getJspBody().invoke(sw);
		message = message + sw.toString();
		
		message = message + "<span class=\"input-group-btn\">";
		message = message + "<button href=\"#"+ this.modalid +"\" type=\"button\" class=\"btn btn-primary\" data-backdrop=\"static\" data-toggle=\"modal\" data-bind=\"click: function() { viewModel."+ this.databind +"(true); }\"><span class=\"glyphicon glyphicon-th-list\"></span></button>";
		message = message + "</span></div></div>";
		
		JspWriter out = getJspContext().getOut();
		out.println(message);
	}
}

//<div class="form-group">
//<label for="chrParentCategory">Parent Category</label>
//<div class="input-group">
//
// 		//Tag Body
//
//		<span class="input-group-btn">
//  		<button href="#modal-contactcategories" type="button" class="btn btn-default" data-backdrop="static" data-toggle="modal" data-bind="click: function() { viewModel.getCategoryTree(true); }"><span class="glyphicon glyphicon-th-list"></span></button>
//	</span>
//	</div>
//</div>