package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;

import java.io.*;

public class InputPostfix extends SimpleTagSupport {

	private String label;
	private String id;
	private String databind;
	private String databindoption;
	private String placeholder;
	private String required;
	private String maxlength;
	private String onkeypress;
	private String autofocus;
	private String onblur;
	private String type;
	private String disabled;
	private String ariadescribedbyvalue;
	
	public void setlabel(String label) { this.label = label; }
	public void setid(String id) { this.id = id; }
	public void setdatabind(String databind) { this.databind = databind; }
    public void setdatabindoption(String databindoption) { this.databindoption = databindoption; }
	public void setplaceholder(String placeholder) { this.placeholder = placeholder; }
	public void setrequired(String required) { this.required = required; }
	public void setdisabled(String disabled) { this.disabled = disabled; }
	public void setmaxlength(String maxlength) { this.maxlength = maxlength; }
	public void setonkeypress(String onkeypress) { this.onkeypress = onkeypress; }
	public void setautofocus(String autofocus) { this.autofocus = autofocus; }
	public void setonblur(String onblur) { this.onblur = onblur; }
	public void settype(String type) { this.type = type; }
	public void setariadescribedbyvalue(String ariadescribedbyvalue) { this.ariadescribedbyvalue = ariadescribedbyvalue; }
	
	public void doTag() throws JspException, IOException
	{
		String message = "";
		//<label> tag
		if(this.label != null)
		message = "<label>"+ this.label +"</label> ";
			
		// Highlights if mandatory
		if(this.required != null)
		{
			if (this.required.equalsIgnoreCase("true"))
				message = message + "<div class=\"input-group has-error\">";
		}else{
			message = message + "<div class=\"input-group\">";
		}
		
		
		//<input> tag
		message = message + "<input class=\"form-control\"";
		if(this.type != null)
			message = message + " type=\""+ this.type +"\"";
		if(this.id != null)
			message = message + " id=\""+ this.id +"\"";
		if(this.databind != null)
			message = message + " data-bind=\"value: "+ this.databind +"";
		if(databindoption != null)
			   message = message + ", "+ this.databindoption;
		message = message + "\"";
		if(this.required != null)
			message = message + " required=\""+ this.required +"\"";
		if(this.disabled != null)
			message = message + " disabled=\""+ this.disabled +"\"";
		if(this.placeholder != null)
			message = message + " placeholder=\""+ this.placeholder +"\"";
		if(this.maxlength != null)
			message = message + " maxlength=\""+ this.maxlength +"\"";
		if(this.onkeypress != null)
			message = message + " onkeypress=\""+ this.onkeypress +"\"";
		if(this.autofocus != null)
			message = message + " autofocus";
	
			message = message + " aria-describedby= addon";
		if(this.onblur != null)
			message = message + " onblur=\""+ this.onblur +"\"";
		message = message + "></input>";
		if(this.ariadescribedbyvalue != null)
			message = message + " <span class=\"input-group-addon\" id=\"addon\">"+this.ariadescribedbyvalue +"</span>";
		message = message + "</div>";
		
		
		JspWriter out = getJspContext().getOut();
		out.println(message);
	}
	
}

//<div class="form-group">
//<label for="brandName">Brand Name</label> 
//<input type="text" class="form-control" id="brandName" data-bind="value: vhrBrandName" required="true" placeholder="Enter Category Name" maxlength="100">
//</div>