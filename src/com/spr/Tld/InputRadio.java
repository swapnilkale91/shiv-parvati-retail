package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;

import java.io.*;

public class InputRadio extends SimpleTagSupport {

	private String label;
	private String databindoption;
	private String observable;
	private String text;
	private String value;
	private String caption;
	private String bindvalue;
	private String required;
	private String onchange;
	private String id;
	private String disabled;
	private String binddisabled;

	public void setlabel(String label) { this.label = label; }
	public void setdatabindoption(String databindoption) { this.databindoption = databindoption; }
	public void setobservable(String observable) { this.observable = observable; }
	public void settext(String text) { this.text = text; }
	public void setvalue(String value) { this.value = value; }
	public void setcaption(String caption) { this.caption = caption; }
	public void setbindvalue(String bindvalue) { this.bindvalue = bindvalue; }
	public void setrequired(String required) { this.required = required; }
	public void setonchange(String onchange) { this.onchange = onchange; }
	public void setid(String id) { this.id = id; }
	public void setdisabled(String disabled) { this.disabled = disabled; }
	public void setbinddisabled(String binddisabled) { this.binddisabled = binddisabled; }
	
	public void doTag() throws JspException, IOException
	{
		String message = "<div class=\"form-group\">";
		
		// Highlights if mandatory
		if(this.required != null)
		{
			if (this.required.equalsIgnoreCase("true"))
				message = "<div class=\"form-group\">";
		}
		
		//<label> tag
		if(this.label != null)
			message = message + "<label>"+ this.label +"</label> ";
		
		//<select> tag
		message = message + "<select class=\"form-control\" data-bind=\"options: viewModel."+ this.observable +", optionsText: '"+ this.text +"', optionsValue: '"+ this.value +"', optionsCaption: '"+ this.caption +"', value: "+ this.bindvalue +"";
		if(databindoption != null)
 		   message = message + ", "+ this.databindoption;
		if(this.binddisabled != null)
			message = message + ", disable : "+this.binddisabled;	
		message = message + "\"";
		
		if(this.id != null)
			message = message + " id=\""+ this.id +"\"";
		if(this.required != null)
			message = message + " required=\""+ this.required +"\"";
		if(this.onchange != null)
			message = message + " onchange=\""+ this.onchange +"\"";
		if(this.disabled != null)
		{
			if(this.disabled.equalsIgnoreCase("true"))
				message = message + " disabled";
		}
		message = message + "></select>";
		
		message = message + "</div>";
		
		JspWriter out = getJspContext().getOut();
		out.println(message);
	}
}

//<div class="form-group">
//<label for="chrStateCode">State</label> 
//<select class="form-control" data-bind="options: viewModel.states, optionsText: 'vhrState', optionsValue: 'tinStateCode', optionsCaption: 'Choose State', value: chrStateCode" required></select>
//</div>