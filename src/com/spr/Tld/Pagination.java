package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class Pagination extends SimpleTagSupport {

	private String currentpage;
	private String noofpages;
	private String first;
	private String previous;
	private String next;
	private String last;
	   
	public void setcurrentpage(String currentpage) { this.currentpage = currentpage; }
	public void setnoofpages(String noofpages) { this.noofpages = noofpages; }
	public void setfirst(String first) { this.first = first; }
	public void setprevious(String previous) { this.previous = previous; }
	public void setnext(String next) { this.next = next; }
	public void setlast(String last) { this.last = last; }
	
	public void doTag() throws JspException, IOException {
		if (this.noofpages != null && this.currentpage != null) {
			String message = "<div class=\"col-md-12\">";
			message = message + "<span class=\"pull-left paging\">";
			message = message + "<span class=\"label label-primary\" data-bind=\"text: viewModel." + currentpage + "\"></span>";
			message = message + "<span class=\"pagingslash fa-2x\">/</span><span class=\"label label-primary\" data-bind=\"text: viewModel." + noofpages + "\"></span>";
			message = message + "</span>";
			message = message + "<ul class=\"pagination pull-right\" style=\"padding: 0;margin: 0;\">";
			if (this.first == null) 
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel.firstPage(); }\">&laquo;</a></li>";
			else
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel."+ this.first +"(); }\">&laquo;</a></li>";
			if (this.previous == null)
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel.prevPage(); }\">&lt;</a></li>";				
			else
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel."+ this.previous +"(); }\">&lt;</a></li>";
			if (this.next == null)
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel.nextPage(); }\">&gt;</a></li>";
			else
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel."+ this.next +"(); }\">&gt;</a></li>";
			if (this.last == null)
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel.lastPage(); }\">&raquo;</a></li>";
			else
				message = message + "<li><a href=\"#\" data-bind=\"click: function() { viewModel."+ this.last +"(); }\">&raquo;</a></li>";
			message = message + "</ul>";
			message = message + "<div class=\"clearfix\"></div>";
			message = message + "</div>";
			JspWriter out = getJspContext().getOut();
			out.println(message);
			
		}
	}

}