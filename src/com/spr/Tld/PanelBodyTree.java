package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class PanelBodyTree extends SimpleTagSupport {

	private String tree;
	 
	public void settree(String tree) { this.tree = tree; }

	public void doTag() throws JspException, IOException {
		if (this.tree != null) {
			String message = "<div class=\"treeBody\">";
			message = message + "<table class=\"table table-hover table-condensed\">";
			message = message + "<tr><td>";
			message = message + "<input type=\"button\" value=\"-\" class=\"btn-mini btn-danger\" onclick=\"$('#"+ this.tree +"').jstree('close_all');\">";
			message = message + "<input type=\"button\" value=\"+\" class=\"btn-mini btn-primary\" onclick=\"$('#"+ this.tree +"').jstree('open_all');\">";
			message = message + "</td></tr>";
			message = message + "<tr><td id=\""+ this.tree +"\"></td></tr>";
			message = message + "</table>";
			message = message + "</div>";

			JspWriter out = getJspContext().getOut();
			out.println(message);
		}
	}
}