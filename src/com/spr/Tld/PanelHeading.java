package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class PanelHeading extends SimpleTagSupport {

	private String title = null;
	private String exportfn;
	private String searchdatabindfn;
	private String searchdatabindvalue;
	private String addmodal;
	private String addmodaltitle;
	private String addfn;
	 
	public void settitle(String title) { this.title = title; }
	public void setexportfn(String exportfn) { this.exportfn = exportfn; }
	public void setsearchdatabindfn(String searchdatabindfn) { this.searchdatabindfn = searchdatabindfn; }
	public void setsearchdatabindvalue(String searchdatabindvalue) { this.searchdatabindvalue = searchdatabindvalue; }
	public void setaddmodal(String addmodal) { this.addmodal = addmodal; }
	public void setaddmodaltitle(String addmodaltitle) { this.addmodaltitle = addmodaltitle; }
	public void setaddfn(String addfn) { this.addfn = addfn; }

	public void doTag() throws JspException, IOException {
		if (this.searchdatabindfn != null && this.searchdatabindvalue != null && this.addmodal != null && this.addmodaltitle != null && this.addfn != null) {
			String message = "<div class=\"panel-heading\" style=\"height: 100%;\">";
			message = message + "<b>"+ this.title +"</b>";
			message = message + "<div class=\"pull-right\">";
			message = message + "<div class=\"col-md-3 pull-left\">";
			if (this.exportfn ==null)
				message = message + "<button class=\"btn btn-default\" data-bind=\"click: function() { viewModel.export(); }\">Export</button>";
			else
				message = message + "<button class=\"btn btn-default\" data-bind=\"click: function() { viewModel."+ this.exportfn +"(); }\">Export</button>";
			message = message + "</div>";
			message = message + "<div class=\"col-md-8 pull-left\">";
			
			//Searching
			//message = message + "<tag:search databindfn=\""+ this.searchdatabindfn +"\" databindvalue=\""+ this.searchdatabindvalue+"\"></tag:search>";
			message = "<form class=\"form-inline\" data-bind=\"submit: function() { viewModel." + this.searchdatabindfn + "(); }\">"; 
	    	message = message + "<div class=\"input-group\">";
	    	message = message + "<input type=\"text\" class=\"form-control\" required=\"true\" placeholder=\"Search\" data-bind=\"value: viewModel." + this.searchdatabindvalue + "\">";
	    	message = message + "<span class=\"input-group-btn\">";
	    	message = message + "<button class=\"btn btn-default\" type=\"submit\"><span class=\"glyphicon glyphicon-search\"></span></button>";
	    	message = message + "</span></div></form>";
	    	   
			message = message + "</div>";
			message = message + "<div class=\"col-md-1 pull-left\" style=\"padding-top:6px\">";
			message = message + "<div class=\"form-group\">";
			message = message + "<a href=\"#"+ this.addmodal +"\" data-toggle=\"modal\" data-backdrop=\"static\" class=\"glyphicon glyphicon-plus\" title=\""+ this.addmodaltitle +"\" data-bind=\"click: function() { viewModel."+ this.addfn +"(null); }\"></a>";
			message = message + "</div></div></div><div class=\"clearfix\"></div></div>";

			JspWriter out = getJspContext().getOut();
			out.println(message);
		}
	}
}

//<div class="panel-heading" style="height: 100%;">
//<b>Brands</b>
//<div class="pull-right">
//	<div class="col-md-3 pull-left">
//		<button class="btn btn-default" data-bind="click: function() { viewModel.exportCrmBrand(); }">Export</button>
//	</div>
//	<div class="col-md-8 pull-left">
//		  <tag:search databindfn="searchCrmBrands" databindvalue="searchTextCrmBrand"></tag:search>
//	</div>
//	<div class="col-md-1 pull-left" style="padding-top:6px">
//		<div class="form-group">
//			<a href="#modal-crmbrand" data-toggle="modal" data-backdrop="static" class="glyphicon glyphicon-plus" title="Add Brand" data-bind="click: function() { viewModel.addEditCrmBrand(null); }"></a>
//		</div>
//	</div>
//</div>
//<div class="clearfix"></div>
//</div>