package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class Search extends SimpleTagSupport {

	   private String databindfn;
	   private String databindvalue;
	   private String databindresetfn;
	   
	   public void setdatabindfn(String databindfn) { this.databindfn = databindfn; }
	   public void setdatabindvalue(String databindvalue) { this.databindvalue = databindvalue; }
	   public void setdatabindresetfn(String databindresetfn) { this.databindresetfn = databindresetfn; }
	   
	   public void doTag() throws JspException, IOException
	    {
	       if (databindfn != null && databindvalue != null && databindresetfn != null) 
	       {
	    	   String message = "<form class=\"form-inline\" data-bind=\"submit: function() { viewModel." + databindfn + "(); }\">"; 
	    	   message = message + "<div class=\"input-group\">";
	    	   message = message + "<input type=\"text\" class=\"form-control\" required=\"true\" placeholder=\"Search\" data-bind=\"value: viewModel." + databindvalue + "\">";
	    	   message = message + "<i data-bind=\"click: function() { viewModel." + databindresetfn + "(); }\" class=\"fa fa-refresh refresh-icon\"></i>";
	    	   message = message + "<div data-bind=\"click: function() { viewModel." + databindfn + "(); }\" class=\"input-group-btn\"><button type=\"button\" class=\"btn btn-primary\">";
	    	   message = message + "<i class=\"fa fa-search\"></i></button></div></div></form>";
	    	   JspWriter out = getJspContext().getOut();
	    	   out.println(message);
	
	       }
	   }

	}
//<form class="form-inline" data-bind="submit: function() { viewModel.searchtblwmscurrencies();}"> 
//<div class="input-group">
//<input type="text" class="form-control" placeholder="Search.." data-bind="value: viewModel.searchTexttblwmscurrency">
//<i class="fa fa-refresh refresh-icon" data-bind="click: function() { viewModel.refreshtblpurchaseuoms();}"></i>
//<div class="input-group-btn">
//	<button type="button" class="btn btn-primary">
//		<i class="fa fa-search"></i>
//	</button>
//</div>
//</div>
//</form>