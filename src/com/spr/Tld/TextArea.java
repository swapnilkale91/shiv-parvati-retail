package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class TextArea extends SimpleTagSupport {

	private String label;
	private String id;
	private String databind;
	private String placeholder;
	private String required;
	private String maxlength;
	private String rows;
	
	public void setlabel(String label) { this.label = label; }
	public void setid(String id) { this.id = id; }
	public void setdatabind(String databind) { this.databind = databind; }
	public void setplaceholder(String placeholder) { this.placeholder = placeholder; }
	public void setrequired(String required) { this.required = required; }
	public void setmaxlength(String maxlength) { this.maxlength = maxlength; }
	public void setrows(String rows) { this.rows = rows; }
	
	public void doTag() throws JspException, IOException
	{
		String message = "<div class=\"form-group\">";
		
		// Highlights if mandatory
		if(this.required != null)
		{
			if (this.required.equalsIgnoreCase("true"))
				message = "<div class=\"form-group has-error\">";
		}
		
		//<label> tag
		if(this.label != null)
			message = message + "<label for=\""+ this.id +"\">"+ this.label +"</label> ";
		//<input> tag
		message = message + "<textarea type=\"text\" style=\"resize: none; overflow-y: scroll;\" class=\"form-control\"";
		if(this.id != null)
			message = message + " id=\""+ this.id +"\"";
		if(this.databind != null)
			message = message + " data-bind=\"value: "+ this.databind +"\"";
		if(this.required != null)
			message = message + " required=\""+ this.required +"\"";
		if(this.placeholder != null)
			message = message + " placeholder=\""+ this.placeholder +"\"";
		if(this.maxlength != null)
			message = message + " maxlength=\""+ this.maxlength +"\"";
		if(this.rows != null)
			message = message + " rows=\""+ this.rows +"\"";
		message = message + "></textarea>";
		
		message = message + "</div>";

		JspWriter out = getJspContext().getOut();
		out.println(message);
	}
}

//<div class="form-group">
//<label>Remarks</label> 
//<textarea type="text" rows="2" style="resize: none; overflow-y: scroll;" class="form-control" data-bind="value : vhrAssignmentRemarks" placeholder="Enter Remarks" maxlength="1000"></textarea>
//</div>