package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;

public class TreeModal extends SimpleTagSupport {

	private String id;
	private String title;
	private String tree;
	private String selectfn;
	
	public void setid(String id) { this.id = id; }
	public void settitle(String title) { this.title = title; }
	public void settree(String tree) { this.tree = tree; }
	public void setselectfn(String selectfn) { this.selectfn = selectfn; }
	
	StringWriter sw = new StringWriter();		

	public void doTag() throws JspException, IOException {
		if (this.id != null && this.selectfn != null && this.title != null && this.tree != null) {
			String message = "<div class=\"modal fade\" id=\""+ this.id +"\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">";
			message = message + "<div class=\"modal-dialog\">";
			message = message + "<div class=\"modal-content\">";
			message = message + "<div class=\"modal-header\">";
			message = message + "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">";
			message = message + "<span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span>";
			message = message + "</button>";
			message = message + "<h4 class=\"modal-title\" id=\"myModalLabel\">"+ this.title +"</h4>";
			message = message + "</div>";
			message = message + "<div class=\"modal-body\">";
			message = message + "<table class=\"table table-condensed\">";
			message = message + "<tr><td>";
			message = message + "<input type=\"button\" value=\"-\" class=\"btn-mini btn-danger\" onclick=\"$('#"+ this.tree +"').jstree('close_all');\">";
			message = message + "<input type=\"button\" value=\"+\" class=\"btn-mini btn-primary\" onclick=\"$('#"+ this.tree +"').jstree('open_all');\">";
			message = message + "</td></tr>";
			message = message + "<tr><td id=\""+ this.tree +"\"></td></tr>";
			message = message + "</table>";
			message = message + "</div>";
			message = message + "<div class=\"modal-footer\">";
			message = message + "<button class=\"btn btn-default\" data-dismiss=\"modal\" \">Close</button>";
			message = message + "<button class=\"btn btn-success\" data-dismiss=\"modal\" data-bind=\"click: function() { viewModel."+ this.selectfn +"(true); }\">Confirm</button>";
			message = message + "</div></div></div></div>";
			
			JspWriter out = getJspContext().getOut();
			out.println(message);
		}
	}
}

//<div class="modal fade" id="modal-productcategories" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
//<div class="modal-dialog">
//	<div class="modal-content">
//		<div class="modal-header">
//			<button type="button" class="close" data-dismiss="modal">
//				<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
//			</button>
//			<h4 class="modal-title" id="myModalLabel">Product Categories</h4>
//		</div>
//		<div class="modal-body">
//			<table class="table table-condensed">				
//				<tr>
//					<td>
//						<input type="button" value="-" class="btn-mini btn-danger"
//						onclick="$('#crmProductCatgeoriesModalTree').jstree('close_all');">
//						<input type="button" value="+" class="btn-mini btn-primary"
//						onclick="$('#crmProductCatgeoriesModalTree').jstree('open_all');">
//					</td>
//				</tr>
//				<tr>
//					<td id="crmProductCatgeoriesModalTree"></td>
//				</tr>
//			</table>
//		</div>
//		<div class="modal-footer">
//			<button class="btn btn-default" data-dismiss="modal">Close</button>
//			<button class="btn btn-success" data-dismiss="modal"
//				data-bind="click: function() { viewModel.closeCategoryTree(true); }">Confirm</button>
//		</div>
//	</div>
//</div>
//</div>