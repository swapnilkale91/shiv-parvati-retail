package com.spr.Tld;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;

import java.io.*;

public class ViewButton extends SimpleTagSupport {

   private String databind;
   private String databindoption;
   private String title;
   private String id;
   
   public void setdatabind(String databind) { this.databind = databind; }
   public void setdatabindoption(String databindoption) { this.databindoption = databindoption; }
   public void settitle(String title) { this.title = title; }
   public void setid(String id) { this.id = id; }

   StringWriter sw = new StringWriter();		
   
   public void doTag() throws JspException, IOException
    {
       if (databind != null) 
       {
    	   String message = "<button type=\"button\" class=\"btn btn-primary btn-xs\""; 
    	   if(title != null)
    		   message = message + " title=\""+ this.title +"\"";
    	   message = message + " data-bind=\"click: function() { viewModel."+ this.databind +"($data); }";
		   
    	   if(databindoption != null)
			   message = message + ", "+ this.databindoption;
	   		message = message  + "\" ";
			
	   		if(id != null)
    		   message = message + "id=\""+ this.id +"\""; 
			
			message = message +">";
			
			getJspBody().invoke(sw);
			message = message + sw.toString();
			
			message = message +"</button>";
			
    	   JspWriter out = getJspContext().getOut();
    	   out.println(message);
       }
   }
}

//<button type="button" class="btn btn-primary btn-xs" title="View Details" data-bind="click: function() { viewModel.addEditCrmSalesInquiry($data); }">View Details</button>
//<button type="button" class="btn btn-primary btn-xs"title="Cancel ASN"data-bind="click: function() { viewModel.addEdittblasn($data); }, visible: $data.tinCancelStatusCode()" id="cancel-ASN">
